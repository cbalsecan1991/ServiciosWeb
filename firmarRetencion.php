<?php

require_once 'lib/nusoap.php';
require_once 'FirmaFactura/xml_retencion.php';
require_once 'variables_globales.php';

	date_default_timezone_set('America/Bogota');
	
	//Instancio o objeto del servicio
	$servicio = new soap_server();
	//Instacion conexion a la base de datos
	
	//Name Espace
	$ns = "urn:firmarRecwsdl";

	//configuramos el servicio 	
	//1er parametro Nombre del Webservice
	//2do parametro Name space
	$servicio->configureWSDL("firmarretencion",$ns);
	//almacena el espacio de nobre de destino
	$servicio->schemaTargetNamespace = $ns;

	//Metodo (register) para registrar el servicio 4 parametros
	//1er parametro Nombre de la Funcion que se va a Desarrollar
	//2do parametros de Entrada para la Funcion
	//3er parametro return de errores o de respuestas
	//4to Name space

	//funcion para guardar cabecera de facturascli
	$servicio->register("firmarRec", 
		array(
			'idretencion' => 'xsd:integer'),
			array('return' => 'xsd:string'),
		$ns);

	//creamos la funcion cabecerafac
	function firmarRec($idretencion)
	{
		//Instancio objeto par generar el xml
		$xml_retencion = new xml_retencion();

		//array de Respuestas o Retorno
		$resultado = array();

		//Busco las variables Globales para realizar la conexion
		$var = new variables_globales();

		//Realizo la conexion a la base de datos
		$conexion = mysqli_connect($var->FS_DB_HOST, $var->FS_DB_USER, $var->FS_DB_PASS, $var->FS_DB_NAME);

		//Busco la existencia de la Factura
		$facturas = $conexion->query("SELECT * FROM facturasprov WHERE idfactura = ".$idretencion);
		
		$arr_fac = mysqli_fetch_array($facturas);

		//Verifico si exite la factura.
		if(!empty($arr_fac))
		{
			$resultado['retencion'] = "Retencion encontrada correctamente";
			//busco en las co_facturas si se encuentra aprobadas
			$co_facturas = $conexion->query("SELECT * FROM co_factura WHERE doc_instancias_id = '".$idretencion."'AND tipo_doc = '3' AND estado = 'AUTORIZADO';");

			$arr_facs = mysqli_fetch_array($co_facturas);
			if(empty($arr_facs))
			{
				$resultado += $xml_retencion->generar_xml_retencion($arr_fac);

				//Busco la existencia de la Factura
				$facturas = $conexion->query("SELECT * FROM facturasprov WHERE idfactura = ".$idretencion." AND estado_sri = 'AUTORIZADO'");
				$arr_fac_a = mysqli_fetch_array($facturas);
				
				if(!empty($arr_fac_a))
				{
					$resultado['retencion_autorizacion'] = "Retencion Autorizada correctamente";
					$resultado['autoriza_sri'] = $arr_fac_a['autoriza_numero_sri'];
					$resultado['mensaje_sri'] = $arr_fac_a['mensaje_sri'];
					$resultado['estado_sri'] = $arr_fac_a['estado_sri'];
					$resultado['fecha_autorizacion'] = $arr_fac_a['fecha_autorizacion_sri'];
					$resultado['hora_autorizacion'] = $arr_fac_a['hora_autorizacion_sri'];
					$resultado['codigo_acceso_sri'] = $arr_fac_a['codigo_acceso_sri'];
					$resultado['documento'] = $arr_fac_a['numero_documento_sri'];
					$resultado['tipo_documento'] = $arr_fac_a['tipo_doc_sri'];
				}else{
					$resultado['error_autorizacion'] = "Error al Autorizar la Retencion";
				}

				/*formo el numero de documento
				$almacenes = $conexion->query("SELECT * FROM almacenes WHERE codalmacen = '".$arr_fac[0]['codalmacen']."';");
				$arr_almacen = array();

				//valores por defecto en caso de que no esxista el almacen
				$codEsta = '001';
				$ptoEmis = '001';

				//transformo los datos de la consulta a un array
				while ($alm = mysqli_fetch_array($almacenes))
				{
					$arr_almacen[] = $alm;
				}

				if(!empty($arr_almacen))
				{
					$codEsta = $arr_almacen[0]['codestablecimiento'];
					$ptoEmis = $arr_almacen[0]['puntoemision'];
				}

				$num_doc = $codEsta."-".$ptoEmis."-".str_pad($arr_fac[0]['numero'],9,"0",STR_PAD_LEFT);

				//Busco el Almacen para encontrar su establecimiento y punto de emision

				$sql = "INSERT INTO co_factura (tipo_doc, numero_documento, mensaje, estado, doc_instancias_id, fecha_actualizacion, hora_actualizacion) VALUES ('17', '".$num_doc."', 'PENDIENTE', 'NO AUTORIZADO', '".$idretencion."', '".date('Y-m-d')."', '".date('H:m:s')."');";
				
				if($conexion->query($sql)===TRUE)
				{
					$resultado['estado'] = "La Factura se encuentra pendiente de autorizar";
					//genero los parametros para enviar el mail
					$to = $var->email_a;
					$subject = 'Factura Pendiente de Autorizar';
					$mensaje = 'La factura '.$arr_fac[0]['codigo'].' esta pendiente de Autorizar';
					$headers = "From: ".$var->email_from . "\r\n" . "CC: ".$var->email_cc;

					if(!mail($to,$subject,$mensaje,$headers))
					{
						$resultado['mail'] = "No se pudo enviar el email";
					}					
				}else{
					$resultado['factura'] = "No Se Pudo Ingresar el co_factura";
				}*/

			}else{
				$resultado['retencion'] = "La Retencion se encuentra Autorizada";
				$resultado['autoriza_sri'] = $arr_facs['autoriza_numero_sri'];
				$resultado['mensaje_sri'] = $arr_facs['mensaje_sri'];
				$resultado['estado_sri'] = $arr_facs['estado_sri'];
				$resultado['codigo_acceso_sri'] = $arr_facs['codigo_acceso_sri'];
				$resultado['documento'] = $arr_facs['numero_documento_sri'];
				$resultado['tipo_documento'] = $arr_facs['tipo_doc_sri'];
				$resultado['fecha_autorizacion'] = $arr_facs['fecha_autorizacion_sri'];
				$resultado['hora_autorizacion'] = $arr_facs['hora_autorizacion_sri'];
			}

		}else{			
			$resultado['retencion'] = "No se encuentra la Retencion ".$idretencion;
		}
		
		return json_encode($resultado);
	}


	$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
	$servicio->service(file_get_contents("php://input"));
	
?>