<?php

require_once 'lib/nusoap.php';
require_once 'FirmaFactura/xml_nota_credito.php';
require_once 'variables_globales.php';

	date_default_timezone_set('America/Bogota');
	
	//Instancio o objeto del servicio
	$servicio = new soap_server();
	//Instacion conexion a la base de datos
	
	//Name Espace
	$ns = "urn:firmarNotaCreditowsdl";

	//configuramos el servicio 	
	//1er parametro Nombre del Webservice
	//2do parametro Name space
	$servicio->configureWSDL("firmarnotacredito",$ns);
	//almacena el espacio de nobre de destino
	$servicio->schemaTargetNamespace = $ns;

	//Metodo (register) para registrar el servicio 4 parametros
	//1er parametro Nombre de la Funcion que se va a Desarrollar
	//2do parametros de Entrada para la Funcion
	//3er parametro return de errores o de respuestas
	//4to Name space

	//funcion para guardar cabecera de facturascli
	$servicio->register("firmarNotaCred", 
		array(
			'idnotacredito' => 'xsd:integer'),
			array('return' => 'xsd:string'),
		$ns);

	//creamos la funcion cabecerafac
	function firmarNotaCred($idnotacredito)
	{

		//Quitar espacios blancos
		$idnotacredito = trim($idnotacredito);


		//Instancio objeto par generar el xml
		$xml_nc = new xml_nota_credito();

		//array de Respuestas o Retorno
		$resultado = array();

		//Busco las variables Globales para realizar la conexion
		$var = new variables_globales();


		//Realizo la conexion a la base de datos
		$conexion = mysqli_connect($var->FS_DB_HOST, $var->FS_DB_USER, $var->FS_DB_PASS, $var->FS_DB_NAME);

		//Busco la existencia de la Factura
		$consulta = $conexion->query("SELECT * FROM facturascli WHERE idfactura = '".$idnotacredito."' AND codserie = 'N';");
		
		$arr_nota = mysqli_fetch_array($consulta);
		//Verifico si exite la factura.
		if(!empty($arr_nota))
		{
			$resultado['notacredito'] = "Nota de Credito encontrada correctamente";
			//busco en las co_facturas si se encuentra aprobadas
			$co_facturas = $conexion->query("SELECT * FROM co_factura WHERE doc_instancias_id = '".$idnotacredito."'AND tipo_doc = '4' AND estado = 'AUTORIZADO';");

			$arr_nota_aut = mysqli_fetch_array($co_facturas);
			if(empty($arr_nota_aut))
			{
				$consulta = $conexion->query("SELECT * FROM facturascli WHERE idfactura = ".$arr_nota['idfacturarect']." AND estado_sri = 'AUTORIZADO';");
				$arr_fact_a = mysqli_fetch_array($consulta);
				if (empty($arr_fact_a)) {
					$resultado['idfactura'] = "La Factura de Venta no Se encuentra Autorizada";
				}else{
					$resultado['idfactura'] = "Factura encontrada correctamente";
					$resultado += $xml_nc->generar_xml_nota_credito($arr_nota);
					//Busco la existencia de la Factura
					$consulta = $conexion->query("SELECT * FROM facturascli WHERE idfactura = ".$idnotacredito." AND estado_sri = 'AUTORIZADO' AND codserie = 'N';");
					$arr_fac_a = mysqli_fetch_array($consulta);
					if(!empty($arr_fac_a))
					{
						$resultado['notacredito'] = "Nota de Credito Autorizada correctamente";
						$resultado['autoriza_sri'] = $arr_fac_a['autoriza_numero_sri'];
						$resultado['mensaje_sri'] = $arr_fac_a['mensaje_sri'];
						$resultado['estado_sri'] = $arr_fac_a['estado_sri'];
						$resultado['fecha_autorizacion'] = $arr_fac_a['fecha_autorizacion_sri'];
						$resultado['hora_autorizacion'] = $arr_fac_a['hora_autorizacion_sri'];
						$resultado['codigo_acceso_sri'] = $arr_fac_a['codigo_acceso_sri'];
						$resultado['documento'] = $arr_fac_a['numero_documento_sri'];
						$resultado['tipo_documento'] = $arr_fac_a['tipo_doc_sri'];
					}else{
						$resultado['error_autorizacion'] = "Error al Autorizar la Nota de Credito";
					}
				}
			}else{
				$resultado['notacredito'] = "La Nota de Credito se encuentra Autorizada";
				$resultado['autoriza_sri'] = $arr_nota_aut['autoriza_numero'];
				$resultado['mensaje_sri'] = $arr_nota_aut['mensaje'];
				$resultado['estado_sri'] = $arr_nota_aut['estado'];
				$resultado['codigo_acceso_sri'] = $arr_nota_aut['codigo_acceso'];
				$resultado['documento'] = $arr_nota_aut['numero_documento'];
				$resultado['tipo_documento'] = $arr_nota_aut['tipo_doc'];
				$resultado['fecha_autorizacion'] = $arr_nota_aut['fecha_actualizacion'];
				$resultado['hora_autorizacion'] = $arr_nota_aut['hora_actualizacion'];
			}
		}else{			
			$resultado['notacredito'] = "No se encuentra la Nota de Credito ".$idretencion;
		}
		
		return json_encode($resultado);
	}


	$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
	$servicio->service(file_get_contents("php://input"));
	
?>