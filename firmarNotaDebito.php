<?php

require_once 'lib/nusoap.php';
require_once 'FirmaFactura/xml_nota_debito.php';
require_once 'variables_globales.php';

	date_default_timezone_set('America/Bogota');
	
	//Instancio o objeto del servicio
	$servicio = new soap_server();
	//Instacion conexion a la base de datos
	
	//Name Espace
	$ns = "urn:firmarNotaDebitowsdl";

	//configuramos el servicio 	
	//1er parametro Nombre del Webservice
	//2do parametro Name space
	$servicio->configureWSDL("firmarnotadebito",$ns);
	//almacena el espacio de nobre de destino
	$servicio->schemaTargetNamespace = $ns;

	//Metodo (register) para registrar el servicio 4 parametros
	//1er parametro Nombre de la Funcion que se va a Desarrollar
	//2do parametros de Entrada para la Funcion
	//3er parametro return de errores o de respuestas
	//4to Name space

	//funcion para guardar cabecera de guia de remision
	$servicio->register("firmarND", 
		array(
			'idfactura' => 'xsd:integer'),
			array('return' => 'xsd:string'),
		$ns);

	//creamos la funcion cabecerafac
	function firmarND($idfactura)
	{
		$clave = FALSE;
		$idfactura = trim($idfactura);


		//Instancio objeto par generar el xml
		$xml_nota_debito = new xml_nota_debito();

		//array de Respuestas o Retorno
		$resultado = array();

		//Busco las variables Globales para realizar la conexion
		$var = new variables_globales();

		//Realizo la conexion a la base de datos
		$conexion = mysqli_connect($var->FS_DB_HOST, $var->FS_DB_USER, $var->FS_DB_PASS, $var->FS_DB_NAME);

		//Busco la existencia de la Factura
		$factura = $conexion->query("SELECT * FROM facturascli WHERE idfactura = ".$idfactura);
		$factura = mysqli_fetch_array($factura);

		//Verifico si exite la factura.
		if(!empty($factura))
		{
			$resultado['idfactura'] = "Factura de Venta encontrada correctamente";
			//VERIFICAR SI EXISTE LA CABECERA DE LA NOTA DE DÉBITO DE DICHA FACTURA
			$consulta = $conexion->query("SELECT * FROM notadebitocli WHERE idfactura = '".$idfactura."';");
			$nota = mysqli_fetch_array($consulta);
			if(!empty($nota)){
				$consulta = $conexion->query("SELECT * FROM co_factura WHERE doc_instancias_id = '".$nota['idnotadebito']."' AND tipo_doc = '5' AND estado = 'AUTORIZADO';");
				$nota_aut = mysqli_fetch_array($consulta);
				$consulta2 = $conexion->query("SELECT * FROM notadebitocli WHERE idnotadebito = '".$nota['idnotadebito']."' AND estado_sri = 'AUTORIZADO';");
				$nota_aut2 = mysqli_fetch_array($consulta2);
				if (!empty($nota_aut) OR !empty($nota_aut2)) {
					$resultado['notadebito'] = "La nota de debito ya se encuentra autorizada";
					$resultado['autoriza_sri'] = $nota_aut['autoriza_numero'];
					$resultado['mensaje_sri'] = $nota_aut['mensaje'];
					$resultado['estado_sri'] = $nota_aut['estado'];
					$resultado['fecha_autorizacion'] = $nota_aut['fecha_actualizacion'];
					$resultado['hora_autorizacion'] = $nota_aut['hora_actualizacion'];
					$resultado['codigo_acceso_sri'] = $nota_aut['codigo_acceso'];
					$resultado['documento'] = $nota_aut['numero_documento'];
					$resultado['tipo_documento'] = $nota_aut['tipo_doc'];
				}else{
					$resultado += $xml_nota_debito->generar_xml_nota_debito($nota);
					$consulta = $conexion->query("SELECT * FROM co_factura WHERE doc_instancias_id = '".$nota['idnotadebito']."' AND tipo_doc = '5' AND estado = 'AUTORIZADO';");
					$nota_aut = mysqli_fetch_array($consulta);
					if(!empty($nota_aut)){
						$resultado['notadebito'] = "Nota de Debito Autorizada correctamente";
						$resultado['autoriza_sri'] = $nota_aut['autoriza_numero'];
						$resultado['mensaje_sri'] = $nota_aut['mensaje'];
						$resultado['estado_sri'] = $nota_aut['estado'];
						$resultado['fecha_autorizacion'] = $nota_aut['fecha_actualizacion'];
						$resultado['hora_autorizacion'] = $nota_aut['hora_actualizacion'];
						$resultado['codigo_acceso_sri'] = $nota_aut['codigo_acceso'];
						$resultado['documento'] = $nota_aut['numero_documento'];
						$resultado['tipo_documento'] = $nota_aut['tipo_doc'];
					}else{
						$resultado['notadebito'] = "Problemas al Autorizar la Nota de Debito";
					}

				}
			}else{
				$resultado['error_notaDB'] = 'No existe una nota de Debito creado para firmar.';
			}
		}else{
			$resultado['idfactura'] = "No se encuentra la Factura de Venta '".$idfactura."' para hacer la Nota de Debito";
		}

		return json_encode($resultado);
	}


	$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
	$servicio->service(file_get_contents("php://input"));
?>

