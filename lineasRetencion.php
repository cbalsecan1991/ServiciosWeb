<?php

require_once 'lib/nusoap.php';
require_once 'variables_globales.php';

	date_default_timezone_set('America/Bogota');

	//Instancio o objeto del servicio
	$servicio = new soap_server();
	//Instacion conexion a la base de datos
	
	//Name Espace
	$ns = "urn:lineasRecwsdl";

	//configuramos el servicio 	
	//1er parametro Nombre del Webservice
	//2do parametro Name space
	$servicio->configureWSDL("lineasretencion",$ns);
	//almacena el espacio de nombre de destino
	$servicio->schemaTargetNamespace = $ns;

	//Metodo (register) para registrar el servicio 4 parametros
	//1er parametro Nombre de la Funcion que se va a Desarrollar
	//2do parametros de Entrada para la Funcion
	//3er parametro return de errores o de respuestas
	//4to Name space

	//funcion para guardar cabecera de facturascli
	$servicio->register("lineasRec", 
		array(
			'idretencion' => 'xsd:integer',
			'porcentaje_iva' => 'xsd:integer',
			'Neto' => 'xsd:double',
			'coretencion_iva' => 'xsd:string',
			'coretencion_renta' => 'xsd:string'),
			array('return' => 'xsd:string'),
		$ns);

	//creamos la funcion cabecerafac
	function lineasRec($idretencion,$porcentaje_iva,$Neto,$coretencion_iva,$coretencion_renta){
		//quito los espacios en blanco
		$idretencion = trim($idretencion);
		$porcentaje_iva = trim($porcentaje_iva);
		$Neto = trim($Neto);
		$coretencion_iva = trim($coretencion_iva);
		$coretencion_renta = trim($coretencion_renta);
		// Validadcion de los decimales
		$Neto = str_replace(',', '.', $Neto);

		//array de Respuestas o Retorno
		$resultado = array();
		//Busco las variables Globales para realizar la conexion
		$var = new variables_globales();
		//Realizo la conexion a la base de datos
		$conexion = mysqli_connect($var->FS_DB_HOST, $var->FS_DB_USER, $var->FS_DB_PASS, $var->FS_DB_NAME);

		#FACTURA DE COMPRA
		//Busco la existencia de la Factura
		$facturas = $conexion->query("SELECT * FROM facturasprov WHERE idfactura = ".$idretencion);
		$arr_fac = mysqli_fetch_array($facturas);
		//Verifico si exite la factura.
		if(!empty($arr_fac))
		{
			$resultado['retencion'] = "Retencion encontrada correctamente";

			#CODIGO IMPUESTOS
			$codimpuesto = '-1';
			$idcodiva = '-1';
			$idcodrenta = '-1';
			// busco en código del iva
			if ($porcentaje_iva == 12) {
				$codimpuesto = 'IVA12';
				$resultado['impuesto'] = "Porcentaje de Iva Correcto: ".$porcentaje_iva;
			}else if($porcentaje_iva == 0){
				$codimpuesto = 'IVA0';
			}else{
				$resultado['impuesto'] = "Porcentaje de Iva no Valido: ".$porcentaje_iva;
			}
			//busco el codigo de retencion del iva
			$impuesto = $conexion->query("SELECT * FROM co_tipos_retencion WHERE especie = 'iva' AND idtiporetencion = '".$coretencion_iva."';");
			$arr_iva = mysqli_fetch_array($impuesto);

			if (!empty($arr_iva)) {
				$idcodiva = $arr_iva['idtiporetencion'];
			}
			//busco el codigo de retencion de la renta
			$impuesto = $conexion->query("SELECT * FROM co_tipos_retencion WHERE especie = 'renta' AND idtiporetencion = '".$coretencion_renta."';");
			$arr_renta = mysqli_fetch_array($impuesto);
			if (!empty($arr_renta)) {
				$idcodrenta = $arr_renta['idtiporetencion'];
			}


			#ARTICULO
			$art_referencia = "artretencion";
			$art_descripcion = "Valor total de retencion";
			if ($idcodiva != '-1' && $idcodrenta != '-1' && $codimpuesto != '-1'){
				// Articulo por defecto
				$articulo = $conexion->query("SELECT * FROM articulos WHERE referencia = 'artretencion';");
				$arr_art = mysqli_fetch_array($articulo);
				if(empty($arr_art)){
					$sql = "INSERT INTO articulos (factualizado, referencia,descripcion,codimpuesto, sevende,secompra,nostock) VALUES ('".date('Y-m-d')."','".$art_referencia."', '".$art_descripcion."', '".$codimpuesto."', '1','1','0');";
					if ($conexion->query($sql)) {
						$clave = TRUE;
					}else{
						$clave = FALSE;
					}
				}else{
					$clave = TRUE;
				}
				#GUARDA LA LÍNEA DE LA RETENCION || FACTURA COMPRA
				if ($clave) {
					$sql = "INSERT INTO lineasfacturasprov (cantidad, bon, pvp_nuevo, codimpuesto, descripcion, dtolineal, dtopor, idfactura, irpf, iva, pvpsindto, pvptotal, pvpunitario, recargo, referencia, idtipo_retencion_iva, idtipo_retencion_renta, ret_subcuentas, id_centro_costos) VALUES ('1', '0', '0', '".$codimpuesto."', '".$art_descripcion."', '0', '0','".$idretencion."', '0', '".$porcentaje_iva."', '".$Neto."', '".$Neto."', '".$Neto."', '0', '$art_referencia', '".$idcodiva."', '".$idcodrenta."', '0', '1');";
					if ($conexion->query($sql)) {
						$resultado['linea'] = "Se ingreso la linea correctamente";
					}else{
						$resultado['linea'] = "No se pudo ingresar la linea correctamente";
					}
				}else{
					$resultado['linea'] = "No se pudo ingresar la linea correctamente";
				}
			}
		}else{
			$resultado['retencion'] = "No se encuentra la Retencion ".$idretencion;
		}
		
		return json_encode($resultado);
	}

	$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
	$servicio->service(file_get_contents("php://input"));
	
?>