<?php

require_once 'lib/nusoap.php';
require_once 'FirmaFactura/xml_guia_remision.php';
require_once 'variables_globales.php';

	date_default_timezone_set('America/Bogota');
	
	//Instancio o objeto del servicio
	$servicio = new soap_server();
	//Instacion conexion a la base de datos
	
	//Name Espace
	$ns = "urn:firmarGuiaRemisionwsdl";

	//configuramos el servicio 	
	//1er parametro Nombre del Webservice
	//2do parametro Name space
	$servicio->configureWSDL("firmarguiaremision",$ns);
	//almacena el espacio de nobre de destino
	$servicio->schemaTargetNamespace = $ns;

	//Metodo (register) para registrar el servicio 4 parametros
	//1er parametro Nombre de la Funcion que se va a Desarrollar
	//2do parametros de Entrada para la Funcion
	//3er parametro return de errores o de respuestas
	//4to Name space

	//funcion para guardar cabecera de guia de remision
	$servicio->register("firmarGuia", 
		array(
			'idfactura' => 'xsd:integer'),
			array('return' => 'xsd:string'),
		$ns);

	//creamos la funcion cabecerafac
	function firmarGuia($idfactura)
	{
		$clave = FALSE;
		$idfactura = trim($idfactura);

		//Instancio objeto par generar el xml
		$xml_guia_remision = new xml_guia_remision();

		//array de Respuestas o Retorno
		$resultado = array();

		//Busco las variables Globales para realizar la conexion
		$var = new variables_globales();

		//Realizo la conexion a la base de datos
		$conexion = mysqli_connect($var->FS_DB_HOST, $var->FS_DB_USER, $var->FS_DB_PASS, $var->FS_DB_NAME);

		//Busco la existencia de la Factura
		$factura = $conexion->query("SELECT * FROM facturascli WHERE idfactura = ".$idfactura);
		$factura = mysqli_fetch_array($factura);

		//Verifico si exite la factura.
		if(!empty($factura))
		{
			$resultado['idfactura'] = "Factura de Venta encontrada correctamente";
			$consulta = $conexion->query("SELECT * FROM co_factura WHERE doc_instancias_id = '".$idfactura."'AND tipo_doc = '6' AND estado = 'AUTORIZADO';");
			$co_fact = mysqli_fetch_array($consulta);
			if (empty($co_fact)) {
				$consulta = $conexion->query("SELECT * FROM facturascli WHERE idfactura = ".$idfactura." AND estado_guia_sri = 'AUTORIZADO';");
				$ret = mysqli_fetch_array($consulta);
				if (empty($ret)) {

					//realizo la elaboración de xml y su envio al sri
					$resultado+= $xml_guia_remision->generar_xml_guia($factura);

					//Compruebo si se autorizó la guia
					$consulta = $conexion->query("SELECT * FROM co_factura WHERE doc_instancias_id = '".$idfactura."'AND tipo_doc = '6' AND estado = 'AUTORIZADO';");
					$ret_aut = mysqli_fetch_array($consulta);
					if ($ret_aut) {
						$resultado['guiaremision'] = 'Guia de Remision autorizada correctamente';
						$resultado['autoriza_sri'] = $ret_aut['autoriza_numero'];
						$resultado['mensaje_sri'] = $ret_aut['mensaje'];
						$resultado['estado_sri'] = $ret_aut['estado'];
						$resultado['fecha_autorizacion'] = $ret_aut['fecha_actualizacion'];
						$resultado['hora_autorizacion'] = $ret_aut['hora_actualizacion'];
						$resultado['codigo_acceso_sri'] = $ret_aut['codigo_acceso'];
						$resultado['documento'] = $ret_aut['numero_documento'];
						$resultado['tipo_documento'] = $ret_aut['tipo_doc'];						
					}else{
						$resultado['guiaremision'] = ['Problemas al autorizar la Guia de Remision'];
					}
				}else{
					$consulta = $conexion->query("SELECT * FROM co_factura WHERE doc_instancias_id = '".$idfactura."'AND tipo_doc = '6' AND estado = 'AUTORIZADO';");
					$ret_aut = mysqli_fetch_array($consulta);
					$resultado['guiaremision'] = 'La Guia De Remision se encuentra ya autorizada';
					$resultado['autoriza_sri'] = $ret_aut['autoriza_numero'];
					$resultado['mensaje_sri'] = $ret_aut['mensaje'];
					$resultado['estado_sri'] = $ret_aut['estado'];
					$resultado['fecha_autorizacion'] = $ret_aut['fecha_actualizacion'];
					$resultado['hora_autorizacion'] = $ret_aut['hora_actualizacion'];
					$resultado['codigo_acceso_sri'] = $ret_aut['codigo_acceso'];
					$resultado['documento'] = $ret_aut['numero_documento'];
					$resultado['tipo_documento'] = $ret_aut['tipo_doc'];
				}
			}else{
				$resultado['guiaremision'] = "La Guia De Remision se encuentra ya autorizada";
				$resultado['autoriza_sri'] = $co_fact['autoriza_numero_sri'];
				$resultado['mensaje_sri'] = $co_fact['mensaje_sri'];
				$resultado['estado_sri'] = $co_fact['estado_sri'];
				$resultado['fecha_autorizacion'] = $co_fact['fecha_autorizacion_sri'];
				$resultado['hora_autorizacion'] = $co_fact['hora_autorizacion_sri'];
				$resultado['codigo_acceso_sri'] = $co_fact['codigo_acceso_sri'];
				$resultado['documento'] = $co_fact['numero_documento_sri'];
				$resultado['tipo_documento'] = $co_fact['tipo_doc_sri'];				
			}
		}else{
			$resultado['idfactura'] = "No se encuentra la Factura de Venta ".$idfactura;
		}

		return json_encode($resultado);
	}


	$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
	$servicio->service(file_get_contents("php://input"));
	




			// $resultado['guiaremision'] = "guiaremision encontrada correctamente";
			// //busco en las co_facturas si se encuentra aprobadas
			// $co_facturas = $conexion->query("SELECT * FROM co_factura WHERE doc_instancias_id = '".$idfactura."'AND tipo_doc = '3' AND estado = 'AUTORIZADO';");

			// $arr_facs = mysqli_fetch_array($co_facturas);
			// if(empty($arr_facs))
			// {
			// 	$resultado += $xml_guiaremision->generar_xml_guiaremision($arr_fac);

			// 	//Busco la existencia de la Factura
			// 	$facturas = $conexion->query("SELECT * FROM facturasprov WHERE idfactura = ".$idfactura." AND estado_sri = 'AUTORIZADO'");
			// 	$arr_fac_a = mysqli_fetch_array($facturas);
				
			// 	if(!empty($arr_fac_a))
			// 	{
			// 		$resultado['guiaremision_autorizacion'] = "guiaremision Autorizada correctamente";
			// 		$resultado['autoriza_sri'] = $arr_fac_a['autoriza_numero_sri'];
			// 		$resultado['mensaje_sri'] = $arr_fac_a['mensaje_sri'];
			// 		$resultado['estado_sri'] = $arr_fac_a['estado_sri'];
			// 		$resultado['fecha_autorizacion'] = $arr_fac_a['fecha_autorizacion_sri'];
			// 		$resultado['hora_autorizacion'] = $arr_fac_a['hora_autorizacion_sri'];
			// 		$resultado['codigo_acceso_sri'] = $arr_fac_a['codigo_acceso_sri'];
			// 		$resultado['documento'] = $arr_fac_a['numero_documento_sri'];
			// 		$resultado['tipo_documento'] = $arr_fac_a['tipo_doc_sri'];
			// 	}else{
			// 		$resultado['error_autorizacion'] = "Error al Autorizar la guiaremision";
			// 	}

			// }else{
			// 	$resultado['guiaremision'] = "La guiaremision se encuentra Autorizada";
			// 	$resultado['autoriza_sri'] = $arr_facs['autoriza_numero_sri'];
			// 	$resultado['mensaje_sri'] = $arr_facs['mensaje_sri'];
			// 	$resultado['estado_sri'] = $arr_facs['estado_sri'];
			// 	$resultado['codigo_acceso_sri'] = $arr_facs['codigo_acceso_sri'];
			// 	$resultado['documento'] = $arr_facs['numero_documento_sri'];
			// 	$resultado['tipo_documento'] = $arr_facs['tipo_doc_sri'];
			// 	$resultado['fecha_autorizacion'] = $arr_facs['fecha_autorizacion_sri'];
			// 	$resultado['hora_autorizacion'] = $arr_facs['hora_autorizacion_sri'];
			// }
?>

