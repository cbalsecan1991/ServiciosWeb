<?php

require_once 'lib/nusoap.php';
require_once 'variables_globales.php';
require_once '../plugins/facturacion_base/extras/Utilities/Utilities.php';

	date_default_timezone_set('America/Bogota');

	//Instancio o objeto del servicio
	$servicio = new soap_server();
		
	//Name Espace
	$ns = "urn:cabeceraFacwsdl";

	//configuramos el servicio 	
	//1er parametro Nombre del Webservice
	//2do parametro Name space
	$servicio->configureWSDL("cabecerafactura",$ns);
	//almacena el espacio de nobre de destino
	$servicio->schemaTargetNamespace = $ns;

	//Metodo (register) para registrar el servicio 4 parametros
	//1er parametro Nombre de la Funcion que se va a Desarrollar
	//2do parametros de Entrada para la Funcion
	//3er parametro return de errores o de respuestas
	//4to Name space

	//funcion para guardar cabecera de facturascli
	$servicio->register("cabecerafac", 
		array(
			'identificacion' => 'xsd:string',
			'nombrecliente' => 'xsd:string',
			'telefono' => 'xsd:string',
			'email' => 'xsd:string',
			'provincia' => 'xsd:string',
			'ciudad' => 'xsd:string',
			'direccion' => 'xsd:string',
			'almacen' => 'xsd:string',
			'fecha' => 'xsd:string',
			'hora' => 'xsd:string',
			'establecimiento' => 'xsd:string',
			'punto_emision' => 'xsd:string',
			'numerofactura' => 'xsd:string',
			'formapago' => 'xsd:string',
			'neto' => 'xsd:double',
	 		'totaliva' => 'xsd:double',
			'totaldescuento' => 'xsd:double',
			'total' => 'xsd:double'),
			array('return' => 'xsd:string'),
		$ns);

	//creamos la funcion cabecerafac
	function cabecerafac($identificacion, $nombrecliente, $telefono, $email, $provincia, $ciudad, $direccion, $almacen, $fecha, $hora, $establecimiento, $punto_emision, $numerofactura, $formapago, $neto, $totaliva, $totaldescuento, $total)
	{
		//quito los espacios en blanco limpiando la cadena
		$identificacion = trim($identificacion);
		$nombrecliente = trim($nombrecliente);
		$telefono = trim($telefono);
		$email = trim($email);
		$provincia = trim($provincia);
		$ciudad = trim($ciudad);
		$direccion = trim($direccion);
		$almacen = trim($almacen);
		$fecha = trim($fecha);
		$hora = trim($hora);
		$establecimiento = trim($establecimiento);
		$punto_emision = trim($punto_emision);
		$numerofactura = trim($numerofactura);
		$formapago = trim($formapago);
		$neto = trim($neto);
		$totaliva = trim($totaliva);
		$totaldescuento = trim($totaldescuento);
		$total = trim($total);

		//Busco las variables Globales para realizar la conexion
		$var = new variables_globales();

		//Realizo la conexion a la base de datos
		$conexion = mysqli_connect($var->FS_DB_HOST, $var->FS_DB_USER, $var->FS_DB_PASS, $var->FS_DB_NAME);
		//Busco la existencia del Cliente

		//Instacion metodos para validacion de cedulas o RUC
		$valida = new Utilities();

		$tipof = "-1";




		//-----------------------------------------------------------------------------

		$llave = TRUE;
		$consulta = $conexion->query("SELECT idfactura FROM facturascli WHERE numero = '".$numerofactura."';");
		$fact_exist = mysqli_fetch_array($consulta);
		if (!empty($fact_exist)) {
			$consulta = $conexion->query("SELECT * FROM co_factura WHERE doc_instancias_id = '".$fact_exist['idfactura']."' AND estado = 'AUTORIZADO' AND tipo_doc = '17'");
			$co_fact_exist = mysqli_fetch_array($consulta);
			if (!empty($co_fact_exist)) {
				$resultado['factura'] = "La factura ya existe y está autorizada";
				$resultado['idfactura'] = "No se obtuvo el id de la factura";
			}else{
				$resultado['factura'] = "La factura ya existe, pero los datos serán modificados";
				$resultado['idfactura'] = $fact_exist['idfactura'];
			}
			$llave = FALSE;

		}



		if(strlen($identificacion) == 13)
		{
			$tipof = "RUC";
		}elseif(strlen($identificacion) == 10){
			$tipof = "CEDULA";
		}


		if($tipof == '-1')
		{
			$resultado['valida_cedula'] = 'La Identificacion del Cliente no es ni Cedula ni Ruc';
			$llave = FALSE;

		}elseif($tipof == 'RUC'){
			if(!$valida->validarRUC($identificacion))
			{
				$resultado['valida_cedula'] = 'Ruc No Valido, en la legalidad Ecuatoriana.';
				$llave = FALSE;
			}
		}elseif($tipof == 'CEDULA'){
			if(!$valida->validarCI($identificacion))
			{
				$resultado['valida_cedula'] = 'Cedula No Valida, en la legalidad Ecuatoriana.';
				$llave = FALSE;
			}
		}

		if ($llave)
		{
			$clientes = $conexion->query("SELECT * FROM clientes WHERE cifnif = ".$identificacion);
			$resultado = array();
			$arr_clientes = array();

			//transformo los datos de la consulta a un array
			while ($cliente = mysqli_fetch_array($clientes))
			{
				$arr_clientes[] = $cliente;
			}
			//$resultado[] = $arr_clientes[0]['codcliente'];
			//Verifico si exite el cliente.
			if(empty($arr_clientes))
			{
				//como el cliente no existe necesito un codigo de cliente para crearlo
				$num_cli = $conexion->query("SELECT max(codcliente) as cli FROM clientes");
				$codcli = array();
				//transformo a array el resultado
				while ($n_cli = mysqli_fetch_array($num_cli))
				{
					$codcli[] = $n_cli;
				}
				//verifico si exite
				if(!empty($codcli))
				{
					$n_c = intval($codcli[0]['cli']) + 1;
					$new_codigo_cli = str_pad($n_c,6,"0",STR_PAD_LEFT);				
				}else{
					$new_codigo_cli = str_pad(1,6,"0",STR_PAD_LEFT);				
				}

				$long_iden = strlen($identificacion);
				if($long_iden == 13)
				{
					$tipof = "RUC";
				}else{
					$tipof = "CEDULA";
				}

				//Inserto el Cliente en la tabla.
				$sql = "INSERT INTO clientes (cifnif, codcliente, email, fechaalta, nombre, razonsocial, regimeniva, telefono1, tipoidfiscal, debaja) VALUES ('".$identificacion."','".$new_codigo_cli."','".$email."','".date('Y-m-d')."','".$nombrecliente."','".$nombrecliente."','General','".$telefono."','".$tipof."','0');";

				//print_r($sql);

				if($conexion->query($sql)===TRUE)
				{
					$resultado['cliente'] = "Se Ingreso el Cliente, No existia en la Base";
				}else{
					$resultado['cliente'] = "No Se Pudo Ingresar el Cliente";
				}

			}else{

				$long_iden = strlen($identificacion);
				if($long_iden == 13)
				{
					$tipof = "RUC";
				}else{
					$tipof = "CEDULA";
				}
				$new_codigo_cli = $arr_clientes[0]['codcliente'];
				$sql = "UPDATE clientes set cifnif = '".$identificacion."', email = '".$email."', nombre = '".$nombrecliente."', razonsocial = '".$nombrecliente."', telefono1 = '".$telefono."', tipoidfiscal = '".$tipof."', debaja = '0' WHERE codcliente = '".$new_codigo_cli."'";
				if($conexion->query($sql)===TRUE)
				{
					$resultado['cliente'] = "Se Actualizo los datos del Cliente";
				}else{
					$resultado['cliente'] = "No Se Pudo Actualizar el Cliente";
				}

			}

			//Busco la direccion del cliente
			$dirs = $conexion->query("SELECT * FROM dirclientes WHERE codcliente = '".$new_codigo_cli."'");
			$arr_dirclientes = array();
			//transformo los datos de la consulta a un array
			while ($dir = mysqli_fetch_array($dirs))
			{
				$arr_dirclientes[] = $dir;
			}

			$id_prov = NULL;
			$id_ciud = NULL;

			//verifico si existen los datos
			if(empty($arr_dirclientes))
			{
				//Busco la provincia
				$provs = $conexion->query("SELECT * FROM provincia WHERE padre IS NULL AND lower(nombre) = '".strtolower($provincia)."'");
				$arr_prov = array();
				//transformo a array
				while ($prov = mysqli_fetch_array($provs))
				{
					$arr_prov[] = $prov;
				}

				//guardo el id de la provincia
				if(!empty($arr_prov))
				{
					$id_prov = $arr_prov[0]['codigo'];
					//Busco la ciudad
					$ciuds = $conexion->query("SELECT * FROM provincia WHERE padre = '".$id_prov."' AND lower(nombre) = '".strtolower($ciudad)."'");
					$arr_ciu = array();
					//transformo a array
					while ($ciud = mysqli_fetch_array($ciuds))
					{
						$arr_ciu[] = $ciud;
					}

					if(!empty($arr_ciu))
					{
						//guardo el id de la ciudad
						$id_ciud = $arr_ciu[0]['codigo'];					
					}
				}

				//Insert a tabla dirclientes
				$sql = "INSERT INTO dirclientes (codcliente, codpais, provincia, ciudad, direccion, descripcion, domfacturacion, fecha) VALUES ('".$new_codigo_cli."','ECU','".$id_prov."','".$id_ciud."','".$direccion."','Principal','1','".date('Y-m-d')."');";

				if($conexion->query($sql)===TRUE)
				{
					$resultado['direccion'] = "Se Ingreso La Direccion, No existia en la Base";
				}else{
					$resultado['direccion'] = "No Se Pudo Ingresar La Direccion";
				}
			}else{

				//Busco la provincia
				$provs = $conexion->query("SELECT * FROM provincia WHERE padre IS NULL AND lower(nombre) = '".strtolower($provincia)."'");
				$arr_prov = array();
				//transformo a array
				while ($prov = mysqli_fetch_array($provs))
				{
					$arr_prov[] = $prov;
				}

				//guardo el id de la provincia
				if(!empty($arr_prov))
				{
					$id_prov = $arr_prov[0]['codigo'];
					//Busco la ciudad
					$ciuds = $conexion->query("SELECT * FROM provincia WHERE padre = '".$id_prov."' AND lower(nombre) = '".strtolower($ciudad)."'");
					$arr_ciu = array();
					//transformo a array
					while ($ciud = mysqli_fetch_array($ciuds))
					{
						$arr_ciu[] = $ciud;
					}

					if(!empty($arr_ciu))
					{
						//guardo el id de la ciudad
						$id_ciud = $arr_ciu[0]['codigo'];
					}
				}
				
				//Insert a tabla dirclientes
				$sql = "UPDATE dirclientes set provincia = '".$id_prov."', ciudad = '".$id_ciud."', direccion = '".$direccion."',  fecha = '".date('Y-m-d')."' WHERE id = '".$arr_dirclientes[0]['id']."';";			

				
				if($conexion->query($sql)===TRUE)
				{
					$resultado['direccion'] = "Se Actualizo La Direccion";
				}else{
					$resultado['direccion'] = "No Se Pudo Actualizar La Direccion";
				}
			}

			//Busco el codigo del Almacen
			$almacenes = $conexion->query("SELECT * FROM almacenes WHERE codestablecimiento = '".$establecimiento."'");

			$arr_almacen = array();

			//transformo los datos de la consulta a un array
			while ($alm = mysqli_fetch_array($almacenes))
			{
				$arr_almacen[] = $alm;
			}

			if(!empty($arr_almacen))
			{
				//guardo el id de la ciudad
				$almacen_cod = $arr_almacen[0]['codalmacen'];
				$resultado['almacen'] = "Almacen encontrado correctamente";
			}else
			{
				$resultado['almacen'] = "No Se encontro el Almacen asignado al establecimiento ".$establecimiento;
			}

			//Busco el año de la factura para encontrar el codigo del ejercicio
			$fec = explode("-", $fecha);
			//selecciono el año de la factura
			$ejer = $fec[0];
			//formo el codigo de la Factura
			$cod = "FAC".$fec[0]."A".$numerofactura;

				$neto = str_replace(',', '.', $neto);
				$totaliva = str_replace(',', '.', $totaliva);
				$totaldescuento = str_replace(',', '.', $totaldescuento);
				$total = str_replace(',', '.', $total);

			//Preparo Insert para Cabecera de La factura
			$sql = "INSERT INTO facturascli (cifnif, ciudad, codalmacen, codcliente, coddivisa, codejercicio, codigo, codpago, totalpago, codpais, codserie, direccion, fecha, hora, neto, nombrecliente, numero, provincia, total, totaliva, totaldescuento) VALUES ('".$identificacion."','".$id_ciud."','".$almacen_cod."','".$new_codigo_cli."','USD','".$ejer."','".$cod."','".$formapago."','".str_replace(",",".",$total)."','ECU','A','".$direccion."','".$fecha."','".$hora."','".str_replace(",",".",$neto)."','".$nombrecliente."','".$numerofactura."','".$id_prov."','".str_replace(",",".",$total)."','".str_replace(",",".",$totaliva)."','".str_replace(",",".",$totaldescuento)."');";
			if($conexion->query($sql)===TRUE)
			{
				$resultado['factura'] = "Se Ingreso La Factura Correctamente.";

				//Recupero el id de la Factura Ingresada.
				$sql = "SELECT max(idfactura) as id FROM facturascli";
				//Realizo la consulta del valor del id

				$fact = $conexion->query($sql);
				$arr_num = array();
				//transformo los datos de la consulta a un array
				while ($fac_n = mysqli_fetch_array($fact))
				{
					$arr_num[] = $fac_n;
				}

				if(!empty($arr_num))
				{
					$resultado['idfactura'] = $arr_num[0]['id'];
				}else{
					$resultado['idfactura'] = "No se Encontro el id de la Factura";
				}

			}else{

				$resultado['factura'] = "No Se Pudo Ingresar La Factura";
			}
		}		

		return json_encode($resultado);
	}


	$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
	$servicio->service(file_get_contents("php://input"));
	
?>