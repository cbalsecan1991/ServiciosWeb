<?php

require_once 'lib/nusoap.php';
require_once 'variables_globales.php';
require_once 'FirmaFactura/xml_nota_credito.php';


	date_default_timezone_set('America/Bogota');
	
	//Instancio o objeto del servicio
	$servicio = new soap_server();
	//Instacion conexion a la base de datos
	
	//Name Espace
	$ns = "urn:consultarAutorizacionNCwsdl";

	//configuramos el servicio 	
	//1er parametro Nombre del Webservice
	//2do parametro Name space
	$servicio->configureWSDL("consultarautorizacionnc",$ns);
	//almacena el espacio de nobre de destino
	$servicio->schemaTargetNamespace = $ns;

	//Metodo (register) para registrar el servicio 4 parametros
	//1er parametro Nombre de la Funcion que se va a Desarrollar
	//2do parametros de Entrada para la Funcion
	//3er parametro return de errores o de respuestas
	//4to Name space

	//funcion para guardar cabecera de facturascli
	$servicio->register("consultarNC", 
		array(
			'idnotacredito' => 'xsd:integer'),
			array('return' => 'xsd:string'),
		$ns);

	//creamos la funcion cabecerafac
	function consultarNC($idnotacredito)
	{
		$clave = FALSE;
		$idnotacredito = trim($idnotacredito);
		//array de Respuestas o Retorno
		$resultado = array();

		//Busco las variables Globales para realizar la conexion
		$var = new variables_globales();

		$xml_nota_credito = new xml_nota_credito();

		//Realizo la conexion a la base de datos
		$conexion = mysqli_connect($var->FS_DB_HOST, $var->FS_DB_USER, $var->FS_DB_PASS, $var->FS_DB_NAME);
		//Busco la existencia de la Factura
		$facturas = $conexion->query("SELECT * FROM facturascli WHERE idfactura = ".$idnotacredito." AND estado_sri = 'AUTORIZADO'");
		$arr_fac = mysqli_fetch_array($facturas);
			if(empty($arr_fac))
			{
				//busco en las co_facturas si se encuentra aprobadas
				$co_facturas = $conexion->query("SELECT * FROM co_factura WHERE doc_instancias_id = '".$idnotacredito."'AND tipo_doc = '4' AND estado = 'AUTORIZADO';");
				$arr_facs = mysqli_fetch_array($co_facturas);

				if(empty($arr_facs))
				{				

					$consulta = $conexion->query("SELECT * FROM facturascli WHERE idfactura ='".$arr_fac['idfacturarect']."' AND estado_sri = 'AUTORIZADO';");
					$arr_venta = mysqli_fetch_array($consulta);
					//Verifico si exite la factura.
					if (!empty($arr_venta)) {
						$resultado['idfactura'] = "Factura de Venta encontrada correctamente";
						//Busco la existencia de la Factura
						$facturas = $conexion->query("SELECT * FROM facturascli WHERE idfactura = ".$idnotacredito);
						$arr_fac = mysqli_fetch_array($facturas);

						//Verifico si exite la factura.
						if(!empty($arr_fac))
						{

							$resultado['notacredito'] = "Nota De Credito encontrada correctamente";
							print_r($arr_fac);
							$resultado = $xml_nota_credito->generar_xml_nota_credito($arr_fac);

							//Busco la existencia de la Factura
							$facturas = $conexion->query("SELECT * FROM facturascli WHERE idfactura = ".$idnotacredito." AND estado_sri = 'AUTORIZADO'");
							$arr_fac_a = mysqli_fetch_array($facturas);

							if(!empty($arr_fac_a))
							{
								$resultado['notacredito_autorizacion'] = "La Nota de Credito Autorizada correctamente";
								$resultado['autoriza_sri'] = $arr_fac_a['autoriza_numero_sri'];
								$resultado['mensaje_sri'] = $arr_fac_a['mensaje_sri'];
								$resultado['estado_sri'] = $arr_fac_a['estado_sri'];
								$resultado['fecha_autorizacion'] = $arr_fac_a['fecha_autorizacion_sri'];
								$resultado['hora_autorizacion'] = $arr_fac_a['hora_autorizacion_sri'];
								$resultado['codigo_acceso_sri'] = $arr_fac_a['codigo_acceso_sri'];
								$resultado['documento'] = $arr_fac_a['numero_documento_sri'];
								$resultado['tipo_documento'] = $arr_fac_a['tipo_doc_sri'];

							}else{

								$resultado['error_autorizacion'] = "Error al Autorizar la Nota de Credito";

							}
						}else{
							$resultado['notacredito'] = "No se encuentra la Nota de Credito ".$idnotacredito;
						}
					}else{
						$resultado['idfactura'] = "La Factura de Venta no se encuentra autorizada";
					}
				}else{
					$resultado['notacredito'] = "La Nota de Credito se encuentra Autorizada";
					$resultado['autoriza_sri'] = $arr_facs['autoriza_numero'];
					$resultado['mensaje_sri'] = $arr_facs['mensaje'];
					$resultado['estado_sri'] = $arr_facs['estado'];
					$resultado['fecha_autorizacion'] = $arr_facs['fecha_actualizacion'];
					$resultado['hora_autorizacion'] = $arr_facs['hora_actualizacion'];
					$resultado['codigo_acceso_sri'] = $arr_facs['codigo_acceso'];
					$resultado['documento'] = $arr_facs['numero_documento'];
					$resultado['tipo_documento'] = $arr_facs['tipo_doc'];
				}

			}else{			
				$resultado['notacredito_autorizacion'] = "La Nota de Credito Autorizada correctamente";
				$resultado['autoriza_sri'] = $arr_fac['autoriza_numero_sri'];
				$resultado['mensaje_sri'] = $arr_fac['mensaje_sri'];
				$resultado['estado_sri'] = $arr_fac['estado_sri'];
				$resultado['fecha_autorizacion'] = $arr_fac['fecha_autorizacion_sri'];
				$resultado['hora_autorizacion'] = $arr_fac['hora_autorizacion_sri'];
				$resultado['codigo_acceso_sri'] = $arr_fac['codigo_acceso_sri'];
				$resultado['documento'] = $arr_fac['numero_documento_sri'];
				$resultado['tipo_documento'] = $arr_fac['tipo_doc_sri'];
			}
		
		return json_encode($resultado);
	}


	$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
	$servicio->service(file_get_contents("php://input"));
	
?>