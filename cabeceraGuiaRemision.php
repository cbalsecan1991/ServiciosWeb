<?php

require_once 'lib/nusoap.php';
require_once 'variables_globales.php';
require_once '../plugins/facturacion_base/extras/Utilities/Utilities.php';

	date_default_timezone_set('America/Bogota');

	//Instancio o objeto del servicio
	$servicio = new soap_server();
		
	//Name Espace
	$ns = "urn:cabeceraGuiaRemisiondwsdl";

	//configuramos el servicio 	
	//1er parametro Nombre del Webservice
	//2do parametro Name space
	$servicio->configureWSDL("cabeceraguiaremision",$ns);
	//almacena el espacio de nobre de destino
	$servicio->schemaTargetNamespace = $ns;

	//Metodo (register) para registrar el servicio 4 parametros
	//1er parametro Nombre de la Funcion que se va a Desarrollar
	//2do parametros de Entrada para la Funcion
	//3er parametro return de errores o de respuestas
	//4to Name space


	//funcion para guardar cabecera de facturascli
	$servicio->register("cabeceraGuia",
		array(
			'idfactura' => 'xsd:integer',
			'nombretransp' => 'xsd:string',
			'apellidostransp' => 'xsd:string',
			'identificaciontransp' => 'xsd:string',
			'Descripciontransp' => 'xsd:string',
			'motivotransp' => 'xsd:string',
			'rutatransp' => 'xsd:string',
			'placatransp' => 'xsd:string',
			'docaduanero' => 'xsd:string',
			'Fechainicio' => 'xsd:string',
			'Fechafin' => 'xsd:string',
			'Establecimientodest' => 'xsd:string',
			'Dirpartida' => 'xsd:string'),
			array('return' => 'xsd:string'),
		$ns);

	//creamos la funcion cabeceranotaCred
	function cabeceraGuia($idfactura,$nombretransp,$apellidostransp,$identificaciontransp,$Descripciontransp,$motivotransp,$rutatransp,$placatransp,$docaduanero,$Fechainicio,$Fechafin,$Establecimientodest,$Dirpartida){

		//quito los espacios en blanco limpiando la cadena
		$idfactura = trim($idfactura);
		$nombretransp = trim($nombretransp);
		$apellidostransp = trim($apellidostransp);
		$identificaciontransp = trim($identificaciontransp);
		$Descripciontransp = trim($Descripciontransp);
		$motivotransp = trim($motivotransp);
		$rutatransp = trim($rutatransp);
		$placatransp = trim($placatransp);
		$docaduanero = trim($docaduanero);
		$Fechainicio = trim($Fechainicio);
		$Fechafin = trim($Fechafin);
		$Establecimientodest = trim($Establecimientodest);
		$Dirpartida = trim($Dirpartida);

		$llave = TRUE;
		$resultado = array();
		$codalmacen = '';
		$puntoemision = '';

		//Busco las variables Globales para realizar la conexion
		$var = new variables_globales();

		//Realizo la conexion a la base de datos
		$conexion = mysqli_connect($var->FS_DB_HOST, $var->FS_DB_USER, $var->FS_DB_PASS, $var->FS_DB_NAME);
		//Busco la existencia del Cliente

		//Instacion metodos para validacion de cedulas o RUC
		$valida = new Utilities();

		$consulta = $conexion->query("SELECT * FROM facturascli WHERE idfactura = '".$idfactura."';");
		$factura = mysqli_fetch_array($consulta);



		if (empty($factura)) {
			$resultado['idfactura'] = "No se encontro la factura de venta para Realizar la Guia de Remision";
			$llave = FALSE;
		}else{
			if (empty($factura['numero_documento_sri'])) {
				$resultado['error'] = "La factura debe estar enviada al SRI";
				$llave = FALSE;
			}else{
				$consulta = $conexion->query("SELECT * FROM dirclientes WHERE codcliente = '".$factura['codcliente']."';");
				$dircliente = mysqli_fetch_array($consulta);
				if (empty($dircliente)) {
					$resultado['error'] = "El cliente de la factura ".$idfactura." no registra una dirección";
					$llave = FALSE;
				}
			}
		}

		$agencia = $conexion->query("SELECT * FROM agenciastrans WHERE ruc_trans = '".$identificaciontransp."'");
		$agencia = mysqli_fetch_array($agencia);
		if (!empty($agencia)) {
			$codagencia = $agencia['codtrans'];
		}else{
			$codagencia = 'NA';
		}

		if ($llave) {
			$resultado['idfactura'] = "Factura de Venta encontrada correctamente";
			$sql = "UPDATE facturascli SET 
			nombreenv='".$nombretransp."', 
			apellidosenv='".$apellidostransp."',
			ruc_transportista='".$identificaciontransp."', 
			descripcion_trans='".$Descripciontransp."', 
			motivo_trans='".$motivotransp."', 
			direccion_ruta='".$rutatransp."', 
			placaenv='".$placatransp."', 
			doc_aduanero='".$docaduanero."',
			fecha_ini_trans='".$Fechainicio."', 
			fecha_fin_trans='".$Fechafin."', 
			cod_estab_destino='".$Establecimientodest."', 
			dir_partida='".$Dirpartida."',
			direccionenv='".$dircliente['direccion']."', 
			codtransenv='".$codagencia."' 
			WHERE (idfactura='".$idfactura."');";
			if ($conexion->query($sql)) {
				$resultado['transporte'] = 'Datos de Transporte Guardados correctamente';
			}else{
				$resultado['transporte'] = 'No se PUdo Guardar los datos del Transporte';
			}


		}
		return json_encode($resultado);
	}
	$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
	$servicio->service(file_get_contents("php://input"));
?>