<?php

require_once 'lib/nusoap.php';
require_once 'variables_globales.php';
require_once '../plugins/facturacion_base/extras/Utilities/Utilities.php';

	date_default_timezone_set('America/Bogota');

	//Instancio o objeto del servicio
	$servicio = new soap_server();
		
	//Name Espace
	$ns = "urn:cabeceraNotaDebitodwsdl";

	//configuramos el servicio 	
	//1er parametro Nombre del Webservice
	//2do parametro Name space
	$servicio->configureWSDL("cabeceranotadebito",$ns);
	//almacena el espacio de nobre de destino
	$servicio->schemaTargetNamespace = $ns;

	//Metodo (register) para registrar el servicio 4 parametros
	//1er parametro Nombre de la Funcion que se va a Desarrollar
	//2do parametros de Entrada para la Funcion
	//3er parametro return de errores o de respuestas
	//4to Name space


	//funcion para guardar cabecera de facturascli
	$servicio->register("cabeceraND",
		array(
			'idfactura' => 'xsd:integer',
			'porcentaje' => 'xsd:integer',
			'baseimponible' => 'xsd:double',
			'formapago' => 'xsd:string',
			'motivo' => 'xsd:string'),
			array('return' => 'xsd:string'),
		$ns);

	//creamos la funcion cabeceranotaCred
	function cabeceraND($idfactura,$porcentaje,$baseimponible,$formapago,$motivo){

		//ESPACIO EN BLANCO
		$idfactura = trim($idfactura);
		$porcentaje = trim($porcentaje);
		$baseimponible = trim($baseimponible);
		$formapago = trim($formapago);
		$motivo = trim($motivo);

		//PREVENCIÓN DE QUE EL DATO LLEGUE CON ,
		$baseimponible = str_replace(',', '.', $baseimponible);

		//VARIABLES VERIFICADORAS
		$llave = TRUE;

		//VARIABLES GLOBALES
		$var = new variables_globales();

		//CONEXION A LA BASE DE DATOS
		$conexion = mysqli_connect($var->FS_DB_HOST, $var->FS_DB_USER, $var->FS_DB_PASS, $var->FS_DB_NAME);
		//Busco la existencia del Cliente

		//CLASE PARA VALIDAR RUC O CI
		$valida = new Utilities();

		//VARIABLES COMUNES


		//DATOS DE LA FACTURA DEL CLIENTE
		$consulta = $conexion->query("SELECT * FROM facturascli WHERE idfactura = '".$idfactura."';");
		$factura = mysqli_fetch_array($consulta);
		if (empty($factura)) {
			$resultado['idfactura'] = "No se encontro la factura de venta para Realizar la Nota de Debito";
			$llave = FALSE;
		}else{
			if ($factura['estado_sri'] != 'AUTORIZADO') {
				$resultado['error'] = "La factura debe estar enviada al SRI";
				$llave = FALSE;
			}else{
				$consulta = $conexion->query("SELECT * FROM dirclientes WHERE codcliente = '".$factura['codcliente']."';");
				$dircliente = mysqli_fetch_array($consulta);
				if (empty($dircliente)) {
					$resultado['error'] = "El cliente de la factura ".$idfactura." no registra una dirección";
					$llave = FALSE;
				}
			}
		}

		//CREACIÓN DE LA NUEVA TABLA notadebito
		$resultado['idfactura'] = "Factura de Venta encontrada correctamente";
		$tabla = "CREATE TABLE IF NOT EXISTS notadebitocli (
					idnotadebito INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
					idfactura INT(11) NOT NULL,
					cifnif VARCHAR(20) NOT NULL,
					iva DOUBLE NOT NULL,
					total DOUBLE NOT NULL,
					codpago VARCHAR(10) NOT NULL,
					motivo TEXT NOT NULL,
					estado_sri TEXT
				)";
		$tabla = $conexion->query($tabla);
		if ($tabla) {
			$llave = TRUE;
		}else{
			$llave = FALSE;
		}

		//SI CUMPLE CON TODOS LOS REQUERIMIENTOS
		if ($llave) {
			$sql = "INSERT INTO notadebitocli (idfactura,cifnif,iva,total,codpago,motivo) VALUES ('".$idfactura."', '".$factura['cifnif']."', '".$porcentaje."', '".$baseimponible."', '".$formapago."', '".$motivo."');";
			if ($conexion->query($sql)) {
				$resultado['notadebitocli'] = "Datos de la nota de debito Guardados Correctamente";
				if($notadebito = mysqli_fetch_array($conexion->query("SELECT max(idnotadebito) as idnotadebito FROM notadebitocli"))){
					$resultado['idnotadebitocli'] = $notadebito['idnotadebito'];
				}else{
					$resultado['idnotadebitocli'] = "No se encuentra la nota de debito";
				}
			}else{
				$resultado['notadebitocli'] = "No se Pudo Guardar los datos de la Nota de Debito";
			}
		}
		return json_encode($resultado);
	}
	$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
	$servicio->service(file_get_contents("php://input"));
?>