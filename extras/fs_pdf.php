<?php
/*
 * This file is part of FacturaScripts
 * Copyright (C) 2013-2017  Carlos Garcia Gomez  neorazorx@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once 'ezpdf/Cezpdf.php';
require_once 'variables_globales.php';

/**
 * Permite la generación de PDFs algo más sencilla.
 */
class fs_pdf
{
   /**
    * Ruta al logotipo de la empresa.
    * @var type 
    */
   public $logo;
   public $formato;
   public $observacion;
   public $pie;
   public $forma_pago;
   public $pie_e;

   
   /**
    * Documento Cezpdf
    * @var Cezpdf
    */
   public $pdf;
   
   public $table_header;
   public $table_rows;

   public $var;
   
   public function __construct($paper = 'a4', $orientation = 'portrait', $font = 'Helvetica')
   {      
      $this->cargar_logo();
      $this->pdf = new Cezpdf($paper, $orientation);
      $this->pdf->selectFont(__DIR__."/ezpdf/fonts/".$font.".afm");
   }
   
   /**
    * Vuelca el documento PDF en la salida estándar.
    * @param type $filename
    */
   public function show($filename = 'doc.pdf')
   {
      $this->pdf->ezStream( array('Content-Disposition' => $filename) );
   }
   
   /**
    * Guarda el documento PDF en el archivo $filename
    * @param type $filename
    * @return boolean
    */
   public function save($filename)
   {
      if($filename)
      {
         if( file_exists($filename) )
         {
            unlink($filename);
         }
         
         $file = fopen($filename, 'a');
         if($file)
         {
            fwrite($file, $this->pdf->ezOutput());
            fclose($file);
            return TRUE;
         }
         else
            return TRUE;
      }
      else
         return FALSE;
   }
   
   /**
    * Devuelve la coordenada Y actual en el documento.
    * @return type
    */
   public function get_y()
   {
      return $this->pdf->y;
   }
   
   /**
    * Establece la coordenada Y actual en el documento.
    * @param type $y
    */
   public function set_y($y)
   {
      $this->pdf->ezSetY($y);
   }

   
   /**
    * Carga la ruta del logotipo de la empresa.
    */
   private function cargar_logo()
   {
      $this->logo = FALSE;
      if(file_exists('../images/logo.png') )
      {
         $this->logo = '../images/logo.png';
      }
      else if( file_exists('../images/logo.jpg') )
      {
         $this->logo = '../images/logo.jpg';
      }
   }
   
   /**
    * Añade la cabecera al PDF con el logotipo y los datos de la empresa.
    * @param type $empresa
    * @param int $lppag
    */
   public function generar_pdf_cabecera(&$empresa, &$lppag)
   {
      //$this->pdf->addJpegFromFile($this->formato, 0, 0, 610, 870);
      /// ¿Añadimos el logo?
      if($this->logo)
      {
         if(function_exists('imagecreatefromstring') )
         {
            $lppag -= 2; /// si metemos el logo, caben menos líneas
            
            $tamanyo = $this->calcular_tamanyo_logo();
            if( substr( strtolower($this->logo), -4 ) == '.png' )
            {
               $this->pdf->addPngFromFile($this->logo, 35, 740, $tamanyo[0], $tamanyo[1]);
            }
            else
            {
               $this->pdf->addJpegFromFile($this->logo, 35, 740, $tamanyo[0], $tamanyo[1]);
            }
            
            $this->pdf->ez['rightMargin'] = 40;
            $this->pdf->ezText("<b>".$empresa->nombre."</b>", 12, array('justification' => 'right'));
            $this->pdf->ezText(FS_CIFNIF.": ".$empresa->cifnif, 8, array('justification' => 'right'));
            
            $direccion = $empresa->direccion . "\n";
            if($empresa->apartado)
            {
               $direccion .= ucfirst(FS_APARTADO) . ': ' . $empresa->apartado . ' - ';
            }
            
            if($empresa->codpostal)
            {
               $direccion .= 'CP: ' . $empresa->codpostal . ' - ';
            }
            
            if($empresa->ciudad)
            {
               $direccion .= $empresa->ciudad . ' - ';
            }
            
            if($empresa->provincia)
            {
               $direccion .= '(' . $empresa->provincia . ')';
            }
            
            if($empresa->telefono)
            {
               $direccion .= "\nTeléfono: " . $empresa->telefono;
            }
            
            $this->pdf->ezText($this->fix_html($direccion)."\n", 9, array('justification' => 'right'));
            $this->set_y(750);
         }
         else
         {
            die('ERROR: no se encuentra la función imagecreatefromstring(). '
                    . 'Y por tanto no se puede usar el logotipo en los documentos.');
         }
      }
      else
      {
         $this->pdf->ezText("<b>".$empresa->nombre."</b>", 16, array('justification' => 'center'));
         $this->pdf->ezText(FS_CIFNIF.": ".$empresa->cifnif, 8, array('justification' => 'center'));
         
         $direccion = $empresa->direccion;
         if($empresa->apartado)
         {
            $direccion .= ' - ' . ucfirst(FS_APARTADO) . ': ' . $empresa->apartado;
         }
         
         if($empresa->codpostal)
         {
            $direccion .= ' - CP: ' . $empresa->codpostal;
         }
         
         if($empresa->ciudad)
         {
            $direccion .= ' - ' . $empresa->ciudad;
         }
         
         if($empresa->provincia)
         {
            $direccion .= ' (' . $empresa->provincia . ')';
         }
         
         if($empresa->telefono)
         {
            $direccion .= ' - Teléfono: ' . $empresa->telefono;
         }
         
         $this->pdf->ezText($this->fix_html($direccion), 9, array('justification' => 'center'));
      }
   }

   public function generar_pdf_cabecera_f(&$empresa, &$lppag)
   {
      //$this->pdf->addJpegFromFile($this->formato, 0, 0, 610, 870);
      /// ¿Añadimos el logo?
      if($this->logo)
      {
         if( function_exists('imagecreatefromstring') )
         {
            $lppag -= 2; /// si metemos el logo, caben menos líneas
            
            $tamanyo = $this->calcular_tamanyo_logo();

            if( substr( strtolower($this->logo), -4 ) == '.png' )
            {
               $this->pdf->addPngFromFile($this->logo, 35, 740, $tamanyo[0], $tamanyo[1]);
            }
            else
            {
               $this->pdf->addJpegFromFile($this->logo, 35, 740, $tamanyo[0], $tamanyo[1]);
            }

          //  $this->pdf->addJpegFromFile($this->presu, 20, 570, 550, 150);

            /*$this->pdf->ez['rightMargin'] = 40;
            $this->pdf->ezText("<b>".$empresa->nombre."</b>", 12, array('justification' => 'right'));
            $this->pdf->ezText(FS_CIFNIF.": ".$empresa->cifnif, 8, array('justification' => 'right'));*/
            
            /*$direccion = $empresa->direccion . "\n";
            if($empresa->apartado)
            {
               $direccion .= ucfirst(FS_APARTADO) . ': ' . $empresa->apartado . ' - ';
            }
            
            if($empresa->codpostal)
            {
               $direccion .= 'CP: ' . $empresa->codpostal . ' - ';
            }
            
            if($empresa->ciudad)
            {
               $direccion .= $empresa->ciudad . ' - ';
            }
            
            if($empresa->provincia)
            {
               $direccion .= '(' . $empresa->provincia . ')';
            }
            
            if($empresa->telefono)
            {
               $direccion .= "\nTeléfono: " . $empresa->telefono;
            }
            
            $this->pdf->ezText($this->fix_html($direccion)."\n", 9, array('justification' => 'right'));
            $this->set_y(750);*/
         }
         else
         {
            die('ERROR: no se encuentra la función imagecreatefromstring(). '
                    . 'Y por tanto no se puede usar el logotipo en los documentos.');
         }
      }
      else
      {
         $this->pdf->ezText("<b>".$empresa->nombre."</b>", 16, array('justification' => 'center'));
         $this->pdf->ezText(FS_CIFNIF.": ".$empresa->cifnif, 8, array('justification' => 'center'));
         
         $direccion = $empresa->direccion;
         if($empresa->apartado)
         {
            $direccion .= ' - ' . ucfirst(FS_APARTADO) . ': ' . $empresa->apartado;
         }
         
         if($empresa->codpostal)
         {
            $direccion .= ' - CP: ' . $empresa->codpostal;
         }
         
         if($empresa->ciudad)
         {
            $direccion .= ' - ' . $empresa->ciudad;
         }
         
         if($empresa->provincia)
         {
            $direccion .= ' (' . $empresa->provincia . ')';
         }
         
         if($empresa->telefono)
         {
            $direccion .= ' - Teléfono: ' . $empresa->telefono;
         }
         
         $this->pdf->ezText($this->fix_html($direccion), 9, array('justification' => 'center'));
      }
   }


   public function generar_pdf_cabecera_eti(&$empresa, &$lppag)
   {
      //$this->pdf->addJpegFromFile($this->formato, 0, 0, 610, 870);
      /// ¿Añadimos el logo?
      
      if( function_exists('imagecreatefromstring') )
      {
         $lppag -= 2; /// si metemos el logo, caben menos líneas

         $this->pdf->addPngFromFile($this->etigraf, 10, 710, 550, 100);
         $this->set_y(710);
         $this->pdf->ez['rightMargin'] = 40;
         $this->pdf->ezText("<b>".$empresa->nombre."</b>", 12);
         $this->pdf->ezText("<b>RUC:</b>       ".$empresa->cifnif, 12);
         
         $direccion = "<b>Dirección:</b>    ".$empresa->direccion ." ";
         if($empresa->apartado)
         {
            $direccion .= ucfirst(FS_APARTADO) . ': ' . $empresa->apartado . ' - ';
         }
         
         if($empresa->codpostal)
         {
            $direccion .= '<b>CP:</b> ' . $empresa->codpostal ;
         }
         
         $this->pdf->ezText($this->fix_html($direccion), 9);
         $this->pdf->ezText("<b>Teléfono:</b>     ".$empresa->telefono, 9);
         $this->pdf->ezText("<b>Email:</b>          ".$empresa->email, 9);
      }
      else
      {
         die('ERROR: no se encuentra la función imagecreatefromstring(). '
                 . 'Y por tanto no se puede usar el logotipo en los documentos.');
      }
   }

   public function generar_pdf_cabecera_preimpresa(&$empresa, &$lppag)
   {
      //$this->pdf->addJpegFromFile($this->formato, 0, 0, 610, 870);      
   }
   
   private function calcular_tamanyo_logo()
   {
      $tamanyo = $size = getimagesize($this->logo);
      if($size[0] > 200)
      {
         $tamanyo[0] = 200;
         $tamanyo[1] = $tamanyo[1] * $tamanyo[0]/$size[0];
         $size[0] = $tamanyo[0];
         $size[1] = $tamanyo[1];
      }
      
      if($size[1] > 80)
      {
         $tamanyo[1] = 80;
         $tamanyo[0] = $tamanyo[0] * $tamanyo[1]/$size[1];
      }
      
      return $tamanyo;
   }
   
   public function center_text($word = '', $tot_width = 140)
   {
      if( strlen($word) == $tot_width )
      {
         return $word;
      }
      else if( strlen($word) < $tot_width )
      {
         return $this->center_text2($word, $tot_width);
      }
      else
      {
         $result = '';
         $nword = '';
         foreach( explode(' ', $word) as $aux )
         {
            if($nword == '')
            {
               $nword = $aux;
            }
            else if( strlen($nword) + strlen($aux) + 1 <= $tot_width )
            {
               $nword = $nword.' '.$aux;
            }
            else
            {
               if($result != '')
               {
                  $result .= "\n";
               }
               $result .= $this->center_text2($nword, $tot_width);
               $nword = $aux;
            }
         }
         if($nword != '')
         {
            if($result != '')
            {
               $result .= "\n";
            }
            $result .= $this->center_text2($nword, $tot_width);
         }
         return $result;
      }
   }
   
   private function center_text2($word = '', $tot_width = 140)
   {
      $symbol = " ";
      $middle = round($tot_width / 2);
      $length_word = strlen($word);
      $middle_word = round($length_word / 2);
      $last_position = $middle + $middle_word;
      $number_of_spaces = $middle - $middle_word;
      $result = sprintf("%'{$symbol}{$last_position}s", $word);
      for($i = 0; $i < $number_of_spaces; $i++)
      {
         $result .= "$symbol";
      }
      return $result;
   }
   
   public function new_table()
   {
      $this->table_header = array();
      $this->table_rows = array();
   }
   
   public function add_table_header($header)
   {
      $this->table_header = $header;
   }
   
   public function add_table_row($row)
   {
      $this->table_rows[] = $row;
   }
   
   public function save_table($options)
   {
      if( !$this->table_header )
      {
         foreach( array_keys($this->table_rows[0]) as $k )
         {
            $this->table_header[$k] = '';
         }
      }
      
      $this->pdf->ezTable($this->table_rows, $this->table_header, '', $options);
   }
   
   public function save_table_P($options)
   {
      
      $this->pdf->ezTable($this->table_rows, '', '', $options);
   }

   public function fix_html($txt)
   {
      $newt = str_replace('&lt;', '<', $txt);
      $newt = str_replace('&gt;', '>', $newt);
      $newt = str_replace('&quot;', '"', $newt);
      $newt = str_replace('&#39;', "'", $newt);
      return $newt;
   }
   
   public function get_lineas_iva($lineas)
   {
      $retorno = array();
      $lineasiva = array();
      
      foreach($lineas as $lin)
      {
         if( isset($lineasiva[$lin['codimpuesto']]) )
         {
            if($lin['recargo'] > $lineasiva[$lin['codimpuesto']]['recargo'])
            {
               $lineasiva[$lin['codimpuesto']]['recargo'] = $lin['recargo'];
            }
            
            $lineasiva[$lin['codimpuesto']]['neto'] += $lin['pvptotal'];
            $lineasiva[$lin['codimpuesto']]['totaliva'] += ($lin['pvptotal']*$lin['iva'])/100;
            $lineasiva[$lin['codimpuesto']]['totalrecargo'] += ($lin['pvptotal']*$lin['recargo'])/100;
            $lineasiva[$lin['codimpuesto']]['totallinea'] = $lineasiva[$lin['codimpuesto']]['neto']
                    + $lineasiva[$lin['codimpuesto']]['totaliva'] + $lineasiva[$lin['codimpuesto']]['totalrecargo'];
         }
         else
         {
            $lineasiva[$lin['codimpuesto']] = array(
                'codimpuesto' => $lin['codimpuesto'],
                'iva' => $lin['iva'],
                'recargo' => $lin['recargo'],
                'neto' => $lin['pvptotal'],
                'totaliva' => ($lin['pvptotal']*$lin['iva'])/100,
                'totalrecargo' => ($lin['pvptotal']*$lin['recargo'])/100,
                'totallinea' => 0
            );
            $lineasiva[$lin['codimpuesto']]['totallinea'] = $lineasiva[$lin['codimpuesto']]['neto']
                    + $lineasiva[$lin['codimpuesto']]['totaliva'] + $lineasiva[$lin['codimpuesto']]['totalrecargo'];
         }
      }
      
      foreach($lineasiva as $lin)
      {
         $retorno[] = $lin;
      }
      
      return $retorno;
   }

   // FACTURA ELECTRONICA

  public function generar_pdf_cabecera_fac_elec(&$empresa, &$lppag, &$documento, &$numero)
   {
      // Volvemos a consultar los datos de la factura para rescatar la información proporcionada por el SRI  
      //Busco las variables Globales para realizar la conexion
      $var = new variables_globales();

      //Realizo la conexion a la base de datos
      $conexion = mysqli_connect($var->FS_DB_HOST, $var->FS_DB_USER, $var->FS_DB_PASS, $var->FS_DB_NAME);

      //Busco la existencia de la Factura
      $facturas = $conexion->query("SELECT * FROM facturascli WHERE idfactura = ".$documento[40]);
      
      $factura = array();

      //transformo los datos de la consulta a un array
      while ($factura_b = mysqli_fetch_array($facturas))
      {
         $factura[] = $factura_b;
      }
      /// ¿Añadimos el logo?
      if($this->logo)
      {
         if( function_exists('imagecreatefromstring') )
         {
            $lppag -= 2; /// si metemos el logo, caben menos líneas
            
            $tamanyo = $this->calcular_tamanyo_logo();
            if( substr( strtolower($this->logo), -4 ) == '.png' )
            {
               $this->pdf->addPngFromFile($this->logo, 35, 740, $tamanyo[0], $tamanyo[1]);
            }
            else
            {
               $this->pdf->addJpegFromFile($this->logo, 35, 740, $tamanyo[0], $tamanyo[1]);
            }

          }else{
            die('ERROR: no se encuentra la función imagecreatefromstring(). '
                    . 'Y por tanto no se puede usar el logotipo en los documentos.');
         }
      }else{
        $this->pdf->addJpegFromFile('images/no_logo.jpg', 35, 680, 200, 130);
      }
            $this->new_table();
            $this->add_table_row(
               array(
                   'campo' => "RUC: ".$empresa[0]['cifnif']
               )
            );
            $this->add_table_row(
               array(
                   'campo' => '<b>FACTURA</b>: '.$factura[0][77]
               )
            );
            /*$this->add_table_row(
               array(
                   'campo' => '<b>Cod. '.$documento->codigo.'</b>'
               )
            );*/
            $this->add_table_row(
               array(
                   'campo' => "\n".'<b>NÚMERO DE AUTORIZACIÓN:</b>  '
               )
            );

            // GENERO CLAVE DE ACCESO
            $this->add_table_row(
               array(
                   'campo' => $factura[0][78]
               )
            );
            $this->add_table_row(
               array(
                   'campo' => "\n".'FECHA Y HORA DE AUTORIZACIÓN'
               )
            );
            $this->add_table_row(
               array(
                   'campo' => $factura[0][83] . ' : ' .$factura[0][84]
               )
            );

            // CONDICIONAMOS SI EL AMBIENTE ES EN DESARROLLO O PRODUCCION
            if ($var->FS_AMBIENTE_XML==1) {
               $ambiente = 'PRUEBAS';
            }
            if ($var->FS_AMBIENTE_XML==2) {
               $ambiente = 'PRODUCCIÓN';
            }

            $this->add_table_row(
               array(
                   'campo' => "\n".'<b>AMBIENTE: </b>'.$ambiente
               )
            );

            $this->add_table_row(
               array(
                   'campo' => "\n".'<b>EMISIÓN:</b> NORMAL'
               )
            );

            $this->add_table_row(
               array(
                   'campo' => "\n".'CLAVE DE ACCESO'
               )
            );

            $y = $this->get_y();
            $this->set_y(650);
            //$this->pdf->ezImage('https://www.cerotec.net/gen_barcode.php?c='. $factura->codigo_acceso_sri .'&t=iso', 30, $width=200, $resize= 'width', $just= 'right', $border= 0);

           //$this->pdf->ezImage('http://exses.kapitalcompany.com/images/codigos_de_barras.jpg', 30, $width=250, $resize= 'width', $just= 'right', $border= 0);

            $this->pdf->addJpegFromFile('../images/codigos_de_barras.jpg',300,580,250); 

            $this->set_y($y);

            $this->add_table_row(
               array(
                   'campo' =>  "\n\n\n\n\n\n\n" .'             '.$factura[0][81]
               )
            );

           

            $this->save_table(
               array(
                  'cols' => array(
                     'campo' => array('justification' => 'left'),
                  ),
                  'showLines' => 1,
                  'xPos'=>'right',
                  'xOrientation'=>'left',
                  'showHeadings' => 0,
                  'width' => 290,
                  'shaded' => 0,
                  'fontSize' =>8
               )
            );

            $this->pdf->ez['leftMargin'] = 25;
            $this->pdf->ez['rightMargin'] = 25;
            //$this->set_y(718);
            $this->set_y(735);
            
            //ob_start();
              
   }

   public function generar_pdf_cabecera_pedido(&$empresa, &$lppag, &$documento, &$numero)
   {
      /// ¿Añadimos el logo?
      if($this->logo)
      {
         if( function_exists('imagecreatefromstring') )
         {
            $lppag -= 2; /// si metemos el logo, caben menos líneas
            
            $tamanyo = $this->calcular_tamanyo_logo();
            if( substr( strtolower($this->logo), -4 ) == '.png' )
            {
               $this->pdf->addPngFromFile($this->logo, 35, 740, $tamanyo[0], $tamanyo[1]);
            }
            else
            {
               $this->pdf->addJpegFromFile($this->logo, 35, 740, $tamanyo[0], $tamanyo[1]);
            }

          }else{
            die('ERROR: no se encuentra la función imagecreatefromstring(). '
                    . 'Y por tanto no se puede usar el logotipo en los documentos.');
         }
      }else{
        $this->pdf->addJpegFromFile('images/no_logo.jpg', 35, 680, 200, 130);
      }
            $this->new_table();
            $this->add_table_row(
               array(
                   'campo' => FS_CIFNIF.": ".$empresa->cifnif
               )
            );
            $this->add_table_row(
               array(
                   'campo' => '<b>'.ucfirst(FS_FACTURA).'</b>: '.$documento->retornaNumeroDoc()
               )
            );
            /*$this->add_table_row(
               array(
                   'campo' => '<b>Cod. '.$documento->codigo.'</b>'
               )
            );*/
            $this->add_table_row(
               array(
                   'campo' => "\n".'<b>NÚMERO DE AUTORIZACIÓN:</b>  '
               )
            );

            // GENERO CLAVE DE ACCESO
            $this->add_table_row(
               array(
                   'campo' => $factura->autoriza_numero_sri
               )
            );
            $this->add_table_row(
               array(
                   'campo' => "\n".'FECHA Y HORA DE AUTORIZACIÓN'
               )
            );
            $this->add_table_row(
               array(
                   'campo' => $factura->fecha_autorizacion_sri . ' : ' .$factura->hora_autorizacion_sri
               )
            );

            // CONDICIONAMOS SI EL AMBIENTE ES EN DESARROLLO O PRODUCCION
            if (FS_AMBIENTE_XML==1) {
               $ambiente = 'PRUEBAS';
            }
            if (FS_AMBIENTE_XML==2) {
               $ambiente = 'PRODUCCIÓN';
            }

            $this->add_table_row(
               array(
                   'campo' => "\n".'<b>AMBIENTE: </b>'.$ambiente
               )
            );

            $this->add_table_row(
               array(
                   'campo' => "\n".'<b>EMISIÓN:</b> NORMAL'
               )
            );

            $this->add_table_row(
               array(
                   'campo' => "\n".'CLAVE DE ACCESO'
               )
            );

            $y = $this->get_y();
            $this->set_y(650);
            //$this->pdf->ezImage('https://www.cerotec.net/gen_barcode.php?c='. $factura->codigo_acceso_sri .'&t=iso', 30, $width=200, $resize= 'width', $just= 'right', $border= 0);

           //$this->pdf->ezImage('http://exses.kapitalcompany.com/images/codigos_de_barras.jpg', 30, $width=250, $resize= 'width', $just= 'right', $border= 0);

            $this->pdf->addJpegFromFile('../images/codigos_de_barras.jpg',300,580,250); 

            $this->set_y($y);

            $this->add_table_row(
               array(
                   'campo' =>  "\n\n\n\n\n\n\n" .'             '.$factura->codigo_acceso_sri
               )
            );

           

            $this->save_table(
               array(
                  'cols' => array(
                     'campo' => array('justification' => 'left'),
                  ),
                  'showLines' => 1,
                  'xPos'=>'right',
                  'xOrientation'=>'left',
                  'showHeadings' => 0,
                  'width' => 290,
                  'shaded' => 0,
                  'fontSize' =>8
               )
            );

            $this->pdf->ez['leftMargin'] = 25;
            $this->pdf->ez['rightMargin'] = 25;
            //$this->set_y(718);
            $this->set_y(735);
            
            //ob_start();
              
   }

   // FACTURA ELECTRONICA

  public function generar_pdf_cabecera_nota_cre_elec(&$empresa, &$lppag, &$documento, &$numero)
   {
      // Volvemos a consultar los datos de la factura para rescatar la información proporcionada por el SRI 

      $var = new variables_globales();

      //Realizo la conexion a la base de datos
      $conexion = mysqli_connect($var->FS_DB_HOST, $var->FS_DB_USER, $var->FS_DB_PASS, $var->FS_DB_NAME);


      $consulta = $conexion->query("SELECT * FROM facturascli WHERE idfactura = '".$documento['idfactura']."';");
      $factura = mysqli_fetch_array($consulta);

      /// ¿Añadimos el logo?
      if($this->logo)
      {
         if( function_exists('imagecreatefromstring') )
         {
            $lppag -= 2; /// si metemos el logo, caben menos líneas
            
            $tamanyo = $this->calcular_tamanyo_logo();
            if( substr( strtolower($this->logo), -4 ) == '.png' )
            {
               $this->pdf->addPngFromFile($this->logo, 35, 730, $tamanyo[0], $tamanyo[1]);
            }
            else
            {
               $this->pdf->addJpegFromFile($this->logo, 35, 730, $tamanyo[0], $tamanyo[1]);
            }

          }else{
            die('ERROR: no se encuentra la función imagecreatefromstring(). '
                    . 'Y por tanto no se puede usar el logotipo en los documentos.');
         }
      }else{
        $this->pdf->addJpegFromFile('images/no_logo.jpg', 35, 680, 200, 130);
      }
            $this->new_table();
            $this->add_table_row(
               array(
                   'campo' => "RUC: ".$empresa['cifnif']
               )
            );
            $this->add_table_row(
               array(
                   'campo' => "<b>N O T A  D E  C R E D I T O<b>"
               )
            );
            $this->add_table_row(
               array(
                   'campo' => '<b>No.<b> '.$numero
               )
            );
            /*$this->add_table_row(
               array(
                   'campo' => '<b>Cod. '.$documento->codigo.'</b>'
               )
            );*/
            $this->add_table_row(
               array(
                   'campo' => "\n".'<b>NÚMERO DE AUTORIZACIÓN:</b>  '
               )
            );

            // GENERO CLAVE DE ACCESO
            $this->add_table_row(
               array(
                   'campo' => $factura['autoriza_numero_sri']
               )
            );
            $this->add_table_row(
               array(
                   'campo' => "\n".'FECHA Y HORA DE AUTORIZACIÓN'
               )
            );
            $this->add_table_row(
               array(
                   'campo' => $factura['fecha_autorizacion_sri'] . ' : ' .$factura['hora_autorizacion_sri']
               )
            );

            // CONDICIONAMOS SI EL AMBIENTE ES EN DESARROLLO O PRODUCCION
            if ($var->FS_AMBIENTE_XML==1) {
               $ambiente = 'PRUEBAS';
            }
            if ($var->FS_AMBIENTE_XML==2) {
               $ambiente = 'PRODUCCIÓN';
            }

            $this->add_table_row(
               array(
                   'campo' => "\n".'<b>AMBIENTE: </b>'.$ambiente
               )
            );

            $this->add_table_row(
               array(
                   'campo' => "\n".'<b>EMISIÓN:</b> NORMAL'
               )
            );

            $this->add_table_row(
               array(
                   'campo' => "\n".'CLAVE DE ACCESO'
               )
            );

            $y = $this->get_y();
            $this->set_y(650);
            //$this->pdf->ezImage('https://www.cerotec.net/gen_barcode.php?c='. $factura->codigo_acceso_sri .'&t=iso', 30, $width=200, $resize= 'width', $just= 'right', $border= 0);

           //$this->pdf->ezImage('http://exses.kapitalcompany.com/images/codigos_de_barras.jpg', 30, $width=250, $resize= 'width', $just= 'right', $border= 0);

            $this->pdf->addJpegFromFile('../images/codigos_de_barras.jpg',300,580,250); 

            $this->set_y($y);

            $this->add_table_row(
               array(
                   'campo' =>  "\n\n\n\n\n\n" .'             '.$factura['codigo_acceso_sri']
               )
            );

           

            $this->save_table(
               array(
                  'cols' => array(
                     'campo' => array('justification' => 'left'),
                  ),
                  'showLines' => 1,
                  'xPos'=>'right',
                  'xOrientation'=>'left',
                  'showHeadings' => 0,
                  'width' => 290,
                  'shaded' => 0,
                  'fontSize' =>8
               )
            );

            $this->pdf->ez['leftMargin'] = 25;
            $this->pdf->ez['rightMargin'] = 25;
            //$this->set_y(718);
            $this->set_y(733);
            
            //ob_start();
              
   }




   public function generar_pdf_cabecera_guia_remision(&$empresa, &$lppag, &$documento, &$numero)
   {

      // Volvemos a consultar los datos de la factura para rescatar la información proporcionada por el SRI 

      $var = new variables_globales();

      //Realizo la conexion a la base de datos
      $conexion = mysqli_connect($var->FS_DB_HOST, $var->FS_DB_USER, $var->FS_DB_PASS, $var->FS_DB_NAME);


     // Volvemos a consultar los datos de la factura para rescatar la información proporcionada por el SRI  
      $consulta = $conexion->query("SELECT * FROM co_factura WHERE estado = 'AUTORIZADO' AND tipo_doc = '6' AND doc_instancias_id= '".$documento['idfactura']."';");
      $co_factura = mysqli_fetch_array($consulta);

      //print_r($co_factura);
      //exit();
    /// ¿Añadimos el logo?
      if($this->logo)
      {
         if( function_exists('imagecreatefromstring') )
         {
            $lppag -= 2; /// si metemos el logo, caben menos líneas
            
            $tamanyo = $this->calcular_tamanyo_logo();
            if( substr( strtolower($this->logo), -4 ) == '.png' )
            {
               $this->pdf->addPngFromFile($this->logo, 35, 740, $tamanyo[0], $tamanyo[1]);
            }
            else
            {
               $this->pdf->addJpegFromFile($this->logo, 35, 740, $tamanyo[0], $tamanyo[1]);
            }

          }else{
            die('ERROR: no se encuentra la función imagecreatefromstring(). '
                    . 'Y por tanto no se puede usar el logotipo en los documentos.');
         }
      }else{
        $this->pdf->addJpegFromFile('images/no_logo.jpg', 35, 680, 200, 130);
      }
            $this->new_table();
            $this->add_table_row(
               array(
                   'campo' => "RUC: ".$empresa['cifnif']."\n".'<b>GUIA DE REMISION</b>'
               )
            );
            $this->add_table_row(
               array(
                   'campo' => '<b>'.'No.'.'</b>'.' '. $co_factura['numero_documento']
               )
            );
            /*$this->add_table_row(
               array(
                   'campo' => '<b>Cod. '.$documento->codigo.'</b>'
               )
            );*/
            $this->add_table_row(
               array(
                   'campo' => "\n".'<b>NÚMERO DE AUTORIZACIÓN:</b>  '
               )
            );

            // GENERO CLAVE DE ACCESO

          
            $this->add_table_row(
               array(
                   'campo' => $co_factura['autoriza_numero'],
               )
            );
            $this->add_table_row(
               array(
                   'campo' => "\n".'FECHA Y HORA DE AUTORIZACIÓN'
               )
            );
            $this->add_table_row(
               array(
                   'campo' => $documento['fecha_autorizacion_sri'] . ' : ' .$documento['hora_autorizacion_sri']
               )
            );

            // CONDICIONAMOS SI EL AMBIENTE ES EN DESARROLLO O PRODUCCION
            if ($var->FS_AMBIENTE_XML==1) {
               $ambiente = 'PRUEBAS';
            }
            if ($var->FS_AMBIENTE_XML==2) {
               $ambiente = 'PRODUCCIÓN';
            }

            $this->add_table_row(
               array(
                   'campo' => "\n".'<b>AMBIENTE: </b>'.$ambiente
               )
            );

            $this->add_table_row(
               array(
                   'campo' => "\n".'<b>EMISIÓN:</b> NORMAL'
               )
            );

            $this->add_table_row(
               array(
                   'campo' => "\n".'CLAVE DE ACCESO'
               )
            );

            $y = $this->get_y();
            $this->set_y(640);
            //$this->pdf->ezImage('https://www.cerotec.net/gen_barcode.php?c='.$co_factura->codigo_acceso.'&t=iso', 30, $width=250, $resize= 'width', $just= 'right', $border= 0);

            $this->pdf->addJpegFromFile('../images/codigos_de_barras.jpg',300,580,250);

            $this->add_table_row(
               array(
                   'campo' =>  "\n\n\n\n\n\n" .'             '.$co_factura['codigo_acceso']
               )
            );

            

            $this->set_y($y);



            $this->save_table(
               array(
                  'cols' => array(
                     'campo' => array('justification' => 'left'),
                  ),
                  'showLines' => 1,
                  'xPos'=>'right',
                  'xOrientation'=>'left',
                  'showHeadings' => 0,
                  'width' => 290,
                  'shaded' => 0,
                  'fontSize' =>8
               )
            );

            $this->pdf->ez['leftMargin'] = 25;
            $this->pdf->ez['rightMargin'] = 25;
            $this->set_y(760);
              
   }






      // FACTURA ELECTRONICA RETENCIONES

      public function generar_pdf_cabecera_fac_elec_ret(&$empresa, &$lppag, &$documento)
   {

      // Volvemos a consultar los datos de la factura para rescatar la información proporcionada por el SRI  
      $var = new variables_globales();

      //Realizo la conexion a la base de datos
      $conexion = mysqli_connect($var->FS_DB_HOST, $var->FS_DB_USER, $var->FS_DB_PASS, $var->FS_DB_NAME);

      $consulta = $conexion->query("SELECT * FROM facturasprov WHERE idfactura = '".$documento['idfactura']."';");
      $facturaprov = mysqli_fetch_array($consulta);

      $consulta = $conexion->query("SELECT * FROM co_factura
              WHERE estado = 'AUTORIZADO' AND tipo_doc = '3' AND doc_instancias_id= '".$documento['idfactura']."';");
      $co_factura = mysqli_fetch_array($consulta);
      /// ¿Añadimos el logo?
      if($this->logo)
      {
         if( function_exists('imagecreatefromstring') )
         {
            $lppag -= 2; /// si metemos el logo, caben menos líneas
            
            $tamanyo = $this->calcular_tamanyo_logo();
            if( substr( strtolower($this->logo), -4 ) == '.png' )
            {
               $this->pdf->addPngFromFile($this->logo, 35, 740, $tamanyo[0], $tamanyo[1]);
            }
            else
            {
               $this->pdf->addJpegFromFile($this->logo, 35, 740, $tamanyo[0], $tamanyo[1]);
            }

          }else{
            die('ERROR: no se encuentra la función imagecreatefromstring(). '
                    . 'Y por tanto no se puede usar el logotipo en los documentos.');
         }
      }else{
        $this->pdf->addJpegFromFile('images/no_logo.jpg', 35, 680, 200, 130);
      }
            $this->new_table();
            $this->add_table_row(
               array(
                   'campo' => "RUC: ".$empresa['cifnif']
               )
            );
            $this->add_table_row(
               array(
                   'campo' => '<b>COMPROBANTE DE RETENCIÓN</b>'
               )
            );
            $this->add_table_row(
               array(
                   'campo' => '<b>Nro. '.$facturaprov['num_retencion'].'</b>'
               )
            );
            $this->add_table_row(
               array(
                   'campo' => "\n".'<b>NÚMERO DE AUTORIZACIÓN:</b>  '
               )
            );

            $this->add_table_row(
               array(
                   'campo' => $co_factura['autoriza_numero']
               )
            );

            $this->add_table_row(
               array(
                   'campo' => "\n".'<b>FECHA Y HORA DE AUTORIZACIÓN</b>'
               )
            );

            $this->add_table_row(
               array(
                   'campo' => $facturaprov['fecha_autorizacion_sri'] . ' : ' .$facturaprov['hora_autorizacion_sri']
               )
            );            

            // CONDICIONAMOS SI EL AMBIENTE ES EN DESARROLLO O PRODUCCION
            if ($var->FS_AMBIENTE_XML==1) {
               $ambiente = 'DESARROLLO';
            }
            if ($var->FS_AMBIENTE_XML==2) {
               $ambiente = 'PRODUCCIÓN';
            }


            $this->add_table_row(
               array(
                   'campo' => "\n".'<b>AMBIENTE: </b>'.$ambiente
               )
            );

            $this->add_table_row(
               array(
                   'campo' => "\n".'<b>EMISIÓN:</b> NORMAL'
               )
            );

            $this->add_table_row(
               array(
                   'campo' => '<b>CLAVE DE ACCESO</b>'
               )
            );

            $y = $this->get_y()-10;
            $this->set_y(620);

            $this->pdf->addJpegFromFile('../images/codigos_de_barras.jpg',300,580,240);

            $this->add_table_row(
               array(
                   'campo' =>  "\n\n\n\n\n" .'             '.$facturaprov['codigo_acceso_sri']
               )
            );

            $this->set_y($y);



            $this->save_table(
               array(
                  'cols' => array(
                     'campo' => array('justification' => 'left'),
                  ),
                  'showLines' => 1,
                  'xPos'=>'right',
                  'xOrientation'=>'left',
                  'showHeadings' => 0,
                  'width' => 290,
                  'shaded' => 0,
                  'fontSize' =>8
               )
            );

            $this->pdf->ez['leftMargin'] = 25;
            $this->pdf->ez['rightMargin'] = 25;
            $this->set_y(718);
              
   }


      // FACTURA ELECTRONICA NOTA DE DEBITO

      public function generar_pdf_cabecera_nota_debito_elec($empresa, $lppag, $documento)
   {

      // Volvemos a consultar los datos de la factura para rescatar la información proporcionada por el SRI  
      $var = new variables_globales();

      //Realizo la conexion a la base de datos
      $conexion = mysqli_connect($var->FS_DB_HOST, $var->FS_DB_USER, $var->FS_DB_PASS, $var->FS_DB_NAME);

      $consulta = $conexion->query("SELECT * FROM facturascli WHERE idfactura = '".$documento['idfactura']."';");
      $facturacli = mysqli_fetch_array($consulta);

      $consulta = $conexion->query("SELECT * FROM co_factura
              WHERE estado = 'AUTORIZADO' AND tipo_doc = '5' AND doc_instancias_id= '".$documento['idnotadebito']."';");
      $co_factura = mysqli_fetch_array($consulta);
      /// ¿Añadimos el logo?
      if($this->logo)
      {
         if( function_exists('imagecreatefromstring') )
         {
            $lppag -= 2; /// si metemos el logo, caben menos líneas
            
            $tamanyo = $this->calcular_tamanyo_logo();
            if( substr( strtolower($this->logo), -4 ) == '.png' )
            {
               $this->pdf->addPngFromFile($this->logo, 35, 740, $tamanyo[0], $tamanyo[1]);
            }
            else
            {
               $this->pdf->addJpegFromFile($this->logo, 35, 740, $tamanyo[0], $tamanyo[1]);
            }

          }else{
            die('ERROR: no se encuentra la función imagecreatefromstring(). '
                    . 'Y por tanto no se puede usar el logotipo en los documentos.');
         }
      }else{
        $this->pdf->addJpegFromFile('images/no_logo.jpg', 35, 680, 200, 130);
      }
            $this->new_table();
            $this->add_table_row(
               array(
                   'campo' => "RUC: ".$empresa['cifnif']
               )
            );
            $this->add_table_row(
               array(
                   'campo' => '<b>NOTA DE DEBITO</b>'
               )
            );
            $this->add_table_row(
               array(
                  //EL NUMERO ES EL MISMO QUE LA FACTURA
                   'campo' => '<b>Nro. '.$co_factura['numero_documento'].'</b>'
               )
            );
            $this->add_table_row(
               array(
                   'campo' => "\n".'<b>NÚMERO DE AUTORIZACIÓN:</b>  '
               )
            );

            $this->add_table_row(
               array(
                   'campo' => $co_factura['autoriza_numero']
               )
            );

            $this->add_table_row(
               array(
                   'campo' => "\n".'<b>FECHA Y HORA DE AUTORIZACIÓN</b>'
               )
            );

            $this->add_table_row(
               array(
                   'campo' => $co_factura['fecha_actualizacion'] . ' : ' .$co_factura['hora_actualizacion']
               )
            );            

            // CONDICIONAMOS SI EL AMBIENTE ES EN DESARROLLO O PRODUCCION
            if ($var->FS_AMBIENTE_XML==1) {
               $ambiente = 'DESARROLLO';
            }
            if ($var->FS_AMBIENTE_XML==2) {
               $ambiente = 'PRODUCCIÓN';
            }


            $this->add_table_row(
               array(
                   'campo' => "\n".'<b>AMBIENTE: </b>'.$ambiente
               )
            );

            $this->add_table_row(
               array(
                   'campo' => "\n".'<b>EMISIÓN:</b> NORMAL'
               )
            );

            $this->add_table_row(
               array(
                   'campo' => '<b>CLAVE DE ACCESO</b>'
               )
            );

            $y = $this->get_y()-10;
            $this->set_y(620);

            $this->pdf->addJpegFromFile('../images/codigos_de_barras.jpg',300,580,240);

            $this->add_table_row(
               array(
                   'campo' =>  "\n\n\n\n\n" .'             '.$co_factura['codigo_acceso']
               )
            );

            $this->set_y($y);



            $this->save_table(
               array(
                  'cols' => array(
                     'campo' => array('justification' => 'left'),
                  ),
                  'showLines' => 1,
                  'xPos'=>'right',
                  'xOrientation'=>'left',
                  'showHeadings' => 0,
                  'width' => 290,
                  'shaded' => 0,
                  'fontSize' =>8
               )
            );

            $this->pdf->ez['leftMargin'] = 25;
            $this->pdf->ez['rightMargin'] = 25;
            $this->set_y(718);
              
   }


   // FACTURA ELECTRONICA RETENCIONES

      public function generar_pdf_cabecera_fac_elec_ret_pre(&$empresa, &$lppag, &$documento)
   {

      /// ¿Añadimos el logo?
      if($this->logo)
      {
         if( function_exists('imagecreatefromstring') )
         {
            $lppag -= 2; /// si metemos el logo, caben menos líneas
            
            $tamanyo = $this->calcular_tamanyo_logo();
            if( substr( strtolower($this->logo), -4 ) == '.png' )
            {
               $this->pdf->addPngFromFile($this->logo, 35, 740, $tamanyo[0], $tamanyo[1]);
            }
            else
            {
               $this->pdf->addJpegFromFile($this->logo, 35, 740, $tamanyo[0], $tamanyo[1]);
            }

          }else{
            die('ERROR: no se encuentra la función imagecreatefromstring(). '
                    . 'Y por tanto no se puede usar el logotipo en los documentos.');
         }
      }else{
        $this->pdf->addJpegFromFile('images/no_logo.jpg', 35, 680, 200, 130);
      }

            $this->pdf->ez['leftMargin'] = 25;
            $this->pdf->ez['rightMargin'] = 25;
            $this->set_y(718);
              
   }



      // FACTURA ELECTRONICA RETENCIONES

      public function generar_pdf_cabecera_comprobante_egreso(&$empresa, &$documento, &$num_egreso)
      {

         $this->pdf->ezText("<b>".$empresa->nombre."</b>", 12, array('justification' => 'center'));
         
         $direccion = $empresa->direccion;
         if($empresa->apartado)
         {
            $direccion .= ' - ' . ucfirst(FS_APARTADO) . ': ' . $empresa->apartado;
         }
         
         if($empresa->codpostal)
         {
            $direccion .= ' - CP: ' . $empresa->codpostal;
         }
         
         if($empresa->ciudad)
         {
            $direccion .= ' - ' . $empresa->ciudad;
         }
         
         if($empresa->provincia)
         {
            $direccion .= ' (' . $empresa->provincia . ')';
         }
         
         if($empresa->telefono)
         {
            $direccion .= ' - Teléfono: ' . $empresa->telefono;
         }
         
         $this->pdf->ezText($this->fix_html($direccion), 9, array('justification' => 'center'));
         $this->pdf->ezText(FS_CIFNIF.": ".$empresa->cifnif, 9, array('justification' => 'center'));
         $this->pdf->ezText('Comprobante de Egreso N°: '. $num_egreso , 9, array('justification' => 'center'));

            $this->pdf->ez['leftMargin'] = 25;
            $this->pdf->ez['rightMargin'] = 25;
            $this->set_y(750);
              
      }


      public function generar_pdf_cabecera_conciliacion(&$empresa, &$documento, &$num_egreso)
      {

         $this->pdf->ezText("<b>".$empresa->nombre."</b>", 12, array('justification' => 'center'));
         $this->pdf->ezText("<b>Conciliacion N°:</b> ".$num_egreso, 12, array('justification' => 'center'));
         $this->new_table();

         $tarjetas = explode(",", $documento->codtarjeta);
         $descrip_tarjeta =  [];
         foreach ($tarjetas as $tarjeta) {
            $tarjetas_cre_clean = substr($tarjeta, 1,-1);
            $descrip_tarjeta[] = $this->forma_pago->get($tarjetas_cre_clean)->descripcion;
         }
         $descrip_tarjeta = implode(",", $descrip_tarjeta);

         $this->add_table_row(
              array(
                  'campo1' => "<b>Tarjetas:</b>",
                  'dato1' => $descrip_tarjeta,
                  'campo2' => "",
                  'dato2' => ""
              )
         );
         //exit();
         $this->add_table_row(
              array(
                  'campo1' => "<b>Fecha Inicio:</b>",
                  'dato1' => $documento->fechaini,
                  'campo2' => "<b>Fecha Fin:</b>",
                  'dato2' => $documento->fechafin
              )
         );

         $this->add_table_row(   
              array(
                  'campo1' => "<b>Ret.Iva:</b>",
                  'dato1' =>  "$ ".$documento->valretiva,
                  'campo2' => "<b>Ret.Renta:</b>",
                  'dato2' =>  "$ ".$documento->valretrenta
              )
         );
         ///Buscamos el numero de la factura
         $facturaprov = new factura_proveedor();
         $facturaprov = $facturaprov->get($documento->idfacturaprov);
         $this->add_table_row(
              array(
                  'campo1' => "<b>Num.Ret:</b>",
                  'dato1' =>  $documento->estab."-".$documento->ptoem."-".$documento->secuencial,
                  'campo2' => "<b>Factura.Com.:</b>",
                  'dato2' =>  "$ ".$facturaprov->numproveedor
              )
         );

         $this->add_table_row(
              array(
                  'campo1' => "<b>Comisión:</b>",
                  'dato1' =>  "$ ".$documento->comision,
                  'campo2' => "<b>Valor en bancos.:</b>",
                  'dato2' =>  "$ ".$documento->total_banco
              )
         );

         $this->pdf->ezText("\n",10);
         $this->save_table(
              array(
                  'cols' => array(
                      'campo1' => array('justification' => 'right'),
                      'dato1' => array('justification' => 'left'),
                      'campo2' => array('justification' => 'right'),
                      'dato2' => array('justification' => 'left')
                  ),
                  'showLines' => 0,
                  'width' => 550,
                  'shaded' => 0,
                  'xPos'=>'right',
                  'xOrientation'=>'left',
                  'fontSize' =>8
              )
         );

         $this->pdf->ez['leftMargin'] = 25;
         $this->pdf->ez['rightMargin'] = 25;
         $this->set_y(670);
              
      }

      //Cabecera comprobante de ingreso

      public function generar_pdf_cabecera_comprobante_ingreso(&$empresa, &$documento, &$num_ingreso)
      {

         $this->pdf->ezText("<b>".$empresa->nombre."</b>", 12, array('justification' => 'center'));
         
         $direccion = $empresa->direccion;
         if($empresa->apartado)
         {
            $direccion .= ' - ' . ucfirst(FS_APARTADO) . ': ' . $empresa->apartado;
         }
         
         if($empresa->codpostal)
         {
            $direccion .= ' - CP: ' . $empresa->codpostal;
         }
         
         if($empresa->ciudad)
         {
            $direccion .= ' - ' . $empresa->ciudad;
         }
         
         if($empresa->provincia)
         {
            $direccion .= ' (' . $empresa->provincia . ')';
         }
         
         if($empresa->telefono)
         {
            $direccion .= ' - Teléfono: ' . $empresa->telefono;
         }
         
         $this->pdf->ezText($this->fix_html($direccion), 9, array('justification' => 'center'));
         $this->pdf->ezText(FS_CIFNIF.": ".$empresa->cifnif, 9, array('justification' => 'center'));
         $this->pdf->ezText('Comprobante de Ingreso N°: '. $num_ingreso , 9, array('justification' => 'center'));

         $this->pdf->ez['leftMargin'] = 25;
         $this->pdf->ez['rightMargin'] = 25;
         $this->set_y(750);
              
      }

}