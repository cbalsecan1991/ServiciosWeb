<?php

require_once 'lib/nusoap.php';
require_once 'variables_globales.php';

	date_default_timezone_set('America/Los_Angeles');

	//Instancio o objeto del servicio
	$servicio = new soap_server();
	//Instacion conexion a la base de datos
	
	//Name Espace
	$ns = "urn:lineasFacturawsdl";

	//configuramos el servicio 	
	//1er parametro Nombre del Webservice
	//2do parametro Name space
	$servicio->configureWSDL("lineasnotacredito",$ns);
	//almacena el espacio de nobre de destino
	$servicio->schemaTargetNamespace = $ns;

	//Metodo (register) para registrar el servicio 4 parametros
	//1er parametro Nombre de la Funcion que se va a Desarrollar
	//2do parametros de Entrada para la Funcion
	//3er parametro return de errores o de respuestas
	//4to Name space

	//funcion para guardar cabecera de facturascli
	$servicio->register("lineasnotaCred", 
		array(
			'idnotacredito' => 'xsd:integer',
			'referencia' => 'xsd:string',
			'cantidaddevolucion'=>'xsd:double',
			'descuentovalor'=>'xsd:double'),
			array('return' => 'xsd:string'),
		$ns);

	//creamos la funcion cabecerafac
	function lineasnotaCred($idnotacredito,$referencia,$cantidaddevolucion,$descuentovalor){
		//quito los espacios en blanco
		$idnotacredito = trim($idnotacredito);
		$referencia = trim($referencia);
		$cantidaddevolucion = trim($cantidaddevolucion);
		$descuentovalor = trim($descuentovalor);

		//Variables de trabajo
		$clave = TRUE;

		//array de Respuestas o Retorno
		$resultado = array();
		
		//Busco las variables Globales para realizar la conexion
		$var = new variables_globales();

		//Realizo la conexion a la base de datos
		$conexion = mysqli_connect($var->FS_DB_HOST, $var->FS_DB_USER, $var->FS_DB_PASS, $var->FS_DB_NAME);

		//Busco la existencia de la Factura
		$facturas = $conexion->query("SELECT * FROM facturascli WHERE idfactura = '".$idnotacredito."' AND codserie = 'N'");
		$arr_cred = mysqli_fetch_array($facturas);


		//Verifico si exite la nota de crédito.
		if(!empty($arr_cred))
		{
			$resultado['idnotacredito'] = "Nota de Credito encontrada correctamente";
			$consulta = $conexion->query("SELECT * FROM facturascli WHERE codigo = '".$arr_cred['codigorect']."';");
			$arr_fac = mysqli_fetch_array($consulta);
			//Verifico la factura referente a la nota de crédito
			if (!empty($arr_fac)) {
				$consulta = $conexion->query("SELECT * FROM lineasfacturascli WHERE idfactura = '".$arr_fac['idfactura']."';");
				$arr_lineafac = array();
				while ($l = mysqli_fetch_array($consulta)) {
					$arr_lineafac[] = $l;
				}
				// $arr_lineafac = mysqli_fetch_all($consulta,MYSQLI_ASSOC);
				//Verifico la existencia de la factura en lineasfacturas
				if(empty($arr_lineafac)){
					$clave = FALSE;
				}
			}else{
				$clave = FALSE;
			}

			//Verifico la existencia de la referencia
			if ($clave) {
				//Verifico la existencia de la referencia en las líneas y obtengo la cantidad
				foreach ($arr_lineafac as $l) {
					if ($referencia == $l['referencia']) {
						$resultado['referencia'] = "Articulo encontrado correctamente";
						$cantidad = $l['cantidad'];
						$pvpunitario = $l['pvpunitario'];
						$descripcion = $l['descripcion'];
						$pvptotal = $l['pvptotal']*(1.12);
						$codimpuesto = $l['codimpuesto'];
						$dtopor = $l['dtopor'];
						$iva = $l['iva'];
						$clave = TRUE;
						break;
					}
					$resultado['referencia'] = "No se encuentra el articulo para su devolucion ".$referencia;
					$clave = FALSE;
				}
			}

			//Verifico si es por cantidad o por valor
			if($clave){
				if ($cantidaddevolucion > 0) {
					//Verifico que la cantidad a devolver no sea mayor a la linea de la factura
					if ($cantidad < $cantidaddevolucion) {
						$resultado['cantidaddevolucion'] = "La cantidad ingresada es mayor a la del articulo en la Factura";
						$clave = FALSE;
					}else{
						//INGRESO LA LÍNEA DE NOTA DE CRÉDITO POR CANTIDAD
						$sql = "INSERT INTO lineasfacturascli (cantidad,codimpuesto,descripcion,idfactura,iva,pvpsindto,pvptotal,pvpunitario,dtopor,referencia) VALUES ('".$cantidaddevolucion."','".$codimpuesto."','".$descripcion."','".$idnotacredito."','".$iva."','".($pvpunitario*$cantidaddevolucion)."','".($pvpunitario*$cantidaddevolucion)."','".$pvpunitario."','".$dtopor."','".$referencia."')";
						// echo($sql);
						if ($conexion->query($sql)) {
							$resultado['linea'] = "Se ingreso la linea correctamente";
						}else{
							$resultado['linea'] = "No se pudo ingresar la linea";
						}
					}
				}elseif($cantidaddevolucion == 0){
					//Verifico que el valor a descontar no sea mayor a la lina de la factura
					if($descuentovalor > $pvptotal){
						$resultado['descuentovalor'] = "El valor total ingresado no puede ser mayor a la del articulo en la Factura";
						$clave = FALSE;
					}elseif($descuentovalor <=0){
						$resultado['descuentovalor'] = "El valor total ingresado no puede ser negativo o cero";
						$clave = FALSE;
					}else{
						//Verifico si existe el articulo de referencia, sino lo creo
						$art_referencia = "artnotacredito";
						$art_descripcion = "Descuento por valor de nota de crédito";
						$consulta = $conexion->query("SELECT * FROM articulos WHERE referencia = '".$referencia."';");
						$arr_art = mysqli_fetch_array($consulta);

						if (empty($arr_art)) {
							$sql = "INSERT INTO articulos (factualizado, referencia,descripcion,codimpuesto, sevende,secompra,nostock) VALUES ('".date('Y-m-d')."','".$art_referencia."', '".$art_descripcion."', '".$codimpuesto."', '1','1','0');";
							if ($conexion->query($sql)) {
								$clave = TRUE;
							}else{
								$clave = FALSE;
							}
						}
						//Valido que no haya errores anteriores para ingresar la linea
						if ($clave) {
							$descuentovalor = str_replace(',', '.', $descuentovalor);
							$pvp = round(($descuentovalor)/(($iva/100)+1),2);
							$sql = "INSERT INTO lineasfacturascli (cantidad,codimpuesto,descripcion,idfactura,iva,pvpsindto,pvptotal,pvpunitario,referencia) VALUES ('1','".$codimpuesto."','".$art_descripcion."','".$idnotacredito."','".$iva."','".$pvp."','".$pvp."','".$pvp."','".$referencia."')";
							// echo($sql);
							if ($conexion->query($sql)) {
								$resultado['linea'] = "Se ingreso la linea correctamente";
							}else{
								$resultado['linea'] = "No se pudo ingresar la linea";
							}
						}
					}
				}else{
					$resultado['cantidaddevolucion'] = "La cantidad ingresada no puede ser negativa";
					$clave = FALSE;
				}
			}
		}else{			
			$resultado['idnotacredito'] = "No se encuentra la Nota de Credito ".$idnotacredito;
		}
		
		return json_encode($resultado);
	}


	$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
	$servicio->service(file_get_contents("php://input"));
	
?>