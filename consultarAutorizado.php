<?php

require_once 'lib/nusoap.php';
require_once 'variables_globales.php';
require_once 'FirmaFactura/xml_factura.php';


	date_default_timezone_set('America/Bogota');
	
	//Instancio o objeto del servicio
	$servicio = new soap_server();
	//Instacion conexion a la base de datos
	
	//Name Espace
	$ns = "urn:consultarFacturawsdl";

	//configuramos el servicio 	
	//1er parametro Nombre del Webservice
	//2do parametro Name space
	$servicio->configureWSDL("consultarfactura",$ns);
	//almacena el espacio de nobre de destino
	$servicio->schemaTargetNamespace = $ns;

	//Metodo (register) para registrar el servicio 4 parametros
	//1er parametro Nombre de la Funcion que se va a Desarrollar
	//2do parametros de Entrada para la Funcion
	//3er parametro return de errores o de respuestas
	//4to Name space

	//funcion para guardar cabecera de facturascli
	$servicio->register("consultarFac", 
		array(
			'idfactura' => 'xsd:integer'),
			array('return' => 'xsd:string'),
		$ns);

	//creamos la funcion cabecerafac
	function consultarFac($idfactura)
	{
		//array de Respuestas o Retorno
		$resultado = array();

		//Busco las variables Globales para realizar la conexion
		$var = new variables_globales();

		$xml_factura = new xml_factura();

		//Realizo la conexion a la base de datos
		$conexion = mysqli_connect($var->FS_DB_HOST, $var->FS_DB_USER, $var->FS_DB_PASS, $var->FS_DB_NAME);
		//Busco la existencia de la Factura
		$facturas = $conexion->query("SELECT * FROM facturascli WHERE idfactura = ".$idfactura." AND estado_sri = 'AUTORIZADO'");
		
		$arr_fac = array();

		//transformo los datos de la consulta a un array
		while ($factura = mysqli_fetch_array($facturas))
		{
			$arr_fac[] = $factura;
		}
		
		//Verifico si exite la factura.
		if(empty($arr_fac))
		{
			$resultado['factura'] = "Factura encontrada correctamente";
			//busco en las co_facturas si se encuentra aprobadas
			$co_facturas = $conexion->query("SELECT * FROM co_factura WHERE doc_instancias_id = '".$idfactura."'AND tipo_doc = '17' AND estado = 'AUTORIZADO';");

			$arr_facs = array();

			while ($facs = mysqli_fetch_array($co_facturas))
			{
				$arr_facs[] = $facs;
			}				
			

			if(empty($arr_facs))
			{				
				/*$resultado['estado'] = "La Factura se encuentra pendiente de autorizar";
				//genero los parametros para enviar el mail
				$to = 'jonathang@kapitalcompany.com';
				$subject = 'Factura Pendiete de Autorizar';
				$mensaje = 'La factura con id: '.$idfactura.' esta pendiente de Autorizar';
				$headers = "From: sistemakapitalcompany@gmail.com" . "\r\n" . "CC: gabriel.rodriguez@kapitalcompany.com";
				if(!mail($to,$subject,$mensaje,$headers))
				{
					$resultado['mail'] = "No se pudo enviar el email";
				}*/

				//Busco la existencia de la Factura
				$facturas = $conexion->query("SELECT * FROM facturascli WHERE idfactura = ".$idfactura);
				
				$arr_fac = array();

				//transformo los datos de la consulta a un array
				while ($factura = mysqli_fetch_array($facturas))
				{
					$arr_fac[] = $factura;
				}

				//Verifico si exite la factura.
				if(!empty($arr_fac))
				{
					$resultado = $xml_factura->generar_xml_factura($arr_fac);

					//Busco la existencia de la Factura
					$facturas = $conexion->query("SELECT * FROM facturascli WHERE idfactura = ".$idfactura." AND estado_sri = 'AUTORIZADO'");
					
					$arr_fac_a = array();

					//transformo los datos de la consulta a un array
					while ($factura = mysqli_fetch_array($facturas))
					{
						$arr_fac_a[] = $factura;
					}

					if(!empty($arr_fac_a))
					{
						$resultado['factura_autorizacion'] = "La Factura Autorizada correctamente";
						$resultado['autoriza_sri'] = $arr_fac_a[0][78];
						$resultado['mensaje_sri'] = $arr_fac_a[0][79];
						$resultado['estado_sri'] = $arr_fac_a[0][80];
						$resultado['fecha_autorizacion'] = $arr_fac_a[0][83];
						$resultado['hora_autorizacion'] = $arr_fac_a[0][84];
						$resultado['codigo_acceso_sri'] = $arr_fac_a[0][81];
						$resultado['documento'] = $arr_fac_a[0][77];
						$resultado['tipo_documento'] = $arr_fac_a[0][76];

					}else{

						$resultado['error_autorizacion'] = "Error al Autorizar la Factura";

					}
				}else{
					$resultado['factura'] = "No se encuentra la Factura ".$idfactura;
				}

			}else{
				$resultado['factura'] = "La Factura se encuentra Autorizada";
				$resultado['autoriza_sri'] = $arr_facs[0]['autoriza_numero'];
				$resultado['mensaje_sri'] = $arr_facs[0]['mensaje'];
				$resultado['estado_sri'] = $arr_facs[0]['estado'];
				$resultado['fecha_autorizacion'] = $arr_facs[0]['fecha_actualizacion'];
				$resultado['hora_autorizacion'] = $arr_facs[0]['hora_actualizacion'];
				$resultado['codigo_acceso_sri'] = $arr_facs[0]['codigo_acceso'];
				$resultado['documento'] = $arr_facs[0]['numero_documento'];
				$resultado['tipo_documento'] = $arr_facs[0]['tipo_doc'];
			}

		}else{			
			$resultado['factura'] = "La Factura ya se Encuentra Autorizada";
			$resultado['autoriza_sri'] = $arr_fac[0][78];
			$resultado['mensaje_sri'] = $arr_fac[0][79];
			$resultado['estado_sri'] = $arr_fac[0][80];
			$resultado['fecha_autorizacion'] = $arr_fac[0][83];
			$resultado['hora_autorizacion'] = $arr_fac[0][84];
			$resultado['codigo_acceso_sri'] = $arr_fac[0][81];
			$resultado['documento'] = $arr_fac[0][77];
			$resultado['tipo_documento'] = $arr_fac[0][76];
		}
		
		return json_encode($resultado);
	}


	$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
	$servicio->service(file_get_contents("php://input"));
	
?>