<?php
require_once 'lib/nusoap.php';
require_once 'variables_globales.php';
require_once '../plugins/facturacion_base/extras/Utilities/Utilities.php';
require_once 'conectarServidorFacturacionE.php';

class xml_nota_credito
{
  public $messages;
  public $errors;
  public $numeroDeAutorizacionyClave;


  private $articulo_propiedad;
  private $articulo_propiedad_vacio;
  private $cuenta_banco;
  private $divisa;
  private $ejercicio;
  private $empresa;
  private $forma_pago;
  private $impuestos;

  public $iva;
  public $cliente0;

   /**
    * Genera el xml para una factura de venta.
    * Devuelve TRUE si el xml se ha generado correctamente, False en caso contrario.
    * @param factura_cliente $factura
    */
   public function generar_xml_nota_credito($factura)
   {

      /*print_r($factura);
      exit();*/
      date_default_timezone_set('America/Bogota');
      $Utilities = new \Utilities();
      $ok = FALSE;
      $clave = TRUE;

      $lineas = array();
      $lineas = array();
      $detalle = array();
      $detalle2 = array();
      $arrayAux = array();
      $arrayDetalle = array();
      $dato = array();
      $cont = 0;
      $total = 0;
      $descuento = 0;
      $precioTotalSinImpuesto = 0;
      $precioTotalSinImpuestoDetalle = 0;
      $labeseimponible = 0;
      $elivatotal = 0;
      $sinivas = 0;
      $armaTotalImpuesto = "";
      $losErrores = "";
      $valor = array();

      //Variables adaptado
      $resultado = array();
      //Busco las variables Globales para realizar la conexion
      $var = new variables_globales();
      //Realizo la conexion a la base de datos
      $conexion = mysqli_connect($var->FS_DB_HOST, $var->FS_DB_USER, $var->FS_DB_PASS, $var->FS_DB_NAME);

      $monto = 0.00;
      $pvpsindto = 0.00;
      $pvptotal = 0.00;
      $valorDescuento = 0.00;
      $valorIva = 0.00;
      $baseImponible = 0.00;
      $codigoPrincipal = "001";
      $codigoImpuesto = '';
      $importeTotal=0.00;
      $detalle1=array();





      $consulta =$conexion->query("SELECT * FROM lineasfacturascli WHERE idfactura = '".$factura['idfactura']."' ORDER BY orden DESC, idlinea ASC;");
      $detalleFactura = array();
      while ($l = mysqli_fetch_array($consulta)) {
        $detalleFactura[] = $l;
      }
      // $detalleFactura = mysqli_fetch_all($consulta,MYSQLI_ASSOC);

      //print_r($detalleFactura);
      foreach ($detalleFactura as $value){
            //Asingnamos el codigo del impuesto en base a la tabla del SRI
            /*++++++++++++++++++++++++++++++++++++++*/
            /* Impuesto               | Código       */
            /*------------------------|--------------*/
            /*  IVA                   | 2            */
            /*  ICE                   | 3            */
            /*  IRBPNR                | 5            */
            /*+++++++++++++++++++++++++++++++++++++++*/
            $codigoImpuesto = 2;
            //Asingnamos el codigoPorcentaje en base a la tabla del SRI
            /*++++++++++++++++++++++++++++++++++++++*/
            /* Porcentaje de IVA      | Código       */
            /*------------------------|--------------*/
            /*  0%                    | 0            */
            /*  12%                   | 2            */
            /*  14%                   | 3            */
            /*  No Objeto de Impuesto | 6            */
            /*  Exento de IVA         | 7            */
            /*+++++++++++++++++++++++++++++++++++++++*/
            switch ($value['iva']) {
              case 0:
                $codigoPorcentaje = 0;
                break;
              case 12:
                $codigoPorcentaje = 2;
                break;
              case 14:
                $codigoPorcentaje = 3;
                break; 
            }
            if (!isset($codigoPorcentaje)) {
              $resultado['iva_error'] = 'El porcentaje del (IVA) del producto [' . $value['descripcion'] . '] se encuentra fuera del marco establecido en la ley del Ecuador';
              $codigoPorcentaje='';
            }
            // Acumulamos el precio total con iva
            if ($value['iva']==12) {
              $baseImponible +=  $value['pvptotal'];
            }
            // Acumulamos el precio total sin iva
            //if ($value->iva==0) {

              $precioTotalSinImpuesto += $value['pvptotal'];
              //$precioTotalSinImpuestoDetalle = $value->pvptotal;
            //}
            
            $descuento += (round($value['pvpsindto'],2) - $value['pvptotal']);

            $valorDescuento = ( round($value['pvpsindto'],2) - $value['pvptotal']);


            $valorIva += ($value['pvptotal'] * $value['iva']) / 100;

            //$labeseimponible = ( $labeseimponible + $value->pvptotal ) - $valorDescuento;
            $importeTotal = ($precioTotalSinImpuesto + $valorIva);

            $elivatotal = $elivatotal + $value['iva'] / 100 * ($value['pvptotal'] - $valorDescuento);
            //$precioTotalSinImpuesto = doubleval( $monto - $valorDescuento );
            $sustituye = array("(\r\n)", "(\n\r)", "(\n)", "(\r)");
            $descripcion = preg_replace($sustituye, "", $value['descripcion']);

                $detalle['detalle_'.$cont] = Array(
                        "codigoInterno" => $codigoPrincipal,
                        "descripcion" => trim($descripcion),
                        "cantidad" => $value['cantidad'],
                        "precioUnitario" => str_replace(",", "", number_format($value['pvpunitario'], 2)),
                        "descuento" => (isset($valorDescuento)) ? str_replace(",", "",number_format(doubleval($valorDescuento), 2)) : 0.00,
                        "precioTotalSinImpuesto" => str_replace(",", "", number_format(doubleval( ($value['pvpunitario'] * $value['cantidad']) - $valorDescuento), 2)),
                        "impuestos" => Array(
                            "impuesto" => Array(
                                "codigo" => $codigoImpuesto,
                                "codigoPorcentaje" => $codigoPorcentaje,
                                "tarifa" => str_replace(",", "", number_format(doubleval($value['iva']), 2)),
                                "baseImponible" => str_replace(",", "", number_format(doubleval( ($value['pvpunitario'] * $value['cantidad']) - $valorDescuento ), 2)),
                                "valor" => str_replace(",", "", number_format(doubleval($value['pvptotal'] + (($value['pvptotal']*$value['iva'])/100 )), 2))

                            )
                        )
                    );

            $cont++;
      }

      $servidorFacturacionE = $var->FS_AMBIENTE_XML;
      if ( $servidorFacturacionE == 1){
         $servidorFacturacionE = "Desarrollo";
      }elseif ( $servidorFacturacionE == 2) {
          $servidorFacturacionE = "Produccion";
      }
      switch ($servidorFacturacionE) {
            case "Desarrollo":
                $servidor = "Desarrollo";
                $ambiente = "01";
                $tambiente = "1";
                break;
            case "Pruebas":
                $servidor = "Pruebas";
                $ambiente = "01";
                $tambiente = "1";
                break;
            case "Produccion":
                $servidor = "Produccion";
                $ambiente = "02";
                $tambiente = "2";
                break;
        }
      // obtenemos datos de la empresa
      $consulta = $conexion->query("SELECT * FROM empresa");
      $empresa0 = mysqli_fetch_array($consulta);
      if (!empty($empresa0)) {
        $direccionEmpresa = $empresa0['direccion'];
        $nombreEmpresa = $empresa0['nombre'];
        $rucEmpresa = $empresa0['cifnif'];
      }else{
        $clave = FALSE;
      }

      //Obtenemos datos del cliente
      $consulta = $conexion->query("SELECT * FROM clientes WHERE codcliente = '".$factura['codcliente']."';");
      $cliente = mysqli_fetch_array($consulta);
      $razonSocialCliente = $cliente['razonsocial'];
      $razonSocialComprador = $razonSocialCliente;
      $cedulaCliente = $cliente['cifnif'];



      //$tipoidfiscal = $cliente->tipoidfiscal;
      //$this->new_msg_error($cedulaCliente);
      if(strlen($cedulaCliente) == 10){
          $tipoIdentificacion = '05';
      }  elseif (strlen($cedulaCliente) == 13){
          $tipoIdentificacion = '04';
      }  else {
          $tipoIdentificacion = '06';
      }

      
      // Validamos telefono del cliente
      if (empty($cliente['telefono1'])) {
        $resultado['error_telefono'] = "El cliente no registra telefono!";
        $telefonoComprador = "0000000000";
      }else{
        $telefonoComprador = $cliente['telefono1'];
      }
      // Validamos el ruc de la empresa.
      $Utilities = new \Utilities();
      $cedRucValido = $Utilities->validarID($rucEmpresa);


      if (!$cedRucValido) {
        $resultado['ruc_empresa'] = "El Ruc de la empresa no es válido";
        $clave = FALSE;
      }
      /// obtenemos direccion del cliente
      $consulta = $conexion->query("SELECT * FROM dirclientes WHERE codcliente = '".$factura['codcliente']."' ORDER BY id DESC;");
      $datosCliente = mysqli_fetch_array($consulta);
      if (!empty($datosCliente)) {
        $direccionComprador = $datosCliente['direccion'];
      }else{
        $resultado['error_direccion'] = "El cliente no registra direccion";
      }
      if (empty($cliente['email'])) {
        $resultado['error_email'] = "El cliente no cuenta con correo electronico. No se puede enviar la nota de crédito al SRI";
        $email = 'ninguno';
      }else{
        $email = $cliente['email'];
      }

      $lainfoadicional = ''
                . '<campoAdicional nombre="Direccion">' . $direccionComprador . '</campoAdicional>'
                . '<campoAdicional nombre="Telefono">' . $telefonoComprador . '</campoAdicional>'
                . '<campoAdicional nombre="Email">' . $email . '</campoAdicional>';

      $fechaEmision = date('d/m/Y', strtotime($factura['fecha']) );
      $codDoc = "04";
      $codalmacenEmpresa = $factura['codalmacen'];


      $consulta = $conexion->query("SELECT * FROM almacenes WHERE codalmacen = '".$factura['codalmacen']."' ORDER BY codalmacen ASC;");
      $almacen = mysqli_fetch_array($consulta);
      $estab = $almacen['codestablecimiento'];
      $ptoEmi = $almacen['puntoemision'];
      $secuencial = str_pad($factura['numero'],9,"0",STR_PAD_LEFT);
      $numero = $estab.'-'.$ptoEmi.'-'.$secuencial;
      // $comprobante = array();
      // list($estab, $ptoEmi, $secuencial) = explode("-", $numero);

      $numeroDeAutorizacionyClave = $this->numeroDeAutorizacionyClave($factura, $rucEmpresa, $tambiente);

      //if ($baseImponible > 0) {
                $armaTotalImpuesto.=''
                        . '<totalImpuesto>
                                    <codigo>' . $codigoImpuesto . '</codigo>
                                    <codigoPorcentaje>2</codigoPorcentaje>                                    
                                    <baseImponible>' .str_replace(",", "", number_format(doubleval($baseImponible), 2))  . '</baseImponible>
                                    <valor>' . str_replace(",", "", number_format(doubleval($valorIva), 2))  . '</valor>
                                </totalImpuesto>';
        //    }

      if($var->FS_OB_CONTA == 1){
        $obligadoContabilidad =  'NO';
      }else{
        $obligadoContabilidad =  'SI';
      }
      $consulta = $conexion->query("SELECT * FROM facturascli WHERE idfactura = '".$factura['idfactura']."';");
      $facturaorigen = mysqli_fetch_array($consulta);

      $consulta = $conexion->query("SELECT * FROM almacenes WHERE codalmacen = '".$facturaorigen['codalmacen']."' ORDER BY codalmacen ASC;");
      $almacen = mysqli_fetch_array($consulta);
      $estab_ori = $almacen['codestablecimiento'];
      $ptoEmi_ori = $almacen['puntoemision'];
      $secuencial_ori = str_pad($facturaorigen['numero'],9,"0",STR_PAD_LEFT);
      $numero_fac_ori = $estab_ori.'-'.$ptoEmi_ori.'-'.$secuencial_ori;
      $facturaorigenFechaEmision = date('d/m/Y', strtotime($facturaorigen['fecha']) );

      if (empty($facturaorigenFechaEmision)) {
        $resultado['error_fecha'] = "La factura a la que esta modificando no cuenta con fecha de emisión";
        $clave = FALSE;
      }
      if(empty($factura['observaciones'])){
        $motivo = 'Ninguno';
      }else{
        $motivo = $factura['observaciones'];
      }
      $facturaArray = Array(
            "infoTributaria" => Array
                (
                "ambiente" => $tambiente,
                "tipoEmision" => 1,
                "razonSocial" => $nombreEmpresa,
                "nombreComercial" => $nombreEmpresa,
                "ruc" => $rucEmpresa,
                "claveAcceso" => $numeroDeAutorizacionyClave,
                "codDoc" => $codDoc,
                "estab" => $estab,
                "ptoEmi" => $ptoEmi,
                "secuencial" => $secuencial,
                "dirMatriz" => $direccionEmpresa
            ),
            "infoNotaCredito" => Array
                (
                "fechaEmision" => $fechaEmision,
                "dirEstablecimiento" => $direccionEmpresa,
                "tipoIdentificacionComprador" => $tipoIdentificacion,
                "razonSocialComprador" => $razonSocialComprador,
                "identificacionComprador" => $cedulaCliente,
                "contribuyenteEspecial" => $var->FS_CONTRIBUYENTE_NRO,
                "obligadoContabilidad" => $obligadoContabilidad,
                "codDocModificado" => "01",
                "numDocModificado" => $numero_fac_ori,
                "fechaEmisionDocSustento" => $facturaorigenFechaEmision,
                "totalSinImpuestos" => str_replace(",", "", number_format(doubleval($precioTotalSinImpuesto), 2)),
                "valorModificacion" => str_replace(",", "", number_format(doubleval( $importeTotal ), 2)),
                "moneda" => "DOLAR",
                "totalConImpuestos" => $armaTotalImpuesto,
                "motivo" => $motivo,
                ),
            "detalles" => $detalle,
            "infoAdicional" => $lainfoadicional,
        );
      $fe = new \conectarServidorFacturacionE();
      if (isset($rucEmpresa) && $rucEmpresa != "") {
            $resp = array();

            if (strlen($losErrores) < 1) {
                $respuesta = $fe->envioFactura($factura, $servidor, $facturaArray, $rucEmpresa, $factura['idfactura'], $email, $razonSocialComprador, $numero, $numeroDeAutorizacionyClave, $factura['codcliente'], "NOTA-DE-CREDITO");
                if (!is_numeric($respuesta))
                    $resultado += $respuesta;
            }else {
                //$this->new_message("$losErrores"."</br>");
              // return $losErrores;
            }

        } else {
            $resultado['empresa'] = "No se encuentra configurada la empresa";
        }
        return $resultado;
   }
   


   /**
    * Genera numero de autorizacion y clave.
    * @param $factura|array()
    * @param $rucEmpresa|string
    * @param $tambiente|bolean
    * Devuelve $numeroDeAutorizacionyClave.
    */
   public function numeroDeAutorizacionyClave($factura, $rucEmpresa, $tambiente)
   {
      //Busco las variables Globales para realizar la conexion
      $var = new variables_globales();
      //Realizo la conexion a la base de datos
      $conexion = mysqli_connect($var->FS_DB_HOST, $var->FS_DB_USER, $var->FS_DB_PASS, $var->FS_DB_NAME);
      $codNumerico = $this->generaAutorizacion();
      $fechaEmision = date('d/m/Y', strtotime($factura['fecha']) );
      $codDoc = "04";
      $codalmacenEmpresa = $factura['codalmacen'];

      $consulta = $conexion->query("SELECT * FROM almacenes WHERE codalmacen = '".$factura['codalmacen']."' ORDER BY codalmacen ASC;");
      $almacen = mysqli_fetch_array($consulta);
      $estab = $almacen['codestablecimiento'];
      $ptoEmi = $almacen['puntoemision'];
      $secuencial = str_pad($factura['numero'],9,"0",STR_PAD_LEFT);
      $numero = $estab.'-'.$ptoEmi.'-'.$secuencial;

      $numeroDeAutorizacionyClave = $fechaEmision . $codDoc . $rucEmpresa . $tambiente . $estab . $ptoEmi . $secuencial . $codNumerico["0"] . "1"; 
      $numeroDeAutorizacionyClave = str_replace("/", "", $numeroDeAutorizacionyClave);
      $cadenaInvertida = $this->invertirCadena($numeroDeAutorizacionyClave);
      $eldigitoverificador = $this->obtenerSumaPorDigitos($cadenaInvertida);
      if ($eldigitoverificador == 11)
            $eldigitoverificador = 0;
      if ($eldigitoverificador == 10)
            $eldigitoverificador = 1;
      $numeroDeAutorizacionyClave = $numeroDeAutorizacionyClave . $eldigitoverificador;
      return $numeroDeAutorizacionyClave;
    }
   
   /**
    * Covierte un porcentaje de descuento en valor.
    * @param $porcentaje|float
    * @param $cantodad|float
    * Devuelve $total.
    */

  function generaAutorizacion() {
    $multiplica = array(3, 2, 7, 6, 5, 4, 3, 2);
    $laSuma = 0;
    $final = "";
    $codNumerico = "";
    for ($i = 0; $i < 8; $i++) {
        $numero = rand(1, 9);
        $codNumerico = $codNumerico . $numero;
        $laSuma = $laSuma + ($numero * $multiplica[$i]);
    }
    $result = fmod($laSuma, 11);
    $final = 11 - $result;
    $resultado = array();
    if ($final >= 10)
        $resultado = $this->generaAutorizacion();
    else
        $resultado = array($codNumerico, $final);
    return $resultado;
  }

  function obtenerSumaPorDigitos($cadena) {
      $pivote = 2;
      $longitudCadena = strlen($cadena);
      $cantidadTotal = 0;
      $b = 1;
      for ($i = 0; $i < $longitudCadena; $i++) {
          if ($pivote == 8) {
              $pivote = 2;
          }
          $temporal = intval(substr($cadena, $i, 1));
          $b++;
          $temporal = $temporal * $pivote;
          $pivote++;
          $cantidadTotal = $cantidadTotal + $temporal;
      }
      $cantidadTotal = 11 - $cantidadTotal % 11;
      return $cantidadTotal;
  }

  function invertirCadena($cadena) {
    $cadenaInvertida = "";
    for ($x = strlen($cadena) - 1; $x >= 0; $x--) {
        $cadenaInvertida = $cadenaInvertida . substr($cadena, $x, 1);
    }
    return $cadenaInvertida;
  }
   public function porcentajeAValores($porcentaje=0, $cantidad=0)
   {
       $porcentaje=$porcentaje;     
       $cantidad1=$cantidad;    
       $resultado=$porcentaje*$cantidad1/100; 
       $total = round(($cantidad1-$resultado) *100)/100;
       return $total;
   }
   
}
