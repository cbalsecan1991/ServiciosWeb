<?php

require_once '../model/SoapClientFs.php';
require_once '../model/ObjectToXMLFs.php';
require_once 'genera_pdf_email_electronico.php';
date_default_timezone_set('America/Bogota');

class conectarServidorFacturacionE
{
  function __construct()
  {
  }

  public function convert_array_to_xml($array, $namespace = '') {
    $xml = '';
    $cont=0;
    $newkey='';
    foreach($array as $key => $value) {

      if(is_array($value)) {
          $value = $this->convert_array_to_xml($value);
      }
          // Evaluamos si la key tiene un secuencial de ser asi lo limpiamos.
      $evaluar_key = substr($key, 0,8);
      $evaluar_key2 = substr($key, 0,9);
      if ($evaluar_key=='detalle_') {
          $key = substr($evaluar_key, 0, 7);
          $xml .= "<$key" . rtrim(" $namespace") . ">$value</$key>";
      }
      elseif ($evaluar_key2=='impuesto_') {
          $key = substr($evaluar_key, 0, 8);
          $xml .= "<$key" . rtrim(" $namespace") . ">$value</$key>";
      }
      else

         $xml .= "<$key" . rtrim(" $namespace") . ">$value</$key>";

 }

 return $xml;
}

	function envioFactura($factura, $servidor, $facturaArray, $rucEmpresa, $idDoc, $correoDestino, $elCliente, $numero, $numeroDeAutorizacionyClave, $actor, $tipoDocumento = "FACTURA"){
    $resultados = array();

    //Busco las variables Globales para realizar la conexion
    $var = new variables_globales();

    //Realizo la conexion a la base de datos
    $conexion = mysqli_connect($var->FS_DB_HOST, $var->FS_DB_USER, $var->FS_DB_PASS, $var->FS_DB_NAME);

		$claveDigital = $var->claveDigital;
		
		if (empty($claveDigital)) {
		  $resultados['clave_digital'] = "No cuenta con una clave digital para emitir comprobantes electrónicos";
		}
		$theXML = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>';
		$theXML.= '<formatoXML>';
		$theXML.= '<ruc>' . $rucEmpresa . '</ruc>';
		$theXML.= '<password>' . $claveDigital . '</password>';

	    // Creamos arbol de directorios si no existen
		$carpeta_enviados = '../documentosElectronicos/enviados';
		if (!file_exists($carpeta_enviados)) {
		  mkdir($carpeta_enviados, 0777, true);
		}
		$carpeta_recibidos = '../documentosElectronicos/recibidos';
		if (!file_exists($carpeta_recibidos)) {
		  mkdir($carpeta_recibidos, 0777, true);
		}
		$fac_enviadas = $carpeta_enviados.'/'.'facturasElectronicas';
		if (!file_exists($fac_enviadas)) {
		  mkdir($fac_enviadas, 0777, true);
		}
		$fac_recibidas = $carpeta_recibidos.'/'.'facturasElectronicas';
		if (!file_exists($fac_recibidas)) {
		  mkdir($fac_recibidas, 0777, true);
		}
		$ret_enviadas = $carpeta_enviados.'/'.'retencionesElectronicas';
		if (!file_exists($ret_enviadas)) {
		  mkdir($ret_enviadas, 0777, true);
		}
		$ret_recibidas = $carpeta_recibidos.'/'.'retencionesElectronicas';
		if (!file_exists($ret_recibidas)) {
		  mkdir($ret_recibidas, 0777, true);
		}
		$guias_enviadas = $carpeta_enviados.'/'.'guiasRemisionElectronicas';
		if (!file_exists($guias_enviadas)) {
		  mkdir($guias_enviadas, 0777, true);
		}
		$guias_recibidas = $carpeta_recibidos.'/'.'guiasRemisionElectronicas';
		if (!file_exists($guias_recibidas)) {
		  mkdir($guias_recibidas, 0777, true);
		}
    $notas_credito_enviadas = $carpeta_enviados.'/'.'notasCreditoElectronicas';
    if (!file_exists($notas_credito_enviadas)) {
      mkdir($notas_credito_enviadas, 0777, true);
    }
    $notas_credito_recibidas = $carpeta_recibidos.'/'.'notasCreditoElectronicas';
    if (!file_exists($notas_credito_recibidas)) {
      mkdir($notas_credito_recibidas, 0777, true);
    }
		$notas_credito_enviadas = $carpeta_enviados.'/'.'notasDebitoElectronicas';
		if (!file_exists($notas_credito_enviadas)) {
		  mkdir($notas_credito_enviadas, 0777, true);
		}
		$notas_credito_recibidas = $carpeta_recibidos.'/'.'notasDebitoElectronicas';
		if (!file_exists($notas_credito_recibidas)) {
		  mkdir($notas_credito_recibidas, 0777, true);
		}

    //print_r($facturaArray);


		switch ($tipoDocumento)
		{
	  	case "FACTURA":
			$carpeta_xml_enviados = $fac_enviadas;
			$carpeta_xml_recibidos = $fac_recibidas;
			$notificacion = "factura";
			$theXML.= '<documento><![CDATA[<factura id="comprobante" version="1.1.0">' . $this->convert_array_to_xml($facturaArray) . '</factura>]]></documento>';
			$tipoDocumentoElec = 17;
			break;
	  	case "GUIA-DE-REMISION":
		  	$carpeta_xml_enviados = $guias_enviadas;
		  	$carpeta_xml_recibidos = $guias_recibidas;
		  	$notificacion = "Guía Remisión";
		  	$theXML.= '<documento><![CDATA[<guiaRemision id="comprobante" version="1.0.0">' . $this->convert_array_to_xml($facturaArray) . '</guiaRemision>]]></documento>';
		  	$tipoDocumentoElec = 6;
		  	break;
		  	case "RETENCION":
		  	$carpeta_xml_enviados = $ret_enviadas;
		  	$carpeta_xml_recibidos = $ret_recibidas;
		  	$notificacion = "retencion";
		  	$theXML.= '<documento><![CDATA[<comprobanteRetencion id="comprobante" version="1.0.0">' . $this->convert_array_to_xml($facturaArray) . '</comprobanteRetencion>]]></documento>';
		  	$tipoDocumentoElec = 3;
	  		break;
      case "NOTA-DE-CREDITO":
      $carpeta_xml_enviados = $notas_credito_enviadas;
        $carpeta_xml_recibidos = $notas_credito_recibidas;
        $notificacion = "Nota de credito";
        $theXML.= '<documento><![CDATA[<notaCredito id="comprobante" version="1.0.0">' . $this->convert_array_to_xml($facturaArray) . '</notaCredito>]]></documento>';
        $tipoDocumentoElec = 4;
        break;
	  	case "NOTA-DE-DEBITO":
		 	$carpeta_xml_enviados = $notas_credito_enviadas;
	  		$carpeta_xml_recibidos = $notas_credito_recibidas;
	  		$notificacion = "Nota de debito";
	  		$theXML.= '<documento><![CDATA[<notaDebito id="comprobante" version="1.0.0">' . $this->convert_array_to_xml($facturaArray) . '</notaDebito>]]></documento>';
	  		$tipoDocumentoElec = 5;
	  		break;
		}
	          
		$theXML.= '</formatoXML>';

    
	  //invocar ws firma electronica        
    //$url = 'http://186.101.5.254/redirect.php';
    //Cambio temporal caida del punto de Facturacion Electrónica 22-05-2018
    $url = 'http://soporte.facturacionkapital.com/redirect.php';
		//$url = 'http://test.kapitalcompany.com/redirect.php';
		$data = array('key1'=>$theXML, 'key2'=>$servidor);
		$options = array(
	  	'http' => array(
	      'header' => "Content-type: application/x-www-form-urlencoded\r\n",
	      'method' => 'POST',
	      'content' => http_build_query($data)
	      )
	  	);
     	//Verificamos si la Factura se Encuentra Aprobada en el co_factura
     	$sql = 'SELECT * FROM co_factura WHERE estado = "AUTORIZADO" AND tipo_doc = ' . $tipoDocumentoElec . ' AND doc_instancias_id= '.(int)$idDoc.';';

      //print_r($sql);

      $facturas = $conexion->query($sql);

        $arr_verificar = array();

        //transformo los datos de la consulta a un array
        while ($factura_a = mysqli_fetch_array($facturas))
        {
          $arr_verificar[] = $factura_a;
        }
      	if (empty($arr_verificar)) 
      	{          
          	$context = stream_context_create($options);
          	$result = file_get_contents($url, false, $context);
          	// Creamos el archivo de xml de la factura.

          	$carpeta_xml_enviados = $carpeta_xml_enviados . '/' . $numero . '_enviado.xml';
            //print_r($carpeta_xml_enviados);
          	$file = fopen($carpeta_xml_enviados, "w+");
          	fwrite($file, $theXML);
          	fclose($file);
          	if (strip_tags($result)=="RECIBIDA")
            {
              $resultados['mensaje_sri_envio'] = "Factura enviada al SRI correctamente!";
            }else{
              $resultados['mensaje_sri_envio'] = "EL DOCUMENTO $numero PRESENTA PROBLEMAS CON EL SRI: El Sri no ha Respondido a su Petición";
              //return "EL DOCUMENTO $numero PRESENTA PROBLEMAS CON EL SRI: ".$result;
            }

	        $carpeta = $carpeta_xml_recibidos;
	        $elarchivo = $carpeta . "/" . $numero . ".xml";
	        $elarchivoClave = $carpeta_xml_recibidos . "/" . $numeroDeAutorizacionyClave . ".xml";
	        $xmlHumano = "";
	        $servicioRespuesta = "";

          	switch ($servidor) {
              case "Desarrollo":
              $servicioRespuesta = 'https://celcer.sri.gob.ec/comprobantes-electronicos-ws/AutorizacionComprobantes?wsdl';
              break;
              case "Pruebas":
              $servicioRespuesta = 'https://celcer.sri.gob.ec/comprobantes-electronicos-ws/AutorizacionComprobantes?wsdl';
              break;
              case "Produccion":
              $servicioRespuesta = 'https://cel.sri.gob.ec/comprobantes-electronicos-ws/AutorizacionComprobantes?wsdl';
              break;
              default :
              $servicioRespuesta = 'https://celcer.sri.gob.ec/comprobantes-electronicos-ws/AutorizacionComprobantes?wsdl';
              break;
          	}

              $client1 = new \SoapClientFs($servicioRespuesta);
              $resultAut = $client1->autorizacionComprobante(array("claveAccesoComprobante" => $numeroDeAutorizacionyClave)); //llamamos al método que nos interesa con los parámetros

              $elxmlaobjeto = new \ObjectToXMLFs($resultAut);
              //Almacenamos el objeto devuelto por el sri
              $file = fopen($elarchivoClave, "w+");
              fwrite($file, $elxmlaobjeto);
              fclose($file);

              $fichero_texto = fopen($elarchivoClave, "r");
              $contenido_fichero = fread($fichero_texto, filesize($elarchivoClave));
              fclose($fichero_texto);
              $contenido_fichero = str_replace("&lt;", "<", $contenido_fichero);
              $contenido_fichero = str_replace("&gt;", ">", $contenido_fichero);
              $contenido_fichero = str_replace("                    ", "", $contenido_fichero);
              $desdeEstado = strpos($contenido_fichero, "<estado>");
              $hastaEstado = strpos($contenido_fichero, "</estado>");
              $tamañoEstado = $hastaEstado - $desdeEstado;
              $estadoAutorizado = substr($contenido_fichero, $desdeEstado, $tamañoEstado);
              $estadoAutorizado = trim($estadoAutorizado);
              $estadoAutorizado = (string) $estadoAutorizado;

              $desdeAutorizacion = strpos($contenido_fichero, "<numeroAutorizacion>") + 20;
              $hastaAutorizacion = strpos($contenido_fichero, "</numeroAutorizacion>");
              $tamañoAutorizacion = $hastaAutorizacion - $desdeAutorizacion;
              $idAutorizacionSRI = substr($contenido_fichero, $desdeAutorizacion, $tamañoAutorizacion);
              $idAutorizacionSRI = trim($idAutorizacionSRI);
              $idAutorizacionSRI = $idAutorizacionSRI;

              $desdeFecAutorizacion = strpos($contenido_fichero, "<fechaAutorizacion>") + 19;
              $hastaFecAutorizacion = strpos($contenido_fichero, "</fechaAutorizacion>");
              $tamañoFecAutorizacion = $hastaFecAutorizacion - $desdeFecAutorizacion;
              $FecAutorizacionSRI = substr($contenido_fichero, $desdeFecAutorizacion, $tamañoFecAutorizacion);
              $FecAutorizacionSRI = trim($FecAutorizacionSRI);
              $FecAutorizacionSRI = str_replace("T", " ", $FecAutorizacionSRI);
              $FecAutorizacionSRI = str_replace("-05:00", "", $FecAutorizacionSRI);
              $FecAutorizacionSRI = strtotime($FecAutorizacionSRI);

              $file = fopen($elarchivo, "w+");
              fwrite($file, $contenido_fichero);
              fclose($file);

              unlink($elarchivoClave);
              $estadoAutorizado = substr($estadoAutorizado, 8, 11);
               // Generamos un log de todos los envios al sri
              $sql = "INSERT INTO co_factura (tipo_doc, numero_documento, autoriza_numero, mensaje, estado, codigo_acceso, codigo_acceso_lote, bandera, doc_instancias_id, fecha_actualizacion, hora_actualizacion) VALUES ('".$tipoDocumentoElec."', '".$numero."', '".$idAutorizacionSRI."', '".strip_tags($result)."', '".$estadoAutorizado."', '".$numeroDeAutorizacionyClave."', '".$numeroDeAutorizacionyClave."', '-1', '".$idDoc."', '".date('Y-m-d')."', '".date('H:i:s')."');";
              if($conexion->query($sql)===FALSE)
              {
                $resultados['co_retencion'] = "No Se Pudo Ingresar el co_factura";
              }

              if ($estadoAutorizado == "AUTORIZADO" AND $tipoDocumento=="FACTURA" )
              {
                $sql = "UPDATE facturascli set tipo_doc_sri = '".$tipoDocumentoElec."', numero_documento_sri = '".$numero."', autoriza_numero_sri = '".$idAutorizacionSRI."', mensaje_sri = '".strip_tags($result)."', estado_sri = '".$estadoAutorizado."', codigo_acceso_sri = '".$numeroDeAutorizacionyClave."', codigo_acceso_lote_sri = '".$numeroDeAutorizacionyClave."', fecha_autorizacion_sri = '".date('Y-m-d')."', hora_autorizacion_sri = '".Date('H:i:s')."' WHERE idfactura = '".$idDoc."'";

                  //print_r($sql);

                if($conexion->query($sql)===TRUE)
                {
                  $resultados['estado_factura'] = "Se Actualizo la Informacion en la Factura.";                    
                }else{
                  $resultados['estado_factura'] = "No Se Pudo actualizar la informacion de la factura";
                }
                
                $FS_ORI_RIDE = $var->FS_ORI_RIDE;
                
                // Generamos le pdf
                $pdf = new genera_pdf_email_electronico();

                if ($FS_ORI_RIDE=='vertical')
                {
                  $pdf->generar_pdf_factura_electronica($factura, $numero);
                }
                
                /*elseif ($FS_ORI_RIDE=='horizontal') {
                  $pdf->generar_pdf_factura_electronica_horizontal($factura, $numero);
                }elseif ($FS_ORI_RIDE=='vertical_acu') {
                  $pdf->generar_pdf_factura_electronica_vertical_acu($factura, $numero);
                }*/
                 
                // Enviamos el correo electronico al cliente
                
                $pdf->enviar_email($factura,$numero);

              }elseif ( $tipoDocumento=="FACTURA"){

                 $sql = "UPDATE facturascli set tipo_doc_sri = '".$tipoDocumentoElec."', numero_documento_sri = '".$numero."', autoriza_numero_sri = 'N/A', mensaje_sri = '".strip_tags($result)."', estado_sri = '".$estadoAutorizado."', codigo_acceso_sri = '".$numeroDeAutorizacionyClave."', codigo_acceso_lote_sri = '".$numeroDeAutorizacionyClave."', fecha_autorizacion_sri = '".date('Y-m-d')."', hora_autorizacion_sri = '".Date('H:i:s')."' WHERE idfactura = '".$idDoc."'";

                  //print_r($sql);

                if($conexion->query($sql)===TRUE)
                {
                  $resultados['estado_factura'] = "Se Actualizo la Informacion en la Factura.";                    
                }else{
                  $resultados['estado_factura'] = "No se puede actualizar la información que respondio el sri!";
                }                 
              }

              if ($estadoAutorizado == "AUTORIZADO" AND $tipoDocumento=="RETENCION" ) {
                  // Actualizamos el registro de la factura con la informacion que nos responde el sri.
                 $sql = "UPDATE facturasprov SET  tipo_doc_sri = '".$tipoDocumentoElec."', numero_documento_sri = '".$numero."', autoriza_numero_sri = '".$idAutorizacionSRI."', mensaje_sri = '".strip_tags($result)."', estado_sri = '".$estadoAutorizado."', codigo_acceso_sri = '".$numeroDeAutorizacionyClave."', codigo_acceso_lote_sri = '".$numeroDeAutorizacionyClave."', fecha_autorizacion_sri = '".date('Y-m-d')."', hora_autorizacion_sri = '".Date('H:i:s')."' WHERE idfactura = '".$idDoc."';";
                if ($conexion->query($sql)==TRUE) {
                  $resultados['estado_retencion'] = "Se Actualizo la Informacion en la Retencion.";                    
                }else{
                  $resultados['estado_retencion'] = "No se puede actualizar la información que respondio el sri!";
                }

                 // Generamos le pdf
                $pdf = new genera_pdf_email_electronico();
                $pdf->generar_pdf_retencion_electronica($factura);
                 // Enviamos el correo electronico al cliente

                $pdf->enviar_email($factura,$numero,"RETENCION");
            }elseif ( $tipoDocumento=="RETENCION"){
              $sql = "UPDATE facturasprov SET tipo_doc_sri = '".$tipoDocumentoElec."', numero_documento_sri = '".$numero."', estado_sri = '".$estadoAutorizado."', mensaje_sri = '".strip_tags($result)."', codigo_acceso_sri = '".$numeroDeAutorizacionyClave."', codigo_acceso_lote_sri = '".$numeroDeAutorizacionyClave."', fecha_autorizacion_sri = '', hora_autorizacion_sri = '' WHERE idfactura = '".$idDoc."';";

              if ($conexion->query($sql)) {
                $resultados['estado_retencion'] = "Se Actualizo la Informacion en la Retencion.";                    
              }else{
                $resultados['estado_retencion'] = "No se puede actualizar la información que respondio el sri!";
              }
            }

            if ($estadoAutorizado == "AUTORIZADO" AND $tipoDocumento=="GUIA-DE-REMISION" ) {
                  // Actualizamos el registro de la factura con la informacion que nos responde el sri.

                $sql = "UPDATE facturascli SET estado_guia_sri = '".$estadoAutorizado."' WHERE idfactura = '".$idDoc."'";
                if ($conexion->query($sql)) {
                  $resultados['estado_guia_remision'] = "Se Actualizo la Informacion en la Guia de remision.";                    
                }else{
                  $resultados['estado_guia_remision'] = "No se puede actualizar la información que respondio el sri!";
                }

                 // Generamos el pdf
                 $pdf = new genera_pdf_email_electronico();
                 $pdf->generar_pdf_guia_remision($factura);
                 // Enviamos el correo electronico al cliente
                 $pdf->enviar_email($factura,"GUIA-DE-REMISION");

             }elseif ( $tipoDocumento=="GUIA-DE-REMISION"){
                $sql = "UPDATE facturascli SET estado_guia_sri = 'NO AUTORIZADO' WHERE idfactura = '".$idDoc."'";
                if ($conexion->query($sql)) {
                  $resultados['estado_guia_remision'] = "Se Actualizo la Informacion en la Guia de remision.";                    
                }else{
                  $resultados['estado_guia_remision'] = "No se puede actualizar la información que respondio el sri!";
                }

             }

             if ($estadoAutorizado == "AUTORIZADO" AND $tipoDocumento=="NOTA-DE-CREDITO" ) {
                  // Actualizamos el registro de la factura con la informacion que nos responde el sri.
                 $sql = "UPDATE facturascli SET tipo_doc_sri = '".$tipoDocumentoElec."', numero_documento_sri = '".$numero."', autoriza_numero_sri = '".$idAutorizacionSRI."', mensaje_sri = '".strip_tags($result)."', estado_sri = '".$estadoAutorizado."', codigo_acceso_sri = '".$numeroDeAutorizacionyClave."', codigo_acceso_lote_sri = '".$numeroDeAutorizacionyClave."', fecha_autorizacion_sri = '".date('Y-m-d')."', hora_autorizacion_sri = '".Date('H:i:s')."' WHERE idfactura = '".$idDoc."';";

                if ($conexion->query($sql)) {
                  $resultados['estado_notacredito']="Se Actualizo la Informacion en la Nota de credito";
                }else{
                  $resultados['estado_notacredito']="No se pudo actualizar la Informacion en la Nota de credito";
                }

                 // Generamos le pdf
                 $pdf = new genera_pdf_email_electronico();
                 $pdf->generar_pdf_nota_credito($factura);
                 // Enviamos el correo electronico al cliente
                 $pdf->enviar_email($factura,"NOTA-DE-CREDITO");
             }elseif ( $tipoDocumento=="NOTA-DE-CREDITO"){
                $sql = "UPDATE facturascli set tipo_doc_sri = '".$tipoDocumentoElec."', numero_documento_sri = '".$numero."', autoriza_numero_sri = 'N/A', mensaje_sri = '".strip_tags($result)."', estado_sri = '".$estadoAutorizado."', codigo_acceso_sri = '".$numeroDeAutorizacionyClave."', codigo_acceso_lote_sri = '".$numeroDeAutorizacionyClave."', fecha_autorizacion_sri = '".date('Y-m-d')."', hora_autorizacion_sri = '".Date('H:i:s')."' WHERE idfactura = '".$idDoc."'";
                if ($conexion->query($sql)) {
                  $resultados['estado_notacredito']="Se Actualizo la Informacion en la Nota de credito";
                }else{
                  $resultados['estado_notacredito']="Se Actualizo la Informacion en la Nota de credito";
                }

             }
             if ($estadoAutorizado == "AUTORIZADO" AND $tipoDocumento=="NOTA-DE-DEBITO" ) {
                  // Actualizamos el registro de la factura con la informacion que nos responde el sri.
                 $sql = "UPDATE notadebitocli set estado_sri = '".$estadoAutorizado."' WHERE idnotadebito = '".$idDoc."';";

                if ($conexion->query($sql)) {
                  $resultados['estado_notadebito']="Se Actualizo la Informacion en la Nota de debito";
                }else{
                  $resultados['estado_notadebito']="No se pudo actualizar la Informacion en la Nota de debito";
                }

                 // Generamos le pdf
                 $pdf = new genera_pdf_email_electronico();
                 $pdf->generar_pdf_nota_debito($factura);
                 // Enviamos el correo electronico al cliente
                 $pdf->enviar_email($factura,"NOTA-DE-DEBITO");
             }elseif ( $tipoDocumento=="NOTA-DE-DEBITO"){
                $sql = "UPDATE notadebitocli set estado_sri = '".$estadoAutorizado."'; WHERE idnotadebito = '".$idDoc."';";
                if ($conexion->query($sql)) {
                  $resultados['estado_notadebito']="Se Actualizo la Informacion en la Nota de debito";
                }else{
                  $resultados['estado_notadebito']="No se pudo actualizar la Informacion en la Nota de debito";
                }

             }
         
      	}else{
        	$resultados['documentoautorizado'] = 'EL documento electrónico ya se encuentra aprobado!';
      	}
    return $resultados;
  }
}

?>