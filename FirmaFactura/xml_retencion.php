<?php
require_once '../plugins/facturacion_base/extras/Utilities/Utilities.php';
require_once 'conectarServidorFacturacionE.php';
require_once 'variables_globales.php';

class xml_retencion
{
   public $messages;
   public $errors;
   public $numeroDeAutorizacionyClave;

   
   private $articulo_propiedad;
   private $articulo_propiedad_vacio;
   private $cuenta_banco;
   private $divisa;
   private $ejercicio;
   private $empresa;
   private $forma_pago;
   private $impuestos;

   public $iva;
   public $proveedor0;

   public $losErrores;

   public function generar_xml_retencion(&$factura)
   {
      $resultados = array();
      date_default_timezone_set('America/Bogota');
      $Utilities = new \Utilities();
      $ok = FALSE;
      $paso = TRUE;

      $lineas = array();
      $lineas = array();
      $impuestos = array();
      $cont = 0;
      $total = 0;
      $losErrores = "";
      $codigoImpuesto = '';
      $cods_retenciones = array();
      $cods_iva = array();
      $base_renta = 0;
      $base_iva = 0;

      $sum_renta = array(); // SUMATORIA DE TOTAL A RETENER POR CODIGO DE RENTA
      $neto_renta = array(); // SUMATORIA DE BASE IMPONIBLE POR CODIGO DE RENTA
      $porcentaje_renta = array(); // PORCENTAJES POR CODIGO DE RENTA
      $suma_retencion_renta = 0; // VARIABLE SUMATORIA DE RETENCION DE RENTA
      $suma_neto_renta = 0; // VARIABLE SUMATORIA DE BASE IMPONIBLE DE RENTA

      $sum_iva = array(); // SUMATORIA DE TOTAL A RETENER POR CODIGO DE IVA
      $neto_iva = array(); // SUMATORIA DE BASE IMPONIBLE POR CODIGO DE IVA
      $suma_retencion_iva = 0; // VARIABLE SUMATORIA DE RETENCION DE IVA
      $suma_neto_iva = 0; // VARIABLE SUMATORIA DE BASE IMPONIBLE DE IVA

      // Conexión a la base de datos
      $var = new variables_globales();
      $conexion = mysqli_connect($var->FS_DB_HOST, $var->FS_DB_USER, $var->FS_DB_PASS, $var->FS_DB_NAME);

      // query devuelve un result_query que al recorrer se convierte en un array asociativo sin usar mysqli_fetch_array
      $lineas = array();
      $consulta = $conexion->query("SELECT * FROM lineasfacturasprov WHERE idfactura = ".$factura['idfactura']." ORDER BY idlinea ASC;");
      foreach ($consulta as $l) {
        $lineas[] = $l;
      }
      $numDocSustento = $factura['numproveedor'];
      $numDocSustento = str_replace('-', '',$numDocSustento);

      // //Lista de códigos por impuestos asignados para la retención
      //       /*+++++++++++++++++++++++++++++++++++++++*/
      //       /* Impuesto a Retener     | Código       */
      //       /*------------------------|--------------*/
      //       /*  RENTA                 | 1            */
      //       /*  IVA                   | 2            */
      //       /*  ISD                   | 6            */
      //       /*+++++++++++++++++++++++++++++++++++++++*/

      for ($i=0; $i < count($lineas) ; $i++) {

        switch ($lineas[$i]['iva']) {
          case 0:
            $codigoPorcentaje = 0;
            break;
          case 12:
            $codigoPorcentaje = 2;
            break;
          case 14:
            $codigoPorcentaje = 3;
            break; 
        }
        if (!isset($codigoPorcentaje)) {
          $resultados['iva_incorrecto'] = 'El porcentaje del (IVA) del producto [' . $value[4] . '] se encuentra fuera del marco establecido en la ley del Ecuador';
          $codigoPorcentaje='';
          $paso = FALSE;
        }


        // Tipo de retencion Renta
        $consulta = $conexion->query("SELECT * FROM co_tipos_retencion WHERE idtiporetencion = ".$lineas[$i]['idtipo_retencion_renta'].";");
        $tipo_retencion_renta = mysqli_fetch_array($consulta);
        // Tipo de retencion Iva
        $consulta = $conexion->query("SELECT * FROM co_tipos_retencion WHERE idtiporetencion = ".$lineas[$i]['idtipo_retencion_iva'].";");
        $tipo_retencion_iva = mysqli_fetch_array($consulta);

           //if ($tipo_retencion_renta->codigo_base!=332) {

              array_push($cods_retenciones, $tipo_retencion_renta['codigo_base']);

            //}

            //if ($tipo_retencion_iva->codigo!=0) {

              array_push($cods_iva, $tipo_retencion_iva['codigo_base']);

            //}

        }

      

      foreach ($cods_retenciones as $key => $value) {

          for ($i=0; $i < count($lineas) ; $i++) {

          // Tipo de retencion Renta X2
          $consulta = $conexion->query("SELECT * FROM co_tipos_retencion WHERE idtiporetencion = ".$lineas[$i]['idtipo_retencion_renta'].";");
          $tipo_retencion_renta = mysqli_fetch_array($consulta);
            
            if ($value == $tipo_retencion_renta['codigo_base']) {
              // SUMA DE TOTAL A RETENER
              $val = ((($tipo_retencion_renta['porcentaje'])/100)*$lineas[$i]['pvptotal']);
              $suma_retencion_renta += $val;            
              // SUMA BASE IMPONIBLE
              $val = $lineas[$i]['pvptotal'];
              $suma_neto_renta += $val;  

            }

          }

          $sum_renta += array($value=>$suma_retencion_renta);
          $neto_renta += array($value=>$suma_neto_renta);
          $suma_retencion_renta = 0;
          $suma_neto_renta = 0;

        }


      foreach ($cods_iva as $key => $value) {

          for ($i=0; $i < count($lineas) ; $i++) {

        // Tipo de retencion Ivax2
        $consulta = $conexion->query("SELECT * FROM co_tipos_retencion WHERE idtiporetencion = ".$lineas[$i]['idtipo_retencion_iva'].";");
        $tipo_retencion_iva = mysqli_fetch_array($consulta);
            
            if ($value == $tipo_retencion_iva['codigo_base']) {
              // SUMA TOTAL DE IVA
              $val = $lineas[$i]['pvptotal'] * (($lineas[$i]['iva'])/100);
              $suma_neto_iva += $val;  
              // SUMA DE TOTAL A RETENER DE IVA
              $val = ((($tipo_retencion_iva['porcentaje'])/100)*$val);
              $suma_retencion_iva += $val;            

            }

          }

          $sum_iva += array($value=>$suma_retencion_iva);
          $neto_iva += array($value=>$suma_neto_iva);
          $suma_retencion_iva = 0;
          $suma_neto_iva = 0;

        }

        // Renta
      foreach ($sum_renta as $key => $value) {

          $val_ret = $neto_renta[$key];

          $porcentaje_renta = ($value / $val_ret) * 100;
           
          $impuestos['impuesto_'.$cont] = array(
                      'codigo' => 1,
                      'codigoRetencion' => $key,
                      'baseImponible' => str_replace(",", "", number_format(doubleval($val_ret), 2)),
                      'porcentajeRetener' => str_replace(",", "", number_format(doubleval($porcentaje_renta), 2)),
                      'valorRetenido' => str_replace(",", "", number_format(doubleval($value), 2)),
                      'codDocSustento' => "01",
                      'numDocSustento' => $numDocSustento,
                      'fechaEmisionDocSustento' => date('d/m/Y', strtotime($factura['fecha']) ),

                  );
            
            $cont++;
        }

        //RETENCIÓN DE IVA:
            /*+++++++++++++++++++++++++++++++++++++++*/
            /* Porcentaje IVA         | Código       */
            /*------------------------|--------------*/
            /*  10%                   | 9            */
            /*  20%                   | 10           */
            /*  30%                   | 1            */
            /*  50%                   | 11           */
            /*  70%                   | 2            */
            /*  100%                  | 3            */
            /*+++++++++++++++++++++++++++++++++++++++*/
            /*  Retención en cero:                   */
            /*+++++++++++++++++++++++++++++++++++++++*/
            /*  0%                    | 7            */
            /*+++++++++++++++++++++++++++++++++++++++*/
            /*  RETENCIÓN DE ISD:                    */
            /*+++++++++++++++++++++++++++++++++++++++*/
            /*  5%                    | 4580         */
      //       /*+++++++++++++++++++++++++++++++++++++++*/


        // iva
        foreach ($sum_iva as $key => $value) {

          $val_ret = $neto_iva[$key];

          if ($val_ret!=0) {
            $porcentaje_iva = ($value / $val_ret) * 100;
          }else{
            $porcentaje_iva=0;
          }

            $impuestos['impuesto_'.$cont] = array(
                      'codigo' => 2,
                      'codigoRetencion' => $key,
                      'baseImponible' => str_replace(",", "", number_format(doubleval($val_ret), 2)),
                      'porcentajeRetener' => str_replace(",", "", number_format(doubleval($porcentaje_iva), 2)),
                      'valorRetenido' => str_replace(",", "", number_format(doubleval($value), 2)),
                      'codDocSustento' => "01",
                      'numDocSustento' => $numDocSustento,
                      'fechaEmisionDocSustento' => date('d/m/Y', strtotime($factura['fecha']) ),
                  );

        }

      $servidorFacturacionE = $var->FS_AMBIENTE_XML;
      if ( $servidorFacturacionE == 1){
         $servidorFacturacionE = "Desarrollo";
      }elseif ( $servidorFacturacionE == 2) {
          $servidorFacturacionE = "Produccion";
      }
      switch ($servidorFacturacionE) {
            case "Desarrollo":
                $servidor = "Desarrollo";
                $ambiente = "01";
                $tambiente = "1";
                break;
            case "Pruebas":
                $servidor = "Pruebas";
                $ambiente = "01";
                $tambiente = "1";
                break;
            case "Produccion":
                $servidor = "Produccion";
                $ambiente = "02";
                $tambiente = "2";
                break;
        }
      /// obtenemos datos de la empresa
        $consulta = $conexion->query("SELECT * FROM empresa");
        $empresa0 = mysqli_fetch_array($consulta);
      // $empresa0 = new \empresa();
      $rucEmpresa = $empresa0['cifnif'];
      $direccionEmpresa = $empresa0['direccion'];


      //obtenemos la direccion de la sucursal
      $consulta = $conexion->query("SELECT e.direccion FROM almacenes a, establecimiento e WHERE a.codalmacen = '".$factura['codalmacen']."' AND a.codestablecimiento = e.codestablecimiento");
      $establecimientos = mysqli_fetch_array($consulta);
      $direccionEsta =  $establecimientos['direccion'];

      // ------------------------------------------------------------------------


      if (empty($direccionEsta)) {
        $resultados['error_establecimiento'] = "La dirección del establecimiento no esta parametrizada!";
        $paso = FALSE;
      }
      $nombreEmpresa = $empresa0['nombre'];

      /// obtenemos datos del proveedor
      $consulta = $conexion->query("SELECT * FROM proveedores WHERE codproveedor = '".$factura['codproveedor']."';");
      $proveedor = mysqli_fetch_array($consulta);
      $razonSocialProveedor = $proveedor['razonsocial'];
      // $razonSocialProveedor = $razonSocialProveedor; WTF
      $rucProveedor = $proveedor['cifnif'];
      $telefonoProveedor = $proveedor['telefono1'];
      $tipoidfiscal = $proveedor['tipoidfiscal'];
      // Asignamos los codigos para el tipoidfiscal en base a la normativa del sri TABLA 7
      switch ($tipoidfiscal) {
        case 'RUC':
          $tipoIdentificacionSujetoRetenido = '04';
          break;

        case 'CEDULA':
          $tipoIdentificacionSujetoRetenido = '05';
          break;

        case 'PASAPORTE':
          $tipoIdentificacionSujetoRetenido = '06';
          break;

        case 'VENTA A CONSUMIDOR FINAL*':
          $tipoIdentificacionSujetoRetenido = '07';
          break;

        case 'IDENTIFICACION DELEXTERIOR*':
          $tipoIdentificacionSujetoRetenido = '08';
          break;

        case 'PLACA':
          $tipoIdentificacionSujetoRetenido = '09';
          break;
        
        default:
          $tipoIdentificacionSujetoRetenido = '04';
          break;
      }

      // // Validamos telefono del proveedor
      if (empty($telefonoProveedor)) {
        $resultados['error_telefono'] = "El cliente no registra telefono!";
        $paso = FALSE;
      }


      // Validamos el ruc de la empresa.
      if (!$Utilities->validarID($rucEmpresa)) {
        $resultados['error_ruc'] = "El ruc de la empresa no es válido!";
        $paso = FALSE;
      }
      /// obtenemos direccion del proveedor
      $consulta = $conexion->query("SELECT * FROM dirproveedores WHERE codproveedor = '".$factura['codproveedor']."' ORDER BY id DESC;");
      $datosProveedor = mysqli_fetch_array($consulta);
      if (!empty($datosProveedor)) {
        $direccionProveedor = $datosProveedor['direccion'];
      }else{
        $resultados['error_dir_proveedor'] = "El Proveedor no registra direccion";
        $direccionProveedor = '';
        $paso = FALSE;
      }
      if (empty($proveedor['email'])) {
        $resultados['error_correo'] = "¡El proveedor no cuenta con correo electronico!. No se puede enviar la factura al SRI";
        $email = '';
        $paso = FALSE;
      }else{
        $email = $proveedor['email'];
      }

      $lainfoadicional = ''
                . '<campoAdicional nombre="Direccion">' . $direccionProveedor . '</campoAdicional>'
                . '<campoAdicional nombre="Telefono">' . $telefonoProveedor . '</campoAdicional>'
                . '<campoAdicional nombre="Email">' . $email . '</campoAdicional>';

      $fechaEmision = date('d/m/Y');
      $codDoc = "07";
      $codalmacenEmpresa = $factura['codalmacen'];

      $numero = $factura['num_retencion'];
      $comprobante = array();
      list($estab, $ptoEmi, $secuencial) = explode("-", $numero);


      $numeroDeAutorizacionyClave = $this->numeroDeAutorizacionyClave($factura, $rucEmpresa, $tambiente);
      //$numeroDeAutorizacionyClave = '2110201106179214673900100110020010000000011234567815';

      if($var->FS_OB_CONTA == 1){
         $obligadoContabilidad =  'NO';
      }else{
         $obligadoContabilidad =  'SI';
      }

      // Consultamos el periodo fiscal
      $fechainicio = Date('01-01-Y');
      list($dia, $mes, $año) = explode('-', $fechainicio);
      $periodoFiscal = $mes.'/'.$año;

      $retencionArray = Array(
                "infoTributaria" => Array
                    (
                    "ambiente" => $tambiente,
                    "tipoEmision" => 1,
                    "razonSocial" => $nombreEmpresa,
                    "nombreComercial" => $nombreEmpresa,
                    "ruc" => $rucEmpresa,
                    "claveAcceso" => $numeroDeAutorizacionyClave,
                    "codDoc" => $codDoc,
                    "estab" => $estab,
                    "ptoEmi" => $ptoEmi,
                    "secuencial" => $secuencial,
                    "dirMatriz" => $direccionEmpresa
                ),

                "infoCompRetencion" => Array
                    (
                    "fechaEmision" => $fechaEmision,
                    "dirEstablecimiento" => $direccionEsta,
                    "contribuyenteEspecial" => $var->FS_CONTRIBUYENTE_NRO,
                    "obligadoContabilidad" => $obligadoContabilidad,
                    "tipoIdentificacionSujetoRetenido" => $tipoIdentificacionSujetoRetenido,
                    "razonSocialSujetoRetenido" => $razonSocialProveedor,
                    "identificacionSujetoRetenido" => $rucProveedor,
                    "periodoFiscal" => $periodoFiscal
                ),
                "impuestos" => $impuestos,
                "infoAdicional" => $lainfoadicional,
            );

      // print_r($retencionArray);

      $fe = new \conectarServidorFacturacionE();
      if (isset($rucEmpresa) && $rucEmpresa != "") {
            $resp = array();

            if (strlen($losErrores) < 1) {
                $respuesta = $fe->envioFactura($factura, $servidor, $retencionArray, $rucEmpresa, $factura['idfactura'], $email, $razonSocialProveedor, $numero, $numeroDeAutorizacionyClave, $factura['codproveedor'] , "RETENCION");
                if (!is_numeric($respuesta)){
                  $this->losErrores = $respuesta;
                  $resultados += $respuesta;
                }else{
                  $this->losErrores = '';
                }
            }else {
                //$this->new_message("$losErrores"."</br>");
              // return $losErrores;
            }
        } else {
        $resultados['error_empresa'] = "No se encuentra configurada su empresa por favor póngase en contacto con su proveedor";

        }
        return $resultados;
        
   }
   
   /**
    * Genera numero de autorizacion y clave.
    * @param $factura|array()
    * @param $rucEmpresa|string
    * @param $tambiente|bolean
    * Devuelve $numeroDeAutorizacionyClave.
    */
   public function numeroDeAutorizacionyClave($factura, $rucEmpresa, $tambiente)
   {
      $fe = new \conectarServidorFacturacionE();
      $codNumerico = $this->generaAutorizacion();
      $fechaEmision = date('d/m/Y');
      $codDoc = "07";
      $codalmacenEmpresa = $factura['codalmacen'];
      $numero = $factura['num_retencion'];
      $comprobante = array();
      list($estab, $ptoEmi, $secuencial) = explode("-", $numero);

      $numeroDeAutorizacionyClave = $fechaEmision . $codDoc . $rucEmpresa . $tambiente . $estab . $ptoEmi . $secuencial . $codNumerico["0"] . "1"; 
      $numeroDeAutorizacionyClave = str_replace("/", "", $numeroDeAutorizacionyClave);
      $cadenaInvertida = $this->invertirCadena($numeroDeAutorizacionyClave);
      $eldigitoverificador = $this->obtenerSumaPorDigitos($cadenaInvertida);
      if ($eldigitoverificador == 11)
            $eldigitoverificador = 0;
      if ($eldigitoverificador == 10)
            $eldigitoverificador = 1;
      $numeroDeAutorizacionyClave = $numeroDeAutorizacionyClave . $eldigitoverificador;
      return $numeroDeAutorizacionyClave;
   }

  function generaAutorizacion() {
    $multiplica = array(3, 2, 7, 6, 5, 4, 3, 2);
    $laSuma = 0;
    $final = "";
    $codNumerico = "";
    for ($i = 0; $i < 8; $i++) {
      $numero = rand(1, 9);
      $codNumerico = $codNumerico . $numero;
      $laSuma = $laSuma + ($numero * $multiplica[$i]);
    }
    $result = fmod($laSuma, 11);
    $final = 11 - $result;
    $resultado = array();
    if ($final >= 10)
        $resultado = $this->generaAutorizacion();
    else
        $resultado = array($codNumerico, $final);
    return $resultado;
  }


  function invertirCadena($cadena) {
    $cadenaInvertida = "";
    for ($x = strlen($cadena) - 1; $x >= 0; $x--) {
      $cadenaInvertida = $cadenaInvertida . substr($cadena, $x, 1);
    }
    return $cadenaInvertida;
  }

  function obtenerSumaPorDigitos($cadena) {
      $pivote = 2;
      $longitudCadena = strlen($cadena);
      $cantidadTotal = 0;
      $b = 1;
      for ($i = 0; $i < $longitudCadena; $i++) {
          if ($pivote == 8) {
              $pivote = 2;
          }
          $temporal = intval(substr($cadena, $i, 1));
          $b++;
          $temporal = $temporal * $pivote;
          $pivote++;
          $cantidadTotal = $cantidadTotal + $temporal;
      }
      $cantidadTotal = 11 - $cantidadTotal % 11;
      return $cantidadTotal;
  }
   /**
    * Covierte un porcentaje de descuento en valor.
    * @param $porcentaje|float
    * @param $cantodad|float
    * Devuelve $total.
    */
   public function porcentajeAValores($porcentaje=0, $cantidad=0)
   {
       $porcentaje=$porcentaje;     
       $cantidad1=$cantidad;    
       $resultado=$porcentaje*$cantidad1/100; 
       $total = round(($cantidad1-$resultado) *100)/100;
       return $total;
   }
   
}
