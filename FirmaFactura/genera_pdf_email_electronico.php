<?php
require_once 'conectarServidorFacturacionE.php';
require_once 'variables_globales.php';
require_once 'extras/fs_pdf.php';
require_once 'extras/phpmailer/class.phpmailer.php';
require '../extras/mailgun/vendor/autoload.php';
use Mailgun\Mailgun;
/**
* 
*/
class genera_pdf_email_electronico
{
  public $conexion;
  public $var;
  public $logo;
  public $mensaje;
  
  function __construct()
  {
    
    //Busco las variables Globales para realizar la conexion
    $this->var = new variables_globales();

    //Realizo la conexion a la base de datos
    $this->conexion = mysqli_connect($this->var->FS_DB_HOST, $this->var->FS_DB_USER, $this->var->FS_DB_PASS, $this->var->FS_DB_NAME);
  }

  public function generar_pdf_factura_electronica($factura_b, $numero)
  {    
    // obtenemos datos de la empresa
    $empresa = $this->conexion->query("SELECT * FROM empresa");

    $empresa0 = array();

    //transformo los datos de la consulta a un array
    while ($emp = mysqli_fetch_array($empresa))
    {
      $empresa0[] = $emp;
    }

    /// obtenemos datos del cliente
    $clientes = $this->conexion->query("SELECT * FROM clientes WHERE codcliente = '".$factura_b['codcliente']."'");

    //print_r("SELECT * FROM clientes WHERE codcliente = '".$factura_b[0][8]."'");

    $cliente0 = array();

    //transformo los datos de la consulta a un array
    while ($cli = mysqli_fetch_array($clientes))
    {
      $cliente0[] = $cli;
    }

    /// Creamos el PDF y escribimos sus metadatos
    $pdf_doc = new \fs_pdf();
    $pdf_doc->pdf->addInfo('Title', 'FACTURA '.$factura_b['codigo']);
    $pdf_doc->pdf->addInfo('Subject', 'FACTURA '.$factura_b['codigo']);
    $pdf_doc->pdf->addInfo('Author', $empresa0[0]['nombre']);
    //detalle de la Factura
    $lineasfactura = $this->conexion->query("SELECT * FROM lineasfacturascli WHERE idfactura = '".$factura_b['idfactura']."'");

    //print_r("SELECT * FROM lineasfacturascli WHERE idfactura = '".$factura_b['idfactura']."'");
  
    $lineas = array();

    //transformo los datos de la consulta a un array
    while ($lineas_f = mysqli_fetch_array($lineasfactura))
    {
      $lineas[] = $lineas_f;
    }
    //$lineas = $factura_b->get_lineas();
    $lineas_iva = $pdf_doc->get_lineas_iva($lineas);

    if(!empty($lineas))
    {
      $linea_actual = 0;
      $pagina = 1;

      /// imprimimos las páginas necesarias
      while( $linea_actual < count($lineas) )
      {
        $lppag = 20;

        /// salto de página
        if($linea_actual > 0)
        {
          $pdf_doc->pdf->ezNewPage();
        }

        /*
        * Creamos la cabecera de la página, en este caso para el modelo carta
        */
        /// esta es la cabecera de la página para el modelo 'simple'
        $pdf_doc->generar_pdf_cabecera_fac_elec($empresa0, $lppag, $factura_b, $numero);
        $this->generar_pdf_datos_empresa_fac_elec($pdf_doc, $lppag, $factura_b, $empresa0, $cliente0);
        $this->generar_pdf_datos_cliente_fac_elec($pdf_doc, $lppag, $factura_b, $cliente0,$numero);
        $this->generar_pdf_lineas_fac_elec($pdf_doc, $lineas, $linea_actual, $lppag, $factura_b, $cliente0);
            
        /*if( $linea_actual == count($lineas) )
        {
          if( !$factura_b->pagada AND $this->impresion['print_formapago'] )
          {
            $fp0 = new forma_pago();
            $forma_pago = $fp0->get($factura_b->codpago);
            if($forma_pago)
            {
              $texto_pago = "\n<b>Forma de pago</b>: ".$forma_pago->descripcion;
              if(!$forma_pago->imprimir)
              {
                        /// nada
              }
              else if($forma_pago->domiciliado)
              {
                $cbc0 = new cuenta_banco_cliente();
                $encontrada = FALSE;
                foreach($cbc0->all_from_cliente($factura_b->codcliente) as $cbc)
                {
                  $texto_pago .= "\n<b>Domiciliado en</b>: ";
                  if($cbc->iban)
                  {
                    $texto_pago .= $cbc->iban(TRUE);
                  }

                  if($cbc->swift)
                  {
                    $texto_pago .= "\n<b>SWIFT/BIC</b>: ".$cbc->swift;
                  }
                  
                  $encontrada = TRUE;
                  break;
                }
              
                if(!$encontrada)
                {
                  $texto_pago .= "\n<b>El cliente no tiene cuenta bancaria asignada.</b>";
                }
              }
              else if($forma_pago->codcuenta)
              {
                $cb0 = new cuenta_banco();
                $cuenta_banco = $cb0->get($forma_pago->codcuenta);
                if($cuenta_banco)
                {
                  if($cuenta_banco->iban)
                  {
                    $texto_pago .= "\n<b>IBAN</b>: ".$cuenta_banco->iban(TRUE);
                  }

                  if($cuenta_banco->swift)
                  {
                    $texto_pago .= "\n<b>SWIFT o BIC</b>: ".$cuenta_banco->swift;
                  }
                }
              }

              $texto_pago .= "\n<b>Vencimiento</b>: ".$factura_b->vencimiento;
              $pdf_doc->pdf->ezText($texto_pago, 9);
            }
          }
        }

        $pdf_doc->set_y(80);
        //$this->generar_pdf_totales_fac_elec($pdf_doc, $lineas_iva, $pagina);
        /// pié de página para la factura
        if($empresa0->pie_factura)
        {
          $pdf_doc->pdf->addText(10, 10, 8, $pdf_doc->center_text($pdf_doc->fix_html($empresa0->pie_factura), 180) );
        }*/

        $pagina++;
      }
    }else{
      $pdf_doc->pdf->ezText('¡Factura sin líneas!', 20);
    }

    if(!file_exists('../documentosElectronicos/enviados/facturasElectronicas/pdfs_enviados/') )
    {
      mkdir('../documentosElectronicos/enviados/facturasElectronicas/pdfs_enviados/');
    }
    
    $pdf_doc->save('../documentosElectronicos/enviados/facturasElectronicas/pdfs_enviados/'.$factura_b['codigo'].'.pdf');
  }

  // GENERAR DATOS CLIENTE FACTURA ELECTRONICA SRi

  public function generar_pdf_datos_cliente_fac_elec(&$pdf_doc, &$lppag, &$factura, &$cliente0,$numero)
  {
    //print_r($cliente0);
    $tipo_doc = 'Factura';
    
    $tipoidfiscal = 'RUC';
    if(!empty($cliente0))
    {
      $tipoidfiscal = $cliente0[0]['tipoidfiscal'];
    }
    
    /*
    * Esta es la tabla con los datos del cliente:
    * Albarán:                 Fecha:
    * Cliente:               CIF/NIF:
    * Dirección:           Teléfonos:
    */
    
    $pdf_doc->new_table();
    $pdf_doc->add_table_row(
      array(
        'campo1' => "<b>Razón Social / Nombres y Apellidos:</b> ".$cliente0[0]['nombre'],
        'dato1' => '<b>Guía Remisión: </b>',
      )
    );
      

    $dets = $this->conexion->query("SELECT * FROM lineasfacturascli WHERE idfactura = ".$factura['idfactura']);
    $arr_det = array();
    //transformo a array
    while ($det = mysqli_fetch_array($dets))
    {
      $arr_det[] = $det;
    }

    if(!empty($arr_det))
    {
      $arrList = array();
      $arrList = explode("/:", $arr_det[0]['detalles']);
      $pdf_doc->add_table_row(
        array(
          'campo1' => "<b>Identificación:</b> ".$cliente0[0]['cifnif'],
          'dato1' => $numero,
        )
      );
      $pdf_doc->add_table_row(
        array(
          'campo1' => "<b>Fecha de Emisión:</b> ".$factura['fecha'],
          'dato1' => "OC: ".$arrList[10],
        )
      );      
    }else{
      $pdf_doc->add_table_row(
        array(
          'campo1' => "<b>Identificación:</b> ".$cliente0[0]['cifnif'],
          'dato1' => '',
        )
      );
      $pdf_doc->add_table_row(
        array(
          'campo1' => "<b>Fecha de Emisión:</b> ".$factura['fecha'],
          'dato1' => '',
        )
      );      
    }


    // TODO DE DIRECCION DEL CLIENTE
    $direccion = $factura['direccion'];

    //Busco la provincia
    $provs = $this->conexion->query("SELECT * FROM provincia WHERE padre IS NULL AND codigo = '".$factura['provincia']."'");    
    $arr_prov = array();
    //transformo a array
    while ($prov = mysqli_fetch_array($provs))
    {
      $arr_prov[] = $prov;
    }

    //print_r($arr_prov);

    $provincia="";
    if(!empty($arr_prov))
    {
      $provincia = $arr_prov[0]['nombre'];
    }

    //Busco la ciudad
    $ciuds = $this->conexion->query("SELECT * FROM provincia WHERE codigo = '".$factura['ciudad']."'");
    $arr_ciu = array();
    //transformo a array
    while ($ciud = mysqli_fetch_array($ciuds))
    {
      $arr_ciu[] = $ciud;
    }

    $ciudad="";

    if(!empty($arr_ciu))
    {
      $ciudad = $arr_ciu[0]['nombre'];
    }


    $direccion .= ' - '.$provincia.' ('.$ciudad.')';
    //print_r($factura->ciudad);
    //exit();
    $row = array(
      'campo1' => "<b>Dirección:</b> ".$pdf_doc->fix_html($direccion),
      'dato1' => '',
    );

    $pdf_doc->add_table_row($row);
    $pdf_doc->save_table(
      array(
        'cols' => array(
          'campo1' => array('width' => 350, 'justification' => 'left'),
          'dato1' => array('justification' => 'left'),
        ),
        'showLines' => 0,
        'width' => 550,
        'shaded' => 0, 
        'xPos'=>'left',
        'xOrientation'=>'right',
        'showLines'=> 1,
        'showHeadings' => 0,
        'fontSize' =>8
      )
    );
    
    $pdf_doc->pdf->ezText("\n", 5);
  }
  
  // GENERAR DATOS EMPRESA FACTURA ELECTRONICA SRi

  public function generar_pdf_datos_empresa_fac_elec(&$pdf_doc, &$lppag, &$factura, &$empresa0, &$cliente0)
  {
    $pdf_doc->pdf->ezText("\n", 12);
    $pdf_doc->pdf->ezText("\n", 5);
    //$tipo_doc = ucfirst(FS_ALBARAN);
    $rectificativa = FALSE;
    $tipo_doc = 'Factura';
    
    $tipoidfiscal = 'RUC';
    if(!empty($cliente0))
    {
      $tipoidfiscal = $cliente0[0]['tipoidfiscal'];
    }

    $pdf_doc->new_table();
    $pdf_doc->add_table_row(
      array(
      'campo1' => "<b>".$empresa0[0]['nombre']."</b> ",
      )
    );

    $pdf_doc->add_table_row(
      array(
        'campo1' => "\n"."<b>Dirección Matríz:</b> ".$empresa0[0]['direccion'],
      )
    );

    // Para la sucursal consultamos la direccion del los establecimietos
    $Establec = $this->conexion->query("SELECT e.direccion FROM almacenes a, establecimiento e WHERE a.codalmacen = '".$factura['codalmacen']."' AND a.codestablecimiento = e.codestablecimiento");

    $establecimientos = array();

    //transformo los datos de la consulta a un array
    while ($esta = mysqli_fetch_array($Establec))
    {
      $establecimientos[] = $esta;
    }

    $direccionEsta = $establecimientos[0]['direccion'];

    $diresta = '';
    if(strlen($empresa0[0]['direccion'])<36)
    {
      $diresta = "\n";  
    }
    
    $diresta .= "<b>Dirección Sucursal:</b> ".$direccionEsta;
      $pdf_doc->add_table_row(
        array(
          'campo1' => $diresta,
        )
      );

    $contri = '';
    
    if(strlen($direccionEsta)<39)
    {
      $contri .= "\n";
    }
    
    // VALIDO CONTRIBUYENTE y MUESTRO
    
    $nrocontrib = $this->var->FS_CONTRIBUYENTE_NRO;
    if ($nrocontrib=='000')
    {
      $contri .= "<b>Contribuyente Especial :</b> NO";
    }else{
      $contri .= "<b>Contribuyente Especial Nro:</b> ".$nrocontrib;
    }

    $pdf_doc->add_table_row(
      array(
        'campo1' => $contri,
      )
    );

    // VALIDO SI ESTA OBLIGADO A LLEVAR CONTABILIDAD
    if ($this->var->FS_OB_CONTA==1)
    {
      $ob_conta = 'NO';
    }

    if ($this->var->FS_OB_CONTA==2)
    {
      $ob_conta = 'SI';
    } 

    $pdf_doc->add_table_row(
      array(
        'campo1' => "\n"."<b>OBLIGADO A LLEVAR CONTABILIDAD:</b> ".$ob_conta,
      )
    );

    $pdf_doc->pdf->ezText("\n",10);
    $pdf_doc->save_table(
      array(
        'cols' => array(
          'campo1' => array('justification' => 'left'),
        ),
        'showLines' => 0,
        'width' => 250,
        'shaded' => 0, 
        'xPos'=>'left',
        'xOrientation'=>'right',
        'showLines'=> 1,
        'showHeadings' => 0,
        'fontSize' =>8
      )
    );
    
    $pdf_doc->pdf->ezText("\n", 3);
  }

// GENERAR LINEAS PDF FACTURA ELECTRONICA SRI

public function generar_pdf_lineas_fac_elec(&$pdf_doc, &$lineas, &$linea_actual, &$lppag, $factura, &$cliente0)
{
  /// calculamos el número de páginas
  //$this->impresion = $impresion;
  if( !isset($this->numpaginas) )
  {
    $this->numpaginas = 0;
    $linea_a = 0;
    while( $linea_a < count($lineas) )
    {
      $lppag2 = $lppag;
      foreach($lineas as $i => $lin)
      {
        if($i >= $linea_a AND $i < $linea_a + $lppag2)
        {
          $linea_size = 1;
          $len = mb_strlen($lin['referencia'].' '.$lin['descripcion']);
          while($len > 85)
          {
            $len -= 85;
            $linea_size += 0.5;
          }

          $aux = explode("\n", $lin['descripcion']);
          if( count($aux) > 1 )
          {
            $linea_size += 0.5 * ( count($aux) - 1);
          }

          if($linea_size > 1)
          {
            $lppag2 -= $linea_size - 1;
          }
        }
      }

      $linea_a += $lppag2;
      $this->numpaginas++;
    }

    if($this->numpaginas == 0)
    {
      $this->numpaginas = 1;
    }
  }

  $dec_cantidad = 0;
  $multi_iva = FALSE;
  $multi_re = FALSE;
  $multi_irpf = FALSE;
  $iva = FALSE;
  $re = FALSE;
  $irpf = FALSE;

  $detalles = "";
  /// leemos las líneas para ver si hay que mostrar los tipos de iva, re o irpf
  foreach($lineas as $i => $lin)
  {
    //recupero el detalle de Lacfarma
    $detalles = $lin['detalles'];

    if( $lin['cantidad'] != intval($lin['cantidad']) )
    {
      $dec_cantidad = 2;
    }

    if($iva === FALSE)
    {
      $iva = $lin['iva'];
    }
    else if($lin['iva'] != $iva)
      {
        $multi_iva = TRUE;
      }

    /// restamos líneas al documento en función del tamaño de la descripción
    if($i >= $linea_actual AND $i < $linea_actual+$lppag)
    {
      $linea_size = 1;
      $len = mb_strlen($lin['referencia'].' '.$lin['descripcion']);
      while($len > 85)
      {
        $len -= 85;
        $linea_size += 0.5;
      }
      $aux = explode("\n", $lin['descripcion']);
      
      if( count($aux) > 1 )
      {
        $linea_size += 0.5 * ( count($aux) - 1);
      }

      if($linea_size > 1)
      {
        $lppag -= $linea_size - 1;
      }
    }
  }

  /*
  * Creamos la tabla con las lineas del documento
  */
  
  $pdf_doc->new_table();

  if($detalles!="")
      {
        $arrList = array();
        $arrList = explode("/:", $detalles);
        $arrLote = explode("%", $arrList[4]);
        //$arrFabr = explode("%", $arrList[5]);
        //$arrVenc = explode("%", $arrList[6]);
        if(count($arrLote) == 1)
        {
          $table_header = array(
              //'alb' => '<b>'.ucfirst(FS_ALBARAN).'</b>',
              'ref' => '<b>Cod. Principal</b>',
              'cantidad' => '<b>Cant.</b>',
              'descripcion' => '<b>Descripción</b>',
              'detalle' => '<b>Detalle Adicional</b>',            
              'pvp' => '<b>Precio Unitario</b>',
          );          
        }elseif(count($arrLote) == 2){
            $table_header = array(
              //'alb' => '<b>'.ucfirst(FS_ALBARAN).'</b>',
              'ref' => '<b>Cod. Principal</b>',
              'cantidad' => '<b>Cant.</b>',
              'descripcion' => '<b>Descripción</b>',
              'detalle' => '<b>Detalle Adicional</b>',
              'detalle1' => '<b>Detalle Adicional</b>',            
              'pvp' => '<b>Precio Unitario</b>',
            );
          }elseif(count($arrLote) == 3){
            $table_header = array(
              //'alb' => '<b>'.ucfirst(FS_ALBARAN).'</b>',
              'ref' => '<b>Cod. Principal</b>',
              'cantidad' => '<b>Cant.</b>',
              'descripcion' => '<b>Descripción</b>',
              'detalle' => '<b>Detalle Adicional</b>',
              'detalle1' => 'Detalle Adicional',
              'detalle2' => '<b>Detalle Adicional</b>',
              'pvp' => '<b>Precio Unitario</b>',
            );
          }
      }else{
        $table_header = array(
            //'alb' => '<b>'.ucfirst(FS_ALBARAN).'</b>',
            'ref' => '<b>Cod. Principal</b>',
            'cantidad' => '<b>Cant.</b>',
            'descripcion' => '<b>Descripción</b>',
            'pvp' => '<b>Precio Unitario</b>',
        );
      }
      
  $table_header['dto'] = '<b>Descuento</b>';
  
  if($multi_iva)
  {
    $table_header['iva'] = '<b>Iva</b>';
  }

  $table_header['importe'] = '<b>Precio Total</b>';
  
  $pdf_doc->add_table_header($table_header);
  $totaldescuento = 0.00;
  $subtotal0por = 0.00;
  for($i = $linea_actual; (($linea_actual < ($lppag + $i)) AND ($linea_actual < count($lineas)));)
  {
    $detalles1 = $lineas[$linea_actual]['detalles'];
    $descripcion = $pdf_doc->fix_html($lineas[$linea_actual]['descripcion']);
    /*if( !is_null($lineas[$linea_actual]->referencia) )
    {
      $descripcion = '<b>'.$lineas[$linea_actual]->referencia.'</b> '.$descripcion;
    }*/

    /// ¿El articulo tiene trazabilidad?
    //$descripcion .= $this->generar_trazabilidad($lineas[$linea_actual]);
    // SETEAMOS LA REFERENCIA
    if( !is_null($lineas[$linea_actual]['referencia']) )
    {
      $ref = '<b>'.$lineas[$linea_actual]['referencia'].'</b> ';
    }

    // CALCULAMOS DESCUENTO
    $descuento = ( round($lineas[$linea_actual]['pvpsindto'],2) - $lineas[$linea_actual]['pvptotal']) ;
    $totaldescuento += $descuento;

    if($detalles1!="")
    {
      $arrList = array();
      $arrList = explode("/:", $detalles1);
      $arrLote = explode("%", $arrList[4]);
      $arrFabr = explode("%", $arrList[5]);
      $arrVenc = explode("%", $arrList[6]);
      if(count($arrLote) == 1)
      {
        $fila = array(
          'ref' => $ref,
          'cantidad' => $this->show_numero($lineas[$linea_actual]['cantidad'], $dec_cantidad),
          'descripcion' => utf8_decode($descripcion),
          'detalle' => "Lote: ".$arrLote[0]."\nFab: ".$arrFabr[0]."\nVen: ".$arrVenc[0],
          'pvp' => $this->show_precio($lineas[$linea_actual]['pvpunitario'], 'USD', 5),
          // DESCUENTO
          //'dto' => $this->show_numero($lineas[$linea_actual]->dtopor) . " %",
          'dto' => $this->show_precio($descuento, 'USD', 2),
          'iva' => $this->show_numero($lineas[$linea_actual]['iva']) . " %",
          'importe' => $this->show_precio($lineas[$linea_actual]['pvptotal'], 'USD',2)
        );
      }elseif(count($arrLote) == 2){
          $fila = array(
            'ref' => $ref,
            'cantidad' => $this->show_numero($lineas[$linea_actual]['cantidad'], $dec_cantidad),
            'descripcion' => utf8_decode($descripcion),
            'detalle' => "Lote: ".$arrLote[0]."\nFab: ".$arrFabr[0]."\nVen: ".$arrVenc[0],
            'detalle1' => "Lote: ".$arrLote[1]."\nFab: ".$arrFabr[1]."\nVen: ".$arrVenc[1],
            'pvp' => $this->show_precio($lineas[$linea_actual]['pvpunitario'], 'USD', 5),
            // DESCUENTO
            //'dto' => $this->show_numero($lineas[$linea_sizeactual]->dtopor) . " %",
            'dto' => $this->show_precio($descuento, 'USD', 2),
            'iva' => $this->show_numero($lineas[$linea_actual]['iva']) . " %",
            'importe' => $this->show_precio($lineas[$linea_actual]['pvptotal'], 'USD',2)
          );
        }elseif(count($arrLote) == 3){
          $fila = array(
            'ref' => $ref,
            'cantidad' => $this->show_numero($lineas[$linea_actual]['cantidad'], $dec_cantidad),
            'descripcion' => utf8_decode($descripcion),
            'detalle' => "Lote: ".$arrLote[0]."\nFab: ".$arrFabr[0]."\nVen: ".$arrVenc[0],
            'detalle1' => "Lote: ".$arrLote[1]."\nFab: ".$arrFabr[1]."\nVen: ".$arrVenc[1],
            'detalle2' => "Lote: ".$arrLote[2]."\nFab: ".$arrFabr[2]."\nVen: ".$arrVenc[2],
            'pvp' => $this->show_precio($lineas[$linea_actual]['pvpunitario'], 'USD', 5),
            // DESCUENTO
            //'dto' => $this->show_numero($lineas[$linea_actual]->dtopor) . " %",
            'dto' => $this->show_precio($descuento, 'USD', 2),
            'iva' => $this->show_numero($lineas[$linea_actual]['iva']) . " %",
            'importe' => $this->show_precio($lineas[$linea_actual]['pvptotal'], 'USD',2)
          );
        }
    }else{
      $fila = array(
        'ref' => $ref,
        'cantidad' => $this->show_numero($lineas[$linea_actual]['cantidad'], $dec_cantidad),
        'descripcion' => utf8_decode($descripcion),

        'pvp' => $this->show_precio($lineas[$linea_actual]['pvpunitario'], 'USD', 5),
        // DESCUENTO
        //'dto' => $this->show_numero($lineas[$linea_actual]->dtopor) . " %",
        'dto' => $this->show_precio($descuento, 'USD', 2),
        'iva' => $this->show_numero($lineas[$linea_actual]['iva']) . " %",
        'importe' => $this->show_precio($lineas[$linea_actual]['pvptotal'], 'USD', 2)
      );      
    }

    if($lineas[$linea_actual]['dtopor'] == 0)
    {
      $fila['dto'] = '';
    }    
    
    $pdf_doc->add_table_row($fila);
    $linea_actual++;
  }

    if($detalles!="")
      {
        $pdf_doc->save_table(
              array(
                  'cols' => array(
                      // REFERENCIA
                      'ref' => array('width'=>40,'justification' => 'center'),
                      // CANTIDAD
                      'cantidad' => array('width'=>40,'justification' => 'center'),
                      // DESCRIPCION
                      'descripcion' => array('width'=>150,'justification' => 'left'),
                      // PRECIO UNITARIO
                      'pvp' => array('justification' => 'center'),
                      // DESCUENTO
                      'dto' => array('width'=>50,'justification' => 'center'),
                      'iva' => array('justification' => 'right'),
                      're' => array('justification' => 'right'),
                      'irpf' => array('justification' => 'right'),
                      'importe' => array('justification' => 'right')
                  ),
                  'fontSize' => 7,
                  'width' => 550,
                  'shaded' => 1,
                  'shadeCol' => array(0.95, 0.95, 0.95),
                  'xPos'=>'left',
                  'xOrientation'=>'right',
                  'lineCol' => array(0.3, 0.3, 0.3),
                  'showLines' => 2,
              )
        );

      }else{

        $pdf_doc->save_table(
              array(
                  'cols' => array(
                      // REFERENCIA
                      'ref' => array('width'=>40,'justification' => 'center'),
                      // CANTIDAD
                      'cantidad' => array('width'=>40,'justification' => 'center'),
                      // DESCRIPCION
                      'descripcion' => array('width'=>300,'justification' => 'left'),
                      // PRECIO UNITARIO
                      'pvp' => array('justification' => 'center'),
                      // DESCUENTO
                      'dto' => array('width'=>50,'justification' => 'center'),
                      'iva' => array('justification' => 'right'),
                      're' => array('justification' => 'right'),
                      'irpf' => array('justification' => 'right'),
                      'importe' => array('justification' => 'right')
                  ),
                  'fontSize' => 7,
                  'width' => 550,
                  'shaded' => 1,
                  'shadeCol' => array(0.95, 0.95, 0.95),
                  'xPos'=>'left',
                  'xOrientation'=>'right',
                  'lineCol' => array(0.3, 0.3, 0.3),
                  'showLines' => 2,
              )
        );
      }

    /// ¿Última página?
    if( $linea_actual == count($lineas) )
    {
      // TABLA DE TOTALES
      $condicionPorIva = '12%';
      
      if($detalles!="")
      {
        $pdf_doc->set_y(260);
      }else{
        $pdf_doc->set_y(220);
      }
      
      $pdf_doc->new_table();

      $pdf_doc->add_table_row(
        array(
          'campo' => "<b>SUBTOTAL</b>",
          'valor' => $this->getConIva($factura, 'USD', 2),
        )
      );

      $pdf_doc->add_table_row(
        array(
          'campo' => "<b>SUBTOTAL IVA 0%</b>",
          'valor' => $this->getSinIva($factura, 'USD', 2),
        )
      );
      
      $totaldescuento = ($totaldescuento * 100) / 100; 

      $pdf_doc->add_table_row(
        array(
          'campo' => "<b>DESCUENTO</b>",
          'valor' => $this->show_precio($totaldescuento, 'USD', 2),
        )
      );

      $pdf_doc->add_table_row(
        array(
          'campo' => "<b>IVA $condicionPorIva</b>",
          'valor' => $this->show_precio($factura['totaliva'], 'USD', 2),
        )
      );

      $pdf_doc->add_table_row(
        array(
          'campo' => "<b>VALOR TOTAL</b>",
          'valor' => '<b>'.$this->show_precio($factura['total'], 'USD', 2).'</b>',
        )
      );

      $pdf_doc->save_table(
        array(
          'cols' => array(
            'campo' => array('justification' => 'left'),
            'valor' => array('justification' => 'right'),
          ),
          'width' => 200,
          'shaded' => 0, 
          'xPos'=>'right',
          'xOrientation'=>'left',
          'showLines'=> 2,
          'showHeadings' => 0,
          'fontSize' =>8
        )
      );

      // TABLA DE INFORMACION ADICIONAL
      if($detalles!="")
      {
        $pdf_doc->set_y(260);
      }else{
        $pdf_doc->set_y(220);
      }
      $pdf_doc->new_table();

      // HEADER DE INFORMACION ADICIONAL
      $pdf_doc->add_table_row(
        array(
          'campo1' => "<b>INFORMACIÓN ADICIONAL</b>",
        )
      );

      // TODO DE DIRECCION DEL CLIENTE
      $direccion = $factura['direccion'];
      
      //Busco la provincia
    $provs = $this->conexion->query("SELECT * FROM provincia WHERE padre IS NULL AND codigo = '".$factura['provincia']."'");
    $arr_prov = array();
    //transformo a array
    while ($prov = mysqli_fetch_array($provs))
    {
      $arr_prov[] = $prov;
    }

    $provincia="";
    
    if(!empty($arr_prov))
    {
      $provincia = $arr_prov[0]['nombre'];
    }

    //Busco la ciudad
    $ciuds = $this->conexion->query("SELECT * FROM provincia WHERE codigo = '".$factura['ciudad']."'");
    $arr_ciu = array();
    //transformo a array
    while ($ciud = mysqli_fetch_array($ciuds))
    {
      $arr_ciu[] = $ciud;
    }

    $ciudad="";

    if(!empty($arr_ciu))
    {
      $ciudad = $arr_ciu[0]['nombre'];
    }

    $direccion .= ' - '.$provincia.' ('.$ciudad.')';

      $row = array(
        'campo1' => "<b>Dirección:</b> ".$pdf_doc->fix_html($direccion),
      );

      $pdf_doc->add_table_row($row);

      // VERIFICO SI TIENE TELF.
      if (empty($cliente0[0]['telefono1']))
      {
        $telf = 'N/A';
      }else{
        $telf = $cliente0[0]['telefono1'];
        if (!empty($cliente0[0]['telefono2']))
        {
          $telf .= ' / '.$cliente0[0]['telefono2'];
        }
      }

      $pdf_doc->add_table_row(
        array(
          'campo1' => "<b>Telf. de Contacto:</b> ".$telf,
        )
      );

      // VERIFICO SI EL CLIENTE TIENE EMAIL
      if (empty($cliente0[0]['email']))
      {
        $email = 'N/A';
      }else{
        $email = $cliente0[0]['email'];
      }

      $pdf_doc->add_table_row(
        array(
          'campo1' => "<b>Email: </b> ".$email,
        )
      );

      // VERIFICO SI EL CLIENTE TIENE OBSERVACIONES
      $observacion = 'Ninguna';

      $pdf_doc->add_table_row(
        array(
          'campo1' => "<b>Observaciones: </b> ".$pdf_doc->fix_html($observacion),
        )
      );

      if($detalles!="")
      {
        $arrList = array();
        $arrList = explode("/:", $detalles);
        $arrFab = explode("%", $arrList[5]);
        $arrVen = explode("%", $arrList[6]);

        if(!empty($arrList))
        {
          $pdf_doc->add_table_row(
            array(
              'campo1' => "<b>Nombre Comercial: </b> ".$pdf_doc->fix_html($arrList[1]),
            )
          );
          $pdf_doc->add_table_row(
            array(
              'campo1' => "<b>Nombre Genérico: </b> ".$pdf_doc->fix_html($arrList[2]),
            )
          );
          $pdf_doc->add_table_row(
            array(
              'campo1' => "<b>Presentación: </b> ".$pdf_doc->fix_html($arrList[3]),
            )
          );
          $pdf_doc->add_table_row(
            array(
              'campo1' => "<b>Registro Sanitario: </b> ".$pdf_doc->fix_html($arrList[7]),
            )
          );
          $pdf_doc->add_table_row(
            array(
              'campo1' => "<b>Procedencia: </b> ".$pdf_doc->fix_html($arrList[8]),
            )
          );            
        }
        
      }


      $pdf_doc->save_table(
        array(
          'cols' => array(
            'campo1' => array('justification' => 'left'),
          ),
          'showLines' => 0,
          'width' => 340,
          'shaded' => 0, 
          'xPos'=>'left',
          'xOrientation'=>'right',
          'showLines'=> 1,
          'showHeadings' => 0,
          'fontSize' =>8
        )
      );

      // TABLA DE FORMA DE PAGO
      $pdf_doc->new_table();

      $pdf_doc->add_table_row(
        array(
          'campo' => "<b>FORMA DE PAGO</b>",
          'valor' => "<b>VALOR</b>",
        )
      );

      // VERIFICO EL CODIGO DEL TIPO DE PAGO
      $pagos = $this->conexion->query("SELECT * FROM formaspago WHERE codpago = '".$factura['codpago']."'");

      $forma_pago = array();

      //transformo los datos de la consulta a un array
      while ($cli = mysqli_fetch_array($pagos))
      {
        $forma_pago[] = $cli;
      }      
      
      if(!empty($forma_pago))
      {
        $texto_pago = $forma_pago[0]['descripcion'];
      }else{
        $texto_pago = 'N/A';
      }

      if($detalles!="")
      {
        $pdf_doc->add_table_row(
          array(
            'campo' => $pdf_doc->fix_html($arrList[9]),
            'valor' => '<b>'.$this->show_precio($factura['total'], 'USD', 2).'</b>',
          )
        );
      }else{
        $pdf_doc->add_table_row(
          array(
            'campo' => $texto_pago,
            'valor' => '<b>'.$this->show_precio($factura['total'], 'USD', 2).'</b>',
          )
        );
      }

      $pdf_doc->save_table(
        array(
          'cols' => array(
            'campo' => array('justification' => 'center'),
            'valor' => array('justification' => 'right'),
          ),
          'width' => 200,
          'shaded' => 0, 
          'xPos'=>'left',
          'xOrientation'=>'right',
          'showLines'=> 2,
          'showHeadings' => 0,
          'fontSize' =>8
        )
      );
    }
  }

  /*--------------------------------------------------------------------------
  *           GENERAMOS PDF RENTENCION ELECTRONICA                          *
  --------------------------------------------------------------------------
  */
  public function generar_pdf_retencion_electronica($factura)
  {
    /// obtenemos datos de la empresa
    $empresa = $this->conexion->query("SELECT * FROM empresa");
    $empresa0 = mysqli_fetch_array($empresa);
    /// obtenemos datos del cliente
    $proveedor = $this->conexion->query("SELECT * FROM proveedores WHERE codproveedor = '".$factura['codproveedor']."'");
    $proveedor0 = mysqli_fetch_array($proveedor);
    /// Creamos el PDF y escribimos sus metadatos
    $pdf_doc = new fs_pdf();
    $pdf_doc->pdf->addInfo('Title', ' RETENCION'. $factura['codigo']);
    $pdf_doc->pdf->addInfo('Subject', 'RETENCION ' . $factura['codigo']);
    $pdf_doc->pdf->addInfo('Author', $empresa0['nombre']);

    $consulta = $this->conexion->query("SELECT * FROM lineasfacturasprov WHERE idfactura = '".$factura['idfactura']."'");
    $lineas = array();
    while ($l = mysqli_fetch_array($consulta)) {
      $lineas[] = $l;
    }
    // $lineas = mysqli_fetch_all($consulta,MYSQLI_ASSOC);


    $lineas_iva = $pdf_doc->get_lineas_iva($lineas);

    $pdf_doc->generar_pdf_cabecera_fac_elec_ret($empresa0, $lppag, $factura);

    $this->generar_pdf_datos_empresa_fac_ret($factura, $pdf_doc, $empresa0, $proveedor0);
    $this->generar_pdf_datos_proveedor_fac_ret($pdf_doc, $factura, $empresa0, $proveedor0);
    $this->generar_pdf_lineas_fac_ret($pdf_doc, $factura, $lineas, $empresa0, $proveedor0);

    /// pié de página para la factura
    if($empresa0['pie_factura'])
    {
      $pdf_doc->pdf->addText(10, 10, 8, $pdf_doc->center_text($pdf_doc->fix_html($empresa0->pie_factura), 180) );
    }

    //$pdf_doc->show(FS_FACTURA.'_'.$factura->codigo.'.pdf');

    if( !file_exists('../documentosElectronicos/enviados/retencionesElectronicas/pdfs_enviados/') )
    {
      mkdir('../documentosElectronicos/enviados/retencionesElectronicas/pdfs_enviados/');
    }
    $pdf_doc->save('../documentosElectronicos/enviados/retencionesElectronicas/pdfs_enviados/'.$factura['codigo'].'.pdf');
  }

  // GENERAR DATOS EMPRESA FACTURA ELECTRONICA RETENCION SRi
  public function generar_pdf_datos_empresa_fac_ret(&$factura,&$pdf_doc,&$empresa0,&$proveedor0)
  {
    $pdf_doc->pdf->ezText("\n", 8);
    $pdf_doc->pdf->ezText("\n", 8);
    // $tipo_doc = ucfirst($this->FS_ALBARAN);
    $rectificativa = FALSE;
    $tipo_doc = 'Retencion';

    $tipoidfiscal = 'RUC';
    if(!empty($proveedor0)){
      $tipoidfiscal = $proveedor0['tipoidfiscal'];
    }

    $pdf_doc->new_table();
    $pdf_doc->add_table_row(
      array(
        'campo1' => "<b>".$empresa0['nombre']."</b> ",
      )
    );

    $pdf_doc->add_table_row(
      array(
        'campo1' => "\n"."<b>Dirección Matríz:</b> ".$empresa0['direccion'],
      )
    );

    // Para la sucursal consultamos la direccion del los establecimietos
    $consulta = $this->conexion->query("SELECT * FROM almacenes WHERE codalmacen = '".$factura['codalmacen']."' ORDER BY codalmacen ASC;");
    $almacen = mysqli_fetch_array($consulta);
    $consulta = $this->conexion->query("SELECT * FROM establecimiento WHERE codestablecimiento = '".$almacen['codestablecimiento']."';");
    $direccionEsta = mysqli_fetch_array($consulta);
    $direccionEsta = $direccionEsta['direccion'];
    $pdf_doc->add_table_row(
      array(
        'campo1' => "\n"."<b>Dirección Sucursal:</b> ".$direccionEsta,
      )
    );
    
    // VALIDO CONTRIBUYENTE y MUESTRO
    $nrocontrib = $this->var->FS_CONTRIBUYENTE_NRO;
    if ($nrocontrib=='000')
    {
      $pdf_doc->add_table_row(
        array(
          'campo1' => "\n"."<b>Contribuyente Especial :</b> NO",
        )
      );
    }else{
      $pdf_doc->add_table_row(
        array(
          'campo1' => "\n"."<b>Contribuyente Especial Nro:</b> ".$nrocontrib,
        )
      );
    }

    // VALIDO SI ESTA OBLIGADO A LLEVAR CONTABILIDAD
    if ($this->var->FS_OB_CONTA==1)
    {
      $ob_conta = 'NO';
    }

    if ($this->var->FS_OB_CONTA==2)
    {
      $ob_conta = 'SI';
    } 

    $pdf_doc->add_table_row(
      array(
        'campo1' => "\n"."<b>OBLIGADO A LLEVAR CONTABILIDAD:</b> ".$ob_conta,
      )
    );

    $pdf_doc->pdf->ezText("\n",10);
    
    $pdf_doc->save_table(
      array(
        'cols' => array(
          'campo1' => array('justification' => 'left'),
        ),
        'showLines' => 0,
        'width' => 250,
        'shaded' => 0, 
        'xPos'=>'left',
        'xOrientation'=>'right',
        'showLines'=> 1,
        'showHeadings' => 0,
        'fontSize' =>8
      )
    );

    $pdf_doc->pdf->ezText("\n", 3);
  }

   // GENERAR DATOS CLIENTE FACTURA ELECTRONICA RETENCION SRi

  public function generar_pdf_datos_proveedor_fac_ret(&$pdf_doc,&$factura,&$empresa0,&$proveedor0)
  {

    $pdf_doc->new_table();
    $pdf_doc->add_table_row(
      array(
        'campo1' => "<b>Razón Social / Nombres y Apellidos:</b> ".$proveedor0['nombre'],
      )
    );

    $pdf_doc->add_table_row(
      array(
       'campo1' => "<b>Identificación:</b> ".$proveedor0['cifnif'],
      )
    );

    $pdf_doc->add_table_row(
      array(
        'campo1' => "<b>Fecha de Emisión:</b> ".$factura['fecha'],
      )
    );

    //Agrego la Direccion del proveedor
    $consulta = $this->conexion->query("SELECT * FROM dirproveedores WHERE codproveedor = '".$proveedor0['codproveedor']."';");
    $direc = mysqli_fetch_array($consulta);
    $direccion = $direc['direccion'];

    $pdf_doc->add_table_row(
      array(
        'campo1' => "<b>Dirección:</b> ".$pdf_doc->fix_html($direccion),
      )
    );

    $pdf_doc->save_table(
      array(
        'cols' => array(
          'campo1' => array('justification' => 'left'),
        ),
        'showLines' => 0,
        'width' => 550,
        'shaded' => 0, 
        'xPos'=>'left',
        'xOrientation'=>'right',
        'showLines'=> 1,
        'showHeadings' => 0,
        'fontSize' =>8
      )
    );
    $pdf_doc->pdf->ezText("\n", 5);
  }

  // GENERAR LINEAS PDF FACTURA ELECTRONICA RETENCION SRI

  public function generar_pdf_lineas_fac_ret(&$pdf_doc,&$factura,&$lineas,&$empresa0,&$proveedor0)
  {
    // DETALLE DE LA RETENCION
    $pdf_doc->new_table();

    $pdf_doc->add_table_row(
      array(
        'campo1' => "<b>Comprobante</b>",
        'campo2' => "<b>Número</b>",
        'campo3' => "<b>Fecha Emisión</b>",
        'campo4' => "<b>Ejercicio Fiscal</b>",
        'campo5' => "<b>Base Imponible para la Retención</b>",
        'campo6' => "<b>Impuesto</b>",
        'campo9' => "<b>Código de Impuesto</b>",
        'campo7' => "<b>Porcentaje Retención</b>",
        'campo8' => "<b>Valor Retenido</b>",
      )
    );

    foreach ($this->get_retencion($factura,$lineas) as $value)
    {
      $pdf_doc->add_table_row(
        array(
          'campo1' => $value['comprobante'],
          'campo2' => $value['numero'],
          'campo3' => $value['emision'],
          'campo4' => $value['ejercicio'],
          'campo5' => $value['base'],
          'campo6' => $value['impuesto'],
          'campo9' => $value['codimpuesto'],
          'campo7' => $value['porcentaje'],
          'campo8' => $value['valor'],
        )
      );
    }

    $pdf_doc->save_table(
      array(
        'cols' => array(
          'campo1' => array('justification' => 'center'),
          'campo2' => array('justification' => 'center'),
          'campo3' => array('justification' => 'center'),
          'campo4' => array('justification' => 'center'),
          'campo5' => array('justification' => 'center'),
          'campo6' => array('justification' => 'center'),
          'campo9' => array('justification' => 'center'),
          'campo7' => array('justification' => 'center'),
          'campo8' => array('justification' => 'center'),
        ),
        'width' => 550,
        'shaded' => 0, 
        'xPos'=>'left',
        'xOrientation'=>'right',
        'showLines'=> 2,
        'showHeadings' => 0,
        'fontSize' =>8
      )
    );

    // TABLA DE INFORMACION ADICIONAL
    $pdf_doc->set_y(220);

    $pdf_doc->new_table();

    // HEADER DE INFORMACION ADICIONAL
    $pdf_doc->add_table_row(
      array(
        'campo1' => "\n"."<b>INFORMACIÓN ADICIONAL</b>",
      )
    );

    //Agrego la Direccion del proveedor
    $consulta = $this->conexion->query("SELECT * FROM dirproveedores WHERE codproveedor = '".$proveedor0['codproveedor']."';");
    $direc = mysqli_fetch_array($consulta);
    $direccion = $direc['direccion'];

    $pdf_doc->add_table_row(
      array(
        'campo1' => "\n"."<b>Dirección:</b> ".$pdf_doc->fix_html($direccion),
      )
    );

    // VERIFICO SI TIENE TELF.
    if (empty($proveedor0['telefono1']))
    {
      $telf = 'N/A';
    }else{
      $telf = $proveedor0['telefono1'];
        if (!empty($proveedor0['telefono2']))
        {
          $telf .= ' / '.$proveedor0['telefono2'];
        }
    }

    $pdf_doc->add_table_row(
      array(
        'campo1' => "\n"."<b>Telf. de Contacto:</b> ".$telf,
      )
    );

            // VERIFICO SI EL proveedor TIENE EMAIL

    if (empty($proveedor0['email']))
    {
      $email = 'N/A';
    }else{
      $email = $proveedor0['email'];
    }

    $pdf_doc->add_table_row(
      array(
        'campo1' => "\n"."<b>Email: </b> ".$email,
      )
    );

    // VERIFICO SI EL proveedor TIENE OBSERVACIONES
    if($factura['observaciones'] != '')
    {
      $observacion = $pdf_doc->fix_html($factura['observaciones']);
    }else{
      $observacion = 'Ninguna';
    }

    $pdf_doc->add_table_row(
      array(
        'campo1' => "\n"."<b>Observaciones: </b> ".$pdf_doc->fix_html($factura['observaciones'])."\n",
      )
    );

    $pdf_doc->save_table(
      array(
        'cols' => array(
          'campo1' => array('justification' => 'left'),
        ),
        'showLines' => 0,
        'width' => 340,
        'shaded' => 0, 
        'xPos'=>'left',
        'xOrientation'=>'right',
        'showLines'=> 1,
        'showHeadings' => 0,
        'fontSize' =>8
      )
    );    

  }


  public function get_retencion($factura, $lineas = false)
  {
    if (!$lineas) {
      $consulta = $this->conexion->query("SELECT * FROM lineasfacturasprov WHERE idfactura = '".$factura['idfactura']."'");
      $lineas = array();
      while($l = mysqli_fetch_array($consulta)){
        $lineas[] = $l;
      }
      // $lineas = mysqli_fetch_all($consulta,MYSQLI_ASSOC);


    }


    $resultados = array();
    // CODIGO DEL MÉTODO get_retencion DE FACTURA
    $cods_retenciones = array();
    $cods_iva = array();
    $base_renta = 0;
    $base_iva = 0;

    $sum_renta = array(); // SUMATORIA DE TOTAL A RETENER POR CODIGO DE RENTA
    $neto_renta = array(); // SUMATORIA DE BASE IMPONIBLE POR CODIGO DE RENTA
    $porcentaje_renta = array(); // PORCENTAJES POR CODIGO DE RENTA
    $suma_retencion_renta = 0; // VARIABLE SUMATORIA DE RETENCION DE RENTA
    $suma_neto_renta = 0; // VARIABLE SUMATORIA DE BASE IMPONIBLE DE RENTA

    $sum_iva = array(); // SUMATORIA DE TOTAL A RETENER POR CODIGO DE IVA
    $neto_iva = array(); // SUMATORIA DE BASE IMPONIBLE POR CODIGO DE IVA
    $suma_retencion_iva = 0; // VARIABLE SUMATORIA DE RETENCION DE IVA
    $suma_neto_iva = 0; // VARIABLE SUMATORIA DE BASE IMPONIBLE DE IVA

    foreach ($lineas as $k) {
      $consulta = $this->conexion->query("SELECT * FROM co_tipos_retencion WHERE idtiporetencion = '".$k['idtipo_retencion_iva']."';");
      $tipo_retencion_iva = mysqli_fetch_array($consulta);
      $consulta = $this->conexion->query("SELECT * FROM co_tipos_retencion WHERE idtiporetencion = '".$k['idtipo_retencion_renta']."';");
      $tipo_retencion_renta = mysqli_fetch_array($consulta);
      array_push($cods_retenciones, $tipo_retencion_renta['codigo_base']);
      if (isset($tipo_retencion_iva['codigo']) && $tipo_retencion_iva['codigo']!=0) {
        array_push($cods_iva, $tipo_retencion_iva['codigo']);
      }
    }
    //ACUMULO RENTA POR CODIGO_BASE
    foreach ($cods_retenciones as $key => $value) {
      foreach ($lineas as $k) {
        $consulta = $this->conexion->query("SELECT * FROM co_tipos_retencion WHERE idtiporetencion = '".$k['idtipo_retencion_renta']."';");
        $tipo_retencion_renta = mysqli_fetch_array($consulta);
          if ($value == $tipo_retencion_renta['codigo_base']) {
            // SUMA DE TOTAL A RETENER
            $val = ((($tipo_retencion_renta['porcentaje'])/100)*$k['pvptotal']);
            $suma_retencion_renta += $val;            
            // SUMA BASE IMPONIBLE
            $val = $k['pvptotal'];
            $suma_neto_renta += $val; 
          }
      }
      $sum_renta += array($value=>$suma_retencion_renta);
      $neto_renta += array($value=>$suma_neto_renta);
      $suma_retencion_renta = 0;
      $suma_neto_renta = 0;
    }

    //ACUMULO IVA POR CODIGO_BASE
    foreach ($cods_iva as $key => $value) {
      foreach ($lineas as $k) {
        $consulta = $this->conexion->query("SELECT * FROM co_tipos_retencion WHERE idtiporetencion = '".$k['idtipo_retencion_iva']."';");
        $tipo_retencion_iva = mysqli_fetch_array($consulta);
        if (isset($tipo_retencion_iva['codigo']) && $value == $tipo_retencion_iva['codigo']) {
          // SUMA TOTAL DE IVA
          $val = $k['pvptotal'] * (($k['iva'])/100);
          $suma_neto_iva += $val;  
          // SUMA DE TOTAL A RETENER DE IVA
          $val = ((($tipo_retencion_iva['porcentaje'])/100)*$val);
          $suma_retencion_iva += $val;
        }
      }
      $sum_iva += array($value=>$suma_retencion_iva);
      $neto_iva += array($value=>$suma_neto_iva);
      $suma_retencion_iva = 0;
      $suma_neto_iva = 0;
    }

    foreach ($sum_renta as $key => $value) {
      $val_ret = $neto_renta[$key];
      if($val_ret == 0)
      {
        $porcentaje_renta = 0;
      }
      else{
        $porcentaje_renta = ($value / $val_ret) * 100;
      }
        $resultados[] = 
          array(
              'comprobante' => 'FACTURA',
              'numero' => $factura['numproveedor'],
              'emision' => $factura['fecha'],
              'ejercicio' => $factura['codejercicio'],
              'base' => $this->bround($val_ret,2),
              'impuesto' => 'RENTA',
              'codimpuesto' => $key,
              'porcentaje' => $porcentaje_renta,
              'valor' => $value,
          );
    }
    foreach ($sum_iva as $key => $value) {
      $val_ret = $neto_iva[$key];
      $porcentaje_iva = ($value / $val_ret) * 100;
        $resultados[] = 
          array(
              'comprobante' => 'FACTURA',
              'numero' => $factura['numproveedor'],
              'emision' => $factura['fecha'],
              'ejercicio' => $factura['codejercicio'],
              'base' => $this->bround($val_ret,2),
              'impuesto' => 'IVA',
              'codimpuesto' => $key,
              'porcentaje' => $porcentaje_iva,
              'valor' => $value,
          );
    }

        return $resultados;
  }


/*  --------------------------------------------------------------------------
  *           PROCESO ENVIO DE EMAIL AJUNTO PDF Y XML AL CLIENTE            *
  --------------------------------------------------------------------------
*/
  public function enviar_email($factura_email,$numero_fac, $tipoDocumento = "FACTURA")
  {
    // print_r($factura_email);
    $codigofactura = $factura_email['codigo'];
    $divisa = $factura_email['coddivisa'];
    $total = $factura_email['total'];
    $numero = $factura_email['numero'];
    $codalmacen = $factura_email['codalmacen'];
    $cliente0 = FALSE;
    $this->logo = false;
    $nombre = false;

    if (file_exists('../images/logo.png')) {
      $this->logo = '../images/logo.png';
    } else if (file_exists('../images/logo.jpg')) {
      $this->logo = '../images/logo.jpg';
    } else {
      $this->logo = '../view/img/logo.png';
    }

    if ($tipoDocumento=="RETENCION")
    {
      $codigofactura = $factura_email['codigo'];
      $codalmacen = $factura_email['codalmacen'];
      $total = 0;

      foreach ($this->get_retencion($factura_email) as $ret)
      {
        $total += $ret['valor'];
      }

      $codigofac = $factura_email['num_retencion'];
      $consulta = $this->conexion->query("SELECT * FROM proveedores WHERE codproveedor = '".$factura_email['codproveedor']."';");
      $proveedor = mysqli_fetch_array($consulta);
      $nombre = $proveedor['nombre'];
      $email = $proveedor['email'];
      $razonsocial = $proveedor['razonsocial'];
      $rutaRIDE = '../documentosElectronicos/enviados/retencionesElectronicas/pdfs_enviados/' . $codigofactura . '.pdf';
      $rutaXML = '../documentosElectronicos/recibidos/retencionesElectronicas' .'/' . $factura_email['num_retencion'] . '.xml';
      $saludo = "Le contactamos para enviarle su retención electrónica";
    }elseif ($tipoDocumento=="FACTURA")
    {
      $codigofac = $factura_email['numero_documento_sri'];
      /// obtenemos datos del cliente
      $clientes = $this->conexion->query("SELECT * FROM clientes WHERE codcliente = '".$factura_email['codcliente']."'");

      $cliente0 = array();

      //transformo los datos de la consulta a un array
      while ($cli = mysqli_fetch_array($clientes))
      {
        $cliente0[] = $cli;
      }

      $nombre = $cliente0[0]['nombre'];
      $email = $cliente0[0]['email'];
      $razonsocial = $cliente0[0]['razonsocial'];
      $rutaRIDE = '../documentosElectronicos/enviados/facturasElectronicas/pdfs_enviados/' . $codigofactura . '.pdf';
      $rutaXML = '../documentosElectronicos/recibidos/facturasElectronicas' .'/' . $numero_fac . '.xml';
      $saludo = "Le contactamos para enviarle su factura electrónica";

    }
    elseif ($tipoDocumento=="GUIA-DE-REMISION")
    {
      $consulta = $this->conexion->query("SELECT * FROM clientes WHERE codcliente = '".$factura_email['codcliente']."';");
      $cliente = mysqli_fetch_array($consulta);
      $nombre = $cliente['nombre'];
      $email = $cliente['email'];
      $razonsocial = $cliente['razonsocial'];
      $rutaRIDE = '../documentosElectronicos/enviados/guiasRemisionElectronicas/pdfs_enviados/' .'GUIA-'. $codigofactura . '.pdf';
      $rutaXML = '../documentosElectronicos/recibidos/guiasRemisionElectronicas' .'/' . $codigofac . '.xml';
      $saludo = "Le contactamos para enviarle su guía de remisión electrónica";
    }
  
    elseif ($tipoDocumento=="NOTA-DE-CREDITO")
    {
      $consulta = $this->conexion->query("SELECT * FROM clientes WHERE codcliente = '".$factura_email['codcliente']."';");
      $cliente = mysqli_fetch_array($consulta);
      $nombre = $cliente['nombre'];
      $email = $cliente['email'];
      $razonsocial = $cliente['razonsocial'];
      $rutaRIDE = '../documentosElectronicos/enviados/notasCreditoElectronicas/pdfs_enviados/' . $codigofactura . '.pdf';
      $rutaXML = '../documentosElectronicos/recibidos/notasCreditoElectronicas' .'/' . $codigofac . '.xml';
      $saludo = "Le contactamos para enviarle su nota de crédito electrónica";
    }elseif($tipoDocumento=="NOTA-DE-DEBITO"){
      $consulta = $this->conexion->query("SELECT * FROM clientes WHERE codcliente = '".$factura_email['codcliente']."';");
      $cliente = mysqli_fetch_array($consulta);
      $nombre = $cliente['nombre'];
      $email = $cliente['email'];
      $razonsocial = $cliente['razonsocial'];
      $rutaRIDE = '../documentosElectronicos/enviados/notasDebitoElectronicas/pdfs_enviados/' . $codigofactura . '.pdf';
      $rutaXML = '../documentosElectronicos/recibidos/notasDebitoElectronicas' .'/' . $factura_email['numero_documento_sri'] . '.xml';
      $saludo = "Le contactamos para enviarle su nota de débito electrónica";
    }

    // obtenemos datos de la empresa
    $empresa = $this->conexion->query("SELECT * FROM empresa");

    $empresa0 = mysqli_fetch_array($empresa);

    //transformo los datos de la consulta a un array
    // while ($emp = mysqli_fetch_array($empresa))
    // {
    //   $empresa0[] = $emp;
    // }

    if (TRUE)
    {
      $email = $email;
      $razonsocial = $razonsocial;

      if (file_exists($rutaRIDE))
      {
        //$mail = $empresa0->new_mail();

        //Codigo de envio con MailGun
        //Cambio la separacion de ; por , para enviar a varios mails
        $email = trim($email);
        $email = str_replace(",", ";", $email);
        $email = str_replace(';', ", ", $email);

        //Formo el mensaje del mail
        $this->mensaje = '<html>
        <head>
          <meta charset="utf-8">
          <style type="text/css">
            @import url("  data:application/octet-stream;base64,QGZvbnQtZmFjZSB7CiAgZm9udC1mYW1pbHk6ICdMYXRvJzsKICBmb250LXN0eWxlOiBub3JtYWw7CiAgZm9udC13ZWlnaHQ6IDMwMDsKICBzcmM6IGxvY2FsKCdMYXRvIExpZ2h0JyksIGxvY2FsKCdMYXRvLUxpZ2h0JyksIHVybChodHRwczovL2ZvbnRzLmdzdGF0aWMuY29tL3MvbGF0by92MTQvbmo0N21BWmUwbVlVSXlTZ2ZuMHdwUS50dGYpIGZvcm1hdCgndHJ1ZXR5cGUnKTsKfQo=");
            body{
              font-family: "Lato", sans-serif;
              font-size: 14px;
            }
          </style>
        </head>
        <body>
          <table height="100%" width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; height: 100%; margin: 0; padding: 0; width: 100%; background-color: #F3F3F3;">
            <tr><td align="center" style="height: 100%; margin: 0; padding: 100px 10px; width: 100%; border-top: 0;">

              <table height="100%" width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; border: 0; max-width: 600px!important; background-color: #ffffff;">
                <tr><td style="text-align:center; padding: 10px 25px;">';
                  $this->mensaje.= '<b>'.$empresa0["nombre"].'</b>';
                $this->mensaje.='<hr style="border: 1px solid #e8e8e8;"></td></tr>
                <tr><td style="padding: 10px 25px;">
                  <strong>Estimado(a) ' . $nombre . ' </strong><br>

                  <p>' . $saludo . ' N°: ' . $codigofac . ', por ' . $divisa .' '. $total . '.</p>
                  <p>Gracias por ser parte de nuestra empresa</p>
                  <br>
                  <p>Atentamente:</p>
                  <p>' . $empresa0["nombre"] . '</p>
                  <p>RUC:' . $empresa0["cifnif"] . '</p>
                  <p style="font-size:12px;">Comunicamos que la dirección a través de la cual usted está recibiendo el presente e-mail será utilizada únicamente para el servicio de notificaciones, le agradecemos no responder este correo ni utilizarlo como vía de comunicación para realizar consultas personales.</p>
                </td></tr>
              </table>

              <p style="font-size:13px;">&copy; 2018 kapitalcompany.com. Todos los derechos reservados</p>



            </td></tr>
          </table>
        </body>
        </html>';


        //Genero los parametros de envio de mailgun
        $parameters = array('from' => $empresa0["nombre"].' (Documentos Electronicos) '.'<postmaster@facturacion.kapitalcompany.com>',
                'to' => $email,
                'subject' => $empresa0["nombre"] . ': Su ' . $tipoDocumento .' '. $codigofac,
                "html" => $this->mensaje,
                'attachment[1]' => curl_file_create($rutaRIDE, 'application/pdf', $codigofactura.'.pdf'),
                'attachment[2]' => curl_file_create($rutaXML, 'application/xml', $codigofac.'.xml'));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.mailgun.net/v3/facturacion.kapitalcompany.com/messages');
        curl_setopt($ch, CURLOPT_USERPWD, 'api:key-1ee4f0919912e5ed0cb8b5dd2283c388');

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: multipart/form-data'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);
      }else
        //$this->new_error_msg('El archivo PDF no existe, no se pudo enviar el correo al cliente.'."</br>");
        return false;
    }else{
      //$this->new_error_msg("No tiene configurado el correo electronico en su empresa");
    }
  }

  public function is_html($txt)
  {
    if (stripos($txt, '<html') === FALSE)
    {
      return FALSE;
    } else {
      return TRUE;
    }
  }

  /*------------------------------------------------------------------------
  *           GENERA PDF FACTURA ELECTRONICA EN VERTICAL ACUMULADA        *
  ------------------------------------------------------------------------
  public function generar_pdf_factura_electronica_vertical_acu($factura, $numero)
  {
    /// obtenemos datos de la empresa
    $empresa0 = new \empresa();
    /// obtenemos datos del cliente
    $cliente0 =new \cliente();
    $cliente0 = $cliente0->get($factura->codcliente);
    /// Creamos el PDF y escribimos sus metadatos
    $pdf_doc = new \fs_pdf();
    $pdf_doc->pdf->addInfo('Title', ucfirst(FS_FACTURA).' '. $factura->codigo);
    $pdf_doc->pdf->addInfo('Subject', ucfirst(FS_FACTURA).' ' . $factura->codigo);
    $pdf_doc->pdf->addInfo('Author', $empresa0->nombre);
    $lineas = $factura->get_lineas();
    $det_acu = $factura->get_lineas_acu();
    $lineas_iva = $pdf_doc->get_lineas_iva($lineas);
    $this->documento = $factura;
    $list_ref = array();
    if ($det_acu)
    {
      $numero = $this->documento->retornaNumeroDoc();
      $pdf_doc->generar_pdf_cabecera_fac_elec($empresa0, $lppag, $factura, $numero);
      $this->generar_pdf_datos_empresa_fac_elec($pdf_doc, $lppag, $factura, $empresa0, $cliente0);
      $this->generar_pdf_datos_cliente_fac_elec($pdf_doc, $lppag, $factura, $cliente0);
      $puntoy = 475;
      $pdf_doc->new_table();
    
      $table_header = array(
        //'alb' => '<b>'.ucfirst(FS_ALBARAN).'</b>',
        'ref' => '<b>Cod. Principal</b>',
        'cantidad' => '<b>Cant.</b>',
        'descripcion' => '<b>Descripción</b>',
        'pvp' => '<b>Precio Unitario</b>',
        'dto' => '<b>Desc.</b>',
        'importeSinDesc' => '<b>Tot.Sin.Desc</b>',
        'importe' => '<b>Precio Total</b>',
      );
      
      $pdf_doc->add_table_header($table_header);
      
      foreach ($det_acu as $key => $value)
      {
        if($puntoy < 50)
        {
          $pdf_doc->save_table(
            array(
              'cols' => array(
                // REFERENCIA
                'ref' => array('width'=>100, 'justification' => 'center'),
                // CANTIDAD
                'cantidad' => array('width'=>40, 'justification' => 'center'),
                // DESCRIPCION
                'descripcion' => array('width'=>210, 'justification' => 'left'),
                // PRECIO UNITARIO
                'pvp' => array('width'=>50, 'justification' => 'center'),
                // DESCUENTO
                'dto' => array('width'=>50,'justification' => 'center'),
                'importeSinDesc' => array('width'=>50,'justification' => 'right'),
                'importe' => array('width'=>50,'justification' => 'right')
              ),
              'fontSize' => 7,
              //'width' => 795,
              'width' => 550,
              'shaded' => 1,
              'shadeCol' => array(0.95, 0.95, 0.95),
              'xPos'=>'left',
              'xOrientation'=>'right',
              'lineCol' => array(0.3, 0.3, 0.3),
              'showLines' => 2,
            )
          );
          
          $pdf_doc->pdf->ezNewPage();
          
          $pdf_doc->new_table();
          
          $table_header = array(
            //'alb' => '<b>'.ucfirst(FS_ALBARAN).'</b>',
            'ref' => '<b>Cod. Principal</b>',
            'cantidad' => '<b>Cant.</b>',
            'descripcion' => '<b>Descripción</b>',
            'pvp' => '<b>Precio Unitario</b>',
            'dto' => '<b>Desc.</b>',
            'importeSinDesc' => '<b>Tot.Sin.Desc</b>',
            'importe' => '<b>Precio Total</b>',
          );
          
          $pdf_doc->add_table_header($table_header);

          $puntoy = 789;
        }

        $pos = strpos($value['descripcion'], ","); //Verificamos si el string tiene una coma
        if ($pos===FALSE)
        {
          $puntoy = $puntoy - 12.1;
          $string1 =$value['descripcion'];
        }else{
          $puntoy = $puntoy - 20.2;
          list($string1, $string2) =  explode(",", $value['descripcion']);
        }
        //Total sin descuento
        //$totalSinDescuento = $value['pvpunitario'] * $value['cantidad'];
        $fila = array(
          'ref' => $value['referencia'],
          'cantidad' => $this->show_numero($value['cantidad']),
          'descripcion' => utf8_decode($string1),
          'pvp' => $this->show_precio($value['pvpunitario'], 'USD', TRUE, 5),
          // DESCUENTO
          'dto' => $this->show_precio($value['totaldescuento'] < 0 ? 0.00 : $value['totaldescuento'], 'USD', TRUE,FS_NF0_ART),
          'importeSinDesc' => $this->show_precio($value['totalsindescuento'], 'USD', TRUE, FS_NF0_ART),
          'importe' => $this->show_precio($value['pvptotal'], 'USD', TRUE,FS_NF0_ART)
        );
          $pdf_doc->add_table_row($fila);
      }

      $pdf_doc->save_table(
        array(
          'cols' => array(
            // REFERENCIA
            'ref' => array('width'=>100, 'justification' => 'center'),
            // CANTIDAD
            'cantidad' => array('width'=>40, 'justification' => 'center'),
            // DESCRIPCION
            'descripcion' => array('width'=>210, 'justification' => 'left'),
            // PRECIO UNITARIO
            'pvp' => array('width'=>50, 'justification' => 'center'),
            // DESCUENTO
            'dto' => array('width'=>50,'justification' => 'center'),
            'importeSinDesc' => array('width'=>50,'justification' => 'right'),
            'importe' => array('width'=>50,'justification' => 'right')
          ),
          'fontSize' => 7,
          //'width' => 795,
          'width' => 550,
          'shaded' => 1,
          'shadeCol' => array(0.95, 0.95, 0.95),
          'xPos'=>'left',
          'xOrientation'=>'right',
          'lineCol' => array(0.3, 0.3, 0.3),
          'showLines' => 2,
        )
      );
          
      $condicionPorIva= '';
      if(date('d-m-Y', strtotime($this->documento->fecha)) < '01-06-2017')
      {
        $condicionPorIva = '14%';
      }else{
        $condicionPorIva = '12%';
      }

      // Totalizamos el descuento
      $totaldescuento = 0.00;
      foreach ($det_acu as $key => $value)
      {
        $totaldescuento += $value['totaldescuento'] < 0 ? 0.00 : $value['totaldescuento'];
      }

      if($puntoy < 165)
      {
        $pdf_doc->pdf->ezNewPage();
      }
      
      // TABLA DE TOTALES
      $pdf_doc->set_y(167);

      $pdf_doc->new_table();

      $pdf_doc->add_table_row(
        array(
          'campo' => "<b>SUBTOTAL $condicionPorIva </b>",
          // En esta linea al neto se le suma el descuento porque el neto ya viene sin descuento
          'valor' => $this->show_precio($this->documento->neto + $totaldescuento, 'USD', TRUE, FS_NF0),
        )
      );

      $pdf_doc->add_table_row(
        array(
          'campo' => "<b>SUBTOTAL IVA 0%</b>",
          'valor' => $this->show_precio($this->getSinIva($this->documento), 'USD', TRUE, FS_NF0),
        )
      );

      $pdf_doc->add_table_row(
        array(
          'campo' => "<b>DESCUENTO</b>",
          'valor' => $this->show_precio($totaldescuento, $this->documento->coddivisa, TRUE, FS_NF0),
        )
      );

      $pdf_doc->add_table_row(
        array(
          'campo' => "<b>IVA $condicionPorIva</b>",
          'valor' => $this->show_precio($this->documento->totaliva, $this->documento->coddivisa, TRUE, FS_NF0),
        )
      );

      $pdf_doc->add_table_row(
        array(
          'campo' => "<b>VALOR TOTAL</b>",
          'valor' => '<b>'.$this->show_precio($this->documento->total, $this->documento->coddivisa, TRUE, FS_NF0).'</b>',
        )
      );

      $pdf_doc->save_table(
        array(
          'cols' => array(
            'campo' => array('justification' => 'left'),
            'valor' => array('justification' => 'right'),
          ),
          'width' => 200,
          'shaded' => 0,
          'xPos'=>'right',
          'xOrientation'=>'left',
          'showLines'=> 2,
          'showHeadings' => 0,
          'fontSize' =>8
        )
      );

      // TABLA DE INFORMACION ADICIONAL
      $pdf_doc->set_y(167);

      $pdf_doc->new_table();

      // HEADER DE INFORMACION ADICIONAL
      $pdf_doc->add_table_row(
        array(
          'campo1' => "<b>INFORMACIÓN ADICIONAL</b>",
        )
      );

      // TODO DE DIRECCION DEL CLIENTE
      $direccion = $this->documento->direccion;
      if($this->documento->apartado)
      {
        $direccion .= ' - '.ucfirst(FS_APARTADO).': '.$this->documento->apartado;
      }
  
      if($this->documento->codpostal)
      {
        $direccion .= ' - CP: '.$this->documento->codpostal;
      }

      $ciudad="";
      if ($this->documento->ciudad)
      {
        $ciudad = $this->ciud_provin->get($this->documento->ciudad)->nombre;
      }

      $provincia="";
      if ($this->documento->provincia)
      {
        $provincia = $this->ciud_provin->get($this->documento->provincia)->nombre;
      }

      $direccion .= ' - '.$provincia.' ('.$ciudad.')';

      $row = array(
        'campo1' => "<b>Dirección:</b> ".$pdf_doc->fix_html($direccion),
      );

      $pdf_doc->add_table_row($row);

      // VERIFICO SI TIENE TELF.
      if (empty($this->cliente->telefono1))
      {
        $telf = 'N/A';
      }else{
        $telf = $this->cliente->telefono1;
        if (!empty($this->cliente->telefono2))
        {
          $telf .= ' / '.$this->cliente->telefono2;
        }
      }

      $pdf_doc->add_table_row(
        array(
          'campo1' => "<b>Telf. de Contacto:</b> ".$telf,
        )
      );

      // VERIFICO SI EL CLIENTE TIENE EMAIL
      if (empty($this->cliente->email))
      {
        $email = 'N/A';
      }else{
        $email = $this->cliente->email;
      }

      $pdf_doc->add_table_row(
        array(
          'campo1' => "<b>Email: </b> ".$email,
        )
      );

      // VERIFICO SI EL CLIENTE TIENE OBSERVACIONES
      if($this->documento->observaciones != '')
      {
        $observacion = $pdf_doc->fix_html($this->documento->observaciones);
      }else{
        $observacion = 'Ninguna';
      }

      $pdf_doc->add_table_row(
        array(
          'campo1' => "<b>Observaciones: </b> ".$pdf_doc->fix_html($this->documento->observaciones)."\n",
        )
      );

      $pdf_doc->save_table(
        array(
          'cols' => array(
            'campo1' => array('justification' => 'left'),
          ),
          'showLines' => 0,
          'width' => 200,
          'shaded' => 0,
          'xPos'=>'left',
          'xOrientation'=>'right',
          'showLines'=> 1,
          'showHeadings' => 0,
          'fontSize' =>8
        )
      );

      // TABLA DE FORMA DE PAGO
      $pdf_doc->new_table();

      $pdf_doc->add_table_row(
        array(
          'campo' => "<b>FORMA DE PAGO</b>",
          'valor' => "<b>VALOR</b>",
        )
      );

      // VERIFICO EL CODIGO DEL TIPO DE PAGO
      $fp0 = new forma_pago();

      $forma_pago = $fp0->get($this->documento->codpago);
      if($forma_pago)
      {
        $texto_pago = $forma_pago->descripcion;
      }else{
        $texto_pago = 'N/A';
      }

      $pdf_doc->add_table_row(
        array(
          'campo' => $texto_pago,
          'valor' => '<b>'.$this->show_precio($this->documento->total, $this->documento->coddivisa, TRUE, FS_NF0).'</b>',
        )
      );

      $pdf_doc->save_table(
        array(
          'cols' => array(
            'campo' => array('justification' => 'center'),
            'valor' => array('justification' => 'right'),
          ),
          'width' => 200,
          'shaded' => 0,
          'xPos'=>'left',
          'xOrientation'=>'right',
          'showLines'=> 2,
          'showHeadings' => 0,
          'fontSize' =>8
        )
      );
            
    }

    if( !file_exists('documentosElectronicos/enviados/facturasElectronicas/pdfs_enviados/') )
    {
      mkdir('documentosElectronicos/enviados/facturasElectronicas/pdfs_enviados/');
    }

    $pdf_doc->save('documentosElectronicos/enviados/facturasElectronicas/pdfs_enviados/'.$factura->codigo.'.pdf');

  }


  public function pdf_det_acu_vertical(&$pdf_doc, &$det_acu, &$linea_actual, &$lppag, &$totaldescuento, &$cliente0, &$factura)
  {
        /// calculamos el número de páginas
    if( !isset($this->numpaginas) )
    {
         $this->numpaginas = 0;
         $linea_a = 0;

         $linea_actual = 0;
         $pagina = 1;
         while( $linea_a < count($det_acu) )
         {

            
            $lppag2 = $lppag;
            foreach($det_acu as $i => $lin)
            {
                //print_r($det_acu);
                //echo $i." ".$lin['descripcion']." ".$lin['cantidad']."</br>";

               if($i >= $linea_a AND $i < $linea_a + $lppag2)
               {
                  $linea_size = 1;
                  $len = mb_strlen($i.' '.$lin['descripcion']);

                  while($len > 85)
                  {
                     $len -= 85;
                     $linea_size += 0.5;
                  }

                  $aux = explode("\n", $lin['descripcion']);
                  if( count($aux) > 1 )
                  {
                     $linea_size += 0.5 * ( count($aux) - 1);
                  }

                  if($linea_size > 1)
                  {
                     $lppag2 -= $linea_size - 1;

                  }
               }
            }

            $linea_a += $lppag2;
            $this->numpaginas++;

         }

         if($this->numpaginas == 0)
         {
            $this->numpaginas = 1;
         }
      }

      $dec_cantidad = 0;
      $multi_iva = FALSE;
      $multi_re = FALSE;
      $multi_irpf = FALSE;
      $iva = FALSE;
      $re = FALSE;
      $irpf = FALSE;
      /// leemos las líneas para ver si hay que mostrar los tipos de iva, re o irpf
      foreach($det_acu as $i => $lin)
      {
        //echo $i." ".$lin['descripcion']."</br>";
        
         if( $lin['cantidad'] != intval($lin['cantidad'] ) )
         {
            $dec_cantidad = 2;
         }
         /// restamos líneas al documento en función del tamaño de la descripción
         if($i >= $linea_actual AND $i < $linea_actual+$lppag)
         {
            $linea_size = 1;
            $len = mb_strlen($i.' '.$lin['descripcion']);
            while($len > 85)
            {
               $len -= 85;
               $linea_size += 0.5;
            }

            $aux = explode("\n", $lin['descripcion']);
            if( count($aux) > 1 )
            {
               $linea_size += 0.5 * ( count($aux) - 1);
            }

            if($linea_size > 1)
            {
               $lppag -= $linea_size - 1;
            }
         }
      }
      
      
             //Creamos la tabla con las lineas del documento
             
            $pdf_doc->new_table();
            $table_header = array(
                //'alb' => '<b>'.ucfirst(FS_ALBARAN).'</b>',
                'ref' => '<b>Cod. Principal</b>',
                'cantidad' => '<b>Cant.</b>',
                'descripcion' => '<b>Descripción</b>',
                'pvp' => '<b>Precio Unitario</b>',
                'importe' => '<b>Precio Total</b>',
            );
            $pdf_doc->add_table_header($table_header);

      for($i = $linea_actual; (($linea_actual < ($lppag + $i)) AND ($linea_actual < count( $det_acu )));)
      {
          $pos = strpos($det_acu[$linea_actual]['descripcion'], ","); //Verificamos si el string tiene una coma
              if ($pos===FALSE) {
                $string1 =$det_acu[$linea_actual]['descripcion'];
              }else{
                list($string1, $string2) =  explode(",", $det_acu[$linea_actual]['descripcion']);
              }
          
          $fila = array(
                   'alb' => '-',
                   'ref' => $det_acu[$linea_actual]['referencia'],
                   'cantidad' => $det_acu[$linea_actual]['cantidad'],
                   'descripcion' => utf8_decode($string1),
                   'pvp' => $this->show_precio($det_acu[$linea_actual]['pvpunitario'], $factura->coddivisa, TRUE, FS_NF0_ART),
                   // DESCUENTO
                   'dto' => $this->show_precio($det_acu[$linea_actual]['totaldescuento'] < 0 ? 0.00 : $det_acu[$linea_actual]['totaldescuento'], 
                                               $factura->coddivisa, TRUE, FS_NF0_ART),
                   'importe' => $this->show_precio($det_acu[$linea_actual]['pvptotal'], $factura->coddivisa)
               );
          $pdf_doc->add_table_row($fila);
          $linea_actual++;


      }

            $cont=1;
            

           $pdf_doc->save_table(
              array(
                  'cols' => array(
                      // REFERENCIA
                      'ref' => array('width'=>40,'justification' => 'center'),
                      // CANTIDAD
                      'cantidad' => array('width'=>40,'justification' => 'center'),
                      // DESCRIPCION
                      'descripcion' => array('width'=>350,'justification' => 'left'),
                      // PRECIO UNITARIO
                      'pvp' => array('justification' => 'center'),
                      // DESCUENTO
                      'dto' => array('width'=>50,'justification' => 'center'),
                      'iva' => array('justification' => 'right'),
                      're' => array('justification' => 'right'),
                      'irpf' => array('justification' => 'right'),
                      'importe' => array('justification' => 'right')
                  ),
                  'fontSize' => 7,
                  //'width' => 795,
                  'width' => 550,
                  'shaded' => 1,
                  'shadeCol' => array(0.95, 0.95, 0.95),
                  'xPos'=>'left',
                  'xOrientation'=>'right',
                  'lineCol' => array(0.3, 0.3, 0.3),
                  'showLines' => 2,
              )
            );
       /// ¿Última página?
      if( $linea_actual == count($det_acu) )
      {
            // TABLA DE TOTALES
            if ($factura->fecha < '01-06-2017') {
              $condicionPorIva = '14%';
            }else{
              $condicionPorIva = '12%';
            }

            $pdf_doc->set_y(170);

            $pdf_doc->new_table();

            $pdf_doc->add_table_row(
                    array(
                        'campo' => "<b>SUBTOTAL $condicionPorIva</b>",
                        'valor' => $factura->neto,
                    )
            );

            $pdf_doc->add_table_row(
                    array(
                        'campo' => "<b>SUBTOTAL IVA 0%</b>",
                        'valor' => $this->getSinIva($factura),
                    )
            );

            $totaldescuento = bround(($totaldescuento * 100),2) / 100;

            $pdf_doc->add_table_row(
                    array(
                        'campo' => "<b>DESCUENTO</b>",
                        //'valor' => $totaldescuento
                        'valor' => 0.00
                    )
            );

            $pdf_doc->add_table_row(
                    array(
                        'campo' => "<b>IVA $condicionPorIva</b>",
                        'valor' => $factura->totaliva,
                    )
            );

            $pdf_doc->add_table_row(
                    array(
                        'campo' => "<b>VALOR TOTAL</b>",

                        'valor' => '<b>'.$this->show_precio($factura->total, $factura->coddivisa, TRUE, FS_NF0_ART).'</b>',
                    )
            );

            $pdf_doc->save_table(
               array(
                  'cols' => array(
                      'campo' => array('justification' => 'left'),
                      'valor' => array('justification' => 'right'),
                  ),
                  'width' => 200,
                  'shaded' => 0,
                  'xPos'=>'right',
                  'xOrientation'=>'left',
                  'showLines'=> 2,
                  'showHeadings' => 0,
                  'fontSize' =>8

               )
            );

            // TABLA DE INFORMACION ADICIONAL

            $pdf_doc->set_y(170);

            $pdf_doc->new_table();

            // HEADER DE INFORMACION ADICIONAL

            $pdf_doc->add_table_row(
                    array(
                        'campo1' => "<b>INFORMACIÓN ADICIONAL</b>",
                    )
            );

            // TODO DE DIRECCION DEL CLIENTE

                $direccion = $factura->direccion;
                if($factura->apartado)
                {
                   $direccion .= ' - '.ucfirst(FS_APARTADO).': '.$factura->apartado;
                }
                if($factura->codpostal)
                {
                   $direccion .= ' - CP: '.$factura->codpostal;
                }

                $this->ciud_provin = new \provincia();

                $ciudad="";

                if ($factura->ciudad)
                {
                  $ciudad = $this->ciud_provin->get($factura->ciudad)->nombre;
                }

                $provincia="";
                if ($factura->provincia)
                {
                  $provincia = $this->ciud_provin->get($factura->provincia)->nombre;
                }

                $direccion .= ' - '.$provincia.' ('.$ciudad.')';


                $row = array(
                    'campo1' => "<b>Dirección:</b> ".$pdf_doc->fix_html($direccion),
                );

                $pdf_doc->add_table_row($row);

          // VERIFICO SI TIENE TELF.

            if (empty($cliente0->telefono1)) {
              $telf = 'N/A';
            }else{
              $telf = $cliente0->telefono1;
              if (!empty($cliente0->telefono2)) {
               $telf .= ' / '.$cliente0->telefono2;
              }
            }

            $pdf_doc->add_table_row(
                    array(
                        'campo1' => "<b>Telf. de Contacto:</b> ".$telf,
                    )
            );

          // VERIFICO SI EL CLIENTE TIENE EMAIL

            if (empty($cliente0->email)) {
              $email = 'N/A';
            }else{
              $email = $cliente0->email;
            }

          $pdf_doc->add_table_row(
                  array(
                      'campo1' => "<b>Email: </b> ".$email,
                  )
          );

          // VERIFICO SI EL CLIENTE TIENE OBSERVACIONES

          if($factura->observaciones != '')
          {
            $observacion = $pdf_doc->fix_html($factura->observaciones);
          }else{
            $observacion = 'Ninguna';
          }

          $pdf_doc->add_table_row(
                  array(
                      'campo1' => "<b>Observaciones: </b> ".$pdf_doc->fix_html($factura->observaciones)."\n",
                  )
          );

          $pdf_doc->save_table(
             array(
                'cols' => array(
                    'campo1' => array('justification' => 'left'),
                ),
                'showLines' => 0,
                'width' => 200,
                'shaded' => 0,
                'xPos'=>'left',
                'xOrientation'=>'right',
                'showLines'=> 1,
                'showHeadings' => 0,
                'fontSize' =>8

             )
          );

            // TABLA DE FORMA DE PAGO

            $pdf_doc->new_table();

            $pdf_doc->add_table_row(
                    array(
                        'campo' => "<b>FORMA DE PAGO</b>",
                        'valor' => "<b>VALOR</b>",
                    )
            );

            // VERIFICO EL CODIGO DEL TIPO DE PAGO

            $fp0 = new forma_pago();

            $forma_pago = $fp0->get($factura->codpago);

            if($forma_pago){
                $texto_pago = $forma_pago->descripcion;
            }else{
                $texto_pago = 'N/A';
            }

            $pdf_doc->add_table_row(
                    array(
                        'campo' => $texto_pago,

                        'valor' => '<b>'.$this->show_precio($factura->total, $factura->coddivisa, TRUE, FS_NF0_ART).'</b>',
                    )
            );

            $pdf_doc->save_table(
               array(
                  'cols' => array(
                      'campo' => array('justification' => 'center'),
                      'valor' => array('justification' => 'right'),
                  ),
                  'width' => 200,
                  'shaded' => 0,
                  'xPos'=>'left',
                  'xOrientation'=>'right',
                  'showLines'=> 2,
                  'showHeadings' => 0,
                  'fontSize' =>8

               )
            );


        }  
    
   }

    //-------------------------------------------------------------------------
    // GENERA PDF FACTURA ELECTRONICA EN VERTICAL HORIZONTAL ACUMULADA FIN
    //--------------------------------------------------------------------------

    //-------------------------------------------------------------------------
    //          GENERA PDF FACTURA ELECTRONICA EN HORIZONTAL                  
    //--------------------------------------------------------------------------
    public function generar_pdf_factura_electronica_horizontal($factura, $numero)
   {

      /// obtenemos datos de la empresa
      $empresa0 = new \empresa();
      /// obtenemos datos del cliente
      $cliente0 =new \cliente();
      $cliente0 = $cliente0->get($factura->codcliente);
      /// Creamos el PDF y escribimos sus metadatos
      $pdf_doc = new \fs_pdf_horizontal();
      $pdf_doc->pdf->addInfo('Title', ucfirst(FS_FACTURA).' '. $factura->codigo);
      $pdf_doc->pdf->addInfo('Subject', ucfirst(FS_FACTURA).' ' . $factura->codigo);
      $pdf_doc->pdf->addInfo('Author', $empresa0->nombre);
      $lineas = $factura->get_lineas();
      $det_acu = $factura->get_lineas_acu();
      $lineas_iva = $pdf_doc->get_lineas_iva($lineas);
      $list_ref = array();
       
       if ($det_acu) {

          $linea_actual = 0;
          $pagina = 1;

          /// imprimimos las páginas necesarias
           while($linea_actual<count($det_acu))
           {

            $lppag = 15;

              /// salto de página
              if($linea_actual > 0)
              {
                 $pdf_doc->pdf->ezNewPage();
              }

              $pdf_doc->generar_pdf_cabecera_fac_elec($empresa0, $lppag,$factura, $numero);
              $this->generar_pdf_datos_empresa_fac_elec_horizontal($pdf_doc, $lppag, $factura, $empresa0, $cliente0);
              $this->generar_pdf_datos_cliente_fac_elec_horizontal($pdf_doc, $lppag, $factura, $cliente0);
              $this->pdf_det_acu_horizontal($pdf_doc, $det_acu, $linea_actual, $lppag, $totaldescuento, $cliente0, $factura);

              $pdf_doc->set_y(80);
              $this->generar_pdf_totales_fac_elec($pdf_doc, $lineas_iva, $pagina);
              $pagina++;
            }
            
        }
        
        if( !file_exists('documentosElectronicos/enviados/facturasElectronicas/pdfs_enviados/') )
          {
            mkdir('documentosElectronicos/enviados/facturasElectronicas/pdfs_enviados/');
          }
        //$pdf_doc->show(FS_FACTURA.'_'.$factura->codigo.'.pdf');
        $pdf_doc->save('documentosElectronicos/enviados/facturasElectronicas/pdfs_enviados/'.$factura->codigo.'.pdf');

   }
   public function generar_pdf_datos_cliente_fac_elec_horizontal(&$pdf_doc, &$lppag, &$factura, &$cliente0)
   {
      $tipo_doc = ucfirst(FS_ALBARAN);
      $rectificativa = FALSE;
      if( get_class_name($factura) == 'factura_cliente' )
      {
         if($factura->idfacturarect)
         {
            $tipo_doc = ucfirst(FS_FACTURA_RECTIFICATIVA);
            $rectificativa = TRUE;
         }
         else
         {
            $tipo_doc = 'Factura';
         }
      }
      
      $tipoidfiscal = FS_CIFNIF;
      if($cliente0)
      {
         $tipoidfiscal = $cliente0->tipoidfiscal;
      }
      
      // Esta es la tabla con los datos del cliente:
      // Albarán:                 Fecha:
      // Cliente:               CIF/NIF:
      // Dirección:           Teléfonos:
       
      $pdf_doc->new_table();
      $pdf_doc->add_table_row(
              array(
                  'campo1' => "<b>Razón Social / Nombres y Apellidos:</b> ".$cliente0->nombre,
                  'dato1' => '<b>Guía Remisión: </b>',              )
      );
      
      $pdf_doc->add_table_row(
              array(
                  'campo1' => "<b>Identificación:</b> ".$cliente0->cifnif,
                  'dato1' => '',
              )
      );

      $pdf_doc->add_table_row(
              array(
                  'campo1' => "<b>Fecha de Emisión:</b> ".$factura->fecha,
                  'dato1' => '',
              )
      );

      // TODO DE DIRECCION DEL CLIENTE
      
      $direccion = $factura->direccion;
      if($factura->apartado)
      {
         $direccion .= ' - '.ucfirst(FS_APARTADO).': '.$factura->apartado;
      }
      if($factura->codpostal)
      {
         $direccion .= ' - CP: '.$factura->codpostal;
      }

      $this->ciud_provin = new \provincia();

      
      $ciudad="";

      if ($factura->ciudad)
      {
        $ciudad = $this->ciud_provin->get($factura->ciudad)->nombre;
      }

      $provincia="";
      if ($factura->provincia)
      {
        $provincia = $this->ciud_provin->get($factura->provincia)->nombre;
      }

      $direccion .= ' - '.$provincia.' ('.$ciudad.')';

      $row = array(
          'campo1' => "<b>Dirección:</b> ".$pdf_doc->fix_html($direccion),
          'dato1' => '',
      );

      $pdf_doc->add_table_row($row);
      $pdf_doc->save_table(
         array(
            'cols' => array(
                'campo1' => array('width' => 350, 'justification' => 'left'),
                'dato1' => array('justification' => 'left'),
            ),
            'showLines' => 0,
            'width' => 540,
            'shaded' => 0, 
            'xPos'=>'left',
            'xOrientation'=>'right',
            'showLines'=> 1,
            'showHeadings' => 0,
            'fontSize' =>8

         )
      );
      $pdf_doc->pdf->ezText("\n", 5);
   }
    public function generar_pdf_datos_empresa_fac_elec_horizontal(&$pdf_doc, &$lppag, &$factura, &$empresa0, $cliente0)
   {
      $pdf_doc->pdf->ezText("\n", 5);
      $pdf_doc->pdf->ezText("\n", 5);
      $tipo_doc = ucfirst(FS_ALBARAN);
      $rectificativa = FALSE;
      if( get_class_name($factura) == 'factura_cliente' )
      {
         if($factura->idfacturarect)
         {
            $tipo_doc = ucfirst(FS_FACTURA_RECTIFICATIVA);
            $rectificativa = TRUE;
         }
         else
         {
            $tipo_doc = 'Factura';
         }
      }

      $tipoidfiscal = FS_CIFNIF;
      if($cliente0)
      {
         $tipoidfiscal = $cliente0->tipoidfiscal;
      }

      $pdf_doc->new_table();
      $pdf_doc->add_table_row(
              array(
                  'campo1' => "<b>".$empresa0->nombre."</b> ",
              )
      );

      $pdf_doc->add_table_row(
              array(
                  'campo1' => "<b>Dirección Matríz:</b> ".$empresa0->direccion,
              )
      );

      $pdf_doc->add_table_row(
              array(
                  'campo1' => "<b>Dirección Sucursal:</b> ".$empresa0->direccion,
              )
      );
      // VALIDO CONTRIBUYENTE y MUESTRO
      $nrocontrib = FS_CONTRIBUYENTE_NRO;
      if ($nrocontrib=='000') {
        $pdf_doc->add_table_row(
              array(
                  'campo1' => "<b>Contribuyente Especial :</b> NO",
              )
        );
      }else{
        $pdf_doc->add_table_row(
              array(
                  'campo1' => "<b>Contribuyente Especial Nro:</b> ".$nrocontrib."\n",
              )
       );
      }

      // VALIDO SI ESTA OBLIGADO A LLEVAR CONTABILIDAD
      if (FS_OB_CONTA==1) {
         $ob_conta = 'NO';
      }
      if (FS_OB_CONTA==2) {
         $ob_conta = 'SI';
      }

      $pdf_doc->add_table_row(
              array(
                  'campo1' => "<b>OBLIGADO A LLEVAR CONTABILIDAD:</b> ".$ob_conta,
              )
      );

      $pdf_doc->pdf->ezText("\n",10);
      $pdf_doc->save_table(
         array(
            'cols' => array(
                'campo1' => array('justification' => 'left'),
            ),
            'showLines' => 0,
            'width' => 275,
            'shaded' => 0,
            'xPos'=>290,
            'xOrientation'=>'right',
            'showLines'=> 1,
            'showHeadings' => 0,
            'fontSize' =>8

         )
      );
      $pdf_doc->pdf->ezText("\n", 3);
   }

    public function pdf_det_acu_horizontal(&$pdf_doc, &$det_acu, &$linea_actual, &$lppag, &$totaldescuento, &$cliente0, &$factura)
    {
        /// calculamos el número de páginas
      if( !isset($this->numpaginas) )
      {
         $this->numpaginas = 0;
         $linea_a = 0;

         $linea_actual = 0;
         $pagina = 1;
         while( $linea_a < count($det_acu) )
         {

            
            $lppag2 = $lppag;
            foreach($det_acu as $i => $lin)
            {
                //print_r($det_acu);
                //echo $i." ".$lin['descripcion']." ".$lin['cantidad']."</br>";

               if($i >= $linea_a AND $i < $linea_a + $lppag2)
               {
                  $linea_size = 1;
                  $len = mb_strlen($i.' '.$lin['descripcion']);

                  while($len > 85)
                  {
                     $len -= 85;
                     $linea_size += 0.5;
                  }

                  $aux = explode("\n", $lin['descripcion']);
                  if( count($aux) > 1 )
                  {
                     $linea_size += 0.5 * ( count($aux) - 1);
                  }

                  if($linea_size > 1)
                  {
                     $lppag2 -= $linea_size - 1;

                  }
               }
            }

            $linea_a += $lppag2;
            $this->numpaginas++;

         }

         if($this->numpaginas == 0)
         {
            $this->numpaginas = 1;
         }
      }
      
      $dec_cantidad = 0;
      $multi_iva = FALSE;
      $multi_re = FALSE;
      $multi_irpf = FALSE;
      $iva = FALSE;
      $re = FALSE;
      $irpf = FALSE;
      /// leemos las líneas para ver si hay que mostrar los tipos de iva, re o irpf
      foreach($det_acu as $i => $lin)
      {
        //echo $i." ".$lin['descripcion']."</br>";
        
         if( $lin['cantidad'] != intval($lin['cantidad'] ) )
         {
            $dec_cantidad = 2;
         }
         /// restamos líneas al documento en función del tamaño de la descripción
         if($i >= $linea_actual AND $i < $linea_actual+$lppag)
         {
            $linea_size = 1;
            $len = mb_strlen($i.' '.$lin['descripcion']);
            while($len > 85)
            {
               $len -= 85;
               $linea_size += 0.5;
            }

            $aux = explode("\n", $lin['descripcion']);
            if( count($aux) > 1 )
            {
               $linea_size += 0.5 * ( count($aux) - 1);
            }

            if($linea_size > 1)
            {
               $lppag -= $linea_size - 1;
            }
         }
      }
      
      
            // Creamos la tabla con las lineas del documento
             
            $pdf_doc->new_table();
            $table_header = array(
                //'alb' => '<b>'.ucfirst(FS_ALBARAN).'</b>',
                'ref' => '<b>Cod. Principal</b>',
                'cantidad' => '<b>Cant.</b>',
                'descripcion' => '<b>Descripción</b>',
                'pvp' => '<b>Precio Unitario</b>',
                'importe' => '<b>Precio Total</b>',
            );
            $pdf_doc->add_table_header($table_header);

      for($i = $linea_actual; (($linea_actual < ($lppag + $i)) AND ($linea_actual < count( $det_acu )));)
      {
          $pos = strpos($det_acu[$linea_actual]['descripcion'], ","); //Verificamos si el string tiene una coma
              if ($pos===FALSE) {
                $string1 =$det_acu[$linea_actual]['descripcion'];
              }else{
                list($string1, $string2) =  explode(",", $det_acu[$linea_actual]['descripcion']);
              }
          
          $fila = array(
                   'alb' => '-',
                   'ref' => $det_acu[$linea_actual]['referencia'],
                   'cantidad' => $this->show_numero($det_acu[$linea_actual]['cantidad']),
                   'descripcion' => utf8_decode($string1),
                   'pvp' => $this->show_precio($det_acu[$linea_actual]['pvpunitario'], $factura->coddivisa, TRUE, FS_NF0_ART),
                   // DESCUENTO
                   'dto' => $this->show_precio($det_acu[$linea_actual]['totaldescuento'] < 0 ? 0.00 : $det_acu[$linea_actual]['totaldescuento'], 
                                               $factura->coddivisa, TRUE, FS_NF0_ART),
                   'importe' => $this->show_precio($det_acu[$linea_actual]['pvptotal'], $factura->coddivisa)
               );
          $pdf_doc->add_table_row($fila);
          $linea_actual++;


      }

            $cont=1;
            

            $pdf_doc->save_table(
              array(
                  'cols' => array(
                      // REFERENCIA
                      'ref' => array('width'=>40,'justification' => 'center'),
                      // CANTIDAD
                      'cantidad' => array('width'=>40,'justification' => 'center'),
                      // DESCRIPCION
                      'descripcion' => array('width'=>550,'justification' => 'left'),
                      // PRECIO UNITARIO
                      'pvp' => array('justification' => 'center'),
                      // DESCUENTO
                      'dto' => array('width'=>50,'justification' => 'center'),
                      'iva' => array('justification' => 'right'),
                      're' => array('justification' => 'right'),
                      'irpf' => array('justification' => 'right'),
                      'importe' => array('justification' => 'right')
                  ),
                  'fontSize' => 7,
                  'width' => 795,
                  'shaded' => 1,
                  'shadeCol' => array(0.95, 0.95, 0.95),
                  'xPos'=>'left',
                  'xOrientation'=>'right',
                  'lineCol' => array(0.3, 0.3, 0.3),
                  'showLines' => 2,
              )
            );

       /// ¿Última página?
      if( $linea_actual == count($det_acu) )
      {
            // TABLA DE TOTALES
            if ($factura->fecha < '01-06-2017') {
              $condicionPorIva = '14%';
            }else{
              $condicionPorIva = '12%';
            }

            $pdf_doc->set_y(170);

            $pdf_doc->new_table();

            $pdf_doc->add_table_row(
                    array(
                        'campo' => "<b>SUBTOTAL $condicionPorIva</b>",
                        'valor' => $factura->neto,
                    )
            );

            $pdf_doc->add_table_row(
                    array(
                        'campo' => "<b>SUBTOTAL IVA 0%</b>",
                        'valor' => $this->getSinIva($factura),
                    )
            );

            $totaldescuento = bround(($totaldescuento * 100),2) / 100;

            $pdf_doc->add_table_row(
                    array(
                        'campo' => "<b>DESCUENTO</b>",
                        //'valor' => $totaldescuento
                        'valor' => 0.00
                    )
            );

            $pdf_doc->add_table_row(
                    array(
                        'campo' => "<b>IVA $condicionPorIva</b>",
                        'valor' => $factura->totaliva,
                    )
            );

            $pdf_doc->add_table_row(
                    array(
                        'campo' => "<b>VALOR TOTAL</b>",

                        'valor' => '<b>'.$this->show_precio($factura->total, $factura->coddivisa, TRUE, FS_NF0_ART).'</b>',
                    )
            );

            $pdf_doc->save_table(
               array(
                  'cols' => array(
                      'campo' => array('justification' => 'left'),
                      'valor' => array('justification' => 'right'),
                  ),
                  'width' => 200,
                  'shaded' => 0,
                  'xPos'=>'right',
                  'xOrientation'=>'left',
                  'showLines'=> 2,
                  'showHeadings' => 0,
                  'fontSize' =>8

               )
            );

            // TABLA DE INFORMACION ADICIONAL

            $pdf_doc->set_y(170);

            $pdf_doc->new_table();

            // HEADER DE INFORMACION ADICIONAL

            $pdf_doc->add_table_row(
                    array(
                        'campo1' => "<b>INFORMACIÓN ADICIONAL</b>",
                    )
            );

            // TODO DE DIRECCION DEL CLIENTE

                $direccion = $factura->direccion;
                if($factura->apartado)
                {
                   $direccion .= ' - '.ucfirst(FS_APARTADO).': '.$factura->apartado;
                }
                if($factura->codpostal)
                {
                   $direccion .= ' - CP: '.$factura->codpostal;
                }

                $this->ciud_provin = new \provincia();

                
                $ciudad="";

                if ($factura->ciudad)
                {
                  $ciudad = $this->ciud_provin->get($factura->ciudad)->nombre;
                }

                $provincia="";
                if ($factura->provincia)
                {
                  $provincia = $this->ciud_provin->get($factura->provincia)->nombre;
                }

                $direccion .= ' - '.$provincia.' ('.$ciudad.')';

                $row = array(
                    'campo1' => "<b>Dirección:</b> ".$pdf_doc->fix_html($direccion),
                );

                $pdf_doc->add_table_row($row);

          // VERIFICO SI TIENE TELF.

            if (empty($cliente0->telefono1)) {
              $telf = 'N/A';
            }else{
              $telf = $cliente0->telefono1;
              if (!empty($cliente0->telefono2)) {
               $telf .= ' / '.$cliente0->telefono2;
              }
            }

            $pdf_doc->add_table_row(
                    array(
                        'campo1' => "<b>Telf. de Contacto:</b> ".$telf,
                    )
            );

          // VERIFICO SI EL CLIENTE TIENE EMAIL

            if (empty($cliente0->email)) {
              $email = 'N/A';
            }else{
              $email = $cliente0->email;
            }

          $pdf_doc->add_table_row(
                  array(
                      'campo1' => "<b>Email: </b> ".$email,
                  )
          );

          // VERIFICO SI EL CLIENTE TIENE OBSERVACIONES

          if($factura->observaciones != '')
          {
            $observacion = $pdf_doc->fix_html($factura->observaciones);
          }else{
            $observacion = 'Ninguna';
          }

          $pdf_doc->add_table_row(
                  array(
                      'campo1' => "<b>Observaciones: </b> ".$pdf_doc->fix_html($factura->observaciones)."\n",
                  )
          );

          $pdf_doc->save_table(
             array(
                'cols' => array(
                    'campo1' => array('justification' => 'left'),
                ),
                'showLines' => 0,
                'width' => 340,
                'shaded' => 0,
                'xPos'=>'left',
                'xOrientation'=>'right',
                'showLines'=> 1,
                'showHeadings' => 0,
                'fontSize' =>8

             )
          );

            // TABLA DE FORMA DE PAGO

            $pdf_doc->new_table();

            $pdf_doc->add_table_row(
                    array(
                        'campo' => "<b>FORMA DE PAGO</b>",
                        'valor' => "<b>VALOR</b>",
                    )
            );

            // VERIFICO EL CODIGO DEL TIPO DE PAGO

            $fp0 = new forma_pago();

            $forma_pago = $fp0->get($factura->codpago);

            if($forma_pago){
                $texto_pago = $forma_pago->descripcion;
            }else{
                $texto_pago = 'N/A';
            }

            $pdf_doc->add_table_row(
                    array(
                        'campo' => $texto_pago,

                        'valor' => '<b>'.$this->show_precio($factura->total, $factura->coddivisa, TRUE, FS_NF0_ART).'</b>',
                    )
            );

            $pdf_doc->save_table(
               array(
                  'cols' => array(
                      'campo' => array('justification' => 'center'),
                      'valor' => array('justification' => 'right'),
                  ),
                  'width' => 200,
                  'shaded' => 0,
                  'xPos'=>'left',
                  'xOrientation'=>'right',
                  'showLines'=> 2,
                  'showHeadings' => 0,
                  'fontSize' =>8

               )
            );


        }  
    
   }

*/
    // GENERAR TOTALES PDF FACTURA ELECTRONICA SRI
   public function generar_pdf_totales_fac_elec(&$pdf_doc, &$lineas_iva, $pagina)
   {
      if( isset($_GET['noval']) )
      {
         $pdf_doc->pdf->addText(10, 10, 8, $pdf_doc->center_text('Página '.$pagina.'/'.$this->numpaginas, 250) );
      }
      else
      {

         $pdf_doc->new_table();
         $titulo = array('pagina' => '<b>Página</b>');
         $fila = array(
             'pagina' => $pagina . '/' . $this->numpaginas,
         );
         $opciones = array(
             'cols' => array(
                 'pagina' => array('justification' => 'right'),
             ),
             'showLines' => 0,
             'fontSize' => 8,
             'xPos' =>'right',
             'xOrientation' => 'left',
             'width' => 50
         );

         $pdf_doc->add_table_header($titulo);
         $pdf_doc->add_table_row($fila);
         $pdf_doc->save_table($opciones);
      }
   }
/*
  //-------------------------------------------------------------------------
  //           FIN GENERA PDF FACTURA ELECTRONICA EN HORIZONTAL              
  //--------------------------------------------------------------------------

  //-------------------------------------------------------------------------
  //           GENERA PDF GUIA REMISION ELECTRONICA EN HORIZONTAL            
  //--------------------------------------------------------------------------
*/     
    public function generar_pdf_guia_remision($factura)
    {
     
      /// obtenemos datos de la empresa
      $consulta = $this->conexion->query("SELECT * FROM empresa");
      $empresa0 = mysqli_fetch_array($consulta);

      /// obtenemos datos del cliente
      $consulta = $this->conexion->query("SELECT * FROM clientes WHERE codcliente = '".$factura['codcliente']."';");
      $cliente0 = mysqli_fetch_array($consulta);

      /// Creamos el PDF y escribimos sus metadatos
      $pdf_doc = new \fs_pdf();
      $pdf_doc->pdf->addInfo('Title', 'Guia de remision '. $factura['codigo']);
      $pdf_doc->pdf->addInfo('Subject', 'Guia de remision ' . $factura['codigo']);
      $pdf_doc->pdf->addInfo('Author', $empresa0['nombre']);

      $consulta = $this->conexion->query("SELECT * FROM lineasfacturascli WHERE idfactura = '".$factura['idfactura']."';");
      $lineas = array();
      while($l = mysqli_fetch_array($consulta)){
        $lineas[] = $l;
      }
      // $lineas = mysqli_fetch_all($consulta, MYSQLI_ASSOC);



      //ARTICULOS COMBINADOS, MÉTODO get_lineas_acu
      $det_acu = array();
      $consulta = $this->conexion->query("SELECT articulo_combinaciones.idvalor, lineasfacturascli.codcombinacion, lineasfacturascli.referencia,  articulo_combinaciones.valor, descripcion, pvpunitario, SUM(cantidad) as cantidad, SUM(ROUND(pvptotal/((100-dtopor)/100),2)) as totalsindescuento, SUM(pvptotal) as pvptotal, SUM(ROUND(pvpsindto*(dtopor/100),2)) as totaldescuento  FROM lineasfacturascli, articulo_combinaciones WHERE lineasfacturascli.codcombinacion = articulo_combinaciones.codigo AND idfactura = '".$factura['idfactura']."' and nombreatributo = 'Color' GROUP BY referencia, pvpunitario, articulo_combinaciones.valor;");
      if ($consulta) {
        while($l = mysqli_fetch_array($consulta)){
          $det_acu[] = $l;
        }
        // $det_acu = mysqli_fetch_all($consulta, MYSQLI_ASSOC);
      }
      
      $consulta = $this->conexion->query("SELECT '' as idvalor ,codcombinacion, referencia, descripcion, pvpunitario, cantidad, ROUND(pvptotal/((100-dtopor)/100),2) as totalsindescuento, pvptotal as pvptotal, ROUND(pvpsindto*(dtopor/100),2) as totaldescuento FROM lineasfacturascli WHERE idfactura = '".$factura['idfactura']."' AND  (codcombinacion = '' or codcombinacion is null);");
      if ($consulta) {
        while($l = mysqli_fetch_array($consulta)){
          $det_acu[] = $l;
        }
        // $det_acu += mysqli_fetch_all($consulta, MYSQLI_ASSOC);
      }
      //-----------------FIN METODO get_lineas_acu


      $lineas_iva = $pdf_doc->get_lineas_iva($lineas);

      $list_ref = array();
      $numero = $factura['codigo'];  

       if ($det_acu) {

          $linea_actual = 0;
          $pagina = 1;

          /// imprimimos las páginas necesarias
           while($linea_actual<count($det_acu))
           {

            $lppag;

              /// salto de página
              if($linea_actual > 0)
              {
                 $pdf_doc->pdf->ezNewPage();
              }
              if($pagina===1){
                $lppag=18;

              $pdf_doc->generar_pdf_cabecera_guia_remision($empresa0, $lppag, $factura, $numero);
              $this->generar_pdf_datos_empresa_guia_remision($pdf_doc, $lppag, $factura, $empresa0, $cliente0);
              $this->generar_pdf_datos_cliente_guia_remision($pdf_doc, $lppag, $cliente0, $factura);
              $this->generar_pdf_datos_cliente_guia_remision_detalle($empresa0, $factura, $pdf_doc, $lppag, $cliente0);

              }else{
                 $lppag=45;
              }

              $this->pdf_det_acu_vertical_guia_remision($pdf_doc, $det_acu, $linea_actual, $lppag, $totaldescuento, $factura, $cliente0);
              
              $pagina++;
            }
            
        }

        if( !file_exists('../documentosElectronicos/enviados/guiasRemisionElectronicas/pdfs_enviados/') )
          {
            mkdir('../documentosElectronicos/enviados/guiasRemisionElectronicas/pdfs_enviados/');
          }
        //$pdf_doc->show(FS_FACTURA.'_'.$factura->codigo.'.pdf');
        $pdf_doc->save('../documentosElectronicos/enviados/guiasRemisionElectronicas/pdfs_enviados/'.'GUIA-'.$factura['codigo'].'.pdf');

   }
  private function pdf_det_acu_vertical_guia_remision(&$pdf_doc, &$det_acu, &$linea_actual, &$lppag, &$totaldescuento, &$factura, $cliente0)
    {
        /// calculamos el número de páginas
      if( !isset($this->numpaginas) )
      {
         $this->numpaginas = 0;
         $linea_a = 0;

         $linea_actual = 0;
         $pagina = 1;
         while( $linea_a < count($det_acu) )
         {

            
            $lppag2 = $lppag;
            foreach($det_acu as $i => $lin)
            {
                //print_r($det_acu);
                //echo $i." ".$lin['descripcion']." ".$lin['cantidad']."</br>";

               if($i >= $linea_a AND $i < $linea_a + $lppag2)
               {
                  $linea_size = 1;
                  $len = mb_strlen($i.' '.$lin['descripcion']);

                  while($len > 85)
                  {
                     $len -= 85;
                     $linea_size += 0.5;
                  }

                  $aux = explode("\n", $lin['descripcion']);
                  if( count($aux) > 1 )
                  {
                     $linea_size += 0.5 * ( count($aux) - 1);
                  }

                  if($linea_size > 1)
                  {
                     $lppag2 -= $linea_size - 1;

                  }
               }
            }

            $linea_a += $lppag2;
            $this->numpaginas++;

         }

         if($this->numpaginas == 0)
         {
            $this->numpaginas = 1;
         }
      }
      
      $dec_cantidad = 0;
      $multi_iva = FALSE;
      $multi_re = FALSE;
      $multi_irpf = FALSE;
      $iva = FALSE;
      $re = FALSE;
      $irpf = FALSE;
      /// leemos las líneas para ver si hay que mostrar los tipos de iva, re o irpf
      foreach($det_acu as $i => $lin)
      {
        //echo $i." ".$lin['descripcion']."</br>";
        
         if( $lin['cantidad'] != intval($lin['cantidad'] ) )
         {
            $dec_cantidad = 2;
         }
         /// restamos líneas al documento en función del tamaño de la descripción
         if($i >= $linea_actual AND $i < $linea_actual+$lppag)
         {
            $linea_size = 1;
            $len = mb_strlen($i.' '.$lin['descripcion']);
            while($len > 85)
            {
               $len -= 85;
               $linea_size += 0.5;
            }

            $aux = explode("\n", $lin['descripcion']);
            if( count($aux) > 1 )
            {
               $linea_size += 0.5 * ( count($aux) - 1);
            }

            if($linea_size > 1)
            {
               $lppag -= $linea_size - 1;
            }
         }
      }
      
      
       // Creamos la tabla con las lineas del documento
       
       $pdf_doc->new_table();
       $table_header = array(
           'cantidad' => '<b>Cant.</b>',
           'descripcion' => '<b>Descripción</b>',
           'cod_pri' => '<b>Cod. Principal</b>',
           'cod_aux' => '<b>Cod. Auxiliar</b>',
       );
       $pdf_doc->add_table_header($table_header);

      for($i = $linea_actual; (($linea_actual < ($lppag + $i)) AND ($linea_actual < count( $det_acu )));)
      {

          $pos = strpos($det_acu[$linea_actual]['descripcion'], ","); //Verificamos si el string tiene una coma
              if ($pos===FALSE) {
                $string1 =$det_acu[$linea_actual]['descripcion'];
              }else{
                list($string1, $string2) =  explode(",", $det_acu[$linea_actual]['descripcion']);
                
              }
          
          $fila = array(
                   'cod_pri' => $det_acu[$linea_actual]['referencia'],
                   'cod_aux' => $det_acu[$linea_actual]['referencia'],
                   'cantidad' =>$det_acu[$linea_actual]['cantidad'],
                   'descripcion' => utf8_decode($string1),
               );
               $pdf_doc->add_table_row($fila);
          $linea_actual++;
      }

          $cont=1;
            

            $pdf_doc->save_table(
              array(
                  'cols' => array(
                      // REFERENCIA
                      'ref' => array('width'=>40,'justification' => 'center'),
                      // CANTIDAD
                      'cantidad' => array('width'=>40,'justification' => 'center'),
                      // DESCRIPCION
                      'descripcion' => array('width'=>350,'justification' => 'left'),
                      // PRECIO UNITARIO
                      'pvp' => array('justification' => 'center'),
                      // DESCUENTO
                      'dto' => array('width'=>50,'justification' => 'center'),
                      'iva' => array('justification' => 'right'),
                      're' => array('justification' => 'right'),
                      'irpf' => array('justification' => 'right'),
                      'importe' => array('justification' => 'right')
                  ),
                  'fontSize' => 7,
                  'width' => 550,
                  'shaded' => 1,
                  'shadeCol' => array(0.95, 0.95, 0.95),
                  'xPos'=>'left',
                  'xOrientation'=>'right',
                  'lineCol' => array(0.3, 0.3, 0.3),
                  'showLines' => 2,
              )
            );

       /// ¿Última página?
      if( $linea_actual == count($det_acu) )
      {
            // TABLA DE TOTALES

            
            // TABLA DE INFORMACION ADICIONAL

            $pdf_doc->set_y(105);

            $pdf_doc->new_table();

            // HEADER DE INFORMACION ADICIONAL

            $pdf_doc->add_table_row(
                    array(
                        'campo1' => "<b>INFORMACIÓN ADICIONAL</b>",
                    )
            );

            // TODO DE DIRECCION DEL CLIENTE
            $direccion = $factura['direccion'];
            $consulta = $this->conexion->query("SELECT * FROM provincia WHERE codigo = '".$factura['provincia']."';");
            $arr_prov = mysqli_fetch_array($consulta);
            $provincia="";
            if (!empty($arr_prov)) {
              $direccion .= ' - '.$arr_prov['nombre'];
            }

            $consulta = $this->conexion->query("SELECT * FROM provincia WHERE codigo = '".$factura['ciudad']."';");
            $arr_ciu = mysqli_fetch_array($consulta);
            $ciudad="";
            if (!empty($arr_ciu)) {
              $direccion .= '('.$arr_ciu['nombre'].')';
            }
                $row = array(
                    'campo1' => "<b>Dirección:</b> ".$pdf_doc->fix_html($direccion),
                );

                $pdf_doc->add_table_row($row);

          // VERIFICO SI TIENE TELF.

            if (empty($cliente0['telefono1'])) {
              $telf = 'N/A';
            }else{
              $telf = $cliente0['telefono1'];
              if (!empty($cliente0['telefono2'])) {
               $telf .= ' / '.$cliente0['telefono2'];
              }
            }

            $pdf_doc->add_table_row(
                    array(
                        'campo1' => "<b>Telf. de Contacto:</b> ".$telf,
                    )
            );

          // VERIFICO SI EL CLIENTE TIENE EMAIL

            if (empty($cliente0['email'])) {
              $email = 'N/A';
            }else{
              $email = $cliente0['email'];
            }

          $pdf_doc->add_table_row(
                  array(
                      'campo1' => "<b>Email: </b> ".$email,
                  )
          );

          // VERIFICO SI EL CLIENTE TIENE OBSERVACIONES

          if($factura['observaciones'] != '')
          {
            $observacion = $pdf_doc->fix_html($factura['observaciones']);
          }else{
            $observacion = 'Ninguna';
          }

          $pdf_doc->add_table_row(
                  array(
                      'campo1' => "<b>Observaciones: </b> ".$pdf_doc->fix_html($factura['observaciones'])."\n",
                  )
          );

          $pdf_doc->save_table(
             array(
                'cols' => array(
                    'campo1' => array('justification' => 'left'),
                ),
                'showLines' => 0,
                'width' => 550,
                'shaded' => 0,
                'xPos'=>'left',
                'xOrientation'=>'right',
                'showLines'=> 1,
                'showHeadings' => 0,
                'fontSize' =>8

             )
          );

            // TABLA DE FORMA DE PAGO



        }  
    
   }

  public function generar_pdf_datos_empresa_guia_remision(&$pdf_doc, &$lppag, &$factura, &$empresa0, $cliente0)
  {
      $pdf_doc->pdf->ezText("\n\n\n\n\n", 5);
      $pdf_doc->pdf->ezText("\n", 5);
      $tipo_doc = 'Guia de remision';
      $tipoidfiscal = 'RUC';
      $rectificativa = FALSE;
      if($cliente0)
      {
         $tipoidfiscal = $cliente0['tipoidfiscal'];
      }

      $pdf_doc->new_table();
      $pdf_doc->add_table_row(
              array(
                  'campo1' => "SERVICIO DE RENTAS INTERNAS ",
              )
      );
      
      $pdf_doc->add_table_row(
              array(
                  'campo1' => "\n"."<b>Dirección Matríz:</b> ".$empresa0['direccion'],
              )
      );

      $pdf_doc->add_table_row(
              array(
                  'campo1' => "\n"."<b>Dirección Sucursal:</b> ".$empresa0['direccion'],
              )
      );
      // VALIDO CONTRIBUYENTE y MUESTRO
      $nrocontrib = $this->var->FS_CONTRIBUYENTE_NRO;
      if ($nrocontrib=='000') {
        $pdf_doc->add_table_row(
              array(
                  'campo1' => "\n"."<b>Contribuyente Especial :</b> NO",
              )
        );
      }else{
        $pdf_doc->add_table_row(
              array(
                  'campo1' => "\n"."<b>Contribuyente Especial Nro:</b> ".$nrocontrib,
              )
       );
      }

      // VALIDO SI ESTA OBLIGADO A LLEVAR CONTABILIDAD
      if ($this->var->FS_OB_CONTA==1) {
         $ob_conta = 'NO';
      }
      if ($this->var->FS_OB_CONTA==2) {
         $ob_conta = 'SI';
      } 

      $pdf_doc->add_table_row(
              array(
                  'campo1' => "\n"."<b>OBLIGADO A LLEVAR CONTABILIDAD:</b> ".$ob_conta,
              )
      );

      $pdf_doc->pdf->ezText("\n",10);
      $pdf_doc->save_table(
         array(
            'cols' => array(
                'campo1' => array('justification' => 'left'),
            ),
            'showLines' => 0,
            'width' => 255,
            'shaded' => 0, 
            'xPos'=> 'left',
            'xOrientation'=>'right',
            'showLines'=> 1,
            'showHeadings' => 0,
            'fontSize' =>8

         )
      );
      $pdf_doc->pdf->ezText("", 3);
   }

   public function generar_pdf_datos_cliente_guia_remision(&$pdf_doc, &$lppag, &$cliente0, &$factura)
   {
      $tipo_doc = 'Guia de remision';
      $rectificativa = FALSE;
      $tipoidfiscal = 'RUC';
      if($cliente0)
      {
         $tipoidfiscal = $cliente0['tipoidfiscal'];
      }

      
       // Esta es la tabla con los datos del cliente:
       // Albarán:                 Fecha:
       // Cliente:               CIF/NIF:
       // Dirección:           Teléfonos:
       
      $pdf_doc->new_table();
      $pdf_doc->add_table_row(
              array(
                    'campo1' => "<b>Identificación:</b> ".$cliente0['cifnif'],
                         )
      );

      $pdf_doc->add_table_row(
              array(
                  'campo1' => "<b>Razón Social / Nombres y Apellidos:</b> ".$cliente0['nombre'],                 
              )
      );

      $pdf_doc->add_table_row(
              array(
                  'campo1' => "<b>Placa:</b> ".$factura['placaenv'],                 
              )
      );

      $direccion = $factura['direccion'];
      $pdf_doc->add_table_row(
              array(
                  'campo1' => "<b>Punto De Partida:</b> ".$factura['direccion'],
                 // 'campo1' => "<b>Dirección:</b> ".$direccion,
              )
      );
       $pdf_doc->add_table_row(
              array(
                  'campo1' => "<b>Fecha Inicio Transporte :</b> ".$factura['fecha_ini_trans']."<b>      Fecha Fin Transporte :</b> ".$factura['fecha_fin_trans'],
                 
              )
      );


      $direccion = $factura['direccion'];


      $consulta = $this->conexion->query("SELECT * FROM provincia WHERE codigo = '".$factura['ciudad']."';");
      $arr_ciu = mysqli_fetch_array($consulta);
      $ciudad="";
      if (!empty($arr_ciu)) {
        $ciudad = $arr_ciu['nombre'];
      }

      $consulta = $this->conexion->query("SELECT * FROM provincia WHERE codigo = '".$factura['provincia']."';");
      $arr_prov = mysqli_fetch_array($consulta);
      $provincia="";
      if (!empty($arr_prov)) {
        $provincia = $arr_prov['nombre'];
      }

      $direccion .= ' - '.$provincia.' ('.$ciudad.')';

      $row = array(
          'campo1' => " ",
         
      );

      $pdf_doc->add_table_row($row);
      $pdf_doc->save_table(
         array(
            'cols' => array(
                'campo1' => array('width' => 550, 'justification' => 'left'),
                'dato1' => array('justification' => 'left'),
            ),
            'showLines' => 0,
            'width' => 50,
            'shaded' => 0, 
            'width' => 40,
            'shaded' => 0,
            'xPos'=>'left',
            'xOrientation'=>'right',
            'showLines'=> 1,
            'showHeadings' => 0,
            'fontSize' =>8

         )
      );
      $pdf_doc->pdf->ezText("", 5);
   }

   public function generar_pdf_datos_cliente_guia_remision_detalle(&$empresa_trae,&$factura, &$pdf_doc, &$lppag, &$cliente0)
   {
      $tipo_doc = 'Guia de remision';
      $rectificativa = FALSE;
      
      $tipoidfiscal = 'RUC';
      if($cliente0)
      {
         $tipoidfiscal = $cliente0['tipoidfiscal'];
      }
      
      
       // Esta es la tabla con los datos del cliente:
       // Albarán:                 Fecha:
       // Cliente:               CIF/NIF:
       // Dirección:           Teléfonos:
       
      $pdf_doc->new_table();
      $pdf_doc->add_table_row(
              array(
                    'campo1' => "<b>Comprobante de Venta:</b>                       <b>FACTURA</b>:".$factura['numero_documento_sri']. "                                     <b>Fecha Emisión</b>  :</b> ".$factura['fecha'],
                         )
      );
      

        // GENERO CLAVE DE ACCESO

      $consulta = $this->conexion->query("SELECT * FROM facturascli WHERE idfactura = '".$factura['idfactura']."';");
      $facturacli = mysqli_fetch_array($consulta);

      $pdf_doc->add_table_row(
              array(
                  'campo1' => "<b>Numero De Autorización</b> ".$facturacli['autoriza_numero_sri'],                 
              )
      );

      $pdf_doc->add_table_row(
              array(
                  'campo1' => "<b> Motivo Traslado: </b> "."Envio",                 
              )
      );

    $direccion = $factura['direccion'];

      $pdf_doc->add_table_row(
              array(
                  'campo1' => "<b> Destino(Punto De LLegada) </b> ".$pdf_doc->fix_html($direccion),               
              )
      );



      $pdf_doc->add_table_row(
              array(
                  'campo1' => "<b> Identificación(Destinatario)</b> ".$cliente0['cifnif'],                 
              )
      );

      $pdf_doc->add_table_row(
              array(
                  'campo1' => "<b>Razón Social / Nombres y Apellidos:</b> ".$cliente0['nombre'],                 
              )
      );

      $pdf_doc->add_table_row(
              array(
                  'campo1' => "<b>Documento Aduanero:</b> ".$factura['doc_aduanero'],                 
              )
      );

      $direccion = $factura['direccion'];
      $pdf_doc->add_table_row(
              array(
                  'campo1' => "<b>Codigo Establecimiento De Destino:</b> ".$factura['cod_estab_destino'],
                 // 'campo1' => "<b>Dirección:</b> ".$direccion,
              )
      );
       $pdf_doc->add_table_row(
              array(
                  'campo1' => "<b>Ruta:</b> ".$factura['direccion_ruta'],
                 
              )
      );



      // TODO DE DIRECCION DEL CLIENTE

      $consulta = $this->conexion->query("SELECT * FROM provincia WHERE codigo = '".$factura['ciudad']."';");
      $arr_ciu = mysqli_fetch_array($consulta);
      $ciudad="";
      if (!empty($arr_ciu)) {
        $ciudad = $arr_ciu['nombre'];
      }

      $consulta = $this->conexion->query("SELECT * FROM provincia WHERE codigo = '".$factura['provincia']."';");
      $arr_prov = mysqli_fetch_array($consulta);
      $provincia="";
      if (!empty($arr_prov)) {
        $provincia = $arr_prov['nombre'];
      }

      $direccion .= ' - '.$provincia.' ('.$ciudad.')';

      $row = array(
          'campo1' => " ",
         
      );

      $pdf_doc->add_table_row($row);
      $pdf_doc->save_table(
         array(
            'cols' => array(
                'campo1' => array('width' => 550, 'justification' => 'left'),
                'dato1' => array('justification' => 'left'),
            ),
            'showLines' => 0,
            'width' => 550,
            'shaded' => 0, 
            'xPos'=>'left',
            'xOrientation'=>'right',
            'showLines'=> 1,
            'showHeadings' => 0,
            'fontSize' =>8

         )
      );
      $pdf_doc->pdf->ezText("", 5);
   }

    //-------------------------------------------------------------------------
    //         FIN GENERA PDF GUIA REMISION ELECTRONICA EN HORIZONTAL         
    //---------------------------------------------------------------------------
    
   //--------------------------------------------------------------------------
    //                 GENERAR PDF PARA NOTA DE PEDIDO                         
    //--------------------------------------------------------------------------
/*   public function generar_pdf_pedido($pedido = FALSE)
   {
      
      $pdf_doc = new fs_pdf();
      $pdf_doc->pdf->addInfo('Title', ucfirst(FS_PEDIDO).' '. $pedido->codigo);
      $pdf_doc->pdf->addInfo('Subject', ucfirst(FS_PEDIDO).' de cliente ' . $pedido->codigo);
      $pdf_doc->pdf->addInfo('Author', $this->empresa->nombre);
      
      $lineas = $pedido->get_lineas();
      $lineas_iva = $pdf_doc->get_lineas_iva($lineas);
      if($lineas)
      {
         $linea_actual = 0;
         $pagina = 1;
         
         /// imprimimos las páginas necesarias
         while( $linea_actual < count($lineas) )
         {
            $lppag = 35;
            
            /// salto de página
            if($linea_actual > 0)
            {
               $pdf_doc->pdf->ezNewPage();
            }
            
            $pdf_doc->generar_pdf_cabecera($this->empresa, $lppag);
            $this->generar_pdf_datos_cliente($pdf_doc, $lppag);
            $this->generar_pdf_lineas($pdf_doc, $lineas, $linea_actual, $lppag);
            
            $pdf_doc->set_y(80);
            $this->generar_pdf_totales($pdf_doc, $lineas_iva, $pagina);
            $pagina++;
         }
      }
      else
      {
         $pdf_doc->pdf->ezText('¡'.ucfirst(FS_PEDIDO).' sin líneas!', 20);
      }
      

      if( !file_exists('documentosElectronicos/enviados/guiasRemisionElectronicas/pdfs_enviados/') )
      {
        mkdir('documentosElectronicos/enviados/guiasRemisionElectronicas/pdfs_enviados/');
      }
        //$pdf_doc->show(FS_FACTURA.'_'.$factura->codigo.'.pdf');
      $pdf_doc->save('documentosElectronicos/enviados/guiasRemisionElectronicas/pdfs_enviados/'.'GUIA-'.$factura->codigo.'.pdf');
      
   }
    //-------------------------------------------------------------------------
    //               FIN GENERAR PDF PARA NOTA DE PEDIDO                      *
    //--------------------------------------------------------------------------
*/
    //-------------------------------------------------------------------------
    //               INICIO GENERAR PDF PARA NOTA DE CREDITO                  *
    //--------------------------------------------------------------------------

       // GENERAR NOTA CREDITO ELECTRÓNICA SRI
      public function generar_pdf_nota_credito($ncc)
    {

      /// obtenemos datos de la empresa
      $consulta = $this->conexion->query("SELECT * FROM empresa");
      $empresa0 = mysqli_fetch_array($consulta);

      /// obtenemos datos del cliente
      $consulta = $this->conexion->query("SELECT * FROM clientes WHERE codcliente = '".$ncc['codcliente']."';");
      $cliente0 = mysqli_fetch_array($consulta);
      /// Creamos el PDF y escribimos sus metadatos
      $pdf_doc = new \fs_pdf();

      ///Consultamos el numero del documento
      $consulta = $this->conexion->query("SELECT * FROM facturascli WHERE idfactura = '".$ncc['idfactura']."';");
      $nota_cre = mysqli_fetch_array($consulta);
      $numero_documento_sri = $nota_cre['numero_documento_sri'];

      $pdf_doc->pdf->addInfo('Title', 'NOTA DE CRÉDITO '. $ncc['codigo']);
      $pdf_doc->pdf->addInfo('Subject','NOTA DE CRÉDITO ' . $ncc['codigo']);
      $pdf_doc->pdf->addInfo('Author', $empresa0['nombre']);

      $consulta = $this->conexion->query("SELECT * FROM lineasfacturascli WHERE idfactura = '".$ncc['idfactura']."' ORDER BY orden DESC, idlinea ASC;");
      $lineas = array();
      while ($l = mysqli_fetch_array($consulta)) {
        $lineas[] = $l;
      }
      // $lineas = mysqli_fetch_array($consulta);
      $lineas_iva = $pdf_doc->get_lineas_iva($lineas);

       /// obtenemos los datos de configuración de impresión
      $this->impresion = array(
        'print_ref' => '1',
        'print_dto' => '1',
        'print_alb' => '0',
        'print_formapago' => '1'
        );

      if($lineas)
      {
         $linea_actual = 0;
         $pagina = 1;

         /// imprimimos las páginas necesarias
         while( $linea_actual < count($lineas) )
         {
            $lppag = 20;

            /// salto de página
            if($linea_actual > 0)
            {
               $pdf_doc->pdf->ezNewPage();
            }
            
            $pdf_doc->generar_pdf_cabecera_nota_cre_elec($empresa0, $lppag,$ncc, $numero_documento_sri);
            $this->generar_pdf_datos_empresa_nota_cre_elec($pdf_doc, $ncc, $empresa0, $cliente0);
            $this->generar_pdf_datos_cliente_nota_cre_elec($pdf_doc, $ncc, $cliente0);

            $this->generar_pdf_lineas_nota_cre_elec($pdf_doc, $lineas, $linea_actual, $lppag, $ncc, $this->impresion, $cliente0);

            if( $linea_actual == count($lineas) )
            {
               if( !$ncc['pagada'] AND $this->impresion['print_formapago'] )
               {
                  $consulta = $this->conexion->query("SELECT * FROM formaspago WHERE codpago = '".$ncc['codpago']."';");
                  $forma_pago = mysqli_fetch_array($consulta);
                  if($forma_pago)
                  {
                     $texto_pago = "\n<b>Forma de pago</b>: ".$forma_pago['descripcion'];

                     $texto_pago .= "\n<b>Vencimiento</b>: ".$ncc['vencimiento'];
                     $pdf_doc->pdf->ezText($texto_pago, 9);
                  }
               }
            }

            $pdf_doc->set_y(80);
            $this->generar_pdf_totales_fac_elec($pdf_doc, $lineas_iva, $pagina);

            /// pié de página para la factura
            if($empresa0['pie_factura'])
            {
               $pdf_doc->pdf->addText(10, 10, 8, $pdf_doc->center_text($pdf_doc->fix_html($empresa0['pie_factura']), 180) );
            }

            $pagina++;
         }
      }
      else
      {
         $pdf_doc->pdf->ezText('¡'.ucfirst($this->var->FS_FACTURA).' sin líneas!', 20);
      }

      if( !file_exists('../documentosElectronicos/enviados/notasCreditoElectronicas/pdfs_enviados/') )
      {
        mkdir('../documentosElectronicos/enviados/notasCreditoElectronicas/pdfs_enviados/');
      }
        //$pdf_doc->show(FS_FACTURA.'_'.$factura->codigo.'.pdf');
      $pdf_doc->save('../documentosElectronicos/enviados/notasCreditoElectronicas/pdfs_enviados/'.$ncc['codigo'].'.pdf');

   }

   // GENERAR DATOS EMPRESA NOTA CREDITO ELECTRONICA SRi

      public function generar_pdf_datos_empresa_nota_cre_elec(&$pdf_doc, &$ncc, &$empresa0, &$cliente0)
   {
      $pdf_doc->pdf->ezText("\n", 5);
      $pdf_doc->pdf->ezText("\n", 5);
      $tipo_doc = 'Nota de credito';
      $tipoidfiscal = 'RUC';
      $rectificativa = FALSE;
      if($cliente0)
      {
         $tipoidfiscal = $cliente0['tipoidfiscal'];
      }

      $pdf_doc->new_table();
      $pdf_doc->add_table_row(
              array(
                  'campo1' => "<b>".$empresa0['nombre']."</b> ",
              )
      );

      $pdf_doc->add_table_row(
              array(
                  'campo1' => "\n"."<b>Dirección Matríz:</b> ".$empresa0['direccion'],
              )
      );

      $pdf_doc->add_table_row(
              array(
                  'campo1' => "\n"."<b>Dirección Sucursal:</b> ".$empresa0['direccion'],
              )
      );
      // VALIDO CONTRIBUYENTE y MUESTRO
      $nrocontrib = $this->var->FS_CONTRIBUYENTE_NRO;
      if ($nrocontrib=='000') {
        $pdf_doc->add_table_row(
              array(
                  'campo1' => "\n"."<b>Contribuyente Especial :</b> NO",
              )
        );
      }else{
        $pdf_doc->add_table_row(
              array(
                  'campo1' => "\n"."<b>Contribuyente Especial Nro:</b> ".$nrocontrib,
              )
       );
      }

      // VALIDO SI ESTA OBLIGADO A LLEVAR CONTABILIDAD
      if ($this->var->FS_OB_CONTA==1) {
         $ob_conta = 'NO';
      }
      if ($this->var->FS_OB_CONTA==2) {
         $ob_conta = 'SI';
      }

      $pdf_doc->add_table_row(
              array(
                  'campo1' => "\n"."<b>OBLIGADO A LLEVAR CONTABILIDAD:</b> ".$ob_conta,
              )
      );

      $pdf_doc->pdf->ezText("\n",10);
      $pdf_doc->save_table(
         array(
            'cols' => array(
                'campo1' => array('justification' => 'left'),
            ),
            'showLines' => 0,
            'width' => 250,
            'shaded' => 0,
            'xPos'=>'left',
            'xOrientation'=>'right',
            'showLines'=> 1,
            'showHeadings' => 0,
            'fontSize' =>8

         )
      );
      $pdf_doc->pdf->ezText("\n", 3);
   }

   //Generar datos cliente factura Horizontal

   public function generar_pdf_datos_cliente_nota_cre_elec(&$pdf_doc, &$ncc, &$cliente0)
   {

      //Consultamos los datos de la factura de origen
      $consulta = $this->conexion->query("SELECT * FROM facturascli WHERE idfactura = '".$ncc['idfacturarect']."'");
      $factura_origen = mysqli_fetch_array($consulta);

      $tipo_doc = 'Nota de credito';
      $rectificativa = FALSE;
      
      $tipoidfiscal = 'RUC';
      if($cliente0)
      {
         $tipoidfiscal = $cliente0['tipoidfiscal'];
      }
      
      
       // Esta es la tabla con los datos del cliente:
       // Albarán:                 Fecha:
       // Cliente:               CIF/NIF:
       // Dirección:           Teléfonos:
       
      $pdf_doc->new_table();
      $pdf_doc->add_table_row(
              array(
                  'campo1' => "<b>Razón Social / Nombres y Apellidos:</b> ".$cliente0['nombre'],
                  'dato1' => '<b>Identificación: </b>'.$cliente0['cifnif'],              )
      );
      
      $pdf_doc->add_table_row(
              array(
                  'campo1' => "<b>Fecha de Emisión:</b> ".$ncc['fecha'],
                  'dato1' => '',
              )
      );

      $pdf_doc->add_table_row(
              array(
                  'campo1' => "____________________________________________________________________________",
                  'dato1' => '______________________________________',
              )
      );

      $pdf_doc->add_table_row(
              array(
                  'campo1' => "<b>Comprobante que se modifica:</b> ".$ncc['fecha'],
                  'dato1' => '<b>FACTURA: </b>'.$factura_origen['numero_documento_sri'],
              )
      );

      $pdf_doc->add_table_row(
              array(
                  'campo1' => "<b>Fecha Emisión (Comprobante a modificar):</b> ",
                  'dato1' => $factura_origen['fecha'],
              )
      );

      $pdf_doc->add_table_row(
              array(
                  'campo1' => "<b>Razón de modificacion:</b> ",
                  'dato1' => $ncc['observaciones'],
              )
      );

      

      // TODO DE DIRECCION DEL CLIENTE
      
      $direccion = $ncc['direccion'];
      if($ncc['codpostal'])
      {
         $direccion .= ' - CP: '.$ncc['codpostal'];
      }
      $direccion .= ' - '.$ncc['ciudad'].' ('.$ncc['provincia'].')';
      $row = array(
          'campo1' => "<b>Dirección:</b> ".$pdf_doc->fix_html($direccion),
          'dato1' => '',
      );

      $pdf_doc->add_table_row($row);
      $pdf_doc->save_table(
         array(
            'cols' => array(
                'campo1' => array('width' => 350, 'justification' => 'left'),
                'dato1' => array('justification' => 'left'),
            ),
            'showLines' => 0,
            'width' => 550,
            'shaded' => 0, 
            'xPos'=>'left',
            'xOrientation'=>'right',
            'showLines'=> 1,
            'showHeadings' => 0,
            'fontSize' =>8

         )
      );
      $pdf_doc->pdf->ezText("\n", 1);
   }

   // GENERAR LINEAS PDF NOTA DE CREDITO ELECTRONICA SRI

public function generar_pdf_lineas_nota_cre_elec(&$pdf_doc, &$lineas, &$linea_actual, &$lppag, $factura, &$impresion)
{
      /// calculamos el número de páginas
  $this->impresion = $impresion;
  if( !isset($this->numpaginas) )
  {
   $this->numpaginas = 0;
   $linea_a = 0;
   while( $linea_a < count($lineas) )
   {
    $lppag2 = $lppag;
    foreach($lineas as $i => $lin)
    {
     if($i >= $linea_a AND $i < $linea_a + $lppag2)
     {
      $linea_size = 1;
      $len = mb_strlen($lin['referencia'].' '.$lin['descripcion']);
      while($len > 85)
      {
       $len -= 85;
       $linea_size += 0.5;
     }

     $aux = explode("\n", $lin['descripcion']);
     if( count($aux) > 1 )
     {
       $linea_size += 0.5 * ( count($aux) - 1);
     }

     if($linea_size > 1)
     {
       $lppag2 -= $linea_size - 1;
     }
   }
 }

 $linea_a += $lppag2;
 $this->numpaginas++;
}

if($this->numpaginas == 0)
{
  $this->numpaginas = 1;
}
}

if($this->impresion['print_dto'])
{
 $this->impresion['print_dto'] = FALSE;

         /// leemos las líneas para ver si de verdad mostramos los descuentos
 foreach($lineas as $lin)
 {
  if($lin['dtopor'] != 0)
  {
   $this->impresion['print_dto'] = TRUE;
   break;
 }
}
}

$dec_cantidad = 0;
$multi_iva = FALSE;
$multi_re = FALSE;
$multi_irpf = FALSE;
$iva = FALSE;
$re = FALSE;
$irpf = FALSE;
      /// leemos las líneas para ver si hay que mostrar los tipos de iva, re o irpf
foreach($lineas as $i => $lin)
{
 if( $lin['cantidad'] != intval($lin['cantidad']) )
 {
  $dec_cantidad = 2;
}

if($iva === FALSE)
{
  $iva = $lin['iva'];
}
else if($lin['iva'] != $iva)
{
  $multi_iva = TRUE;
}

if($re === FALSE)
{
  $re = $lin['recargo'];
}
else if($lin['recargo'] != $re)
{
  $multi_re = TRUE;
}

if($irpf === FALSE)
{
  $irpf = $lin['irpf'];
}
else if($lin['irpf'] != $irpf)
{
  $multi_irpf = TRUE;
}

         /// restamos líneas al documento en función del tamaño de la descripción
if($i >= $linea_actual AND $i < $linea_actual+$lppag)
{
  $linea_size = 1;
  $len = mb_strlen($lin['referencia'].' '.$lin['descripcion']);
  while($len > 85)
  {
   $len -= 85;
   $linea_size += 0.5;
 }

 $aux = explode("\n", $lin['descripcion']);
 if( count($aux) > 1 )
 {
   $linea_size += 0.5 * ( count($aux) - 1);
 }

 if($linea_size > 1)
 {
   $lppag -= $linea_size - 1;
 }
}
}

      
       // Creamos la tabla con las lineas del documento
       
      $pdf_doc->new_table();
      $table_header = array(
          //'alb' => '<b>'.ucfirst(FS_ALBARAN).'</b>',
        'ref' => '<b>Cod. Principal</b>',
        'cantidad' => '<b>Cant.</b>',
        'descripcion' => '<b>Descripción</b>',
        'pvp' => '<b>Precio Unitario</b>',
        );
      
      /// ¿Desactivamos la columna de albaran?
    //   if( get_class_name($factura) == 'factura_cliente' )
    //   {
    //    if($this->impresion['print_alb'])
    //    {
    //         /// aunque esté activada, si la factura no viene de un albaran, la desactivamos
    //     $this->impresion['print_alb'] = FALSE;
    //     foreach($lineas as $lin)
    //     {
    //      if($lin['idalbaran'])
    //      {
    //       $this->impresion['print_alb'] = TRUE;
    //       break;
    //       }
    //     }
    //   }

    //   if( !$this->impresion['print_alb'] )
    //   {
    //     unset($table_header['alb']);
    //   }
    // }else{
    //   unset($table_header['alb']);
    // }

 if($this->impresion['print_dto'] AND !isset($_GET['noval']) )
 {
   $table_header['dto'] = '<b>Descuento</b>';
 }

 if($multi_iva AND !isset($_GET['noval']) )
 {
   $table_header['iva'] = '<b>'.FS_IVA.'</b>';
 }

 if($multi_re AND !isset($_GET['noval']) )
 {
   $table_header['re'] = '<b>R.E.</b>';
 }

 if($multi_irpf AND !isset($_GET['noval']) )
 {
   $table_header['irpf'] = '<b>'.FS_IRPF.'</b>';
 }

 if( isset($_GET['noval']) )
 {
   unset($table_header['pvp']);
 }
 else
 {
         //$table_header['importe'] = '<b>Importe</b>';
  $table_header['importe'] = '<b>Precio Total</b>';
}

$pdf_doc->add_table_header($table_header);
$totaldescuento = 0.00;
$subtotal0por = 0.00;
for($i = $linea_actual; (($linea_actual < ($lppag + $i)) AND ($linea_actual < count($lineas)));)
{
 $descripcion = $pdf_doc->fix_html($lineas[$linea_actual]['descripcion']);
         //if( !is_null($lineas[$linea_actual]->referencia) )
         {//
            //$descripcion = '<b>'.$lineas[$linea_actual]->referencia.'</b> '.$descripcion;
          //}

         /// ¿El articulo tiene trazabilidad?
          // $descripcion .= $this->generar_trazabilidad($lineas[$linea_actual]);

         // SETEAMOS LA REFERENCIA
          if( !is_null($lineas[$linea_actual]['referencia']) )
          {
            $ref = '<b>'.$lineas[$linea_actual]['referencia'].'</b> ';
          }

        // CALCULAMOS DESCUENTO
          $descuento = ( $lineas[$linea_actual]['pvpsindto'] - $lineas[$linea_actual]['pvptotal']) ;
          $totaldescuento += $descuento;

          $fila = array(
           'alb' => '-',
           'ref' => $ref,
           'cantidad' => $this->show_numero($lineas[$linea_actual]['cantidad'], $dec_cantidad),
           'descripcion' => $descripcion,
           'pvp' => $this->show_precio($lineas[$linea_actual]['pvpunitario'], $factura['coddivisa'], TRUE),
             // DESCUENTO
             //'dto' => $this->show_numero($lineas[$linea_actual]->dtopor) . " %",
           'dto' => $this->show_precio($descuento, $factura['coddivisa'], TRUE),
           'iva' => $this->show_numero($lineas[$linea_actual]['iva']) . " %",
           're' => $this->show_numero($lineas[$linea_actual]['recargo']) . " %",
           'irpf' => $this->show_numero($lineas[$linea_actual]['irpf']) . " %",
           'importe' => $this->show_precio($lineas[$linea_actual]['pvptotal'], $factura['coddivisa'])
           );

          if($lineas[$linea_actual]['dtopor'] == 0)
          {
            $fila['dto'] = '';
          }

          if($lineas[$linea_actual]['recargo'] == 0)
          {
            $fila['re'] = '';
          }

          if($lineas[$linea_actual]['irpf'] == 0)
          {
            $fila['irpf'] = '';
          }

          if( !$lineas[$linea_actual]['mostrar_cantidad'] )
          {
            $fila['cantidad'] = '';
          }

          if( !$lineas[$linea_actual]['mostrar_precio'] )
          {
            $fila['pvp'] = '';
            $fila['dto'] = '';
            $fila['iva'] = '';
            $fila['re'] = '';
            $fila['irpf'] = '';
            $fila['importe'] = '';
          }



          // if( get_class_name($lineas[$linea_actual]) == 'linea_factura_cliente' AND $this->impresion['print_alb'] )
          // {
          //   $fila['alb'] = $lineas[$linea_actual]->albaran_numero();
          // }

          $pdf_doc->add_table_row($fila);
          $linea_actual++;
        }

        $pdf_doc->save_table(
          array(
            'cols' => array(
                      // REFERENCIA
              'ref' => array('width'=>40,'justification' => 'center'),
                      // CANTIDAD
              'cantidad' => array('width'=>40,'justification' => 'center'),
                      // DESCRIPCION
              'descripcion' => array('width'=>300,'justification' => 'left'),
                      // PRECIO UNITARIO
              'pvp' => array('justification' => 'center'),
                      // DESCUENTO
              'dto' => array('width'=>50,'justification' => 'center'),
              'iva' => array('justification' => 'right'),
              're' => array('justification' => 'right'),
              'irpf' => array('justification' => 'right'),
              'importe' => array('justification' => 'right')
              ),
            'fontSize' => 7,
            'width' => 550,
            'shaded' => 1,
            'shadeCol' => array(0.95, 0.95, 0.95),
            'xPos'=>'left',
            'xOrientation'=>'right',
            'lineCol' => array(0.3, 0.3, 0.3),
            'showLines' => 2,
            )
          );

      /// ¿Última página?
        if( $linea_actual == count($lineas) )
        {
        // TABLA DE TOTALES
        if ($factura['fecha'] < '01-06-2017') {
          $condicionPorIva = '14%';
        }else{
          $condicionPorIva = '12%';
        }

          $pdf_doc->set_y(220);

          $pdf_doc->new_table();

          $pdf_doc->add_table_row(
            array(
              'campo' => "<b>SUBTOTAL $condicionPorIva</b>",
              'valor' => $this->show_precio($factura['neto']),
              )
            );

          $pdf_doc->add_table_row(
            array(
              'campo' => "<b>SUBTOTAL 0%</b>",
              'valor' => $this->getSinIva($factura),
              )
            );

          $totaldescuento = ($totaldescuento * 100) / 100; 

          $pdf_doc->add_table_row(
            array(
              'campo' => "<b>DESCUENTO</b>",
              'valor' => $this->show_precio($totaldescuento),
              )
            );

          $pdf_doc->add_table_row(
            array(
              'campo' => "<b>IVA $condicionPorIva</b>",
              'valor' => $this->show_precio($factura['totaliva']),
              )
            );

            //$pdf_doc->add_table_row(
                    //array(
                     //   'campo' => "<b>IVA 0%</b>",
                    //    'valor' => '0.00',
                   // )
                    //);

          $pdf_doc->add_table_row(
            array(
              'campo' => "<b>VALOR TOTAL</b>",

              'valor' => '<b>'.$this->show_precio($factura['total'], $factura['coddivisa'], TRUE).'</b>',
              )
            );





          $pdf_doc->save_table(
           array(
            'cols' => array(
              'campo' => array('justification' => 'left'),
              'valor' => array('justification' => 'right'),
              ),
            'width' => 200,
            'shaded' => 0, 
            'xPos'=>'right',
            'xOrientation'=>'left',
            'showLines'=> 2,
            'showHeadings' => 0,
            'fontSize' =>8

            )
           );



// TABLA DE INFORMACION ADICIONAL

          $pdf_doc->set_y(220);

          $pdf_doc->new_table();

// HEADER DE INFORMACION ADICIONAL

          $pdf_doc->add_table_row(
            array(
              'campo1' => "\n"."<b>INFORMACIÓN ADICIONAL</b>",
              )
            );

// TODO DE DIRECCION DEL CLIENTE

          $direccion = $factura['direccion'];
         //  if($factura->apartado)
         //  {
         //   $direccion .= ' - '.ucfirst(FS_APARTADO).': '.$factura->apartado;
         // }
         if($factura['codpostal'])
         {
           $direccion .= ' - CP: '.$factura['codpostal'];
         }

          $ciudad="";

          if ($factura['ciudad'])
          {
            $consulta = $this->conexion->query("SELECT * FROM provincia WHERE codigo = '".$factura['ciudad']."';");
            $ciudad = mysqli_fetch_array($consulta);
            $ciudad = $ciudad['nombre'];
          }

          $provincia="";
          if ($factura['provincia'])
          {
            $consulta = $this->conexion->query("SELECT * FROM provincia WHERE codigo = '".$factura['provincia']."';");
            $provincia = mysqli_fetch_array($consulta);
            $provincia = $provincia['nombre'];
          }

          $direccion .= ' - '.$provincia.' ('.$ciudad.')';

         $row = array(
          'campo1' => "\n"."<b>Dirección:</b> ".$pdf_doc->fix_html($direccion),
          );

         $pdf_doc->add_table_row($row);

// VERIFICO SI TIENE TELF.

         if (empty($cliente0['telefono1'])) {
          $telf = 'N/A';
        }else{
          $telf = $cliente0['telefono1'];
          if (!empty($cliente0['telefono2'])) {
           $telf .= ' / '.$cliente0['telefono2'];
         }
       }

       $pdf_doc->add_table_row(
        array(
          'campo1' => "\n"."<b>Telf. de Contacto:</b> ".$telf,
          )
        );

// VERIFICO SI EL CLIENTE TIENE EMAIL

       if (empty($cliente0['email'])) {
        $email = 'N/A';
      }else{
        $email = $cliente0['email'];
      }

      $pdf_doc->add_table_row(
        array(
          'campo1' => "\n"."<b>Email: </b> ".$email,
          )
        );

// VERIFICO SI EL CLIENTE TIENE OBSERVACIONES

      if($factura['observaciones'] != '')
      {
        $observacion = $pdf_doc->fix_html($factura['observaciones']);
      }else{
        $observacion = 'Ninguna';
      }

      $pdf_doc->save_table(
       array(
        'cols' => array(
          'campo1' => array('justification' => 'left'),
          ),
        'showLines' => 0,
        'width' => 340,
        'shaded' => 0, 
        'xPos'=>'left',
        'xOrientation'=>'right',
        'showLines'=> 1,
        'showHeadings' => 0,
        'fontSize' =>8

        )
       );

    }
  }

}
   //*********************************************************************************
   //****************** FIN PDF NOTA DE CREDITO **************************************

   //--------------------------------------------------------------------------
    //           FUNCIONES NECESARIAS PARA ARMAR EL PDF                        *
    //---------------------------------------------------------------------------
    //
    // Devuelve el texto con los números de serie o lotes de la $linea
    // @param linea_albaran_compra $linea
    // @return string
    //
    public function generar_trazabilidad($linea)
    {
      $lineast = array();
      $this->articulo_traza = new articulo_traza();

      if( get_class_name($linea) == 'linea_albaran_cliente' )
      {
       $lineast = $this->articulo_traza->all_from_linea('idlalbventa', $linea->idlinea);
     }
     else
     {
       $lineast = $this->articulo_traza->all_from_linea('idlfacventa', $linea->idlinea);
     }

     $txt = '';
     foreach($lineast as $lt)
     {
       $txt .= "\n";
       if($lt->numserie)
       {
        $txt .= 'N/S: '.$lt->numserie.' ';
      }

      if($lt->lote)
      {
        $txt .= 'Lote: '.$lt->lote;
      }
    }

    return $txt;
  }

    //
    // Devuelve un string con el número en el formato de número predeterminado.
    // @param type $num
    // @param type $decimales
    // @param type $js
    // @return type
    //
  
  public function show_numero($num = 0, $decimales = 2, $js = FALSE)
  {
    if($js)
    {
      return number_format($num, $decimales, '.', '');
    }
    else
      return number_format($num, $decimales, '.', '');
  }

  //
  // Devuelve un string con el precio en el formato predefinido y con la
  // divisa seleccionada (o la predeterminada).
  // @param type $precio
  // @param type $coddivisa
  // @param type $simbolo
  // @param type $dec nº de decimales
  // @return type
  public function show_precio($precio = 0, $coddivisa = FALSE, $dec = 2)
  {
    return number_format($precio, $dec, '.', '').' ';
  }

   /**
    * Devuelve el símbolo de divisa predeterminado
    * o bien el símbolo de la divisa seleccionada.
    * @param type $coddivisa
    * @return string
    */

  public function getSinIva($factura)
  {
    $lineasfactura = $this->conexion->query("SELECT * FROM lineasfacturascli WHERE idfactura = '".$factura['idfactura']."'");
  
    $lineas = array();

    //transformo los datos de la consulta a un array
    while ($lineas_f = mysqli_fetch_array($lineasfactura))
    {
      $lineas[] = $lineas_f;
    }

    $subtotal0por = 0.00;
    foreach ($lineas as $linea)
    {
      if ($linea['iva']==0)
      {
        $subtotal0por += $linea['pvptotal'];
      }
    }
    return $this->show_precio($subtotal0por);
  }

  public function getConIva($factura)
  {
    $lineasfactura = $this->conexion->query("SELECT * FROM lineasfacturascli WHERE idfactura = '".$factura['idfactura']."'");
  
    $lineas = array();

    //transformo los datos de la consulta a un array
    while ($lineas_f = mysqli_fetch_array($lineasfactura))
    {
      $lineas[] = $lineas_f;
    }

    $subtotal12por = 0.00;
    foreach ($lineas as $linea)
    {
      if ($linea['iva']==12)
      {
        $subtotal12por += $linea['pvptotal'];
      }
    }
    return $this->show_precio($subtotal12por);
  }
  function bround($dVal, $iDec = 2) {
      // banker's style rounding or round-half-even
      // (round down when even number is left of 5, otherwise round up)
      // $dVal is value to round
      // $iDec specifies number of decimal places to retain
      static $dFuzz = 0.00001; // to deal with floating-point precision loss

      $iSign = ($dVal != 0.0) ? intval($dVal / abs($dVal)) : 1;
      $dVal = abs($dVal);

      // get decimal digit in question and amount to right of it as a fraction
      $dWorking = $dVal * pow(10.0, $iDec + 1) - floor($dVal * pow(10.0, $iDec)) * 10.0;
      $iEvenOddDigit = floor($dVal * pow(10.0, $iDec)) - floor($dVal * pow(10.0, $iDec - 1)) * 10.0;

      if (abs($dWorking - 5.0) < $dFuzz) {
          $iRoundup = ($iEvenOddDigit & 1) ? 1 : 0;
      } else {
          $iRoundup = ($dWorking > 5.0) ? 1 : 0;
      }

      return $iSign * ((floor($dVal * pow(10.0, $iDec)) + $iRoundup) / pow(10.0, $iDec));
  }


   //*****************************************************************************
   //****************** PDF GENERAR NOTA DEBITO *****************************

   //--------------------------------------------------------------------------
    //           FUNCIONES NECESARIAS PARA ARMAR EL PDF                        *
    //---------------------------------------------------------------------------
    //
    // Devuelve el texto con los números de serie o lotes de la $linea
    // @param linea_albaran_compra $linea
    // @return string
    //


  public function generar_pdf_nota_debito($factura)
  {

    //EMPRESA
    $consulta = $this->conexion->query("SELECT * FROM empresa");
    $empresa0 = mysqli_fetch_array($consulta);

    //NOTA DE DEBITO
    $consulta = $this->conexion->query("SELECT * FROM notadebitocli WHERE idfactura = '".$factura['idfactura']."';");
    $nota = mysqli_fetch_array($consulta);
    //El número de los documentos son los mismos
    $numero_documento_sri = $factura['numero_documento_sri'];

    //CLIENTE
    $consulta = $this->conexion->query("SELECT * FROM clientes WHERE codcliente = '".$factura['codcliente']."';");
    $cliente0 = mysqli_fetch_array($consulta);

    /// Creamos el PDF y escribimos sus metadatos
    $pdf_doc = new \fs_pdf();
    $pdf_doc->pdf->addInfo('Title', 'NOTA DE CRÉDITO '. $factura['codigo']);
    $pdf_doc->pdf->addInfo('Subject','NOTA DE CRÉDITO ' . $factura['codigo']);
    $pdf_doc->pdf->addInfo('Author', $empresa0['nombre']);


    // if (condition) {
    //   # code...
    // }
    $lppag = 10;

    $pdf_doc->generar_pdf_cabecera_nota_debito_elec($empresa0, $lppag, $nota);
    $this->generar_pdf_datos_empresa_nota_debito($factura, $pdf_doc, $empresa0, $cliente0);
    $this->generar_pdf_datos_cliente_nota_debito($pdf_doc, $nota, $cliente0);
    $this->generar_pdf_lineas_nota_debito($pdf_doc, $nota/*, $lineas*/, $empresa0, $cliente0);


    $consulta = $this->conexion->query("SELECT * FROM formaspago WHERE codpago = '".$nota['codpago']."';");
    $forma_pago = mysqli_fetch_array($consulta);
    if($forma_pago)
    {
       $texto_pago = "\n<b>Forma de pago</b>: ".$forma_pago['descripcion'];
       $texto_pago .= "\n<b>Vencimiento</b>: ".$ncc['vencimiento'];
       $pdf_doc->pdf->ezText($texto_pago, 9);
    }
    if($empresa0['pie_factura']){
      $pdf_doc->pdf->addText(10, 10, 8, $pdf_doc->center_text($pdf_doc->fix_html($empresa0['pie_factura']), 180) );
    }
    if( !file_exists('../documentosElectronicos/enviados/notasDebitoElectronicas/pdfs_enviados/') )
    {
      mkdir('../documentosElectronicos/enviados/notasDebitoElectronicas/pdfs_enviados/');
    }
    $pdf_doc->save('../documentosElectronicos/enviados/notasDebitoElectronicas/pdfs_enviados/'.$factura['codigo'].'.pdf');


  }

  // GENERAR DATOS EMPRESA NOTA DE DEBITO SRI
  public function generar_pdf_datos_empresa_nota_debito($factura,$pdf_doc,$empresa0,$cliente0)
  {
    $pdf_doc->pdf->ezText("\n", 8);
    $pdf_doc->pdf->ezText("\n", 8);
    // $tipo_doc = ucfirst($this->FS_ALBARAN);
    $rectificativa = FALSE;
    $tipo_doc = 'Nota de Debito';

    $tipoidfiscal = 'RUC';
    if(!empty($cliente0)){
      $tipoidfiscal = $cliente0['tipoidfiscal'];
    }

    $pdf_doc->new_table();
    $pdf_doc->add_table_row(
      array(
        'campo1' => "<b>".$empresa0['nombre']."</b> ",
      )
    );

    $pdf_doc->add_table_row(
      array(
        'campo1' => "\n"."<b>Dirección Matríz:</b> ".$empresa0['direccion'],
      )
    );

    // Para la sucursal consultamos la direccion del los establecimietos
    $consulta = $this->conexion->query("SELECT * FROM almacenes WHERE codalmacen = '".$factura['codalmacen']."' ORDER BY codalmacen ASC;");
    $almacen = mysqli_fetch_array($consulta);
    $consulta = $this->conexion->query("SELECT * FROM establecimiento WHERE codestablecimiento = '".$almacen['codestablecimiento']."';");
    $direccionEsta = mysqli_fetch_array($consulta);
    $direccionEsta = $direccionEsta['direccion'];
    $pdf_doc->add_table_row(
      array(
        'campo1' => "\n"."<b>Dirección Sucursal:</b> ".$direccionEsta,
      )
    );
    
    // VALIDO CONTRIBUYENTE y MUESTRO
    $nrocontrib = $this->var->FS_CONTRIBUYENTE_NRO;
    if ($nrocontrib=='000')
    {
      $pdf_doc->add_table_row(
        array(
          'campo1' => "\n"."<b>Contribuyente Especial :</b> NO",
        )
      );
    }else{
      $pdf_doc->add_table_row(
        array(
          'campo1' => "\n"."<b>Contribuyente Especial Nro:</b> ".$nrocontrib,
        )
      );
    }

    // VALIDO SI ESTA OBLIGADO A LLEVAR CONTABILIDAD
    if ($this->var->FS_OB_CONTA==1)
    {
      $ob_conta = 'NO';
    }

    if ($this->var->FS_OB_CONTA==2)
    {
      $ob_conta = 'SI';
    } 

    $pdf_doc->add_table_row(
      array(
        'campo1' => "\n"."<b>OBLIGADO A LLEVAR CONTABILIDAD:</b> ".$ob_conta,
      )
    );

    $pdf_doc->pdf->ezText("\n",10);
    
    $pdf_doc->save_table(
      array(
        'cols' => array(
          'campo1' => array('justification' => 'left'),
        ),
        'showLines' => 0,
        'width' => 250,
        'shaded' => 0, 
        'xPos'=>'left',
        'xOrientation'=>'right',
        'showLines'=> 1,
        'showHeadings' => 0,
        'fontSize' =>8
      )
    );

    $pdf_doc->pdf->ezText("\n", 3);
  }

  public function generar_pdf_datos_cliente_nota_debito($pdf_doc, $nota, $cliente0)
  {

    //Consultamos los datos de la factura de origen
    $consulta = $this->conexion->query("SELECT * FROM facturascli WHERE idfactura = '".$nota['idfactura']."'");
    $factura_origen = mysqli_fetch_array($consulta);
    $consulta = $this->conexion->query("SELECT * FROM co_factura WHERE doc_instancias_id = '".$nota['idnotadebito']."' AND estado = 'AUTORIZADO' AND tipo_doc = '5';");
    $co_factura = mysqli_fetch_array($consulta);
    $tipo_doc = 'Nota de credito';
    $rectificativa = FALSE;
    
    $tipoidfiscal = 'RUC';
    if($cliente0)
    {
       $tipoidfiscal = $cliente0['tipoidfiscal'];
    }
    
    
     // Esta es la tabla con los datos del cliente:
     // Albarán:                 Fecha:
     // Cliente:               CIF/NIF:
     // Dirección:           Teléfonos:
     
    $pdf_doc->new_table();
    $pdf_doc->add_table_row(
            array(
                'campo1' => "<b>Razón Social / Nombres y Apellidos:</b> ".$cliente0['nombre'],
                'dato1' => '<b>Identificación: </b>'.$cliente0['cifnif']      
            )
    );
    
    $pdf_doc->add_table_row(
            array(
                'campo1' => "<b>Fecha de Emisión:</b> ",
                'dato1' => $co_factura['fecha_actualizacion']
            )
    );

    $pdf_doc->add_table_row(
            array(
                'campo1' => "____________________________________________________________________________",
                'dato1' => '______________________________________',
            )
    );

    $pdf_doc->add_table_row(
            array(
                'campo1' => "<b>Comprobante que se modifica:</b> ",
                'dato1' => '<b>FACTURA: </b>'.$factura_origen['numero_documento_sri'],
            )
    );

    $pdf_doc->add_table_row(
            array(
                'campo1' => "<b>Fecha Emisión (Comprobante a modificar):</b> ",
                'dato1' => $factura_origen['fecha'],
            )
    );

    

    // TODO DE DIRECCION DEL CLIENTE
    $pdf_doc->save_table(
       array(
          'cols' => array(
              'campo1' => array('width' => 350, 'justification' => 'left'),
              'dato1' => array('justification' => 'left'),
          ),
          'showLines' => 0,
          'width' => 550,
          'shaded' => 0, 
          'xPos'=>'left',
          'xOrientation'=>'right',
          'showLines'=> 1,
          'showHeadings' => 0,
          'fontSize' =>8

       )
    );
    $pdf_doc->pdf->ezText("\n", 1);
  }


  public function generar_pdf_lineas_nota_debito($pdf_doc, $nota/*, $lineas*/, $empresa0, $cliente0)
  {
    $consulta = $this->conexion->query("SELECT * FROM facturascli WHERE idfactura = '".$nota['idfactura']."';");
    $factura = mysqli_fetch_array($consulta);
    $consulta = $this->conexion->query("SELECT * FROM clientes WHERE codcliente = '".$factura['codcliente']."';");
    $cliente0 = mysqli_fetch_array($consulta);

    $pdf_doc->new_table();
    $table_header = array(
      'razon' => '<b>RAZÓN DE LA MODIFICACIÓN</b>',
      'valor' => '<b>VALOR DE LA MODIFICACIÓN</b>',
      );
    $pdf_doc->add_table_header($table_header);


    // foreach ($lineas as $l) {
      $fila = array(
        'razon' => $nota['motivo'],
        'valor' => $this->show_precio($nota['total'])
        );
      $pdf_doc->add_table_row($fila);
    // }
    $pdf_doc->save_table(
      array(
        'cols' => array(
          //RAZON DE MODIFICACION
          'razon' => array('width'=>300, 'justification'=>'center'),
          //VALOR DE MODIFICACION
          'valor' => array('width'=>250, 'justification'=>'center')
        ),
        'fontSize' => 12,
        'width' => 550,
        'shaded' => 1,
        'shadeCol' => array(0.95, 0.95, 0.95),
        'xPos' => 'left',
        'xOrientation' => 'right',
        'lineCol' => array(0.3, 0.3, 0.3),
        'showLines' => 2
      )
    );

    $pdf_doc->set_y(220);
    $pdf_doc->new_table();
    if ($nota['iva'] !=0) {
      $pdf_doc->add_table_row(
        array(
          'campo' => "<b>SUBTOTAL ".$nota['iva']."%<b>",
          'valor' => $this->show_precio($nota['total'])
        )
      );
      $pdf_doc->add_table_row(
        array(
          'campo' => "<b>SUBTOTAL 0%<b>",
          'valor' => $this->show_precio(0)
        )
      );
      $pdf_doc->add_table_row(
        array(
          'campo' => "<b>IVA ".$nota['iva']."%<b>",
          'valor' => $this->show_precio($nota['total']*($nota['iva']/100))
        )
      );
    }else{
      $pdf_doc->add_table_row(
        array(
          'campo' => "<b>SUBTOTAL 12%<b>",
          'valor' => $this->show_precio(0)
        )
      );
      $pdf_doc->add_table_row(
        array(
          'campo' => "<b>SUBTOTAL ".$nota['iva']."%<b>",
          'valor' => $this->show_precio($nota['total'])
        )
      );
      $pdf_doc->add_table_row(
        array(
          'campo' => "<b>IVA 12%<b>",
          'valor' => $this->show_precio(0)
        )
      );
    }
    $pdf_doc->add_table_row(
      array(
        'campo' => "<b>VALOR TOTAL<b>",
        'valor' => $this->show_precio($nota['total']+($nota['total']*($nota['iva']/100)))
      )
    );

    $pdf_doc->save_table(
      array(
        'cols' => array(
        'campo' => array('justification' => 'left'),
        'valor' => array('justification' => 'right'),
      ),
      'width' => 200,
      'shaded' => 0, 
      'xPos'=>'right',
      'xOrientation'=>'left',
      'showLines'=> 2,
      'showHeadings' => 0,
      'fontSize' =>8
      )
    );

    //DIRECCION DEL CLIENTE
    $dirCliente = $factura['direccion'];
    if (!empty($factura['provincia']))
    {
      $consulta = $this->conexion->query("SELECT * FROM provincia WHERE codigo = '".$factura['provincia']."';");
      $provincia = mysqli_fetch_array($consulta);
      $dirCliente .= " - ".$provincia['nombre'];
    }
    if (!empty($factura['ciudad'])) {
      $consulta = $this->conexion->query("SELECT * FROM provincia WHERE codigo = '".$factura['ciudad']."';");
      $ciudad = mysqli_fetch_array($consulta);
      $dirCliente .= " (".$ciudad['nombre'].")";
    }
    //TELEFONOS DEL CLIENTE
    $telf = "NA";
    if (!empty($cliente0['telefono1'])) {
      $telf = $cliente0['telefono1'];
      if (!empty($cliente0['telefono2'])) {
        $telf .= ' / '.$cliente0['telefono2'];
      }
    }

    //TABLA DE CAMPOS ADICIONALES
    $pdf_doc->set_y(220);
    $pdf_doc->new_table();
    $pdf_doc->add_table_row(
      array(
        'campo1' => "\n"."<b>INFORMACIÓN ADICIONAL</b>"
      )
    );
    $pdf_doc->add_table_row(
      array(
        'campo1' => "\n"."<b>Dirección: </b>".$pdf_doc->fix_html($dirCliente)
      )
    );
    $pdf_doc->add_table_row(
      array(
        'campo1' => "\n"."<b>Telf. de Contacto: </b>".$telf
      )
    );

    $pdf_doc->add_table_row(
      array(
        'campo1' => "\n"."<b>Email: </b> ".$cliente0['email'],
      )
    );

    if($factura['observaciones'] != '')
    {
      $observacion = $pdf_doc->fix_html($factura['observaciones']);
    }else{
      $observacion = 'Ninguna';
    }
    $pdf_doc->save_table(
      array(
        'cols' => array(
          'campo1' => array('justification' => 'left'),
        ),
        'showLines' => 0,
        'width' => 340,
        'shaded' => 0, 
        'xPos'=>'left',
        'xOrientation'=>'right',
        'showLines'=> 1,
        'showHeadings' => 0,
        'fontSize' =>8
      )
    );
  }

}

?>