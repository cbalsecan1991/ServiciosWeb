<?php  
require_once 'variables_globales.php';
require_once '../plugins/facturacion_base/extras/Utilities/Utilities.php';
require_once 'conectarServidorFacturacionE.php';


class xml_nota_debito
{

	function __construct()
	{
		# code...
	}


	public function generar_xml_nota_debito($nota = null)
	{
		//Busco las variables Globales para realizar la conexion
		$var = new variables_globales();
		//Realizo la conexion a la base de datos
		$conexion = mysqli_connect($var->FS_DB_HOST, $var->FS_DB_USER, $var->FS_DB_PASS, $var->FS_DB_NAME);
		//Instacion metodos para validacion de cedulas o RUC
		$valida = new Utilities();

		//VARIABLES EXTRAS
		$resultado = array();

		//EMPRESA
		$consulta = $conexion->query("SELECT * FROM empresa;");
		$empresa0 = mysqli_fetch_array($consulta);
		$rucEmpresa = $empresa0['cifnif'];
		$direccionEmpresa = $empresa0['direccion'];
		$nombreEmpresa = $empresa0['nombre'];

		//FACTURA DE VENTA
		$consulta = $conexion->query("SELECT * FROM facturascli WHERE idfactura = '".$nota['idfactura']."';");
		$factura = mysqli_fetch_array($consulta);


		//CLIENTE   
		$consulta = $conexion->query("SELECT * FROM clientes WHERE codcliente = '".$factura['codcliente']."';");
		$cliente = mysqli_fetch_array($consulta);
		$razonSocialComprador = $cliente['razonsocial'];
		$cedulaCliente = $cliente['cifnif'];
		$telefonoComprador = $cliente['telefono1'];
		$email = $cliente['email'];


		//DIRECCION CLIENTE
      $consulta = $conexion->query("SELECT * FROM dirclientes WHERE codcliente = '".$factura['codcliente']."' ORDER BY id DESC;");
      $dircliente = mysqli_fetch_array($consulta);
      $direccionCliente = $dircliente['direccion'];

		//ALMACEN
		$consulta = $conexion->query("SELECT * FROM almacenes WHERE codalmacen = '".$factura['codalmacen']."' ORDER BY codalmacen ASC;");
      $almacen = mysqli_fetch_array($consulta);
      $estab = $almacen['codestablecimiento'];
      $ptoEmi = $almacen['puntoemision'];

      //NUMERACION DE LA NOTA DE DEBITO
      #El mismo de la factura a retificar
      $secuencial = str_pad($factura['numero'],9,"0",STR_PAD_LEFT);
      $numero = $estab.'-'.$ptoEmi.'-'.$secuencial;


      //VALIDACIÓN DE LOS DATOS ADICIONALES DEL CLIENTE
      if(empty($direccionCliente)){
      	$direccionCliente = 'NA';
      }
      if (empty($email)) {
      	$email = 'NA';
      	$resultado['error_mail'] = "El cliente no cuenta con correo electronico. No se puede enviar la nota de crédito al SRI";
      }
      if (empty($telefonoComprador)) {
      	$telefonoComprador = 'NA';
      }


		//CODIGO IMPUESTO DE IVA
		$codImp = 2;

		//CODIGO IMPUESTO DEL IVA
		switch ($nota['iva']) {
			case '12':
				$codVal = 2;
				break;
			case '14':
				$codVal = 3;
				break;
			case '0':
				$codVal = 0;
				break;
			default:
				$codVal = -1;
				$resultado['error_iva'] = "El valor de iva registrado no es válido en el Ecuador";
				break;
		}

		//AMBIENTE
		switch ($var->FS_AMBIENTE_XML) {
			case '1':
				$servidor = "Desarrollo";
				$ambiente = "01";
				$tambiente = "1";
				break;
			case '2':
				$servidor = "Produccion";
				$ambiente = "02";
				$tambiente = "2";
				break;
			
			default:
				$servidor = "Desarrollo";
				$ambiente = "01";
				$tambiente = "1";
				break;
		}

		//CODIGO DE DOCUMENTO
		$codDoc = "05";

		//TIPO DE DOCUMENTO DEL CLIENTE
      if(strlen($cedulaCliente) == 10){
          $tipoIdentificacion = '05';
      }  elseif (strlen($cedulaCliente) == 13){
          $tipoIdentificacion = '04';
      }  else {
          $tipoIdentificacion = '06';
      }

      //OBLIGADO A LLEVAR CONTABILIDAD
		if($var->FS_OB_CONTA == 1){
			$obligadoContabilidad =  'NO';
		}else{
			$obligadoContabilidad =  'SI';
		}

		//CALCULO DE VALORES
		$baseImp = round($nota['total'],2);
		$iva = round($nota['iva'], 2);
		$ivaPor = round($nota['iva']/100, 2);
		$totalIva = round($baseImp*$ivaPor,2);
		$valorTotal = round($baseImp + $totalIva,2);

		//NUMERO DE AUTORIZACION
		$numeroDeAutorizacionyClave = $this->numeroDeAutorizacionyClave($factura, $rucEmpresa, $tambiente, $codDoc);


		//ARREGLOS DE NIVEL 2 DEL XML
		#pueden llegar a ser varios
		$impuesto['impuesto'] = array(
			"codigo" => $codImp,
			"codigoPorcentaje" => $codVal,
			"tarifa" => number_format($iva, 2, '.', ''),
			"baseImponible" => number_format($baseImp, 2, '.', ''),
			"valor" => number_format($totalIva, 2, '.', '')
		);

		//ARREGLOs DE NIVEL 1 DEL XML
		$infotributaria = array(
			"ambiente" => $tambiente,
			"tipoEmision" => "1",
			"razonSocial" => $nombreEmpresa,
			"nombreComercial" => $nombreEmpresa,
			"ruc" => $rucEmpresa,
			"claveAcceso" => $numeroDeAutorizacionyClave,
			"codDoc" => $codDoc,
			"estab" => $estab,
			"ptoEmi" => $ptoEmi,
			"secuencial" => $secuencial,
			"dirMatriz" => $direccionEmpresa
		);
		$infonotadebito = array(
			"fechaEmision" => date('d/m/Y'),
			"dirEstablecimiento" => $direccionEmpresa,
			"tipoIdentificacionComprador" => $tipoIdentificacion,
			"razonSocialComprador" =>$razonSocialComprador,
			"identificacionComprador" => $cedulaCliente,
			"contribuyenteEspecial" => $var->FS_CONTRIBUYENTE_NRO,
			"obligadoContabilidad" => $obligadoContabilidad,
			"codDocModificado" => "01",
			"numDocModificado" => $numero,
			"fechaEmisionDocSustento" => date('d/m/Y', strtotime($factura['fecha'])),
			"totalSinImpuestos" => number_format($baseImp, 2, '.', ''),
			"impuestos" => $impuesto,
			"valorTotal" => number_format($valorTotal, 2, '.', '')
		);
		$pagos['pago'] = array(
			"formaPago" => "20",
			"total" => number_format($valorTotal, 2, '.', '')
		);
		$motivos['motivo'] = array(
			"razon" => $nota['motivo'],
			"valor" => number_format($baseImp, 2, '.', '')
		);
		$infoAdicional = '<campoAdicional nombre="Direccion">'.$direccionCliente.'</campoAdicional>';
		$infoAdicional .= '<campoAdicional nombre="Email">'.$email.'</campoAdicional>';
		$infoAdicional .= '<campoAdicional nombre="Telefono">'.$telefonoComprador.'</campoAdicional>';


		//ARREGLOS DE NIVEL 0 DEL XML
		$notadebitoxml = array(
				"infoTributaria" => $infotributaria,
				"infoNotaDebito" => $infonotadebito,
				// "pagos" => $pagos,
				"motivos" => $motivos,
				"infoAdicional" => $infoAdicional
			);



		if (!empty($rucEmpresa) && $codVal>=0) {
      	$fe = new \conectarServidorFacturacionE();
			$resultado += $fe->envioFactura($factura, $servidor, $notadebitoxml, $rucEmpresa, $nota['idnotadebito'], $email, $razonSocialComprador, $numero, $numeroDeAutorizacionyClave, $factura['codcliente'], "NOTA-DE-DEBITO");
		}else{
			$resultado['empresa'] = "No se encuentra configurada la empresa";
		}




		// print_r($notadebitoxml);
		return $resultado;
	}




	public function numeroDeAutorizacionyClave($factura, $rucEmpresa, $tambiente, $codDoc)
	{
		//Busco las variables Globales para realizar la conexion
		$var = new variables_globales();
		//Realizo la conexion a la base de datos
		$conexion = mysqli_connect($var->FS_DB_HOST, $var->FS_DB_USER, $var->FS_DB_PASS, $var->FS_DB_NAME);

		$fe = new \conectarServidorFacturacionE();
		$codNumerico = $this->generaAutorizacion();
		$fechaEmision = date('d/m/Y');
		$codalmacenEmpresa = $factura['codalmacen'];
		$numero = $this->retornaNumeroDoc($factura, $conexion);
		$comprobante = array();
		list($estab, $ptoEmi, $secuencial) = explode("-", $numero);

		$numeroDeAutorizacionyClave = $fechaEmision . $codDoc . $rucEmpresa . $tambiente . $estab . $ptoEmi . $secuencial . $codNumerico["0"] . "1"; 
		$numeroDeAutorizacionyClave = str_replace("/", "", $numeroDeAutorizacionyClave);
		$cadenaInvertida = $this->invertirCadena($numeroDeAutorizacionyClave);
		$eldigitoverificador = $this->obtenerSumaPorDigitos($cadenaInvertida);
		if ($eldigitoverificador == 11)
			$eldigitoverificador = 0;
		if ($eldigitoverificador == 10)
			$eldigitoverificador = 1;
		$numeroDeAutorizacionyClave = $numeroDeAutorizacionyClave . $eldigitoverificador;
		return $numeroDeAutorizacionyClave;
	}

	function invertirCadena($cadena) {
		$cadenaInvertida = "";
		for ($x = strlen($cadena) - 1; $x >= 0; $x--) {
			$cadenaInvertida = $cadenaInvertida . substr($cadena, $x, 1);
		}
		return $cadenaInvertida;
	}

	function obtenerSumaPorDigitos($cadena) {
		$pivote = 2;
		$longitudCadena = strlen($cadena);
		$cantidadTotal = 0;
		$b = 1;
		for ($i = 0; $i < $longitudCadena; $i++) {
			if ($pivote == 8) {
				$pivote = 2;
			}
			$temporal = intval(substr($cadena, $i, 1));
			$b++;
			$temporal = $temporal * $pivote;
			$pivote++;
			$cantidadTotal = $cantidadTotal + $temporal;
		}
		$cantidadTotal = 11 - $cantidadTotal % 11;
		return $cantidadTotal;
	}

	public function porcentajeAValores($porcentaje=0, $cantidad=0)
	{
		$porcentaje=$porcentaje;     
		$cantidad1=$cantidad;    
		$resultado=$porcentaje*$cantidad1/100; 
		$total = round(($cantidad1-$resultado) *100)/100;
		return $total;
	}



	public function retornaNumeroDoc($factura, $conexion)
	{
		$consulta = $conexion->query("SELECT * FROM almacenes WHERE codalmacen = '".$factura['codalmacen']."';");
		$almacen = mysqli_fetch_array($consulta);
		$codEstablecimiento = $almacen['codestablecimiento'];
		$puntoEmision = $almacen['puntoemision'];
		return  $codEstablecimiento ."-". $puntoEmision . "-" . str_pad($factura['numero'],9,"0",STR_PAD_LEFT);
	}

	function generaAutorizacion() {
		$multiplica = array(3, 2, 7, 6, 5, 4, 3, 2);
		$laSuma = 0;
		$final = "";
		$codNumerico = "";
		for ($i = 0; $i < 8; $i++) {
			$numero = rand(1, 9);
			$codNumerico = $codNumerico . $numero;
			$laSuma = $laSuma + ($numero * $multiplica[$i]);
		}
		$result = fmod($laSuma, 11);
		$final = 11 - $result;
		$resultado = array();
		if ($final >= 10)
			$resultado = $this->generaAutorizacion();
		else
			$resultado = array($codNumerico, $final);
		return $resultado;
	}







}


?>