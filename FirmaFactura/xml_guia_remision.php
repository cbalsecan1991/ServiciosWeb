<?php
require_once 'variables_globales.php';
require_once '../plugins/facturacion_base/extras/Utilities/Utilities.php';
require_once 'conectarServidorFacturacionE.php';

class xml_guia_remision
{

 public $messages;
 public $errors;
 public $numeroDeAutorizacionyClave;   
 private $articulo_propiedad;
 private $articulo_propiedad_vacio;
 private $cuenta_banco;
 private $divisa;
 private $ejercicio;
 private $empresa;
 private $forma_pago;
 private $impuestos;
 public $transporte0;
 public $iva;
 public $cliente0;
 public $factura0;

 public function generar_xml_guia(&$factura)
 {
    //Busco las variables Globales para realizar la conexion
    $var = new variables_globales();

    //Realizo la conexion a la base de datos
    $conexion = mysqli_connect($var->FS_DB_HOST, $var->FS_DB_USER, $var->FS_DB_PASS, $var->FS_DB_NAME);

    //Instacion metodos para validacion de cedulas o RUC
    $valida = new Utilities();


    $ok = FALSE;
    $lineas = array();
    $lineas = array();
    $detalle = array();
    $detalle2 = array();
    $arrayAux = array();
    $arrayDetalle = array();
    $dato = array();
    $impuestos = array();
    $cont = 0;
    $total = 0;
    $losErrores = "";
    $resultado = array();

    ///obtenemos datos de la empresa
    $consulta = $conexion->query("SELECT * FROM empresa;");
    $empresa0 = mysqli_fetch_array($consulta);
    $rucEmpresa = $empresa0['cifnif'];
    $direccionEmpresa = $empresa0['direccion'];
    $nombreEmpresa = $empresa0['nombre'];

    /// obtenemos datos del cliente    
    $consulta = $conexion->query("SELECT * FROM clientes WHERE codcliente = '".$factura['codcliente']."';");
    $cliente = mysqli_fetch_array($consulta);
    $razonSocialCliente = $cliente['razonsocial'];
    $razonSocialComprador = $razonSocialCliente;
    $cedulaCliente = $cliente['cifnif'];
    $telefonoComprador = $cliente['telefono1'];
    $email = $cliente['email'];

    // Validamos telefono del cliente
    if (empty($telefonoComprador)) {
      $resultado['error'] = "El cliente no registra telefono";
    }
    // Validamos el ruc de la empresa.
    if (!$valida->validarID($rucEmpresa)) {
      $resultado['error'] = "El ruc de su empresa no es valido";
      $losErrores ='error';
    }

    /// obtenemos direccion del cliente
    $consulta = $conexion->query("SELECT * FROM dirclientes WHERE codcliente = '".$factura['codcliente']."';");
    $datosCliente = mysqli_fetch_array($consulta);
    $direccionComprador = $datosCliente['direccion'];

    if (empty($direccionComprador)) {
      $resultado['error'] = "El cliente no registra direccion";
      $direccionComprador = 'NA';
    }
    if (empty($cliente['email'])) {
      $resultado['error'] = "¡El cliente no cuenta con correo electronico!. No se puede enviar la guia de Remision al SRI";
      $email = 'NA';
    }

    $lainfoadicional = ''
              . '<campoAdicional nombre="Direccion">' . $direccionComprador . '</campoAdicional>'
              . '<campoAdicional nombre="Telefono">' . $telefonoComprador . '</campoAdicional>'
              . '<campoAdicional nombre="Email">' . $email . '</campoAdicional>';

     // Asignamos los codigos para el tipoidfiscal en base a la normativa del sri TABLA 7
    $tipoIdentificacionTransportista='06';
    if ( !empty($factura['ruc_transportista']) ) {
      if ( strlen( $factura['ruc_transportista'] ) == 10 ) 
      {
           $tipoIdentificacionTransportista = '05';
      }elseif ( strlen( $factura['ruc_transportista'] ) == 13 ) 
      {
           $tipoIdentificacionTransportista = '04';
      }  
    }else{
      $resultado['error'] = "Debe ingresar el RUC del trasportista";
      $tipoIdentificacionTransportista = 'NA';
      $losErrores = 'error';
    }

    //Consultamos el tipo de Ambiente en el que se encuentra el sistema
    $servidorFacturacionE = $var->FS_AMBIENTE_XML;
    if ( $servidorFacturacionE == 1){
        $servidorFacturacionE = "Desarrollo";
    }elseif ( $servidorFacturacionE == 2) {
        $servidorFacturacionE = "Produccion";
    }
    switch ($servidorFacturacionE) {
          case "Desarrollo":
              $servidor = "Desarrollo";
              $ambiente = "01";
              $tambiente = "1";
              break;
          case "Pruebas":
              $servidor = "Pruebas";
              $ambiente = "01";
              $tambiente = "1";
              break;
          case "Produccion":
              $servidor = "Produccion";
              $ambiente = "02";
              $tambiente = "2";
              break;
      }

    //Consultamos datos de la Factura
    //$fechaEmision = date('d/m/Y', strtotime($factura->fecha) );
    $fechaEmision = $factura['fecha'];
    $codDoc = "06";
    $codalmacenEmpresa = $factura['codalmacen'];
    $numero = $this->retornaNumeroDoc($factura, $conexion);
    $comprobante = array();
    list($estab, $ptoEmi, $secuencial) = explode("-", $numero);
    $numeroDeAutorizacionyClave = $this->numeroDeAutorizacionyClave($factura, $rucEmpresa, $tambiente, $codDoc);

    //Consultamos si esta obligado a llevar contabilidad
    if($var->FS_OB_CONTA == 1){
       $obligadoContabilidad =  'NO';
    }else{
       $obligadoContabilidad =  'SI';
    }
    //print_r($factura);
    //Consultamos datos del trasporte
    // $this->transporte0= new \agencia_transporte();
    // $transporte=$this->transporte0->get($factura->envio_codtrans);
    // $razonSocialTransportista='NA';
    // $obligadoContaTrans='';
    // $obligadoContabilidadTransporte='';
    // if ($transporte) {
    //   $razonSocialTransportista=$transporte->nombre;
    //   $obligadoContaTrans = $transporte->contabilidad;
    //   if($obligadoContaTrans==1)
    //   {
    //     $obligadoContabilidadTransporte="SI";
    //   }else
    //   {
    //     $obligadoContabilidadTransporte="NO";
    //   }
    // }else{
    //   $resultado['error'] = "Debe escoger una agencia de trasporte!";
    // }
//------------------------Por Defecto va el nombre del conductor

    $agencia = $conexion->query("SELECT * FROM agenciastrans WHERE codtrans = '".$factura['codtransenv']."';");
    $agencia = mysqli_fetch_array($agencia);
    if(!empty($agencia)){
      $razonSocialTransportista = $agencia['nombre'];
      if ($agencia['contabilidad'] == 1) {
        $obligadoContabilidadTransporte="SI";
      }else{
        $obligadoContabilidadTransporte="NO";
      }
    }else{
      $razonSocialTransportista = $factura['nombreenv'] . " " . $factura['apellidosenv'];
      $obligadoContabilidadTransporte="NO";
    }



    $descTrans=$factura['descripcion_trans'];
    $motivo=$factura['motivo_trans'];
    $ruta=$factura['direccion_ruta'];
    $placa=$factura['apartado'];
    $aduanaDoc=$factura['doc_aduanero'];
    $fechaFin=$factura['fecha_fin_trans'];

    //detalle factura
    $lineas = array();
    $lineas = array();
    $detalle = array();
    $detalle2 = array();
    $arrayAux = array();
    $arrayDetalle = array();
    $dato = array();
    $cont = 0;
    $total = 0;
    $descuento = 0;
    $precioTotalSinImpuesto = 0;
    $precioTotalSinImpuestoDetalle = 0;
    $labeseimponible = 0;
    $elivatotal = 0;
    $sinivas = 0;
    $armaTotalImpuesto = "";
    $losErrores = "";
    $valor = array();

    //Consultamos el detalle de la factura
    $consulta = $conexion->query("SELECT * FROM lineasfacturascli WHERE idfactura = '".$factura['idfactura']."';");
    $detalleFactura = array();
    while ($l = mysqli_fetch_array($consulta)) {
      $detalleFactura[] = $l;
    }
    $monto = 0.00;
    $pvpsindto = 0.00;
    $pvptotal = 0.00;
    $valorDescuento = 0.00;
    $valorIva = 0.00;
    $baseImponible = 0.00;
    $codigoPrincipal = "001";
    $codigoImpuesto = '';
    $importeTotal=0.00;
    $detalle1=array();

    //print_r($detalleFactura);
    foreach ($detalleFactura as $value){
          //Asingnamos el codigo del impuesto en base a la tabla del SRI
          /*++++++++++++++++++++++++++++++++++++++*/
          /* Impuesto               | Código       */
          /*------------------------|--------------*/
          /*  IVA                   | 2            */
          /*  ICE                   | 3            */
          /*  IRBPNR                | 5            */
          /*+++++++++++++++++++++++++++++++++++++++*/
          $codigoImpuesto = 2;
          //Asingnamos el codigoPorcentaje en base a la tabla del SRI
          /*++++++++++++++++++++++++++++++++++++++*/
          /* Porcentaje de IVA      | Código       */
          /*------------------------|--------------*/
          /*  0%                    | 0            */
          /*  12%                   | 2            */
          /*  14%                   | 3            */
          /*  No Objeto de Impuesto | 6            */
          /*  Exento de IVA         | 7            */
          /*+++++++++++++++++++++++++++++++++++++++*/
          switch ($value['iva']) {
            case 0:
              $codigoPorcentaje = 0;
              break;
            case 12:
              $codigoPorcentaje = 2;
              break;
            case 14:
              $codigoPorcentaje = 3;
              break; 
          }
          if (!isset($codigoPorcentaje)) {
            $resultado['error'] = "El porcentaje del (IVA) del producto [' . $value->descripcion . '] se encuentra fuera del marco establecido en la ley del Ecuador";
            $losErrores = 'error';
            $codigoPorcentaje='';
          }
          // Acumulamos el precio total con iva
          if ($value['iva']==12) {
            $baseImponible +=  $value['pvptotal'];
          }
          // Acumulamos el precio total sin iva
          //if ($value->iva==0) {

            $precioTotalSinImpuesto += $value['pvptotal'];
            //$precioTotalSinImpuestoDetalle = $value->pvptotal;
          //}
          
          $descuento += ($value['pvpsindto'] - $value['pvptotal']);

          $valorDescuento = ( $value['pvpsindto'] - $value['pvptotal'] );
          $valorIva += ($value['pvptotal'] * $value['iva']) / 100;

          // //$labeseimponible = ( $labeseimponible + $value->pvptotal ) - $valorDescuento;
          $importeTotal = ($precioTotalSinImpuesto + $valorIva);

          $elivatotal = $elivatotal + $value['iva'] / 100 * ($value['pvptotal'] - $valorDescuento);
          // //$precioTotalSinImpuesto = doubleval( $monto - $valorDescuento );
          $sustituye = array("(\r\n)", "(\n\r)", "(\n)", "(\r)");
          $descripcion = preg_replace($sustituye, "", $value['descripcion']);

          // if (FS_SIS_FARM==1) {
              
          //   $articulo_frac_por_unidad = $articulo->get($value->referencia)->frac_por_unidad;

          //   $precioTotalSinImpuesto_frac = ($value->pvpunitario * $value->cantidad) - $valorDescuento;
          //   $precioTotalSinImpuesto_frac += (($value->pvpunitario/$articulo_frac_por_unidad) * $value->cantidad_frac) - $valorDescuento;

          //   $baseImponible_frac = ($value->pvpunitario * $value->cantidad) - $valorDescuento ;
          //   $baseImponible_frac += (($value->pvpunitario/$articulo_frac_por_unidad) * $value->cantidad_frac) - $valorDescuento ;

          //    $detalle['detalle_'.$cont] = Array(
          //             "codigoInterno" => $value->referencia,
          //             "codigoAdicional" => $value->referencia,
          //             "descripcion" => $descripcion,
          //             "cantidad" => $value->cantidad,
          //         );
          //   }else{

              $detalle['detalle_'.$cont] = Array(
                      "codigoInterno" => $value['referencia'],
                      "codigoAdicional" => $value['referencia'],
                      "descripcion" => $descripcion,
                      "cantidad" => $value['cantidad']);

          //   }

          $cont++;
  }

// -------------------------------------------------------------------------------------------------------------------------------------------

    // Validaciones
    $dirPartida = $factura['dir_partida'];
    $docAduaneroUnico = $factura['doc_aduanero'];
    $codEstabDestino = $factura['cod_estab_destino'];
    if ( empty($dirPartida) ) {
      $resultado['error'] = "Debe ingresar la dirección de partida!";
    }
    if ( empty($docAduaneroUnico) ) {
      $resultado['error'] = "Debe ingresar el Documento Aduanero!";
    }
    if ( empty($codEstabDestino) ) {
      $resultado['error'] = "Debe ingresar el Cod. Estab. Destino!";
    }
    if ($factura['fecha_ini_trans'] > $factura['fecha_fin_trans']) {
      $resultado['error'] = "La fecha de inicio es posterior a la final";
      $losErrores = 'error';
    }


    $guia_remisionArray = Array(
      "infoTributaria" => Array
        (
          "ambiente" => $tambiente,
          "tipoEmision" => 1,
          "razonSocial" => $nombreEmpresa,
          "nombreComercial" => $nombreEmpresa,
          "ruc" => $rucEmpresa,
          "claveAcceso" => $numeroDeAutorizacionyClave,
          "codDoc" => $codDoc,
          "estab" => $estab,
          "ptoEmi" => $ptoEmi,
          "secuencial" => $secuencial,
          "dirMatriz" => $direccionEmpresa
        ),
        "infoGuiaRemision" => Array
        (
          "dirEstablecimiento" => $direccionComprador,
          "dirPartida" => $dirPartida,
          "razonSocialTransportista" => $razonSocialTransportista,
          "tipoIdentificacionTransportista" => $tipoIdentificacionTransportista,
          "rucTransportista" => $factura['ruc_transportista'],
          "rise" => 0,
          "obligadoContabilidad" => $obligadoContabilidad,
          "contribuyenteEspecial" => $var->FS_CONTRIBUYENTE_NRO,
          "fechaIniTransporte" => date('d/m/Y', strtotime($factura['fecha_ini_trans']) ),
          "fechaFinTransporte" => date('d/m/Y', strtotime($factura['fecha_fin_trans']) ), 
          "placa" => $factura['placaenv'],
        ),
      "destinatarios" => Array
        (
          "destinatario" => array
          (
            "identificacionDestinatario" => $cedulaCliente,
            "razonSocialDestinatario" => $razonSocialCliente,
            "dirDestinatario" => $factura['direccionenv'],
            "motivoTraslado" => $factura['motivo_trans'], 
            "docAduaneroUnico" => $docAduaneroUnico,
            "codEstabDestino" => $codEstabDestino,
            "ruta" => $factura['direccion_ruta'],
            "codDocSustento" => "01",
            "numDocSustento" => $factura['numero_documento_sri'],
            "numAutDocSustento" => $factura['autoriza_numero_sri'],
            "fechaEmisionDocSustento" => date('d/m/Y', strtotime($factura['fecha']) ),
                 "detalles" => $detalle,
          ),           
        ),                
          "infoAdicional" => $lainfoadicional,
    );
    //conectamos al servidor de facturacion
    $fe = new \conectarServidorFacturacionE();
    if (isset($rucEmpresa) && $rucEmpresa != "") {
          if (strlen($losErrores) < 1) {
              $resultado += $fe->envioFactura($factura, $servidor, $guia_remisionArray, $rucEmpresa, $factura['idfactura'], $email, $razonSocialComprador, $numero, $numeroDeAutorizacionyClave, $factura['codcliente'] , "GUIA-DE-REMISION");
          }else {
            $resultado['estado'] = "No se puede enviar debido a los errores encontrados";
          }

      } else {
          $resultado['error'] = "No se encuentra configurada su empresa por favor póngase en contacto con su proveedor.";
      }
      // print_r($resultado);
  return $resultado;
 }
  
 
 public function numeroDeAutorizacionyClave($factura, $rucEmpresa, $tambiente, $codDoc)
 {
    //Busco las variables Globales para realizar la conexion
    $var = new variables_globales();

    //Realizo la conexion a la base de datos
    $conexion = mysqli_connect($var->FS_DB_HOST, $var->FS_DB_USER, $var->FS_DB_PASS, $var->FS_DB_NAME);

    $fe = new \conectarServidorFacturacionE();
    $codNumerico = $this->generaAutorizacion();
    $fechaEmision = date('d/m/Y', strtotime($factura['fecha_ini_trans']) );
    $codalmacenEmpresa = $factura['codalmacen'];
    $numero = $this->retornaNumeroDoc($factura, $conexion);
    $comprobante = array();
    list($estab, $ptoEmi, $secuencial) = explode("-", $numero);

    $numeroDeAutorizacionyClave = $fechaEmision . $codDoc . $rucEmpresa . $tambiente . $estab . $ptoEmi . $secuencial . $codNumerico["0"] . "1"; 
    $numeroDeAutorizacionyClave = str_replace("/", "", $numeroDeAutorizacionyClave);
    $cadenaInvertida = $this->invertirCadena($numeroDeAutorizacionyClave);
    $eldigitoverificador = $this->obtenerSumaPorDigitos($cadenaInvertida);
    if ($eldigitoverificador == 11)
          $eldigitoverificador = 0;
    if ($eldigitoverificador == 10)
          $eldigitoverificador = 1;
    $numeroDeAutorizacionyClave = $numeroDeAutorizacionyClave . $eldigitoverificador;
    return $numeroDeAutorizacionyClave;
 }

  function invertirCadena($cadena) {
    $cadenaInvertida = "";
    for ($x = strlen($cadena) - 1; $x >= 0; $x--) {
        $cadenaInvertida = $cadenaInvertida . substr($cadena, $x, 1);
    }
    return $cadenaInvertida;
  }

  function obtenerSumaPorDigitos($cadena) {
      $pivote = 2;
      $longitudCadena = strlen($cadena);
      $cantidadTotal = 0;
      $b = 1;
      for ($i = 0; $i < $longitudCadena; $i++) {
          if ($pivote == 8) {
              $pivote = 2;
          }
          $temporal = intval(substr($cadena, $i, 1));
          $b++;
          $temporal = $temporal * $pivote;
          $pivote++;
          $cantidadTotal = $cantidadTotal + $temporal;
      }
      $cantidadTotal = 11 - $cantidadTotal % 11;
      return $cantidadTotal;
  }

 public function porcentajeAValores($porcentaje=0, $cantidad=0)
 {
     $porcentaje=$porcentaje;     
     $cantidad1=$cantidad;    
     $resultado=$porcentaje*$cantidad1/100; 
     $total = round(($cantidad1-$resultado) *100)/100;
     return $total;
 }
 


  public function retornaNumeroDoc($factura, $conexion)
  {
    $consulta = $conexion->query("SELECT * FROM almacenes WHERE codalmacen = '".$factura['codalmacen']."';");
    $almacen = mysqli_fetch_array($consulta);
    $codEstablecimiento = $almacen['codestablecimiento'];
    $puntoEmision = $almacen['puntoemision'];
    return  $codEstablecimiento ."-". $puntoEmision . "-" . str_pad($factura['numero'],9,"0",STR_PAD_LEFT);
  }

  function generaAutorizacion() {
  $multiplica = array(3, 2, 7, 6, 5, 4, 3, 2);
  $laSuma = 0;
  $final = "";
  $codNumerico = "";
  for ($i = 0; $i < 8; $i++) {
      $numero = rand(1, 9);
      $codNumerico = $codNumerico . $numero;
      $laSuma = $laSuma + ($numero * $multiplica[$i]);
  }
  $result = fmod($laSuma, 11);
  $final = 11 - $result;
  $resultado = array();
  if ($final >= 10)
      $resultado = $this->generaAutorizacion();
  else
      $resultado = array($codNumerico, $final);
  return $resultado;
  }
}

