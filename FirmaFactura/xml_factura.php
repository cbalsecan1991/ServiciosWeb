<?php
require_once 'conectarServidorFacturacionE.php';
require_once 'variables_globales.php';
/**
* 
*/
class xml_factura
{

  public $losErrores;
  
  function __construct()
  {
    
  }

  function generar_xml_factura($factura)
  {

    $resultados = array();

  date_default_timezone_set('America/Bogota');

  $ok = FALSE;
  $paso = TRUE;

  $lineas = array();
  $lineas = array();
  $detalle = array();
  $detalle2 = array();
  $arrayAux = array();
  $arrayDetalle = array();
  $dato = array();
  $cont = 0;
  $total = 0;
  $descuento = 0;
  $precioTotalSinImpuesto = 0;
  $precioTotalSinImpuestoDetalle = 0;
  $labeseimponible = 0;
  $elivatotal = 0;
  $sinivas = 0;
  $armaTotalImpuesto = "";
  $losErrores = "";
  $valor = array();

  //Busco las variables Globales para realizar la conexion
  $var = new variables_globales();

  //Realizo la conexion a la base de datos
  $conexion = mysqli_connect($var->FS_DB_HOST, $var->FS_DB_USER, $var->FS_DB_PASS, $var->FS_DB_NAME);

  //Busco la existencia de la Factura
  $lineasfactura = $conexion->query("SELECT * FROM lineasfacturascli WHERE idfactura = ".$factura[0]['idfactura']);
  
  $detalleFactura = array();

  //transformo los datos de la consulta a un array
  while ($lineas = mysqli_fetch_array($lineasfactura))
  {
    $detalleFactura[] = $lineas;
  }
  $monto = 0.00;
  $pvpsindto = 0.00;
  $pvptotal = 0.00;
  $valorDescuento = 0.00;
  $valorIva = 0.00;
  $baseImponible = 0.00;
  $codigoPrincipal = "001";
  $codigoImpuesto = '';
  $importeTotal=0.00;
  $detalle1=array();

  $detallesProducto = "";

  // print_r($detalleFactura);
  foreach ($detalleFactura as $value):

      //capturo el detalle del articulo
      $detallesProducto = $value['detalles'];
        //Asingnamos el codigo del impuesto en base a la tabla del SRI
        /*++++++++++++++++++++++++++++++++++++++*/
        /* Impuesto               | Código       */
        /*------------------------|--------------*/
        /*  IVA                   | 2            */
        /*  ICE                   | 3            */
        /*  IRBPNR                | 5            */
        /*+++++++++++++++++++++++++++++++++++++++*/
        $codigoImpuesto = 2;
        //Asingnamos el codigoPorcentaje en base a la tabla del SRI
        /*++++++++++++++++++++++++++++++++++++++*/
        /* Porcentaje de IVA      | Código       */
        /*------------------------|--------------*/
        /*  0%                    | 0            */
        /*  12%                   | 2            */
        /*  14%                   | 3            */
        /*  No Objeto de Impuesto | 6            */
        /*  Exento de IVA         | 7            */
        /*+++++++++++++++++++++++++++++++++++++++*/
        //$value[12] = iva
        switch ($value['iva']) {
          case 0:
            $codigoPorcentaje = 0;
            break;
          case 12:
            $codigoPorcentaje = 2;
            break;
          case 14:
            $codigoPorcentaje = 3;
            break; 
        }
        if (!isset($codigoPorcentaje)) {
          $resultados['iva_incorrecto'] = 'El porcentaje del (IVA) del producto [' . $value['descripcion'] . '] se encuentra fuera del marco establecido en la ley del Ecuador';
          $codigoPorcentaje='';
          $paso = FALSE;
        }

        //value[15] = pvptotal
        // Acumulamos el precio total con iva
        if ($value['iva']==12) {
          $baseImponible +=  $value['pvptotal'];
        }
        // Acumulamos el precio total sin iva
        //if ($value->iva==0) {
        //formo mi array de detalles adicionales
        $arrList = array();
        $arrList = explode("/:", $detallesProducto);
        // print_r($arrList);
        $armoDetalles = '';
        if(!empty($arrList))
        {
          $arrLot = explode("%", $arrList[4]);
          $arrFab = explode("%", $arrList[5]);
          $arrVen = explode("%", $arrList[6]);
          //$lotes = '';

          //print_r($arrVen);
          foreach ($arrLot as $key => $value1)
          {
            //$val="Lote: ".trim($value1)."Fecha de Fabricacion:".$arrFab[$key]."Fecha de Vencimiento:"."$arrVen[$key]";
            $armoDetalles = ''
                  . '<detAdicional nombre="Detalle'.$key.'" valor="L: '.trim($value1).'"/>'
                  . '<detAdicional nombre="Detalle'.$key.'" valor="Fab: '.$arrFab[$key].'"/>'
                  . '<detAdicional nombre="Detalle'.$key.'" valor="Ven: '.$arrVen[$key].'"/>';
          }

        }

          $precioTotalSinImpuesto += $value['pvptotal'];
        //$precioTotalSinImpuestoDetalle = $value->pvptotal;
        //s}
        
        //value[14] = pvpsindto
        $descuento += (round($value['pvpsindto'],2) - $value['pvptotal']);

        $valorDescuento = ( round($value['pvpsindto'],2) - $value['pvptotal']);
        $valorIva += ($value['pvptotal'] * $value['iva']) / 100;

        //$labeseimponible = ( $labeseimponible + $value->pvptotal ) - $valorDescuento;
        $importeTotal = ($precioTotalSinImpuesto + $valorIva);

        //value[4] = descripcion
        $elivatotal = $elivatotal + $value['iva'] / 100 * ($value['pvptotal'] - $valorDescuento);
        //$precioTotalSinImpuesto = doubleval( $monto - $valorDescuento );
        $sustituye = array("(\r\n)", "(\n\r)", "(\n)", "(\r)");
        $descripcion = preg_replace($sustituye, "", $value['descripcion']);
        
        if(!empty($arrList))
        {
          $detalle['detalle_'.$cont] = Array(
            "codigoPrincipal" => $value['referencia'],
            "descripcion" => trim($descripcion),
            "cantidad" => $value['cantidad'],
            "precioUnitario" => str_replace(",", "", number_format($value['pvpunitario'], 5)),
            "descuento" => (isset($valorDescuento)) ? str_replace(",", "",number_format(doubleval($valorDescuento), 2)) : 0.00,
            "precioTotalSinImpuesto" => str_replace(",", "", number_format(doubleval(($value['pvpunitario'] * $value['cantidad']) - $valorDescuento), 2)),
            "detallesAdicionales" =>  $armoDetalles,
            "impuestos" => Array(
              "impuesto" => Array(
                "codigo" => $codigoImpuesto,
                "codigoPorcentaje" => $codigoPorcentaje,
                "tarifa" => str_replace(",", "", number_format(doubleval($value['iva']), 2)),
                "baseImponible" => str_replace(",", "", number_format(doubleval( ($value['pvpunitario'] * $value['cantidad']) - $valorDescuento ), 2)),
                "valor" => str_replace(",", "", number_format(doubleval($value['pvptotal'] + (($value['pvptotal']*$value['iva'])/100 )), 2))
              )
            )
          );
        }else{
          $detalle['detalle_'.$cont] = Array(
            "codigoPrincipal" => $codigoPrincipal,
            "descripcion" => trim($descripcion),
            "cantidad" => $value['cantidad'],
            "precioUnitario" => str_replace(",", "", number_format($value['unitario'], 5)),
            "descuento" => (isset($valorDescuento)) ? str_replace(",", "",number_format(doubleval($valorDescuento), 2)) : 0.00,
            "precioTotalSinImpuesto" => str_replace(",", "", number_format(doubleval(($value['unitario'] * $value['cantidad']) - $valorDescuento), 2)),
            "impuestos" => Array(
              "impuesto" => Array(
                "codigo" => $codigoImpuesto,
                "codigoPorcentaje" => $codigoPorcentaje,
                "tarifa" => str_replace(",", "", number_format(doubleval($value['iva']), 2)),
                "baseImponible" => str_replace(",", "", number_format(doubleval( ($value['unitario'] * $value['cantidad']) - $valorDescuento ), 2)),
                "valor" => str_replace(",", "", number_format(doubleval($value['pvptotal'] + (($value['pvptotal']*$value['iva'])/100 )), 2))
              )
            )
          );
        }        
      $cont++;
  endforeach;

  // print_r($detalle);

  //Seteo el formato de Facturacion Electrónica
  $servidorFacturacionE = $var->servidorFacturacionE;
  
  switch ($servidorFacturacionE)
  {
    case "Desarrollo":
        $servidor = "Desarrollo";
        $ambiente = "01";
        $tambiente = "1";
        break;
    case "Pruebas":
        $servidor = "Pruebas";
        $ambiente = "01";
        $tambiente = "1";
        break;
    case "Produccion":
        $servidor = "Produccion";
        $ambiente = "02";
        $tambiente = "2";
        break;
  }

  /// obtenemos datos de la empresa
  $empresa = $conexion->query("SELECT * FROM empresa");

  $empresas = array();

  //transformo los datos de la consulta a un array
  while ($emp = mysqli_fetch_array($empresa))
  {
    $empresas[] = $emp;
  }

  $rucEmpresa = $empresas[0]['cifnif'];
  $direccionEmpresa = $empresas[0]['direccion'];
  
  //Para la sucursal consultamos la direccion del los establecimietos
  $Establec = $conexion->query("SELECT e.direccion FROM almacenes a, establecimiento e WHERE a.codalmacen = '".$factura[0]['codalmacen']."' AND a.codestablecimiento = e.codestablecimiento");

  $establecimientos = array();

  //transformo los datos de la consulta a un array
  while ($esta = mysqli_fetch_array($Establec))
  {
    $establecimientos[] = $esta;
  }

  //print_r($establecimientos);

  $direccionEsta = $establecimientos[0]['direccion'];

  if (empty($direccionEsta))
  {
    $resultados['error_establecimiento'] = "La dirección del establecimiento no esta parametrizada!";
    $paso = FALSE;
  }
  $nombreEmpresa = $empresas[0]['nombre'];

  /// obtenemos datos del cliente

  //print_r($factura);
  $clientes = $conexion->query("SELECT * FROM clientes WHERE codcliente = '".$factura[0]['codcliente']."'");

  $cliente = array();

  //transformo los datos de la consulta a un array
  while ($cli = mysqli_fetch_array($clientes,MYSQLI_ASSOC))
  {
    $cliente[] = $cli;
  }

  $razonSocialCliente = $cliente[0]['razonsocial'];
  $razonSocialComprador = $razonSocialCliente;
  $cedulaCliente = $cliente[0]['cifnif'];
  //$tipoidfiscal = $cliente->tipoidfiscal;
  //$this->new_msg_error($cedulaCliente);
  if(strlen($cedulaCliente) == 10){
      $tipoIdentificacion = '05';
  }  elseif (strlen($cedulaCliente) == 13){
      $tipoIdentificacion = '04';
  }  elseif ($cedulaCliente == '9999999999999') {
      $tipoIdentificacion = '07';
  }  elseif ($cedulaCliente == '9999999999') {
      $tipoIdentificacion = '07';
  }  else{
      $tipoIdentificacion = '06';
  }

  $telefonoComprador = $cliente[0]['telefono1'];
  // Validamos telefono del cliente
  if (empty($telefonoComprador))
  {
    $resultados['error_telefono'] = "El cliente no registra telefono!";
    $paso = FALSE;
    $telefonoComprador = 'NA';
  }

  $dirclientes = $conexion->query("SELECT * FROM dirclientes WHERE ".$factura[0]['codcliente']);

  $dircli = array();

  //transformo los datos de la consulta a un array
  while ($dcli = mysqli_fetch_array($dirclientes))
  {
    $dircli[] = $dcli;
  }  
  
  $direccionComprador='';
  foreach ($dircli as $data) {
      $direccionComprador = $data['direccion'];
  }
  if (empty($direccionComprador)) {
    $resultados['error_dir_cliente'] = "El cliente no registra direccion";
    $paso = FALSE;
  }
  if (empty($cliente[0]['email']))
  {
    $resultados['error_correo'] = "¡El cliente no cuenta con correo electronico!. No se puede enviar la factura al SRI";
    $email = '';
    $paso = FALSE;
  }else{
    $email = $cliente[0]['email'];
  }

  $agentes = $conexion->query("SELECT * FROM agentes WHERE codagente = '".$factura[0]['codagente']."'");

  $agente = array();

  //transformo los datos de la consulta a un array
  while ($ag = mysqli_fetch_array($agentes))
  {
    $agente[] = $ag;
  }
  
  $vendedor = " - ";

  if(!empty($agente))
  {        
    $vendedor = $agente[0]['nombre']." ".$agente[0]['apellidos'];
  }

  $arrList = array();
  $arrList = explode("/:", $detallesProducto);
  $arrFab = explode("%", $arrList[5]);
  $arrVen = explode("%", $arrList[6]);
  
  $lainfoadicional = ''
            . '<campoAdicional nombre="Direccion">' . $direccionComprador . '</campoAdicional>'
            . '<campoAdicional nombre="Telefono">' . $telefonoComprador . '</campoAdicional>'
            . '<campoAdicional nombre="Email">' . $email . '</campoAdicional>'
            . '<campoAdicional nombre="Vendedor">' . $vendedor . '</campoAdicional>';
  if(!empty($arrList))
  {
    $lainfoadicional .= '<campoAdicional nombre="Nombre Comercial">' . $arrList[1] . '</campoAdicional>'
            . '<campoAdicional nombre="Nombre Generico">' . $arrList[2] . '</campoAdicional>'
            . '<campoAdicional nombre="Presentacion">' . $arrList[3] . '</campoAdicional>';
    $lainfoadicional .= '<campoAdicional nombre="Registro Sanitario">' . $arrList[7] . '</campoAdicional>'
            . '<campoAdicional nombre="Procedencia">' . $arrList[8] . '</campoAdicional>'
            . '<campoAdicional nombre="OC">' . $arrList[10] . '</campoAdicional>';
  }

  //print_r($lainfoadicional);

  $fechaEmision = date('d/m/Y', strtotime($factura[0]['fecha']));
  $codDoc = "01";

  $comprobante = array();
  //formo el numero de documento
  $almacenes = $conexion->query("SELECT * FROM almacenes WHERE codalmacen = '".$factura[0]['codalmacen']."';");
  $arr_almacen = array();

  //valores por defecto en caso de que no esxista el almacen
  $codEsta = '001';
  $ptoEmis = '001';

  //transformo los datos de la consulta a un array
  while ($alm = mysqli_fetch_array($almacenes))
  {
    $arr_almacen[] = $alm;
  }

  if(!empty($arr_almacen))
  {
    $codEsta = $arr_almacen[0]['codestablecimiento'];
    $ptoEmis = $arr_almacen[0]['puntoemision'];
  }

  $numero = $codEsta."-".$ptoEmis."-".str_pad($factura[0]['numero'],9,"0",STR_PAD_LEFT);
  
  list($estab, $ptoEmi, $secuencial) = explode("-", $numero);
  
  $numeroDeAutorizacionyClave = $this->numeroDeAutorizacionyClave($factura, $rucEmpresa, $tambiente,$numero);

  //print_r($numeroDeAutorizacionyClave);

  //if ($baseImponible > 0) {
      $armaTotalImpuesto.=''
          . '<totalImpuesto>
                      <codigo>' . $codigoImpuesto . '</codigo>
                      <codigoPorcentaje>2</codigoPorcentaje>                                    
                      <baseImponible>' .str_replace(",", "", number_format(doubleval($baseImponible), 2))  . '</baseImponible>
                      <valor>' . str_replace(",", "", number_format(doubleval($valorIva), 2))  . '</valor>
                  </totalImpuesto>';
    //    }

      $FS_OB_CONTA = $var->FS_OB_CONTA;
      $FS_CONTRIBUYENTE_NRO = $var->FS_CONTRIBUYENTE_NRO;

      if(!empty($arrList))
      {
        if($arrList[9] == 'CON UTILIZACION DEL SISTEMA FINANCIERO')
        {
          $fp = '20';
        }else{
          $fp = '01';
        }
      }


      if($FS_OB_CONTA == 1){
         $obligadoContabilidad =  'NO';
      }else{
         $obligadoContabilidad =  'SI';
      }
      $facturaArray = Array(
        "infoTributaria" => Array
            (
            "ambiente" => $tambiente,
            "tipoEmision" => 1,
            "razonSocial" => $nombreEmpresa,
            "nombreComercial" => $nombreEmpresa,
            "ruc" => $rucEmpresa,
            "claveAcceso" => $numeroDeAutorizacionyClave,
            "codDoc" => $codDoc,
            "estab" => $estab,
            "ptoEmi" => $ptoEmi,
            "secuencial" => $secuencial,
            "dirMatriz" => $direccionEmpresa
        ),
        "infoFactura" => Array
            (
            "fechaEmision" => $fechaEmision,
            "dirEstablecimiento" => $direccionEsta,
            "contribuyenteEspecial" => $FS_CONTRIBUYENTE_NRO,
            "obligadoContabilidad" => $obligadoContabilidad, 
            "tipoIdentificacionComprador" => $tipoIdentificacion,
            "razonSocialComprador" => $razonSocialComprador,
            "identificacionComprador" => $cedulaCliente,
            "direccionComprador" => $direccionComprador,
            "totalSinImpuestos" => str_replace(",", "", number_format(doubleval($precioTotalSinImpuesto), 2)),
            "totalDescuento" => str_replace(",", "", number_format(doubleval($descuento), 2)),
            "totalConImpuestos" => $armaTotalImpuesto,
            "propina" => "0.00",
            "importeTotal" => str_replace(",", "", number_format(doubleval( $importeTotal ), 2)),
            "moneda" => "DOLAR",
            "pagos" => Array
            (
              "pago" => Array
              (
                "formaPago" => $fp,
                "total" => str_replace(",", "", number_format(doubleval( $importeTotal ), 2)),
              )
            )
        ),
        "detalles" => $detalle,
        "infoAdicional" => $lainfoadicional,
    );

      //print_r($facturaArray);
  
      $fe = new conectarServidorFacturacionE();
      if (isset($rucEmpresa) && $rucEmpresa != "")
      {
        $resp = array();

        if (strlen($losErrores) < 1)
        {
            $respuesta = $fe->envioFactura($factura[0], $servidor, $facturaArray, $rucEmpresa, $factura[0]['idfactura'], $email, $razonSocialComprador, $numero, $numeroDeAutorizacionyClave, $factura[0]['codcliente'], "FACTURA");

            if (is_numeric($respuesta))
              $this->losErrores = "";
            else{              
              $this->losErrores = $respuesta;
              $resultados += $respuesta;
              //$resultados['errores_sri'] = $respuesta;
            }
        }else {
          //$this->new_message("$losErrores"."</br>");
          //$resultados['errores_sri'] = $this->losErrores;
        }
      }else{
        $resultados['error_empresa'] = "No se encuentra configurada su empresa por favor póngase en contacto con su proveedor";
      }

      return $resultados;
      
  }

 function numeroDeAutorizacionyClave($factura, $rucEmpresa, $tambiente,$numero)
 {
    $codNumerico = $this->generaAutorizacion();
    $fechaEmision = date('d/m/Y', strtotime($factura[0]['fecha']));
    $codDoc = "01";
    $comprobante = array();
    list($estab, $ptoEmi, $secuencial) = explode("-", $numero);

    $numeroDeAutorizacionyClave = $fechaEmision . $codDoc . $rucEmpresa . $tambiente . $estab . $ptoEmi . $secuencial . $codNumerico["0"] . "1"; 
    $numeroDeAutorizacionyClave = str_replace("/", "", $numeroDeAutorizacionyClave);
    $cadenaInvertida = $this->invertirCadena($numeroDeAutorizacionyClave);
    $eldigitoverificador = $this->obtenerSumaPorDigitos($cadenaInvertida);
    if ($eldigitoverificador == 11)
          $eldigitoverificador = 0;
    if ($eldigitoverificador == 10)
          $eldigitoverificador = 1;
    $numeroDeAutorizacionyClave = $numeroDeAutorizacionyClave . $eldigitoverificador;
    return $numeroDeAutorizacionyClave;
 }

  function generaAutorizacion()
  {
    $multiplica = array(3, 2, 7, 6, 5, 4, 3, 2);
    $laSuma = 0;
    $final = "";
    $codNumerico = "";
    for ($i = 0; $i < 8; $i++) {
        $numero = rand(1, 9);
        $codNumerico = $codNumerico . $numero;
        $laSuma = $laSuma + ($numero * $multiplica[$i]);
    }
    $result = fmod($laSuma, 11);
    $final = 11 - $result;
    $resultado = array();
    if ($final >= 10)
        $resultado = $this->generaAutorizacion();
    else
        $resultado = array($codNumerico, $final);
    return $resultado;
  }

  function invertirCadena($cadena)
  {
    $cadenaInvertida = "";
    for ($x = strlen($cadena) - 1; $x >= 0; $x--) {
        $cadenaInvertida = $cadenaInvertida . substr($cadena, $x, 1);
    }
    return $cadenaInvertida;
  }

  function obtenerSumaPorDigitos($cadena)
  {
    $pivote = 2;
    $longitudCadena = strlen($cadena);
    $cantidadTotal = 0;
    $b = 1;
    for ($i = 0; $i < $longitudCadena; $i++) {
        if ($pivote == 8) {
            $pivote = 2;
        }
        $temporal = intval(substr($cadena, $i, 1));
        $b++;
        $temporal = $temporal * $pivote;
        $pivote++;
        $cantidadTotal = $cantidadTotal + $temporal;
    }
    $cantidadTotal = 11 - $cantidadTotal % 11;
    return $cantidadTotal;
  }

}

?>