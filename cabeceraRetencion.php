<?php

require_once 'lib/nusoap.php';
require_once 'variables_globales.php';
require_once '../plugins/facturacion_base/extras/Utilities/Utilities.php';
date_default_timezone_set('America/Bogota');


//Instancio o objeto del servicio
$servicio = new soap_server();
	
//Name Espace
$ns = "urn:cabeceraRecwsdl";

//configuramos el servicio 	
//1er parametro Nombre del Webservice
//2do parametro Name space
$servicio->configureWSDL("cabeceraretencion",$ns);
//almacena el espacio de nobre de destino
$servicio->schemaTargetNamespace = $ns;

//Metodo (register) para registrar el servicio 4 parametros
//1er parametro Nombre de la Funcion que se va a Desarrollar
//2do parametros de Entrada para la Funcion
//3er parametro return de errores o de respuestas
//4to Name space

//funcion para guardar cabecera de facturascli
$servicio->register("cabeceraRec", 
	array(
		'identificacion' => 'xsd:string',
		'nombreproveedor' => 'xsd:string',
		'Telefono' => 'xsd:string',
		'email' => 'xsd:string',
		'Provincia' => 'xsd:string',
		'ciudad' => 'xsd:string',
		'Direccion' => 'xsd:string',
		'fechaemision' => 'xsd:string',
		'horaemision' => 'xsd:string',
		'numerofactura' => 'xsd:string',
		'Formapago' => 'xsd:string',
		'Neto' => 'xsd:double',
 		'Totaliva' => 'xsd:double',
		'totaldescuento' => 'xsd:double',
		'total' => 'xsd:double',
		'establecimiento' => 'xsd:string',
		'numeroretencion' => 'xsd:string'),
		array('return' => 'xsd:string'),
	$ns);

//creamos la funcion cabecerafac
function cabeceraRec($identificacion,$nombreproveedor,$Telefono,$email,$Provincia,$ciudad,$Direccion,$fechaemision,$horaemision,$numerofactura,$Formapago,$Neto, $Totaliva,$totaldescuento,$total,$establecimiento,$numeroretencion){

	//quito los espacios en blanco limpiando la cadena
	$identificacion = trim($identificacion);
	$nombreproveedor = trim($nombreproveedor);
	$Telefono = trim($Telefono);
	$email = trim($email);
	$Provincia = trim($Provincia);
	$ciudad = trim($ciudad);
	$Direccion = trim($Direccion);
	$fechaemision = trim($fechaemision);
	$horaemision = trim($horaemision);
	$numerofactura = trim($numerofactura);
	$Formapago = trim($Formapago);
	$Neto = trim($Neto);
	$Totaliva = trim($Totaliva);
	$totaldescuento = trim($totaldescuento);
	$total = trim($total);
	$establecimiento = trim($establecimiento);
	$numeroretencion = trim($numeroretencion);
	// Validadcion de los decimales
	$Neto = str_replace(',', '.', $Neto);
	$Totaliva = str_replace(',', '.', $Totaliva);
	$totaldescuento = str_replace(',', '.', $totaldescuento);
	$total = str_replace(',', '.', $total);

	
	// VARIABLES
	$llave = TRUE;

	//Busco las variables Globales para realizar la conexion
	$var = new variables_globales();
	//Realizo la conexion a la base de datos
	$conexion = mysqli_connect($var->FS_DB_HOST, $var->FS_DB_USER, $var->FS_DB_PASS, $var->FS_DB_NAME);
	//Instacion metodos para validacion de cedulas o RUC
	$valida = new Utilities();



	#VALIDACIÓN DE LA CÉDULA
	$tipof = "-1";
	if(strlen($identificacion) == 13){
		$tipof = "RUC";
	}elseif(strlen($identificacion) == 10){
		$tipof = "CEDULA";
	}
	if($tipof == '-1'){
		$resultado['valida_cedula'] = 'La Identificacion del Proveedor no es ni Cedula ni Ruc';
		$llave = FALSE;
	}elseif($tipof == 'RUC'){
		if(!$valida->validarRUC($identificacion)){
			$resultado['valida_cedula'] = 'Ruc No Valido, en la legalidad Ecuatoriana.';
			$llave = FALSE;
		}
	}elseif($tipof == 'CEDULA'){
		if(!$valida->validarCI($identificacion)){
			$resultado['valida_cedula'] = 'Cedula No Valida, en la legalidad Ecuatoriana.';
			$llave = FALSE;
		}
	}

	if ($llave){
		$resultado = array();


		#PROVEEDORES
		// $new_codigo_pro = 'ss';
		$proveedores = $conexion->query("SELECT * FROM proveedores WHERE cifnif = ".$identificacion);
		$arr_proveedores = mysqli_fetch_array($proveedores);
		
		//Verifico si exite el proveedor.
		if(empty($arr_proveedores)){
			//Nuevo Código de proveedor
			$nump = $conexion->query("SELECT max(codproveedor) as num FROM proveedores");
			$nump = mysqli_fetch_array($nump);
			if (empty($nump)) {
				$new_codigo_pro = str_pad(1,6,"0",STR_PAD_LEFT);
			}else{
				$new_codigo_pro = $nump['num'];
				$new_codigo_pro++;
				$new_codigo_pro = str_pad($new_codigo_pro,6,"0",STR_PAD_LEFT);
			}
			//Inserto el Proveedor en la tabla.
			$sql = "INSERT INTO proveedores (codproveedor,nombre,razonsocial,tipoidfiscal,cifnif,
               telefono1,email,coddivisa,regimeniva,acreedor,personafisica,fechaalta) VALUES ('" . $new_codigo_pro . "', '" . $nombreproveedor . "', '" . $nombreproveedor . "', '" . $tipof . "', '" . $identificacion . "', '" . $Telefono . "', '" . $email . "', '" . "USD', '" . "General', '" . "0', '" . "1', '" . date('Y-m-d') . "')";
			if($conexion->query($sql)===TRUE){
				$resultado['proveedor'] = "Se Ingreso el Proveedor, No existia en la Base";
			}else{
				$resultado['proveedor'] = "No Se Pudo Ingresar el Proveedor";
			}
		}
		else{
			$new_codigo_pro = $arr_proveedores['codproveedor'];
			$sql = "UPDATE proveedores SET nombre = '" . $nombreproveedor . "' , razonsocial = '" . $nombreproveedor . "' , tipoidfiscal = '" . $tipof . "' , cifnif = '" . $identificacion . "' , telefono1 = '" . $Telefono . "' , email = '" . $email . "' WHERE codproveedor = '" . $new_codigo_pro ."';";
			if($conexion->query($sql)===TRUE){
				$resultado['proveedor'] = "Se Actualizó los datos del Proveedor";
			}else{
				$resultado['proveedor'] = "No Se Pudo Actualizar el Proveedor";
			}
		}
		//Busco la direccion del proveedor
		$arr_dirproveedor = $conexion->query("SELECT * FROM dirproveedores WHERE codproveedor = '".$arr_proveedores['codproveedor']."'");
		$arr_dirproveedor = mysqli_fetch_array($arr_dirproveedor);
			//verifico si existen los datos
		if(empty($arr_dirproveedor)){
			//Busco la provincia
			$provs = $conexion->query("SELECT * FROM provincia WHERE padre IS NULL AND lower(nombre) = '".strtolower($Provincia)."'");
			$arr_prov = mysqli_fetch_array($provs);
				//Buscar la Ciudad
			if(!empty($arr_prov)){
				$id_prov = $arr_prov['codigo'];
				//Busco la ciudad
				$ciuds = $conexion->query("SELECT * FROM provincia WHERE padre = '".$id_prov."' AND lower(nombre) = '".strtolower($ciudad)."'");
				$arr_ciu = mysqli_fetch_array($ciuds);
				if(!empty($arr_ciu)){
					//guardo el id de la ciudad
					$id_ciud = $arr_ciu['codigo'];					
				}
			}
			//Insert a tabla dirclientes
			$sql = "INSERT INTO dirproveedores (codproveedor,codpais,apartado,provincia,ciudad,direccion,direccionppal,descripcion,fecha) VALUES ('". $new_codigo_pro ."', 'ECU', '', '".$id_prov."', '".$id_ciud."', '".$Direccion."', '1"."', 'Principal', '".date('Y-m-d')."');";
			if($conexion->query($sql)===TRUE){
				$resultado['direccion'] = "Se Ingreso La Direccion, No existia en la Base";
			}else{
				$resultado['direccion'] = "No Se Pudo Ingresar La Direccion";
			}
		}else{
			//Busco la provincia
			$provs = $conexion->query("SELECT * FROM provincia WHERE padre IS NULL AND lower(nombre) = '".strtolower($Provincia)."'");
			$arr_prov = mysqli_fetch_array($provs);

			//guardo el id de la provincia
			if(!empty($arr_prov)){
				$id_prov = $arr_prov['codigo'];
				//Busco la ciudad
				$ciuds = $conexion->query("SELECT * FROM provincia WHERE padre = '".$id_prov."' AND lower(nombre) = '".strtolower($ciudad)."'");
				$arr_ciu = mysqli_fetch_array($ciuds);
				if(!empty($arr_ciu)){
					//guardo el id de la ciudad
					$id_ciud = $arr_ciu['codigo'];
				}
			}

			//Insert a tabla dirclientes
			$sql = "UPDATE dirproveedores SET codproveedor = '".$new_codigo_pro."', provincia = '".$id_prov."', ciudad = '".$id_ciud."', direccion = '".$Direccion."'  WHERE id = '".$arr_dirproveedor['id']."';";
			if($conexion->query($sql)===TRUE){
				$resultado['direccion'] = "Se Actualizó La Direccion";
			}else{
				$resultado['direccion'] = "No Se Pudo Actualizar La Direccion";
			}
		}


		#ALMACENES
		//Busco el codigo del Almacen
		$almacenes = $conexion->query("SELECT * FROM almacenes WHERE codestablecimiento = '".$establecimiento."'");

		$arr_almacen = mysqli_fetch_array($almacenes);
		if(!empty($arr_almacen)){
			//guardo el id de la ciudad
			$almacen_cod = $arr_almacen['codalmacen'];
			$resultado['almacen'] = "Almacen encontrado correctamente";
		}else{
			$resultado['almacen'] = "No Se encontro el Almacen asignado al establecimiento ".$establecimiento;
		}



		#FACTURA DE COMPRA
		//Busco el año de la factura para encontrar el codigo del ejercicio
		$fec = explode("-", $fechaemision);
		//Busco el numero de la factura solo
		$numfac = explode('-', $numerofactura);
		$numfac = $numfac[sizeof($numfac)-1];
		$numfac = (int)$numfac;
		//selecciono el año de la factura
		$ejer = $fec[0];
		//formo el codigo de la Factura
		$cod = "FAC".$fec[0]."A".$numfac;
		$serieret = $establecimiento.'-'.$arr_almacen['puntoemision'].'-'.str_pad($numeroretencion,9,"0",STR_PAD_LEFT);
		// Preparo Insert para cabecera de la Factura de Compra con retención
		$sql = "INSERT INTO facturasprov (codigo,numero, total,neto,cifnif,pagada,codagente,codalmacen,nombre,codpago,codproveedor,numproveedor,codserie,totaliva,coddivisa,codejercicio,tasaconv,fecha,hora,fecha_transaccion,num_retencion, fecha_vencimiento,fecha_autorizacion_sri, fec_transaccion,idasientop) VALUES ('".$cod."', '".$numfac."', '".$total."', '".$Neto."', '".$identificacion."', '0', '1', '".$almacen_cod."', '".$nombreproveedor."', '".$Formapago."', '".$new_codigo_pro."', '".$numerofactura."', 'A', '".$Totaliva."', 'USD', '".$ejer."', '1', '".$fechaemision."', '".$horaemision."', '".$fechaemision."', '".$serieret."', '".$fechaemision."', '".$fechaemision."', '".$fechaemision."',null);";
		if($conexion->query($sql)===TRUE){
			$resultado['factura'] = "Se Ingreso La Factura de compra Correctamente.";

			//Recupero el id de la Factura Ingresada.
			$sql = "SELECT max(idfactura) as id FROM facturasprov";
			//Realizo la consulta del valor del id
			$fact = $conexion->query($sql);
			$arr_num = mysqli_fetch_array($fact);
			if(!empty($arr_num)){
				$resultado['idretencion'] = $arr_num['id'];
			}else{
				$resultado['idretencion'] = "No se Encontro el id de la retencion";
			}


		}else{
			$resultado['retencion'] = "No Se Pudo Ingresar La Factura de Compra";
			$resultado['sql'] = $sql;
		}
	}		
	return json_encode($resultado);
}
$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
$servicio->service(file_get_contents("php://input"));
?>