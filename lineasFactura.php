<?php

require_once 'lib/nusoap.php';
require_once 'variables_globales.php';

	date_default_timezone_set('America/Los_Angeles');

	//Instancio o objeto del servicio
	$servicio = new soap_server();
	//Instacion conexion a la base de datos
	
	//Name Espace
	$ns = "urn:lineasFacturawsdl";

	//configuramos el servicio 	
	//1er parametro Nombre del Webservice
	//2do parametro Name space
	$servicio->configureWSDL("lineasfactura",$ns);
	//almacena el espacio de nobre de destino
	$servicio->schemaTargetNamespace = $ns;

	//Metodo (register) para registrar el servicio 4 parametros
	//1er parametro Nombre de la Funcion que se va a Desarrollar
	//2do parametros de Entrada para la Funcion
	//3er parametro return de errores o de respuestas
	//4to Name space

	//funcion para guardar cabecera de facturascli
	$servicio->register("lineasFac", 
		array(
			'idfactura' => 'xsd:integer',
			'referencia' => 'xsd:string',
			'descripcion' => 'xsd:string',
			'cantidad' => 'xsd:double',
			'porcentaje_iva' => 'xsd:integer',
			'porcentaje_descuento' => 'xsd:integer',
			'pvpunitario' => 'xsd:double',
			'pvpsindescuento' => 'xsd:double',
			'pvptotal' => 'xsd:double'),
			array('return' => 'xsd:string'),
		$ns);

	//creamos la funcion cabecerafac
	function lineasFac($idfactura, $referencia, $descripcion, $cantidad, $porcentaje_iva, $porcentaje_descuento, $pvpunitario, $pvpsindescuento, $pvptotal)
	{
		//quito los espacios en blanco
		$referencia = trim($referencia);
		$descripcion = trim($descripcion);

		$descripcion = str_replace(" :", ":", $descripcion);
        /*//saco el Lote
        $ini = strrpos($descripcion, "Lote:") + 5;
        $fin = strrpos($descripcion, "NOMBRE COMERCIAL:") - $ini;
        $lote1 = trim(substr($descripcion,$ini,$fin));*/
        //saco el Nombre Comercial
        $ini = strrpos($descripcion, "NOMBRE COMERCIAL:") + 17;
        $fin = strrpos($descripcion, "NOMBRE GENERICO:") - $ini;
        $nom_com = trim(substr($descripcion,$ini,$fin));
        //saco el Nombre Generico
        $ini = strrpos($descripcion, "NOMBRE GENERICO:") + 16;
        $fin = strrpos($descripcion, "PRESENTACION:") - $ini;
        $nom_gen = trim(substr($descripcion,$ini,$fin));
        //saco Presentacion
        $ini = strrpos($descripcion, "PRESENTACION:") + 13;
        $fin = strrpos($descripcion, "LOTE:") - $ini;
        $pres = trim(substr($descripcion,$ini,$fin));
        //saco LOTE
        $ini = strrpos($descripcion, "LOTE:") + 5;
        $fin = strrpos($descripcion, "FECHA DE FABRICACION:") - $ini;
        $lote2 = trim(substr($descripcion,$ini,$fin));
        //saco Fecha Fabricación
        $ini = strrpos($descripcion, "FECHA DE FABRICACION:") + 21;
        $fin = strrpos($descripcion, "FECHA DE VENCIMIENTO:") - $ini;
        $fech_fab = trim(substr($descripcion,$ini,$fin));
        //saco Fecha de Vencimiento
        $ini = strrpos($descripcion, "FECHA DE VENCIMIENTO:") + 21;
        $fin = strrpos($descripcion, "REGISTRO SANITARIO:") - $ini;
        $fech_ven = trim(substr($descripcion,$ini,$fin));
        //saco Registro Sanitario
	    $ini = strrpos($descripcion, "REGISTRO SANITARIO:") + 19;
        $fin = strrpos($descripcion, "PROCEDENCIA:") - $ini;
        $reg_san = trim(substr($descripcion,$ini,$fin));
        //saco Procedencia
        $ini = strrpos($descripcion, "PROCEDENCIA:") + 12;
        $fin = strrpos($descripcion, "Forma de Pago:") - $ini;
        $proc = trim(substr($descripcion,$ini,$fin));
        //Saco la descripcion de la forma de pago
        $ini = strrpos($descripcion, "Forma de Pago:") + 14;
        $fin = strrpos($descripcion, "OC:") - $ini;
        $fp = trim(substr($descripcion,$ini,$fin));
        //Saco la Orden de Compra
        $ini = strrpos($descripcion, "OC:") + 3;
        $fin = strlen($descripcion)- $ini;
        $oc = trim(substr($descripcion,$ini,$fin));
        //saco la Descripcion
        $ini = 0;
        $fin = strrpos($descripcion, "NOMBRE COMERCIAL:");
        $descripcion = trim(substr($descripcion,$ini,$fin));
        $lote1 = "a";

        $observaciones = $lote1."/:".$nom_com."/:".$nom_gen."/:".$pres."/:".$lote2."/:".$fech_fab."/:".$fech_ven."/:".$reg_san."/:".$proc."/:".$fp."/:".$oc;

        //print_r($observaciones);

		//array de Respuestas o Retorno
		$resultado = array();
		
		//Busco las variables Globales para realizar la conexion
		$var = new variables_globales();

		//Realizo la conexion a la base de datos
		$conexion = mysqli_connect($var->FS_DB_HOST, $var->FS_DB_USER, $var->FS_DB_PASS, $var->FS_DB_NAME);

		//Busco la existencia de la Factura
		$facturas = $conexion->query("SELECT * FROM facturascli WHERE idfactura = ".$idfactura);
		
		$arr_fac = array();

		//transformo los datos de la consulta a un array
		while ($factura = mysqli_fetch_array($facturas))
		{
			$arr_fac[] = $factura;
		}
		
		//Verifico si exite la factura.
		if(!empty($arr_fac))
		{
			$resultado['factura'] = "Factura encontrada correctamente";

			//busco el codigo del iva
			$codimpuesto = '-1';
			if($porcentaje_iva == 12)
			{
				$codimpuesto = 'IVA12';
			}else if ($porcentaje_iva == 0)
				{
					$codimpuesto = 'IVA0';				
				}

			if($codimpuesto != '-1')
			{
				$resultado['impuesto'] = "Porcentaje de Iva Correcto: ".$porcentaje_iva;

				//Reviso si el articulo se encuentra Creado
				$articulos = $conexion->query("SELECT * FROM articulos WHERE referencia = '".$referencia."'");

				$arr_art = array();

				//transformo los datos de la consulta a un array
				while ($articulo = mysqli_fetch_array($articulos))
				{
					$arr_art[] = $articulo;
				}

				if(empty($arr_art))
				{
					//si No se encuentra el articulo lo creo.
					$sql = "INSERT INTO articulos (factualizado, codimpuesto, nostock, activo_fijo, descripcion, secompra, referencia, pvp, sevende) VALUES('".date('Y-m-d')."', '".$codimpuesto."', '0', '0', '".$descripcion."', '1', '".$referencia."', '".$pvpunitario."', '1')";

					if($conexion->query($sql)===TRUE)
					{
						$resultado['articulo'] = "Se Ingreso el Articulo correctamente";						
					}else{
						$resultado['articulo'] = "No Se Pudo Ingresar el Articulo";
					}
				}

				//Elimino las lineas de Iva para volver a generarlas
				$sql = "DELETE FROM lineasivafactcli WHERE idfactura = ".$idfactura;

				if(!$conexion->query($sql) === TRUE)
				{
					$resultado['lineaiva'] = "No se pudo Recalcular las lineas de IVA";
				}

				$sql = "INSERT INTO lineasfacturascli (cantidad, codimpuesto, descripcion, dtopor, idfactura, iva, pvpsindto, pvptotal, pvpunitario, referencia, c_tipo, id_centro_costos, detalles) VALUES ('".$cantidad."', '".$codimpuesto."', '".$descripcion."', '".$porcentaje_descuento."', '".$idfactura."', '".$porcentaje_iva."', '".str_replace(",",".",$pvpsindescuento)."', '".str_replace(",",".",$pvptotal)."', '".str_replace(",",".",$pvpunitario)."', '".$referencia."', '1','1','".$observaciones."');";
				//print_r($sql);
				if($conexion->query($sql)===TRUE)
				{
					$resultado['linea'] = "Se Ingreso La Linea correctamente";

				}else{
					$resultado['linea'] = "No Se Pudo Ingresar La Linea";
				}

			}else{
				$resultado['impuesto'] = "Porcentaje de Iva no Valido: ".$porcentaje_iva;
			}

		}else{			
			$resultado['factura'] = "No se encuentra la Factura ".$idfactura;
		}
		
		return json_encode($resultado);
	}


	$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
	$servicio->service(file_get_contents("php://input"));
	
?>