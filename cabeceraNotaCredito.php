<?php

require_once 'lib/nusoap.php';
require_once 'variables_globales.php';
require_once '../plugins/facturacion_base/extras/Utilities/Utilities.php';

	date_default_timezone_set('America/Bogota');

	//Instancio o objeto del servicio
	$servicio = new soap_server();
		
	//Name Espace
	$ns = "urn:cabeceraNotaCredwsdl";

	//configuramos el servicio 	
	//1er parametro Nombre del Webservice
	//2do parametro Name space
	$servicio->configureWSDL("cabeceranotacredito",$ns);
	//almacena el espacio de nobre de destino
	$servicio->schemaTargetNamespace = $ns;

	//Metodo (register) para registrar el servicio 4 parametros
	//1er parametro Nombre de la Funcion que se va a Desarrollar
	//2do parametros de Entrada para la Funcion
	//3er parametro return de errores o de respuestas
	//4to Name space


	//funcion para guardar cabecera de facturascli
	$servicio->register("cabeceranotaCred",
		array(
			'idfactura' => 'xsd:integer',
			'fechaemision' => 'xsd:string',
			'horaemision' => 'xsd:string',
			'numeronotacredito' => 'xsd:string',
			'neto' =>'xsd:double',
			'totaliva' => 'xsd:double',
			'totaldescuento' => 'xsd:double',
			'total' => 'xsd:double',
			'establecimiento' => 'xsd:string'),
			array('return' => 'xsd:string'),
		$ns);

	//creamos la funcion cabeceranotaCred
	function cabeceranotaCred($idfactura,$fechaemision,$horaemision,$numeronotacredito,$neto,$totaliva,$totaldescuento,$total,$establecimiento){
		//quito los espacios en blanco limpiando la cadena
		$idfactura = trim($idfactura);
		$fechaemision = trim($fechaemision);
		$horaemision = trim($horaemision);
		$numeronotacredito = trim($numeronotacredito);
		$neto = trim($neto);
		$totaliva = trim($totaliva);
		$totaldescuento = trim($totaldescuento);
		$total = trim($total);
		$establecimiento = trim($establecimiento);

		$llave = TRUE;
		$resultado = array();
		$codalmacen = '';
		$puntoemision = '';

		//Busco las variables Globales para realizar la conexion
		$var = new variables_globales();

		//Realizo la conexion a la base de datos
		$conexion = mysqli_connect($var->FS_DB_HOST, $var->FS_DB_USER, $var->FS_DB_PASS, $var->FS_DB_NAME);
		//Busco la existencia del Cliente

		//Instacion metodos para validacion de cedulas o RUC
		$valida = new Utilities();

		$consulta = $conexion->query("SELECT * FROM facturascli WHERE idfactura = '".$idfactura."';");
		$factura = mysqli_fetch_array($consulta);

		if (empty($factura)) {
			$resultado['idfactura'] = "No se encontro la factura de venta para Realizar Nota de Credito";
			$llave = FALSE;
		}


		$llave = TRUE;
		$consulta = $conexion->query("SELECT idfactura FROM facturascli WHERE numero = '".$numeronotacredito."' AND idfacturarect != NULL;");
		$fact_exist = mysqli_fetch_array($consulta);
		if (!empty($fact_exist)) {
			$consulta = $conexion->query("SELECT * FROM co_factura WHERE doc_instancias_id = '".$fact_exist['idfactura']."' AND estado = 'AUTORIZADO' AND tipo_doc = '4'");
			$co_fact_exist = mysqli_fetch_array($consulta);
			if (!empty($co_fact_exist)) {
				$resultado['notacredito'] = "La Nota de crédito ya existe y está autorizada";
				$resultado['idnotacredito'] = "No se obtuvo el id de la nota de crédito";
			}else{
				$resultado['notacredito'] = "La Nota de crédito ya existe, pero los datos serán modificados";
				$resultado['idnotacredito'] = $fact_exist['idfactura'];
			}
			$llave = FALSE;

		}





		if ($llave) {
			$resultado['idfactura'] = "Factura de venta encontrada correctamente";
			$consulta = $conexion->query("SELECT * FROM almacenes WHERE codestablecimiento  = '".$establecimiento."'");
			$almacen = mysqli_fetch_array($consulta);
			if (!empty($almacen)) {
				$codalmacen = $almacen['codalmacen'];
				$numnotacredito = $establecimiento.'-'.$almacen['puntoemision'].'-'.str_pad($numeronotacredito,9,"0",STR_PAD_LEFT);
			}else{
				$resultado['almacen'] = "No se encontro el almacen asignado al establecimiento " .$establecimiento;
			}

			$consulta = $conexion->query("SELECT * FROM clientes WHERE codcliente = '".$factura['codcliente']."'");
			$cliente = mysqli_fetch_array($consulta);
			if (empty($cliente)) {
				 $resultado['cliente'] = "Error al econtrar al Cliente";
			}else{
				$cifnif = $cliente['cifnif'];
				$nombre = $cliente['nombre'];
			}
			$consulta = $conexion->query("SELECT * FROM dirclientes WHERE codcliente = '".$factura['codcliente']."';");
			$dircliente = mysqli_fetch_array($consulta);
			if (empty($dircliente)) {
				$resultado['dircliente'] = "No se encuentra la direccion del cliente";
			}else{
				$direccion = $dircliente['direccion'];
				$ciudad = $dircliente['ciudad'];
			}
			if (isset($codalmacen) && isset($cifnif) && isset($direccion)) {
				$total = str_replace(',', '.', $total);
				$neto = str_replace(',', '.', $neto);
				$totaldescuento = str_replace(',', '.', $totaldescuento);
				$totaliva = str_replace(',', '.', $totaliva);

				$codigo = 'NCC'.date('Y').'N'.$numeronotacredito.'V';
				$sql = "INSERT INTO facturascli (cifnif,nombrecliente,codagente,codalmacen,codcliente,coddivisa,codejercicio,codigo,codigorect, idfacturarect ,neto,total,totaldescuento,totaliva,totalpago,numero,codpais,direccion,codserie,fecha,fecha_ini_trans,fecha_fin_trans,hora,observaciones) VALUES ('".$cifnif."','".$nombre."','1','".$codalmacen."','".$cliente['codcliente']."','USD','".date('Y') ."','".$codigo."','".$factura['codigo']."','".$factura['idfactura']."','".$neto."','".$total."','".$totaldescuento."','".$totaliva."','".$total."','".$numeronotacredito."','ECU','".$direccion."','N','".$fechaemision."','".$fechaemision."','".$fechaemision."','".$horaemision."','Nota de credito');";
				if($conexion->query($sql)){
					$resultado['notacredito'] = "Se ingreso la Nota de Credito Correctamente";
					$consulta = $conexion->query("SELECT max(idfactura) as id FROM facturascli");
					$arr_num = mysqli_fetch_array($consulta);
					if (!empty($arr_num)) {
						$resultado['idnotacredito'] = $arr_num['id'];
					}else{
						$resultado['idnotacredito'] = "No se encontro la nota de credito";
					}
				}else{
					$resultado['notacredito'] = "No Se Pudo Ingresar La Nota de Credito";
				}
			}else{
				$resultado['notacredito'] = "No Se Pudo Ingresar La Nota de Credito";
			}


		}
		return json_encode($resultado);
	}
	$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
	$servicio->service(file_get_contents("php://input"));
?>